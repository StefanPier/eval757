/*
 * comm_stille.c
 *
 * Created: 10/08/2020 15:00:19
 *  Author: Stefano
 *  Company: Qprel srl
 */ 

#include <Bed/Stille.h>
#include "main.h"
#include "manage_debug.h"
#include "protocol.h"
#include "timer.h"
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
/*
#include "regions.h"
#include "memories.h"
#include "rswdt.h"
*/
#include "comm_stille.h"
#include "joinedWdt.h"


static uint8_t Stille_Uart_Data[8];
static cBuffer buffercmd_Stille;
static unsigned char buf_Stille_data[STILLE_BUFFER_LEN];


void comm_StilleInit(uint32_t brate){
	
	//Init RX Buffer for redundant protocol usage, and Protocol structure
	bufferInit( &buffercmd_Stille, buf_Stille_data, sizeof(buf_Stille_data) );
	memset(buf_Stille_data, 0, sizeof(buf_Stille_data));
	//Serial port register
	/* Enable the UART RX FIFO threshold interrupt */
	HAL_UART_Receive_IT(&STILLE_BED_UART, Stille_Uart_Data, 1);
}


#ifdef USE_STILLE
void Stille_Handler(void)
{
	//Populate Steute circular Buffer
	if(!bufferAddToEnd(&buffercmd_Stille, Stille_Uart_Data[0]))
		Stille_Uart_Data[2]++;
	HAL_UART_Receive_IT(&STILLE_BED_UART, Stille_Uart_Data, 1);
}
#endif

void comm_stille_write_byte (unsigned char *dato, int size)
{
	//uart_write(LIFTER_UART, dato);
	for (int i=0;i<size;i++){
		HAL_UART_Transmit(&STILLE_BED_UART, &dato[i], 1, HAL_MAX_DELAY);
	}
}


cBuffer* comm_stille_get_buffercmd(){
	return &buffercmd_Stille;
}
