/*
 * Lifter.c
 *
 * Created: 10/08/2020 14:52:09
 *  Author: Stefano
 *  Company: Qprel srl
 */ 

#include <Stille.h>
#include "main.h"
#include <string.h>
#include "timer.h"
#include "Comm/comm_stille.h"
#include "manage_debug.h"
#include "joinedWdt.h"
#include "stxetxconf.h"
#include <limits.h>

static const uint16_t CRC_TABLE[256] = {
	0x0000,0x1021,0x2042,0x3063, 0x4084, 0x50A5, 0x60C6, 0x70E7,
	0x8108,0x9129,0xA14A,0xB16B, 0xC18C, 0xD1AD, 0xE1CE, 0xF1EF,
	0x1231,0x0210,0x3273,0x2252, 0x52B5, 0x4294, 0x72F7, 0x62D6,
	0x9339,0x8318,0xB37B,0xA35A, 0xD3BD, 0xC39C, 0xF3FF, 0xE3DE,
	0x2462,0x3443,0x0420,0x1401, 0x64E6, 0x74C7, 0x44A4, 0x5485,
	0xA56A,0xB54B,0x8528,0x9509, 0xE5EE, 0xF5CF, 0xC5AC, 0xD58D,
	0x3653,0x2672,0x1611,0x0630, 0x76D7, 0x66F6, 0x5695, 0x46B4,
	0xB75B,0xA77A,0x9719,0x8738, 0xF7DF, 0xE7FE, 0xD79D, 0xC7BC,
	0x48C4,0x58E5,0x6886,0x78A7, 0x0840, 0x1861, 0x2802, 0x3823,
	0xC9CC,0xD9ED,0xE98E,0xF9AF, 0x8948, 0x9969, 0xA90A, 0xB92B,
	0x5AF5,0x4AD4,0x7AB7,0x6A96, 0x1A71, 0x0A50, 0x3A33, 0x2A12,
	0xDBFD,0xCBDC,0xFBBF,0xEB9E, 0x9B79, 0x8B58, 0xBB3B, 0xAB1A,
	0x6CA6,0x7C87,0x4CE4,0x5CC5, 0x2C22, 0x3C03, 0x0C60, 0x1C41,
	0xEDAE,0xFD8F,0xCDEC,0xDDCD, 0xAD2A, 0xBD0B, 0x8D68, 0x9D49,
	0x7E97,0x6EB6,0x5ED5,0x4EF4, 0x3E13, 0x2E32, 0x1E51, 0x0E70,
	0xFF9F,0xEFBE,0xDFDD,0xCFFC, 0xBF1B, 0xAF3A, 0x9F59, 0x8F78,
	0x9188,0x81A9,0xB1CA,0xA1EB, 0xD10C, 0xC12D, 0xF14E, 0xE16F,
	0x1080,0x00A1,0x30C2,0x20E3, 0x5004, 0x4025, 0x7046, 0x6067,
	0x83B9,0x9398,0xA3FB,0xB3DA, 0xC33D, 0xD31C, 0xE37F, 0xF35E,
	0x02B1,0x1290,0x22F3,0x32D2, 0x4235, 0x5214, 0x6277, 0x7256,
	0xB5EA,0xA5CB,0x95A8,0x8589, 0xF56E, 0xE54F, 0xD52C, 0xC50D,
	0x34E2,0x24C3,0x14A0,0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
	0xA7DB,0xB7FA,0x8799,0x97B8, 0xE75F, 0xF77E, 0xC71D, 0xD73C,
	0x26D3,0x36F2,0x0691,0x16B0, 0x6657, 0x7676, 0x4615, 0x5634,
	0xD94C,0xC96D,0xF90E,0xE92F, 0x99C8, 0x89E9, 0xB98A, 0xA9AB,
	0x5844,0x4865,0x7806,0x6827, 0x18C0, 0x08E1, 0x3882, 0x28A3,
	0xCB7D,0xDB5C,0xEB3F,0xFB1E, 0x8BF9, 0x9BD8, 0xABBB, 0xBB9A,
	0x4A75,0x5A54,0x6A37,0x7A16, 0x0AF1, 0x1AD0, 0x2AB3, 0x3A92,
	0xFD2E,0xED0F,0xDD6C,0xCD4D, 0xBDAA, 0xAD8B, 0x9DE8, 0x8DC9,
	0x7C26,0x6C07,0x5C64,0x4C45, 0x3CA2, 0x2C83, 0x1CE0, 0x0CC1,
	0xEF1F,0xFF3E,0xCF5D,0xDF7C, 0xAF9B, 0xBFBA, 0x8FD9, 0x9FF8,
	0x6E17,0x7E36,0x4E55,0x5E74, 0x2E93, 0x3EB2, 0x0ED1, 0x1EF0,
};

stille_cmd available_stille_cmd[RA+1]={	{RG,'R','G'},		//Remote data get
										{RT,'R','T'},		//Remote data Transfer
										{RC,'R','C'},		//Remote Cycling
										{RE,'R','E'},		//Remote execute function
										{RS,'R','S'},		//Remote stop function
										{RO,'R','O'},		//Remote mode open
										{RA,'R','A'},		//Remote mode abort
};

static uint32_t timeout_lifter=0;
/// Array of motor object structures
volatile static motor_obj_t * stille_object;
static uint32_t timeLifter;
static uint32_t timeLifterGetPos;
static uint32_t timeLifterGetMotorMotionStatRG;
static uint32_t timeLifterGetMotorMotionStatRO;
static uint32_t timeLifterGetMotorMotionStatRC;
static uint32_t timeLifterGetMotorMotionStatTot;

uint16_t CalculateChecksum(const unsigned char* pAdr, int len)
{
	if (len < 0)
	{
		return 0;
	}
	uint16_t crc = 0;
	while (len--)
	{
		crc =(uint16_t)(CRC_TABLE[((crc >> 8) ^ *pAdr++) & 0xFF] ^ (crc << 8));
	}
	return crc;
}

/********************************************************************************************************/
/*	BINDING FOR MOTOR CONTROL																			*/
/********************************************************************************************************/
/**
 * \fn void stille_InitData(motor_obj_t * m_obj)
 * \brief Initialize the motor objects array and
 *        add the motor_Machine() function to the co_main calling list
 *
 */

	//Generic initialization
	void stille_InitData(motor_obj_t * m_obj)
	{
		stille_object = m_obj;
	}
	
	uint32_t stille_deInit(void){
		uint8_t msg=0, rx_reply;
		if(!stille_Command(RA,  (uint8_t *)&msg, 0, &rx_reply, 1)){
			DbgPrintf(DBGPRINT_STILLE, "Stille_Write_Par - RA Error\r\n");
			return RES_FAIL;
		}	
		for (int mot =0;mot<MOTOR_MAXOBJ;mot++){
			if(stille_object[mot].motor_model==MOT_STILLE){
				stille_object[mot].initialized=0;
				stille_object[mot].motion_stat = STILLE_STOP;
				stille_object[mot].motor_stat = MOT_NOT_INIT;
				stille_object[mot].pulse_counter = 0;;
				stille_object[mot].pulse_counter = 0;
				stille_object[mot].actual_position = 0;//////INT_MIN;
				stille_object[mot].home_position = 0;
				stille_object[mot].max_position_limit=INT_MAX;
				stille_object[mot].min_position_limit=INT_MIN;
				stille_object[mot].pos_speed=0;
			}
		}
		return RES_SUCCESS;
	}

	//MOTOR_INIT_CMD axis=MOT_STILLE node >= 200
	uint32_t stille_Init(int axis, uint8_t node)
	{
		timeLifter=timer_TimerGetSysTick();

		stille_object[axis].motion_stat = STILLE_STOP;
		stille_object[axis].motor_stat = MOT_NOT_INIT;
		stille_object[axis].pulse_counter = 0;;
		stille_object[axis].pulse_counter = 0;
		stille_object[axis].actual_position = 0;//////INT_MIN;
		stille_object[axis].home_position = 0;
		stille_object[axis].max_position_limit=INT_MAX;
		stille_object[axis].min_position_limit=INT_MIN;
		stille_object[axis].pos_speed=0;
		
		switch(node){
			case STILLE_NODE_MOTOR1:
				stille_object[axis].node_id = STILLE_ACTUATOR0;			//This is the axis motor used
				break;
			case STILLE_NODE_MOTOR2:
				stille_object[axis].node_id = STILLE_ACTUATOR1;			//This is the axis motor used
				break;
			case STILLE_NODE_MOTOR3:
				stille_object[axis].node_id = STILLE_ACTUATOR2;			//This is the axis motor used
				break;
			case STILLE_NODE_MOTOR4:
				stille_object[axis].node_id = STILLE_ACTUATOR3;			//This is the axis motor used
				break;
			case STILLE_NODE_MOTOR5:
				stille_object[axis].node_id = STILLE_ACTUATOR4;			//This is the axis motor used
				break;
			case STILLE_NODE_MOTOR6:
				stille_object[axis].node_id = STILLE_ACTUATOR5;			//This is the axis motor used
				break;
			default:
				stille_object[axis].node_id = STILLE_ACTUATOR0;			//This is the axis motor used
				break;			
		}

		if(check_connection(axis)){
			stille_object[axis].initialized = 1;
			stille_object[axis].motor_model = MOT_STILLE;
		}
		else{
			stille_object[axis].initialized = 0;
			return RES_FAIL;			
		}

		return RES_SUCCESS;
	}
	
	
	/**
	* \fn uint32_t stille_SetMaxPosition(int axis, int32_t max_pos)
	* \brief Set the max position limit for the axis
	*
	* This function perform the commands to set max position limit in step 
	*
	* \param int axis Axis number (index in the motor_object array)
	* \param int32_t max_pos max position.
	* \return  1 = success, 0 = error.
	*
	*/
	uint32_t stille_SetMaxPosition(int axis, int32_t max_pos){
		stille_object[axis].max_position_limit=max_pos;
		return RES_SUCCESS;
	}

	/**
	* \fn uint32_t stille_SetMinPosition(int axis, int32_t max_pos)
	* \brief Set the min position limit for the axis
	*
	* This function perform the commands to set max position limit in step 
	*
	* \param int axis Axis number (index in the motor_object array)
	* \param int32_t min_pos min position.
	* \return  1 = success, 0 = error.
	*
	*/
	uint32_t stille_SetMinPosition(int axis, int32_t min_pos){
		stille_object[axis].min_position_limit=min_pos;
		return RES_SUCCESS;
	}
	
	/**
	* \fn uint32_t stille_SetHome(int axis)
	* \brief Set home position for the motor as the current position
	*
	* \param int axis Axis number (index in the motor_object array)
	* \return  1 = home set success, 0 = home set error.
	*
	*/
	uint32_t stille_SetHome(int axis){
		int32_t pos=0;
		if(stille_GetMotorActPos(axis,&pos)==RES_FAIL)
			return RES_FAIL;
		
		stille_object[axis].home_position=pos;
		
		return RES_SUCCESS;
	}

	
	//MOTOR_HOME_CMD axis=MOT_STILLE
	uint32_t stille_Home(int axis)
	{
		if(stille_object[axis].initialized!=1)
			return RES_FAIL;
		
		if(!stille_move_to_position(stille_object[axis].home_position,stille_object[axis].node_id))
			return RES_FAIL;
					
		return RES_SUCCESS;
	}
	
	//SET_POSITION_CMD axis=MOT_STILLE, new_position (uint32_t) in mm
	uint32_t stille_SetPosition(int axis, uint32_t new_position_step){
		timeLifter=timer_TimerGetSysTick();

		//Check if initialized
		if(stille_object[axis].initialized!=1)
			return RES_FAIL;
			
		//Check if position is out of limits
		if(stille_object[axis].min_position_limit>(int)new_position_step || stille_object[axis].max_position_limit<(int)new_position_step ){
			return RES_FAIL;
		}
					
//		uint32_t new_position = STILLE_MM_TO_STEP(new_position_mm);
		uint32_t new_position = new_position_step;
		
		if(stille_object[axis].max_position_limit!=INT_MAX){
			if(stille_object[axis].node_id==STILLE_ACTUATOR0){
				new_position=stille_object[axis].max_position_limit-new_position;
				if(new_position<0)
					new_position=0;
			}
		}
		if(!stille_move_to_position(new_position, stille_object[axis].node_id))
			return RES_FAIL;

		DbgPrintf(DBGPRINT_STILLE, "Stille_SetPos %ldms\r\n",timer_TimerGetSysTick()-timeLifter);
		return RES_SUCCESS;
	}
	
	//SET_SPEED_CMD axis=MOT_STILLE, speed in step/s
	uint32_t stille_SetSpeed(int axis, uint32_t speed){
		timeLifter=timer_TimerGetSysTick();

		if(stille_object[axis].initialized!=1)
			return RES_FAIL;
	
		//Doesn't perform if just set 
		if((uint32_t)(stille_object[axis].pos_speed)==speed){
			DbgPrintf(DBGPRINT_STILLE, "Stille_SetSpeed %ldms\r\n",timer_TimerGetSysTick()-timeLifter);
			return RES_SUCCESS;
		}
	
		if(!stille_set_speed(speed, stille_object[axis].node_id)){
			return RES_FAIL;
		}
		
		stille_object[axis].pos_speed=speed;
		
		DbgPrintf(DBGPRINT_STILLE, "Stille_SetSpeed %ldms\r\n",timer_TimerGetSysTick()-timeLifter);
		return RES_SUCCESS;
	}

	//GET_MOTION_STATUS_CMD axis=MOT_STILLE, return 0 se fermo, 1 se in moto
	uint32_t stille_GetMotionStatus(int axis){
		return stille_object[axis].motion_stat;
	}
	
	//GET_MOTION_STATUS_CMD axis=MOT_STILLE, return 0 se fermo, 1 se in moto
	uint32_t stille_GetMotionStatus_real(int axis){
		if(stille_object[axis].initialized!=1){
			return RES_FAIL;
		}
					
		//Get Actual Motion Status
		uint8_t status=0;
		if(!stille_Read_Parameter_OnlyValue(STILLE_MOTION_STATUS_BASEADDR+stille_object[axis].node_id, (void *)&status, STILLE_RG_REPLY_HEADER+1)){
			return RES_FAIL;
		}
		stille_object[axis].motion_stat = (status & 0x010) ? STILLE_MOVING : STILLE_STOP;
		//Read number
		return RES_SUCCESS;
	}

	//GET_MOTOR_STATUS_CMD axis=MOT_STILLE, return lo stato di errore riletto secondo lo standard
	uint32_t stille_GetMotorStatus(int axis){
		DbgPrintf(DBGPRINT_STILLE, "last stille_GetMotorAndMotionStatus tot: %ldms, RO: %ldms, RC: %ldms, RG: %ldms,\r\n",
		timeLifterGetMotorMotionStatTot,
		timeLifterGetMotorMotionStatRO,
		timeLifterGetMotorMotionStatRC,
		timeLifterGetMotorMotionStatRG);
		return stille_object[axis].motor_stat;
	}

	//GET_MOTOR_STATUS_CMD axis=MOT_STILLE, return lo stato di errore riletto secondo lo standard
	uint32_t stille_GetMotorStatus_real(int axis){
		if(stille_object[axis].initialized!=1){
			return RES_FAIL;
		}
		
		//Get Actual Speed
		uint32_t motor_status=1;
		//Error_Code 1 (last recent)
		if(!stille_Read_Parameter_OnlyValue(STILLE_MOTOR_STATUS_BASEADDR+stille_object[axis].node_id, (void *)&motor_status, STILLE_RG_REPLY_HEADER+4))
		{
			return RES_FAIL;
		}

		if(motor_status==0){
			stille_object[axis].motor_stat=MOT_READY;
			}else if(motor_status & 0x0000001F){
			stille_object[axis].motor_stat=MOT_FAULT_ERROR;
			}else if(motor_status & 0x00004000){
			stille_object[axis].motor_stat=MOT_OVERCURRENT_ERROR;
			}else {
			if(motor_status & 0xBFD07FFF)//0xBFF07FFF
			stille_object[axis].motor_stat=MOT_ERROR;
			else
			stille_object[axis].motor_stat=MOT_READY;
		}
		return RES_SUCCESS;
	}
	
	
	uint32_t stille_GetMotorAndMotionStatus(int axis){
		
		if(stille_object[axis].initialized!=1){
			return RES_FAIL;
		}
		
		//Get Actual Speed
		uint32_t motor_status=1;
		uint8_t status=0;
		if(!stille_Read_2Parameter_OnlyValues(STILLE_MOTION_ERROR_BASEADDR+stille_object[axis].node_id,STILLE_RG_REPLY_HEADER+4, (void *)&motor_status,
				STILLE_MOTION_STATUS_BASEADDR+stille_object[axis].node_id, STILLE_RG_REPLY_HEADER+1, (void *)&status)){
			stille_object[axis].motor_stat=MOT_ERROR;
			return RES_FAIL;
		}
		stille_object[axis].motor_stat=MOT_READY;

		stille_object[axis].motion_stat = (status & 0x010) ? STILLE_MOVING : STILLE_STOP;
				
		return RES_SUCCESS;
	}


	//M_STOP_CMD axis=MOT_STILLE, return Ack or not
	uint32_t stille_StopHere(int axis){
		if(stille_object[axis].initialized!=1)
			return RES_FAIL;
		
		if(!stille_stop_function(stille_object[axis].node_id, STILLE_RUGGED_STARTSTOP,255))
			return RES_FAIL;
					
		return RES_SUCCESS;
	}
	
	//GET_ACTUAL_POS_CMD axis=MOT_STILLE, return Ack or not
	uint32_t stille_GetMotorActPos(int axis, int32_t* pos){
		if(stille_object[axis].initialized!=1)
			return RES_FAIL;		
		
		*pos=stille_object[axis].actual_position;
		if(stille_object[axis].max_position_limit!=INT_MAX){
			if(stille_object[axis].node_id==STILLE_ACTUATOR0){
				*pos=stille_object[axis].max_position_limit-stille_object[axis].actual_position;
			}
		}
		
		//Avoid error, with negative or above maximum position
		if(*pos<0)
			*pos=0;
		if(*pos>stille_object[axis].max_position_limit)
			*pos=stille_object[axis].max_position_limit;
		
		DbgPrintf(DBGPRINT_STILLE, "last timeLifterGetPos %ldms\r\n",timeLifterGetPos);
		return RES_SUCCESS;
	}

	//GET_ACTUAL_POS_CMD axis=MOT_STILLE, return Ack or not
	uint32_t stille_GetMotorActPos_real(int axis, int32_t* pos){
		
		if(stille_object[axis].initialized!=1)
			return RES_FAIL;
		timeLifterGetPos=timer_TimerGetSysTick();
		
		//Get Actual Position
		uint32_t position=0;
		if(!stille_Read_Parameter_OnlyValue(STILLE_GETPOS_BASEADDR + stille_object[axis].node_id, (void *)&position, STILLE_RG_REPLY_HEADER+4)){
			timeLifterGetPos=timer_TimerGetSysTick()-timeLifterGetPos;
			return RES_FAIL;
		}
		//		*pos=STILLE_STEP_TO_MM(position);
		*pos=position;
		timeLifterGetPos=timer_TimerGetSysTick()-timeLifterGetPos;
		return RES_SUCCESS;
	}
	
	bool set_ActualPositionBurst(void){
		uint16_t Reg_Index = 0x3001;
		uint16_t Reg_Index1 = 0x0010;
		uint16_t LengthPacket = 24;
		uint8_t tx_param[4+26],rx_reply=0xff;
		uint16_t tx_pack_len=LengthPacket+STILLE_PARAM_INDEX_DIM;
		//Lenght of packet
		tx_param[0]=(uint8_t)(tx_pack_len&0x00FF);tx_param[1]=(uint8_t)((tx_pack_len>>8)&0x00FF);
		//Parameter Index
		tx_param[2]=(uint8_t)(Reg_Index&0x00FF);tx_param[3]=(uint8_t)((Reg_Index>>8)&0x00FF);
		//Data position Parameter
		int i=4;
		for(i=4;i<4+12;i++)
		tx_param[i]=0xff;
		//Parameter Index
		tx_param[i++]=(uint8_t)(Reg_Index1&0x00FF);
		tx_param[i++]=(uint8_t)((Reg_Index1>>8)&0x00FF);
		for(int u=i;u<i+10;u++)
			tx_param[u]=0xff;
				
		//Request write Parameter
		if(!stille_Command(RT,  tx_param, LengthPacket+STILLE_RT_TX_HEADER, &rx_reply, 1)){
			DbgPrintf(DBGPRINT_STILLE, "set_ActualPosBurst - RT Error\r\n");
			return false;
		}
		return true;
	}

	bool set_MotorStatusBurst(void){
		uint16_t Reg_Index = 0x3002;
		uint16_t Reg_Index1 = 0x0080;
		uint16_t LengthPacket = 24;
		uint8_t tx_param[4+26],rx_reply=0xff;
		uint16_t tx_pack_len=LengthPacket+STILLE_PARAM_INDEX_DIM;
		//Lenght of packet
		tx_param[0]=(uint8_t)(tx_pack_len&0x00FF);tx_param[1]=(uint8_t)((tx_pack_len>>8)&0x00FF);
		//Parameter Index
		tx_param[2]=(uint8_t)(Reg_Index&0x00FF);tx_param[3]=(uint8_t)((Reg_Index>>8)&0x00FF);
		int i=4;
		for(i=4;i<4+12;i++)
		tx_param[i]=0xff;
		//Parameter Index
		tx_param[i++]=(uint8_t)(Reg_Index1&0x00FF);
		tx_param[i++]=(uint8_t)((Reg_Index1>>8)&0x00FF);
		for(int u=i;u<i+10;u++)
			tx_param[u]=0xff;
				
		//Request write Parameter
		if(!stille_Command(RT,  tx_param, LengthPacket+STILLE_RT_TX_HEADER, &rx_reply, 1)){
			DbgPrintf(DBGPRINT_STILLE, "set_MotorStatusBurst - RT Error\r\n");
			return false;
		}
		return true;
	}
	
	bool set_MotionStatusBurst(void){
		uint16_t Reg_Index = 0x3003;
		uint16_t Reg_Index1 = 0x0170;
		uint16_t LengthPacket = 24;
		uint8_t tx_param[4+26],rx_reply=0xff;
		uint16_t tx_pack_len=LengthPacket+STILLE_PARAM_INDEX_DIM;
		//Lenght of packet
		tx_param[0]=(uint8_t)(tx_pack_len&0x00FF);tx_param[1]=(uint8_t)((tx_pack_len>>8)&0x00FF);
		//Parameter Index
		tx_param[2]=(uint8_t)(Reg_Index&0x00FF);tx_param[3]=(uint8_t)((Reg_Index>>8)&0x00FF);
		int i=4;
		for(i=4;i<4+12;i++)
			tx_param[i]=0xff;
		//Parameter Index
		tx_param[i++]=(uint8_t)(Reg_Index1&0x00FF);
		tx_param[i++]=(uint8_t)((Reg_Index1>>8)&0x00FF);
		for(int u=i;u<i+10;u++)
			tx_param[u]=0xff;
		
		//Request write Parameter
		if(!stille_Command(RT,  tx_param, LengthPacket+STILLE_RT_TX_HEADER, &rx_reply, 1)){
			DbgPrintf(DBGPRINT_STILLE, "set_MotionStatusBurst - RT Error\r\n");
			return false;
		}
		return true;
	}
	
	
	uint32_t stille_GetMotorStatus_all(void){
		timeLifterGetMotorMotionStatTot=timer_TimerSet(0);

		//Request CiclObj?
		uint8_t msg[3];
		uint8_t msg_rx[3+24];
		msg[0]=0x01;msg[1]=0x00;msg[2]=0x01;
		if(!stille_Command(RC,  msg, 3, msg_rx, STILLE_RG_REPLY_HEADER+24)){
			DbgPrintf(DBGPRINT_STILLE, "Slider_Read_Parameter - RC Error\r\n");
			return false;
		}

//		*pos=position;
		//Update motor_stat
		uint32_t motor_status;
		for (int axis=0;axis<MOTOR_MAXOBJ;axis++){
			if(stille_object[axis].initialized==1 && stille_object[axis].motor_model == MOT_STILLE){
				uint16_t msg_idx=STILLE_RG_REPLY_HEADER+(stille_object[axis].node_id)*sizeof(int32_t);
//				motor_status = *(uint32_t*)(&msg_rx[msg_idx]);
				motor_status = PUINT8_TO_V32_LE((&msg_rx[msg_idx]));
				if(motor_status==0){
					stille_object[axis].motor_stat=MOT_READY;
				}else if(motor_status & 0x0000001F){
					stille_object[axis].motor_stat=MOT_FAULT_ERROR;
				}else if(motor_status & 0x00004000){
					stille_object[axis].motor_stat=MOT_OVERCURRENT_ERROR;
				}else {
					if(motor_status & 0xBFD07FFF)//0xBFF07FFF
						stille_object[axis].motor_stat=MOT_ERROR;
					else
						stille_object[axis].motor_stat=MOT_READY;
				}
			}
		}
		timeLifterGetMotorMotionStatTot=timer_TimerSet(0)-timeLifterGetMotorMotionStatTot;
		DbgPrintf(DBGPRINT_STILLE, "last stille_GetMotorStatus_all %ldms\r\n",timeLifterGetMotorMotionStatTot);
		return RES_SUCCESS;
	}
	
	uint32_t stille_GetMotionStatus_all(int32_t* mot_stat){
		timeLifterGetMotorMotionStatTot=timer_TimerSet(0);
			
		//Request CiclObj?
		uint8_t msg[3];
//		uint8_t msg_rx[3+24];
		uint8_t msg_rx[3+6];
		msg[0]=0x01;msg[1]=0x00;msg[2]=0x02;
		if(!stille_Command(RC,  msg, 3, msg_rx, STILLE_RG_REPLY_HEADER+6)){
			DbgPrintf(DBGPRINT_STILLE, "Stille_Read_Parameter - RC Error\r\n");
			return false;
		}

		//		*pos=position;
		//Update motion_stat
		uint8_t status;
		for (int axis=0;axis<MOTOR_MAXOBJ;axis++){
			if(stille_object[axis].initialized==1 && stille_object[axis].motor_model == MOT_STILLE){
				uint16_t msg_idx=STILLE_RG_REPLY_HEADER+(stille_object[axis].node_id)*sizeof(uint8_t);
				status = *(uint8_t*)(&msg_rx[msg_idx]);
				stille_object[axis].motion_stat = (status & 0x010) ? STILLE_MOVING : STILLE_STOP;
			}
		}
		timeLifterGetMotorMotionStatTot=timer_TimerSet(0)-timeLifterGetMotorMotionStatTot;
		DbgPrintf(DBGPRINT_STILLE, "last stille_GetMotionStatus_all %ldms\r\n",timeLifterGetMotorMotionStatTot);
		return RES_SUCCESS;
	}

	
	//GET_ACTUAL_POS_CMD axis=MOT_STILLE, return Ack or not
	uint32_t stille_GetMotorActPos_all_axis(void){
			
		timeLifterGetPos=timer_TimerGetSysTick();
				
		//Request CiclObj?
		uint8_t msg[3];
		uint8_t msg_rx[3+24];
		msg[0]=0x01;msg[1]=0x00;msg[2]=0x00;
		if(!stille_Command(RC,  msg, 3, msg_rx, STILLE_RG_REPLY_HEADER+24)){
			DbgPrintf(DBGPRINT_STILLE, "Stillw_Read_Parameter - RC Error\r\n");
			return false;
		}

		//Update position
		for (int axis=0;axis<MOTOR_MAXOBJ;axis++){
			if(stille_object[axis].initialized==1 && stille_object[axis].motor_model == MOT_STILLE){
				uint16_t msg_idx=STILLE_RG_REPLY_HEADER+(stille_object[axis].node_id)*sizeof(int32_t);
				switch(stille_object[axis].node_id){
					case STILLE_ACTUATOR0:
					stille_object[axis].actual_position = (int32_t)PUINT8_TO_V32_LE((&msg_rx[msg_idx]));
					break;
					case STILLE_NODE_MOTOR2:
					stille_object[axis].actual_position = (int32_t)PUINT8_TO_V32_LE((&msg_rx[msg_idx]));
					break;
					case STILLE_NODE_MOTOR3:
					stille_object[axis].actual_position = (int32_t)PUINT8_TO_V32_LE((&msg_rx[msg_idx]));
					break;
					case STILLE_NODE_MOTOR4:
					stille_object[axis].actual_position = (int32_t)PUINT8_TO_V32_LE((&msg_rx[msg_idx]));
					break;
					case STILLE_NODE_MOTOR5:
					stille_object[axis].actual_position = (int32_t)PUINT8_TO_V32_LE((&msg_rx[msg_idx]));
					break;
					case STILLE_NODE_MOTOR6:
					stille_object[axis].actual_position = (int32_t)PUINT8_TO_V32_LE((&msg_rx[msg_idx]));
					break;
					default:
					stille_object[axis].actual_position = (int32_t)PUINT8_TO_V32_LE((&msg_rx[msg_idx]));
					break;
				}
			}
		}

		timeLifterGetPos=timer_TimerGetSysTick()-timeLifterGetPos;
		DbgPrintf(DBGPRINT_STILLE, "last stille_GetMotorActPos_all_axis %ldms\r\n",timeLifterGetPos);
		return RES_SUCCESS;
	}



/********************************************************************************************************/
/*	END BINDING FOR MOTOR CONTROL																		*/
/********************************************************************************************************/

bool check_connection(uint8_t stille_axes){
	uint16_t speed=MAX_STILLE_SPEED;
	return stille_Read_Parameter_OnlyValue(STILLE_SETSPEED_BASEADDR+ stille_object[stille_axes].node_id, (void *)&speed, STILLE_RG_REPLY_HEADER+2);
}

bool stille_move_to_position(uint32_t Step_Pos, uint8_t stille_axes){
	//Get Actual Speed
	uint16_t speed=stille_object[stille_axes].pos_speed;
	
	//Set Target Position
	if(!stille_set_target_position(Step_Pos,stille_axes))
		return false;	
	
	//Before stop anything else
	if(!stille_stop_function_small(stille_axes, STILLE_RUGGED_STARTSTOP,255))
		return false;
	//Then Move
	if(!stille_start_function(stille_axes,STILLE_MOVE_TO_REMOTE_FUN, 255, (uint8_t)(speed & 0x00FF)))
		return false;
	
	return true;
}

bool stille_set_target_position(uint32_t Step_Pos, uint8_t stille_axes){
	//Set Zero Position
	uint16_t Reg_Index = STILLE_SETPOS_BASEADDR+stille_axes;
	uint16_t LengthPacket = 4;
	uint8_t tx_param[8],rx_reply=0xff;
	uint16_t tx_pack_len=LengthPacket+STILLE_PARAM_INDEX_DIM;
	//Lenght of packet
	tx_param[0]=(uint8_t)(tx_pack_len&0x00FF);tx_param[1]=(uint8_t)((tx_pack_len>>8)&0x00FF);
	//Parameter Index
	tx_param[2]=(uint8_t)(Reg_Index&0x00FF);tx_param[3]=(uint8_t)((Reg_Index>>8)&0x00FF);
	//Data position Parameter
	tx_param[4]=(uint8_t)(Step_Pos&0x00FF);tx_param[5]=(uint8_t)((Step_Pos>>8)&0x00FF);
	tx_param[6]=(uint8_t)((Step_Pos>>16)&0x00FF);tx_param[7]=(uint8_t)((Step_Pos>>24)&0x00FF);
	
	//Write Position
	if(!stille_Write_Parameter(tx_param, LengthPacket+STILLE_RT_TX_HEADER,&rx_reply,1))
		return false;
	
	return true;
};


bool stille_set_speed(uint32_t Speed_step_to_s, uint8_t stille_axes){
	if(Speed_step_to_s>MAX_STILLE_SPEED)
		return false;
	
	//Set Zero Position
	uint16_t Reg_Index = STILLE_SETSPEED_BASEADDR+stille_axes;
	uint16_t LengthPacket = 2;
	uint8_t tx_param[8],rx_reply=0xff;
	uint16_t tx_pack_len=LengthPacket+STILLE_PARAM_INDEX_DIM;
	//Lenght of packet
	tx_param[0]=(uint8_t)(tx_pack_len&0x00FF);tx_param[1]=(uint8_t)((tx_pack_len>>8)&0x00FF);
	//Parameter Index
	tx_param[2]=(uint8_t)(Reg_Index&0x00FF);tx_param[3]=(uint8_t)((Reg_Index>>8)&0x00FF);
	//Data position Parameter
	tx_param[4]=(uint8_t)(Speed_step_to_s&0x00FF);tx_param[5]=(uint8_t)((Speed_step_to_s>>8)&0x00FF);
	
	//Write Speed
	if(!stille_Write_Parameter(tx_param, LengthPacket+STILLE_RT_TX_HEADER,&rx_reply,1)){
		return false;
	}
	return true;
};

bool stille_Command(int cmd_enum, uint8_t *ptr_tx_parameter, uint16_t size_par_tx, uint8_t *ptr_rx_parameter, uint16_t size_par_rx){
	if(cmd_enum>RA || size_par_tx>MAX_STILLE_COMMAND_LEN-STILLE_CRC_LEN-STILLE_CMD_LEN)
		return false;
		
	volatile unsigned char msg[MAX_STILLE_COMMAND_LEN];
	volatile uint16_t crt, par_len=0;

	memset((void *)msg,0,MAX_STILLE_COMMAND_LEN);

	msg[0]=available_stille_cmd[cmd_enum].datal;
	msg[1]=available_stille_cmd[cmd_enum].datah;
	
	switch(cmd_enum){
		case RG:
			//Test
			if(size_par_tx>MAX_PAR_RG)
				return false;
			//	0x52, 0x47 , 0x71, 0x01
			while(par_len<size_par_tx){
				msg[STILLE_CMD_LEN+par_len]=ptr_tx_parameter[par_len];
				par_len++;
			}
			crt= CalculateChecksum((unsigned char*)msg, STILLE_CMD_LEN+par_len);
			msg[STILLE_CMD_LEN+par_len++]=(uint8_t)(crt & 0x00ff);
			msg[STILLE_CMD_LEN+par_len++]=(uint8_t)(crt>>8 & 0x00ff);
			bufferFlush(comm_stille_get_buffercmd());
			if(par_len>size_par_tx+STILLE_CRC_LEN)
				return false;
			par_len=STILLE_CMD_LEN+par_len;//+STILLE_CRC_LEN;
			comm_stille_write_byte((unsigned char*)msg,par_len);
			break;
		case RT:
			//Test
			if(size_par_tx>MAX_PAR_RT)
				return false;
			while(par_len<size_par_tx){
				msg[STILLE_CMD_LEN+par_len]=ptr_tx_parameter[par_len];
				par_len++;
			}
			crt= CalculateChecksum((unsigned char*)msg, STILLE_CMD_LEN+par_len);
			msg[STILLE_CMD_LEN+par_len++]=(uint8_t)(crt & 0x00ff);
			msg[STILLE_CMD_LEN+par_len++]=(uint8_t)(crt>>8 & 0x00ff);
			bufferFlush(comm_stille_get_buffercmd());
			if(par_len>size_par_tx+STILLE_CRC_LEN)
				return false;
			par_len=STILLE_CMD_LEN+par_len;
			comm_stille_write_byte((unsigned char*)msg,par_len);
			break;
		case RC:
			//Test
			if(size_par_tx>MAX_PAR_RC)
				return false;
			//	52 43 01 00 Ff 33 31 8d
			while(par_len<size_par_tx){
				msg[STILLE_CMD_LEN+par_len]=ptr_tx_parameter[par_len];
				par_len++;
			}
			crt= CalculateChecksum((unsigned char*)msg, STILLE_CMD_LEN+par_len);
			msg[STILLE_CMD_LEN+par_len++]=(uint8_t)(crt & 0x00ff);
			msg[STILLE_CMD_LEN+par_len++]=(uint8_t)(crt>>8 & 0x00ff);
			bufferFlush(comm_stille_get_buffercmd());
			if(par_len>size_par_tx+STILLE_CRC_LEN)
				return false;
			par_len=STILLE_CMD_LEN+par_len;//+STILLE_CRC_LEN;
			comm_stille_write_byte((unsigned char*)msg,par_len);
			break;
		case RE:
			//Test
			if(size_par_tx>MAX_PAR_RE)
				return false;
			//	52 43 01 00 Ff 33 31 8d
			while(par_len<size_par_tx){
				msg[STILLE_CMD_LEN+par_len]=ptr_tx_parameter[par_len];
				par_len++;
			}
			crt= CalculateChecksum((unsigned char*)msg, STILLE_CMD_LEN+par_len);
			msg[STILLE_CMD_LEN+par_len++]=(uint8_t)(crt & 0x00ff);
			msg[STILLE_CMD_LEN+par_len++]=(uint8_t)(crt>>8 & 0x00ff);
			bufferFlush(comm_stille_get_buffercmd());
			if(par_len>size_par_tx+STILLE_CRC_LEN)
				return false;
			par_len=STILLE_CMD_LEN+par_len;//+STILLE_CRC_LEN;
			comm_stille_write_byte((unsigned char*)msg,par_len);
			break;
		case RS:
			//Test
			if(size_par_tx>MAX_PAR_RE)
				return false;
			//	52 43 01 00 Ff 33 31 8d
			while(par_len<size_par_tx){
				msg[STILLE_CMD_LEN+par_len]=ptr_tx_parameter[par_len];
				par_len++;
			}
			crt= CalculateChecksum((unsigned char*)msg, STILLE_CMD_LEN+par_len);
			msg[STILLE_CMD_LEN+par_len++]=(uint8_t)(crt & 0x00ff);
			msg[STILLE_CMD_LEN+par_len++]=(uint8_t)(crt>>8 & 0x00ff);
			bufferFlush(comm_stille_get_buffercmd());
			if(par_len>size_par_tx+STILLE_CRC_LEN)
				return false;
			par_len=STILLE_CMD_LEN+par_len;//+STILLE_CRC_LEN;
			comm_stille_write_byte((unsigned char*)msg,par_len);
			break;
		case RO:
			//Test
			if(size_par_tx>MAX_PAR_RO)
				return false;
			//	0x52,0x47,0x00,0x5C,0x2D
			while(par_len<size_par_tx){	
				msg[STILLE_CMD_LEN+par_len]=ptr_tx_parameter[par_len];
				par_len++;
			}
			crt= CalculateChecksum((unsigned char*)msg, ROPEN_CMD_CMD-STILLE_CRC_LEN);
			msg[STILLE_CMD_LEN+par_len++]=(uint8_t)(crt & 0x00ff);
			msg[STILLE_CMD_LEN+par_len++]=(uint8_t)(crt>>8 & 0x00ff);
			bufferFlush(comm_stille_get_buffercmd());
			if(par_len>ROPEN_CMD_CMD-STILLE_CMD_LEN)
				return false;
			comm_stille_write_byte((unsigned char*)msg,ROPEN_CMD_CMD);
			break;
		case RA:
			//Test
			if(size_par_tx>MAX_PAR_RA)
				return false;
			crt= CalculateChecksum((unsigned char*)msg, STILLE_CMD_LEN+par_len);
			msg[STILLE_CMD_LEN+par_len++]=(uint8_t)(crt & 0x00ff);
			msg[STILLE_CMD_LEN+par_len++]=(uint8_t)(crt>>8 & 0x00ff);
			bufferFlush(comm_stille_get_buffercmd());
			if(par_len>size_par_tx+STILLE_CRC_LEN)
				return false;
			par_len=STILLE_CMD_LEN+par_len;//+STILLE_CRC_LEN;
			comm_stille_write_byte((unsigned char*)msg,par_len);
			break;
		default:
//			return false;
			break;
	}
		
		
	// start timer and wait for answer
	volatile uint32_t timeout = timer_TimerSet(STILLE_WDT_TIME);
	volatile int tout = 1;
	do
	{
		// check for answer ..
		if(stille_checkresponse(cmd_enum,comm_stille_get_buffercmd(), ptr_rx_parameter, size_par_rx))
		{
			tout = 0;
			break;
		}
		joinedWdt_restart();
	} while (!timer_TimerTest(timeout));
	//If timeout occurs
	if(tout){
		bufferFlush(comm_stille_get_buffercmd());
		return false;
	}
	bufferFlush(comm_stille_get_buffercmd());
	return (check_stille_error(ptr_rx_parameter[0])==false);
}

int isLifterCmd(cBuffer* cmd_buff, uint16_t bad_start_byte){
	int result=-1;
	uint8_t low,high;
	for (int i=0;i<RA+1;i++){
		low = bufferGetAtIndex(cmd_buff,bad_start_byte+0);
		high = bufferGetAtIndex(cmd_buff,bad_start_byte+1);
		if(available_stille_cmd[i].datal==low &&
			available_stille_cmd[i].datah==high)
		{
			result=i;
			break;
		}
	}
	return result;
}


bool stille_checkresponse(int cmd_enum,cBuffer* buff, uint8_t *ptr_rx_parameter, uint16_t size_par_rx)
{
	volatile uint8_t	reply_msg[STILLE_BUFFER_LEN];
	volatile uint8_t	lchecksum_idx,hchecksum_idx;
	volatile uint16_t	checksum;
	volatile uint16_t	bad_start_byte=0;
	volatile bool res=false;
	memset((void *)reply_msg,0,STILLE_BUFFER_LEN);
	if(cmd_enum>RA || (int)size_par_rx>(int)bufferGetDatalength(buff)-STILLE_CRC_LEN-STILLE_CMD_LEN)
		return res;
	//Check good start
	do{
		reply_msg[0]=bufferGetAtIndex(buff,bad_start_byte);
		if(reply_msg[0]!='R'){
			bad_start_byte++;
		}else{
			break;
		}			
	}while(bad_start_byte<buff->datalength);		
	if(((int)buff->datalength-(int)bad_start_byte)>0){
		//End check good start
		if(isLifterCmd(buff,bad_start_byte)!=cmd_enum){
			return res;
		}
		reply_msg[0]=bufferGetAtIndex(buff,bad_start_byte+0);
		reply_msg[1]=bufferGetAtIndex(buff,bad_start_byte+1);
		reply_msg[2]=bufferGetAtIndex(buff,bad_start_byte+2);
		if(reply_msg[STILLE_CMD_LEN]!=STILLE_ACK){
			size_par_rx=1;			//Bad response					
		}
		switch(cmd_enum){
						case RE:
							if(((int)buff->datalength-(int)bad_start_byte)>=(STILLE_CMD_LEN+STILLE_CRC_LEN+size_par_rx))
							{
								for(int i=1;i<size_par_rx;i++){
									reply_msg[STILLE_CMD_LEN+i]=bufferGetAtIndex(buff,bad_start_byte+2+i);
								}
								lchecksum_idx=STILLE_CMD_LEN+size_par_rx;
								reply_msg[lchecksum_idx]=bufferGetAtIndex(buff,bad_start_byte+lchecksum_idx);
								hchecksum_idx=lchecksum_idx+1;
								reply_msg[hchecksum_idx]=bufferGetAtIndex(buff,bad_start_byte+hchecksum_idx);
								checksum=CalculateChecksum((unsigned char*)reply_msg,STILLE_CMD_LEN+size_par_rx);
								if( reply_msg[lchecksum_idx]==(uint8_t)(checksum & 0x00ff) && reply_msg[hchecksum_idx]==(uint8_t)(checksum>>8 & 0x00ff) ){
									for(int i=0;i<size_par_rx;i++)
									ptr_rx_parameter[i]=reply_msg[STILLE_CMD_LEN+i];	//Respond Errors or Ack
									res=true;
								}
							}
							break;
			case RG:
			case RT:
			case RC:
			case RS:
			case RA:			
				if(((int)buff->datalength-(int)bad_start_byte)>=(STILLE_CMD_LEN+STILLE_CRC_LEN+size_par_rx))
				{	
					for(int i=1;i<size_par_rx;i++){
						reply_msg[STILLE_CMD_LEN+i]=bufferGetAtIndex(buff,bad_start_byte+2+i);
					}
					lchecksum_idx=STILLE_CMD_LEN+size_par_rx;
					reply_msg[lchecksum_idx]=bufferGetAtIndex(buff,bad_start_byte+lchecksum_idx);
					hchecksum_idx=lchecksum_idx+1;
					reply_msg[hchecksum_idx]=bufferGetAtIndex(buff,bad_start_byte+hchecksum_idx);
					checksum=CalculateChecksum((unsigned char*)reply_msg,STILLE_CMD_LEN+size_par_rx);
					if( reply_msg[lchecksum_idx]==(uint8_t)(checksum & 0x00ff) && reply_msg[hchecksum_idx]==(uint8_t)(checksum>>8 & 0x00ff) ){
						for(int i=0;i<size_par_rx;i++)
							ptr_rx_parameter[i]=reply_msg[STILLE_CMD_LEN+i];	//Respond Errors or Ack
						res=true;
					}
				}
				break;
			case RO:
				if((int)buff->datalength-(int)bad_start_byte>=ROPEN_CMD_CMD)
				{	
					reply_msg[3]=bufferGetAtIndex(buff,bad_start_byte+3);
					reply_msg[4]=bufferGetAtIndex(buff,bad_start_byte+4);
					checksum=CalculateChecksum((unsigned char*)reply_msg,ROPEN_CMD_CMD-STILLE_CRC_LEN);
					if( reply_msg[3]==(uint8_t)(checksum & 0x00ff) && reply_msg[4]==(uint8_t)(checksum>>8 & 0x00ff) ){
						ptr_rx_parameter[0]=reply_msg[2];	//Respond Errors or Ack
						res=true;
					}
				}
				break;
			default:
				res=false;
				break;			
		}
	}
	return res;
}

//Using RG command
bool stille_Read_Parameter(uint16_t parameter_idx, uint8_t *rx_reply,int expeted_rx){
	uint8_t msg[3]={0x00,0x5C,0x2D};
	uint8_t msg_rx[10];
	memset(msg_rx,0,10);
	//Open communication
	if(!stille_Command(RO,  msg, 1, msg_rx, 1)){
		DbgPrintf(DBGPRINT_STILLE, "Stille_Read_Par - RO Error\r\n");
		return false;
	}
	//Active communication
	msg[0]=0x01;msg[1]=0x00;msg[2]=0xFF;
	if(!stille_Command(RC,  msg, 3, msg_rx, 1)){
		DbgPrintf(DBGPRINT_STILLE, "Stille_Read_Par - RC Error\r\n");
		return false;
	}
	//Request Parameter
	msg[0]=(uint8_t)(parameter_idx&0x00FF);msg[1]=(uint8_t)((parameter_idx>>8)&0x00FF);
	if(!stille_Command(RG,  msg, 2, rx_reply, expeted_rx)){
		DbgPrintf(DBGPRINT_STILLE, "Stille_Read_Par - RG Error\r\n");
		return false;
	}
	/*
	//Close communication
	if(!stille_Command(RA,  msg, 0, rx_reply, 1)){
		DbgPrintf(DBGPRINT_STILLE, "stille_Read_Parameter - Error in RA cmd\r\n");
		return false;
	}
	*/
	return true;
}


//Using RG command
bool stille_Read_Parameter_OnlyValue(uint16_t parameter_idx, void *rx_reply,int expeted_rx){
	//No enought character to wait
	if(expeted_rx<=STILLE_RG_REPLY_HEADER)
		return false;
	uint8_t msg[3]={0x00,0x5C,0x2D};
	uint8_t msg_rx[STILLE_RG_REPLY_HEADER+4];
	memset(msg_rx,0,STILLE_RG_REPLY_HEADER+4);
	//Open communication
	if(!stille_Command(RO,  msg, 1, msg_rx, 1)){
		DbgPrintf(DBGPRINT_STILLE, "Stille_Read_Par - RO Error\r\n");
		return false;
	}
	//Active communication
	msg[0]=0x01;msg[1]=0x00;msg[2]=0xFF;
	if(!stille_Command(RC,  msg, 3, msg_rx, 1)){
		DbgPrintf(DBGPRINT_STILLE, "Stille_Read_Par - RC Error\r\n");
		return false;
	}
	//Request Parameter
	msg[0]=(uint8_t)(parameter_idx&0x00FF);msg[1]=(uint8_t)((parameter_idx>>8)&0x00FF);
	if(!stille_Command(RG,  msg, 2, msg_rx, expeted_rx)){
		DbgPrintf(DBGPRINT_STILLE, "Stille_Read_Par - RG Error\r\n");
		return false;
	}
	
	uint8_t value_size=expeted_rx-STILLE_RG_REPLY_HEADER;
	switch(value_size){
		case 1:
			*((uint8_t *)rx_reply)=*((uint8_t *)&msg_rx[STILLE_RG_REPLY_HEADER]);
			break;
		case 2:
//			*((uint16_t *)rx_reply)=*((uint16_t *)&msg_rx[STILLE_RG_REPLY_HEADER]);
			*((uint16_t *)rx_reply)=PUINT8_TO_V16_LE((&msg_rx[STILLE_RG_REPLY_HEADER]));
			break;
		case 4:
//			*((uint32_t *)rx_reply)=*((uint32_t *)&msg_rx[STILLE_RG_REPLY_HEADER]);
			*((uint32_t *)rx_reply)=PUINT8_TO_V32_LE((&msg_rx[STILLE_RG_REPLY_HEADER]));
			break;
		default:
//			*((uint32_t *)rx_reply)=*((uint32_t *)&msg_rx[STILLE_RG_REPLY_HEADER]);
			*((uint32_t *)rx_reply)=PUINT8_TO_V32_LE((&msg_rx[STILLE_RG_REPLY_HEADER]));
			break;
	}
	return true;
}



//Using two RG command
bool stille_Read_2Parameter_OnlyValues(uint16_t parameter1_idx,int expeted1_rx, void *rx1_reply,uint16_t parameter2_idx,int expeted2_rx, void *rx2_reply){
	//No enought character to wait
	if(expeted1_rx<=STILLE_RG_REPLY_HEADER)
		return false;
	if(expeted2_rx<=STILLE_RG_REPLY_HEADER)
		return false;
	timeLifterGetMotorMotionStatTot=timer_TimerSet(0);

	uint8_t msg[3]={0x00,0x5C,0x2D};
	uint8_t msg_rx[STILLE_RG_REPLY_HEADER+4];
	memset(msg_rx,0,STILLE_RG_REPLY_HEADER+4);
	//Open communication
	if(!stille_Command(RO,  msg, 1, msg_rx, 1)){
		DbgPrintf(DBGPRINT_STILLE, "Stille_Read_Parameter - RO Error\r\n");
		return false;
	}
	timeLifterGetMotorMotionStatRO=timer_TimerSet(0)-timeLifterGetMotorMotionStatTot;
	//Active communication
	msg[0]=0x01;msg[1]=0x00;msg[2]=0xFF;
	if(!stille_Command(RC,  msg, 3, msg_rx, 1)){
		DbgPrintf(DBGPRINT_STILLE, "Stille_Read_Par - RC Error\r\n");
		return false;
	}
	timeLifterGetMotorMotionStatRC=timer_TimerSet(0)-timeLifterGetMotorMotionStatTot-timeLifterGetMotorMotionStatRO;

	//Request 1Parameter
	msg[0]=(uint8_t)(parameter1_idx&0x00FF);msg[1]=(uint8_t)((parameter1_idx>>8)&0x00FF);
	if(!stille_Command(RG,  msg, 2, msg_rx, expeted1_rx)){
		DbgPrintf(DBGPRINT_STILLE, "Stille_Read_Par - RG Error\r\n");
		return false;
	}
	timeLifterGetMotorMotionStatRG=timer_TimerSet(0)-timeLifterGetMotorMotionStatTot-timeLifterGetMotorMotionStatRC-timeLifterGetMotorMotionStatRO;
	
	uint8_t value_size=expeted1_rx-STILLE_RG_REPLY_HEADER;
	switch(value_size){
		case 1:
		*((uint8_t *)rx1_reply)=*((uint8_t *)&msg_rx[STILLE_RG_REPLY_HEADER]);
		break;
		case 2:
//			*((uint16_t *)rx1_reply)=*((uint16_t *)&msg_rx[STILLE_RG_REPLY_HEADER]);
			*((uint16_t *)rx1_reply)=PUINT8_TO_V16_LE((&msg_rx[STILLE_RG_REPLY_HEADER]));
			break;
		case 4:
//			*((uint32_t *)rx1_reply)=*((uint32_t *)&msg_rx[STILLE_RG_REPLY_HEADER]);
			*((uint32_t *)rx1_reply)=PUINT8_TO_V32_LE((&msg_rx[STILLE_RG_REPLY_HEADER]));
			break;
		default:
//			*((uint32_t *)rx1_reply)=*((uint32_t *)&msg_rx[STILLE_RG_REPLY_HEADER]);
			*((uint32_t *)rx1_reply)=PUINT8_TO_V32_LE((&msg_rx[STILLE_RG_REPLY_HEADER]));
			break;
	}

	//Request 1Parameter
	msg[0]=(uint8_t)(parameter2_idx&0x00FF);msg[1]=(uint8_t)((parameter2_idx>>8)&0x00FF);
	if(!stille_Command(RG,  msg, 2, msg_rx, expeted2_rx)){
		DbgPrintf(DBGPRINT_STILLE, "Stille_Read_Par - RG Error\r\n");
		return false;
	}
	
	value_size=expeted2_rx-STILLE_RG_REPLY_HEADER;
	switch(value_size){
		case 1:
		*((uint8_t *)rx2_reply)=*((uint8_t *)&msg_rx[STILLE_RG_REPLY_HEADER]);
		break;
		case 2:
//			*((uint16_t *)rx2_reply)=*((uint16_t *)&msg_rx[STILLE_RG_REPLY_HEADER]);
			*((uint16_t *)rx2_reply)=PUINT8_TO_V16_LE((&msg_rx[STILLE_RG_REPLY_HEADER]));
			break;
		case 4:
//			*((uint32_t *)rx2_reply)=*((uint32_t *)&msg_rx[STILLE_RG_REPLY_HEADER]);
			*((uint32_t *)rx2_reply)=PUINT8_TO_V32_LE((&msg_rx[STILLE_RG_REPLY_HEADER]));
			break;
		default:
//			*((uint32_t *)rx2_reply)=*((uint32_t *)&msg_rx[STILLE_RG_REPLY_HEADER]);
			*((uint32_t *)rx2_reply)=PUINT8_TO_V32_LE((&msg_rx[STILLE_RG_REPLY_HEADER]));
			break;
	}

	timeLifterGetMotorMotionStatTot=timer_TimerSet(0)-timeLifterGetMotorMotionStatTot;

	return true;
}



bool stille_Write_Parameter(uint8_t * parameter, uint16_t num_tx_par,uint8_t *rx_reply,int expected_rx){
	uint8_t msg[3]={0x00,0x5C,0x2D};
	uint8_t msg_rx[10];
	memset(msg_rx,0,10);
	//Open communication
	if(!stille_Command(RO,  msg, 1, msg_rx, 1)){
		DbgPrintf(DBGPRINT_STILLE, "Stille_Write_Par - RO Error\r\n");
		return false;
	}
	//Active communication
	msg[0]=0x01;msg[1]=0x00;msg[2]=0xFF;
	if(!stille_Command(RC,  msg, 3, msg_rx, 1)){
		DbgPrintf(DBGPRINT_STILLE, "Stille_Write_Par - RC Error\r\n");
		return false;
	}
	//Request write Parameter
	if(!stille_Command(RT,  parameter, num_tx_par, rx_reply, expected_rx)){
		DbgPrintf(DBGPRINT_STILLE, "Stille_Write_Par - RT Error\r\n");
		return false;
	}
	//Close communication
	/*
	if(!stille_Command(RA,  msg, 0, rx_reply, 1)){
		DbgPrintf(DBGPRINT_STILLE, "stille_Write_Parameter - Error in RA cmd\r\n");
		return false;
	}
	*/
	return true;
}

void test_stille_readwrite(){
	uint8_t rx_reply[10];
	uint16_t val=50,read_val;
	uint16_t param_idx=0x3011,tx_pack_len,tx_var_len;
	uint8_t tx_param[20];
	tx_var_len=sizeof(uint16_t);
	tx_pack_len=tx_var_len+STILLE_PARAM_INDEX_DIM;
	//Read Parameter
	if(stille_Read_Parameter(param_idx, rx_reply, STILLE_RG_REPLY_HEADER+tx_var_len)){
		read_val=(rx_reply[STILLE_RG_REPLY_HEADER+1]<<8)+rx_reply[STILLE_RG_REPLY_HEADER];
		DbgPrintf(DBGPRINT_STILLE, "test_stille_readwrite - Read Par 0x%04x = %d\r\n", param_idx,read_val);
	}else{
		DbgPrintf(DBGPRINT_STILLE, "test_stille_readwrite - Read Error par 0x%04x\r\n", param_idx);
		return;
	}

	//Write Parameter
	val=50;
	//Lenght of packet
	tx_param[0]=(uint8_t)(tx_pack_len&0x00FF);tx_param[1]=(uint8_t)((tx_pack_len>>8)&0x00FF);
	//Parameter Index
	tx_param[2]=(uint8_t)(param_idx&0x00FF);tx_param[3]=(uint8_t)((param_idx>>8)&0x00FF);	
	//Parameter value
	tx_param[4]=(uint8_t)(val&0x00FF);tx_param[5]=(uint8_t)((val>>8)&0x00FF);	
	if(stille_Write_Parameter(tx_param, STILLE_LENGHT_DIM+tx_pack_len,rx_reply,1)){
		DbgPrintf(DBGPRINT_STILLE, "Stille - Write Par 0x%04x = %d\r\n", param_idx,val);
	}else{
		DbgPrintf(DBGPRINT_STILLE, "Stille - Write Error par 0x%04x\r\n", param_idx);
		return;
	}


	//Read Parameter
	if(stille_Read_Parameter(param_idx, rx_reply, STILLE_RG_REPLY_HEADER+tx_var_len)){
		read_val=(rx_reply[STILLE_RG_REPLY_HEADER+1]<<8)+rx_reply[STILLE_RG_REPLY_HEADER];
		DbgPrintf(DBGPRINT_STILLE, "Stille - Read Par 0x%04x = %d\r\n", param_idx,read_val);
	}else{
		DbgPrintf(DBGPRINT_STILLE, "Stille - Read Error par 0x%04x\r\n", param_idx);
		return;
	}


	//Write Parameter
	val=75;
	//Parameter value
	tx_param[4]=(uint8_t)(val&0x00FF);tx_param[5]=(uint8_t)((val>>8)&0x00FF);	
	//Write Parameter
	if(stille_Write_Parameter(tx_param, STILLE_LENGHT_DIM+tx_pack_len,rx_reply,1)){
		DbgPrintf(DBGPRINT_STILLE, "Stille - Write Par 0x%04x = %d\r\n", param_idx,val);
	}else{
		DbgPrintf(DBGPRINT_STILLE, "Stille - Write Error par 0x%04x\r\n", param_idx);
		return;
	}
	
	//Read Parameter
	if(stille_Read_Parameter(param_idx, rx_reply, STILLE_RG_REPLY_HEADER+tx_var_len)){
		read_val=(rx_reply[STILLE_RG_REPLY_HEADER+1]<<8)+rx_reply[STILLE_RG_REPLY_HEADER];
		DbgPrintf(DBGPRINT_STILLE, "Stille - Read Par 0x%04x = %d\r\n", param_idx,read_val);
	}else{
		DbgPrintf(DBGPRINT_STILLE, "Stille - Read Error par 0x%04x\r\n", param_idx);
		return;
	}
}

void test_stille_movement(){
	// wait for answer
	uint32_t timeout = timer_TimerSet(2000);
	while (!timer_TimerTest(timeout)){
		joinedWdt_restart();
	};
	uint8_t msg[5]={0x52,0x4f,0x00,0x5C,0x2D};
	uint8_t msg_rx[10];
	memset(msg_rx,0,10);
	if(!stille_Command(RO,  &msg[2], 1, msg_rx, 1)){
		DbgPrintf(DBGPRINT_STILLE, "Stille - RO Error\r\n");
		return;
	}
	memset(msg_rx,0,10);
	uint8_t msg2[5]={0x52,0x43,0x01,0x00,0xFF};
	if(!stille_Command(RC,  &msg2[2], 3, msg_rx, 1)){
		DbgPrintf(DBGPRINT_STILLE, "Stille - RC Error\r\n");
		return;
	}
	memset(msg_rx,0,10);
	//Speed
	//52 54 04 00 11 30 01 00 B5 F2
	uint8_t msg3[8]={0x52 , 0x54 , 0x04 , 0x00 , 0x11 , 0x30 , 0x64, 0x00};
	if(!stille_Command(RT,  &msg3[2], 6, msg_rx, 1)){
		DbgPrintf(DBGPRINT_STILLE, "Stille - RT Error\r\n");
		return;
	}	
	memset(msg_rx,0,10);
	//RE fun id 9
	//52 45 00 09 ff
	uint8_t msg4[7]={0x52, 0x45 , STILLE_MOVE_TO_REMOTE_FUN, STILLE_ACTUATOR0, 0xff};
//	uint8_t msg4[7]={0x52, 0x45 , STILLE_ACTUATOR0, STILLE_MOVEIN_INTERMEDIATE_FUN, 0xff};
	if(!stille_Command(RE,  &msg4[2], 3, msg_rx, 1)){
		DbgPrintf(DBGPRINT_STILLE, "Stille - RE Error\r\n");
		return;
	}
	memset(msg_rx,0,10);
	//RG fun id 9
	//52 45 71 01
	uint8_t msg5[7]={0x52, 0x47 , 0x71, 0x01};
	if(!stille_Command(RG,  &msg5[2], 2, msg_rx, 4)){
		DbgPrintf(DBGPRINT_STILLE,"Stille - RG Error\r\n");
		return;
	}
	DbgPrintf(DBGPRINT_STILLE, "test_Stille Finished\r\n");
	memset(msg_rx,0,10);	
}


bool stille_start_function(uint8_t fun_index,uint8_t fun_par0, uint8_t fun_par1, uint8_t speed_par){
	// wait for answer
	volatile uint8_t msg_rx[10];
	memset((void *)msg_rx,0,10);
	//RE fun id 9
	//52 45 00 09 ff
	uint8_t msg4[3]={fun_index, fun_par0,fun_par1};
	//	uint8_t msg4[7]={0x52, 0x45 , STILLE_ACTUATOR0, STILLE_MOVEIN_INTERMEDIATE_FUN, 0xff};
	if(!stille_Command(RE,  msg4, 3, (uint8_t *)msg_rx, 1)){
		DbgPrintf(DBGPRINT_STILLE, "test_lifter - Error in RE cmd\r\n");
		return false;
	}

	return true;
}

bool stille_stop_function_small(uint8_t fun_index,uint8_t fun_par0, uint8_t fun_par1){
	// wait for answer
	volatile uint8_t msg_rx[10];
	memset((void *)msg_rx,0,10);
	//RS fun id 9
	//52 45 00 09 ff
	uint8_t msg4[3]={fun_index, fun_par0,fun_par1};
	//	uint8_t msg4[7]={0x52, 0x45 , STILLE_ACTUATOR0, STILLE_MOVEIN_INTERMEDIATE_FUN, 0xff};
	if(!stille_Command(RS,  msg4, 2, (uint8_t *)msg_rx, 1)){
		DbgPrintf(DBGPRINT_STILLE, "test_lifter - Error in RS cmd\r\n");
		return false;
	}
	memset((void *)msg_rx,0,10);
	
	return true;
}


bool stille_stop_function(uint8_t fun_index,uint8_t fun_par0, uint8_t fun_par1){
	// wait for answer
	uint8_t msg[1]={0x00};
	volatile uint8_t msg_rx[10];
	memset((void *)msg_rx,0,10);
	if(!stille_Command(RO,  msg, 1, (uint8_t *)msg_rx, 1)){
		DbgPrintf(DBGPRINT_STILLE, "Stille stop - RO Error\r\n");
		return false;
	}
	memset((void *)msg_rx,0,10);
	uint8_t msg2[3]={0x01,0x00,0xFF};
	if(!stille_Command(RC,  msg2, 3, (uint8_t *)msg_rx, 1)){
		DbgPrintf(DBGPRINT_STILLE, "Stille stop - RC Error\r\n");
		return false;
	}
	memset((void *)msg_rx,0,10);
	//RS fun id 9
	//52 45 00 09 ff
	uint8_t msg4[3]={fun_index, fun_par0,fun_par1};
	//	uint8_t msg4[7]={0x52, 0x45 , STILLE_ACTUATOR0, STILLE_MOVEIN_INTERMEDIATE_FUN, 0xff};
	if(!stille_Command(RS,  msg4, 2, (uint8_t *)msg_rx, 1)){
		DbgPrintf(DBGPRINT_STILLE, "Stille stop - RS Error\r\n");
		return false;
	}
	memset((void *)msg_rx,0,10);
	
	/*
	//RG fun id 9
	//52 45 71 01
	uint8_t msg5[2]={0x71, 0x01};
	if(!stille_Command(RG,  msg5, 2, msg_rx, 4)){
		DbgPrintf(DBGPRINT_STILLE,"test_lifter - Error on first RG cmd\r\n");
		return false;
	}
	**********************/
	return true;
}


bool check_stille_error(uint8_t reply){
	bool res=false;
	switch(reply){
		case STILLE_CSE:	//Checksum Error
			DbgPrintf(DBGPRINT_STILLE, "STILLE_CSE error\r\n");
			res=true;
			break;
		case STILLE_PDE:		//Parameter data error
			DbgPrintf(DBGPRINT_STILLE, "STILLE_PDE error\r\n");
			res=true;
			break;
		case STILLE_PCE:		//Parameter count error
			DbgPrintf(DBGPRINT_STILLE, "STILLE_PCE error\r\n");
			res=true;
			break;
		case STILLE_ICE:		//Invalid command error
			DbgPrintf(DBGPRINT_STILLE, "STILLE_ICE error\r\n");
			res=true;
			break;
		case STILLE_PE:		//Permission error
			DbgPrintf(DBGPRINT_STILLE, "STILLE_PE error\r\n");
			res=true;
			break;
		case  STILLE_TOUT:
			DbgPrintf(DBGPRINT_STILLE, "Timeout error\r\n");
			res=true;
			break;
		case STILLE_ACK:
//			DbgPrintf(DBGPRINT_STILLE, "STILLE_ACK\r\n");
			res=false;
			break;
	}
	
	return res;
}

void renew_stille_wdt(){
	uint8_t msg[3]={0x00,0x5C,0x2D};
	uint8_t msg_rx[10];
	memset(msg_rx,0,10);
	//Open communication
	if(!stille_Command(RO,  msg, 1, msg_rx, 1)){
		return;
	}
	//Active communication
	msg[0]=0x01;msg[1]=0x00;msg[2]=0xFF;
	if(!stille_Command(RC,  msg, 3, msg_rx, 1)){
		return;
	}
}

void process_stille_protocol(){
	//Permette una gestione sequenziale del comando stille_GetMotorAndMotionStatus
	static uint8_t current_axis=0;
	static uint8_t first_time_connection=0;
	uint8_t cycled_idx;
	volatile uint8_t check_pos_flag=0;
	
	//First Initialization
	if(first_time_connection==0){
		uint8_t msg_writep_init[3]={0x00,0x5C,0x2D};
		uint8_t msg_rx_writep[10];
		memset(msg_rx_writep,0,10);
		//Open communication
		if(!stille_Command(RO,  msg_writep_init, 1, msg_rx_writep, 1)){
			DbgPrintf(DBGPRINT_STILLE, "Stille process - RO Error\r\n");
			return;
		}
		memset(msg_rx_writep,0,10);
		//Active communication
		msg_writep_init[0]=0x01;msg_writep_init[1]=0x00;msg_writep_init[2]=0xFF;
		if(!stille_Command(RC,  msg_writep_init, 3, msg_rx_writep, 1)){
			DbgPrintf(DBGPRINT_STILLE, "Stille process - RC Error\r\n");
			return;
		}
		
		if(!set_ActualPositionBurst()){
			DbgPrintf(DBGPRINT_STILLE, "set_ActualPositionBurst error\r\n");
			return;
		}

		
		if(!set_MotorStatusBurst()){
			DbgPrintf(DBGPRINT_STILLE, "set_MotorStatusBurst error\r\n");
			return;
		}

		if(!set_MotionStatusBurst()){
			DbgPrintf(DBGPRINT_STILLE, "set_MotionStatusBurst error\r\n");
			return;
		}

		first_time_connection=1;		
	}
	
	//Request get Actual Position	
//	uint8_t pos_vect[24+3];
	if(!stille_GetMotorActPos_all_axis()){
		DbgPrintf(DBGPRINT_STILLE, "stille_GetMotorActPos_all_axis error, to reinit\r\n");
		first_time_connection=0;
	}


//	uint8_t err_vect[24+3];
	if(!stille_GetMotorStatus_all()){
		DbgPrintf(DBGPRINT_STILLE, "stille_GetMotorStatus_all error, to reinit\r\n");
		first_time_connection=0;		
	}

	uint8_t mot_vect[24+3];
	if(!stille_GetMotionStatus_all((void *)mot_vect)){
		DbgPrintf(DBGPRINT_STILLE, "stille_GetMotionStatus_all error, to reinit\r\n");
		first_time_connection=0;
	}
	
	if(timer_TimerTest(timeout_lifter)){
//		renew_stille_wdt();
		for(int axis_idx=0; axis_idx<MOTOR_MAXOBJ; axis_idx++ )
		{
			cycled_idx=(current_axis+axis_idx)%MOTOR_MAXOBJ;
			if((current_axis+axis_idx)/MOTOR_MAXOBJ)
				check_pos_flag=1;							//When turn around the status request, check position
			if(stille_object[cycled_idx].initialized != 0)
			{
				if(stille_object[cycled_idx].motor_model == MOT_STILLE /*&& cycled_idx!=current_axix*/){
//					stille_GetMotorStatus_real(axis_idx);
//					stille_GetMotionStatus_real(axis_idx);
					stille_object[cycled_idx].actual_position;
					
					stille_GetMotorAndMotionStatus(cycled_idx);
					
					if(check_pos_flag==1){
//						uint8_t pos_vect_local[24+3];
						stille_GetMotorActPos_all_axis();
					}
					
					current_axis=(cycled_idx+1)%MOTOR_MAXOBJ;
					break;
				}
			}
		}
		timeout_lifter = timer_TimerSet(STILLE_GUARD_TIME);
	}
}


typedef enum{
	LETTINO_INIT=0,
	LETTINO_GETPOS,	
	LETTINO_GETMOTOR_STATUS,	
	LETTINO_GETMOTION_STATUS
}lettino_state_t;
	
//Process State 	
void process_lettino_protocol(){
	//Permette una gestione sequenziale del comando stille_GetMotorAndMotionStatus
	static lettino_state_t lettino_state=LETTINO_INIT;
	uint8_t check_init=0;
	
	//LETTINO_INIT use
	uint8_t msg_writep_init[3]={0x00,0x5C,0x2D};
	uint8_t msg_rx_writep[10];
			
	//LETTINO_GETMOTION_STATUS
	uint8_t mot_vect[24+3];
		
	if(timer_TimerTest(timeout_lifter)){
		
		//Check if is ok
		for(int axis_idx=0; axis_idx<MOTOR_MAXOBJ; axis_idx++ )
		{
			if(stille_object[axis_idx].initialized != 0 && stille_object[axis_idx].motor_model==MOT_STILLE)
				check_init=1;
		}
			
		if(check_init==0){
			lettino_state = LETTINO_INIT;
			return;
		}
				
		//Lettino Machine State
		switch(lettino_state){
			case LETTINO_INIT:
				memset(msg_rx_writep,0,10);
				//Open communication
				if(!stille_Command(RO,  msg_writep_init, 1, msg_rx_writep, 1)){
					DbgPrintf(DBGPRINT_STILLE, "Stille lettino - RO Error\r\n");
					return;
				}
				memset(msg_rx_writep,0,10);
				//Active communication
				msg_writep_init[0]=0x01;msg_writep_init[1]=0x00;msg_writep_init[2]=0xFF;
				if(!stille_Command(RC,  msg_writep_init, 3, msg_rx_writep, 1)){
					DbgPrintf(DBGPRINT_STILLE, "Stille lettino - RC Error\r\n");
					return;
				}
		
				if(!set_ActualPositionBurst()){
					DbgPrintf(DBGPRINT_STILLE, "set_ActualPositionBurst error\r\n");
					return;
				}
		
				if(!set_MotorStatusBurst()){
					DbgPrintf(DBGPRINT_STILLE, "set_MotorStatusBurst error\r\n");
					return;
				}

				if(!set_MotionStatusBurst()){
					DbgPrintf(DBGPRINT_STILLE, "set_MotionStatusBurst error\r\n");
					return;
				}
				lettino_state = LETTINO_GETPOS;
				break;
			case LETTINO_GETPOS:
				//Request get Actual Position	
				
				if(!stille_GetMotorActPos_all_axis()){
					DbgPrintf(DBGPRINT_STILLE, "stille_GetMotorActPos_all_axis error, to reinit\r\n");
					lettino_state = LETTINO_INIT;
				}
				
				lettino_state = LETTINO_GETMOTOR_STATUS;
				break;
			case LETTINO_GETMOTOR_STATUS:
							
				if(!stille_GetMotorStatus_all()){
					DbgPrintf(DBGPRINT_STILLE, "stille_GetMotorStatus_all error, to reinit\r\n");
					lettino_state = LETTINO_INIT;	
				}
				
				lettino_state = LETTINO_GETMOTION_STATUS;
				break;
			case LETTINO_GETMOTION_STATUS:
						
				if(!stille_GetMotionStatus_all((void *)mot_vect)){
					DbgPrintf(DBGPRINT_STILLE, "stille_GetMotionStatus_all error, to reinit\r\n");
				}
				
				lettino_state = LETTINO_GETPOS;
				break;
		}
		timeout_lifter = timer_TimerSet(STILLE_GUARD_TIME);
	}
}
