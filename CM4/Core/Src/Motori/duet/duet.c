/*! \file duet.c \brief Motor control module */
//*****************************************************************************
//
// File Name	: 'duet.c'
// Title		: Motor control module.
// Author		: Alessandro
// Company		: Qprel s.r.l.
// Created		: 17/09/2014
// Revised		: -
// Version		: 1.0
//
///
/// \code #include "duet.h" \endcode
/// \par Overview
///		This module provide the functions to control the motor in position profile mode 
///
//*****************************************************************************
//@{ 
#include <BrakeManagement.h>

#include "main.h"
#include <string.h>
#include <stdlib.h>
#include "duet.h"
#include "co_main.h"
#include "co_microdep.h"
#include "co_nmt.h"
#include "co_sdo.h"
#include "co_pdo.h"
#include "dig_io.h"
#include "joinedWdt.h"
#include "interpolate.h"
#include "manage_debug.h"


#define		FORCE_DEBUG		true	//DBG_DUET

extern bool slider_enabled;
extern bool service_state;

#ifdef AGNOSTIC_AXIS
syncro_motor_t motor_sync1 = {false,-1};
syncro_motor_t motor_sync2 = {false,-1};
#endif
/// Array of motor object structures
motor_obj_t * duet_object;

/// Private function waiting for motor node status change with timeout
static uint32_t duet_WaitForStatus(int axis, uint32_t status, uint32_t tout_ms);
/// Private function to get parameter from the motor node object dictionary
static uint32_t duet_GetParameter(int axis, uint32_t idx, uint8_t subidx, uint32_t * val, uint32_t size);
#ifdef	OLD_STYLE_COPROCESS
/// Private function for the callback array of co_main, to manage motor nodes.
static void duet_CbackManage(void *param, uint8_t *);
#endif
/// Private function to command new absolute position to the motor node.
static uint32_t duet_GoAbsolute(int axis, int32_t new_position);
///Function to check electric current limit on slider motors
static void check_slider_currentLimit(void);

uint32_t duet_SetHeartbeat(int axis, uint32_t hb_timeout)
{
	// stop checking heartbeat
	// if heartbeat timeout != enable checking
	if(hb_timeout != 0)
		duet_object[axis].heartbeat_timer = timer_TimerSet(hb_timeout*10);
	// save timeout in local var
	duet_object[axis].heardbeat_tout = hb_timeout*2;
	// program motor driver
	if(!duet_SetParameter(axis, 0x1017, 0, &hb_timeout, 2))
		return RES_FAIL;
		
	return RES_SUCCESS;
}

/**
 * \fn void motor_InitData(void)
 * \brief Initialize the motor objects array and
 *        add the motor_Machine() function to the co_main calling list
 *
 */
void duet_InitData(motor_obj_t * m_obj)
{		
	duet_object = m_obj;	
}

/**
 * @brief Initialize the motor drive
 *
 * Send the command sequence to make the motor drive in the operative state,
 * as requested by the CanOpen specifications.
 * - reset the node, and sent command to start it.
 * - sends "shutdown" command to put node in "ready to switch on" state
 * - sends "switch on" command to put node in "switched on" state
 * - sends "enable operation" command to put node in "operation enable" state
 * If the command sequence fails return 0, 1 if success
 *  
 * @param 'int axis' Axis (index in the motor_object array)
 * @param 'uint8_t node' Node id to initialize.
 *
 * @return 1 if success, 0 if fails.
 * 
 */
uint32_t duet_Init(int axis, uint8_t node)
{
	uint32_t stat_res;
	intr_cback_t icb;
	uint32_t val;	
	reset_co_icb(&icb);
			
	// stop heartbeat timer check
	duet_object[axis].heardbeat_tout = 0;

#ifdef	OLD_STYLE_COPROCESS
	// initialize the message queue
	duet_object[axis].rx_queue_read_p = 0;
	duet_object[axis].rx_queue_write_p = 0;
#endif
	// Initialize controlword and statusword for motor control
	duet_object[axis].statusword = 0;
	duet_object[axis].controlword = 0;

	duet_object[axis].motor_stat = MOT_NOT_INIT;
	
	// Read the status word
	uint32_t sw;
	if(duet_ReadStatusWord(axis, &sw))
		duet_object[axis].statusword = sw;
	
	// send NMT message to reset node
	co_nmt_Send(node, NMT_RESET);
	// wait for node boot up and initialize
	HAL_Delay(4000);

	// send NMT message to go operational
	co_nmt_Send(node, NMT_START);
	// wait for "switch on disabled" status
	stat_res = duet_WaitForStatus(axis, MOT_SWITCH_ON_DISABLE, 20/*4000*/);
	if (!stat_res)
	{
		DbgPrintf(DBG_DUET, "Init error: SWITCH_ON_DISABLE failed, code%d\r\n",stat_res);
		duet_object[axis].motor_stat = MOT_INIT_ERROR;
		return RES_FAIL;
	}
	// send shutdown command
	if(!duet_StateCommand(axis, MOT_CMD_SHUTDOWN))
	{
		DbgPrintf(DBG_DUET, "Init error: SHUTDOWN failed\r\n");
		duet_object[axis].motor_stat = MOT_INIT_ERROR;
		return RES_FAIL;
	}
	// wait for "ready to switch on" status
	stat_res = duet_WaitForStatus(axis, MOT_READY_TO_SWITCH_ON, 5/*1000*/);
	if (!stat_res)
	{
		DbgPrintf(DBG_DUET, "Init error: READY_TO_SWITCH_ON failed, code%d\r\n",stat_res);
		duet_object[axis].motor_stat = MOT_INIT_ERROR;
		return RES_FAIL;
	}
	// send "switch on" command
	if(!duet_StateCommand(axis,MOT_CMD_SWITCH_ON))
	{
		DbgPrintf(DBG_DUET, "Init error: SWITCH_ON failed\r\n");
		duet_object[axis].motor_stat = MOT_INIT_ERROR;
		return RES_FAIL;
	}
	// wait for "switched on" status
	stat_res = duet_WaitForStatus(axis, MOT_SWITCHED_ON, 5/*1000*/);
	if (!stat_res)
	{
		DbgPrintf(DBG_DUET, "Init error: SWITCHED_ON failed, code%d\r\n",stat_res);
		duet_object[axis].motor_stat = MOT_INIT_ERROR;
		return RES_FAIL;
	}
#ifndef AGNOSTIC_AXIS	
	if(axis != AXIS_GANTRY && !(axis == AXIS_SLIDER1 && node==NODE_SLIDER1) || (axis == AXIS_SLIDER2 && node==NODE_SLIDER2))
#else	
	if(axis != AXIS_GANTRY && !(node == NODE_SLIDER1 || node==NODE_SLIDER2))
#endif
	{
		if(duet_OperationEnable(axis)==0)
			return RES_FAIL;
	}
	
	#ifdef SAME_AS_SYNCRO
		if(!duet_SetModeOperation(axis, OP_PROFILED_POSITION))
			duet_object[axis].motor_stat = MOT_SET_PARAM_ERROR;		
	#else
		// set operational mode as position profile
#ifndef AGNOSTIC_AXIS
		if( (axis == AXIS_SLIDER1 && node==NODE_SLIDER1) || (axis == AXIS_SLIDER2 && node==NODE_SLIDER2) )
#else
		if( (node==NODE_SLIDER1) || (node==NODE_SLIDER2) )
#endif
		{
			if(!duet_SetModeOperation(axis, OP_INTERPOLATE_POSITION))
				duet_object[axis].motor_stat = MOT_SET_PARAM_ERROR;
		}else{
			if(!duet_SetModeOperation(axis, OP_PROFILED_POSITION))
				duet_object[axis].motor_stat = MOT_SET_PARAM_ERROR;
		}
	#endif	
	//read rated current
	uint32_t tmp_uint;
	
	//Set Time between Sync (us)
	tmp_uint = MOT_SYNC_RX_TOUT*1000;
	stat_res = duet_SetParameter(axis, 0x1006, 0, &tmp_uint, 4);

	//Set Time Stamp Consuming
	tmp_uint = 0x80000100;	//Enable Time Stamp Consuming
	stat_res = duet_SetParameter(axis, 0x1012, 0, &tmp_uint, 4);
	
	// mapping TX PDO1 (configuration on 0x1800)
	// reset 0x1A00,0
	tmp_uint = 0;
	stat_res = duet_SetParameter(axis, 0x1A00, 0, &tmp_uint, 1);
	// map status word 0x6041,0
	tmp_uint = 0x60410010;		//0x6041 on a 0x1a00 sub1 of 2byte (16bit: 0x0010), on each SYNC (Elmo Controller)
	stat_res = duet_SetParameter(axis, 0x1A00, 1, &tmp_uint, 4);
	//Optimizing the TX PDO1 packet this is added
	tmp_uint = 0x60780010;
	stat_res = duet_SetParameter(axis, 0x1A00, 2, &tmp_uint, 4);
	// map actual position 0x6063,0
	tmp_uint = 0x60630020;
	stat_res = duet_SetParameter(axis, 0x1A00, 3, &tmp_uint, 4);
	// set PDO1 transmission on event
//	tmp_uint = 254;
	tmp_uint = 1;
	stat_res = duet_SetParameter(axis, 0x1800, 2, &tmp_uint, 1);
	// set PDO1 event timer
	//tmp_uint = 100;
	//stat_res = motor_SetParameter(axis, 0x1800, 5, &tmp_uint, 2);
	// activate PDO1 with 3 object
	tmp_uint = 3;
	stat_res = duet_SetParameter(axis, 0x1A00, 0, &tmp_uint, 1);	//TPDO number of Mapped obj = 3


	//Mora data to debug
	// mapping TX PDO2 (configuration on 0x1801)
	tmp_uint = 0;
	stat_res = duet_SetParameter(axis, 0x1A01, 0, &tmp_uint, 1);	//TPDO number of Mapped obj = 0
	// map status word 0x2F11,0
	tmp_uint = 0x2F110010;
	stat_res = duet_SetParameter(axis, 0x1A01, 1, &tmp_uint, 4);	// map 0x6041,0 obj: PVT head pointer
	// map status word 0x2F11,0
	tmp_uint = 0x2F120010;		
	stat_res = duet_SetParameter(axis, 0x1A01, 2, &tmp_uint, 4);	// map 0x6041,0 obj: PVT tail pointer
	// map status word 0x6040,0
	tmp_uint = 0x60400010;		//0x6041 on a 0x1a01 sub1 of 2byte (16bit: 0x0010), on each SYNC (Elmo Controller)
	stat_res = duet_SetParameter(axis, 0x1A01, 3, &tmp_uint, 4);
	// set PDO2 transmission on event
	tmp_uint = 1;
	stat_res = duet_SetParameter(axis, 0x1801, 2, &tmp_uint, 1);	//Syncronous trasmission type
	// activate PDO2 with 3 object
	tmp_uint = 3;
	stat_res = duet_SetParameter(axis, 0x1A01, 0, &tmp_uint, 1);	//TPDO number of Mapped obj = 2


// map RPDO 3 per l'oggetto 0x60C1 sub 1
	// reset 0x1602,0
	tmp_uint = 0;
	stat_res = duet_SetParameter(axis, 0x1602, 0, &tmp_uint, 1);
	// map 0x60C1,1
	tmp_uint = 0x60C10120;
	stat_res = duet_SetParameter(axis, 0x1602, 1, &tmp_uint, 4);
	// set RPDO3 asincrono
	tmp_uint = 255;
	stat_res = duet_SetParameter(axis, 0x1402, 2, &tmp_uint, 1);
	// activate RPDO3
	tmp_uint = 1;
	stat_res = duet_SetParameter(axis, 0x1602, 0, &tmp_uint, 1);


	//set motor position limits and default motion parameters
	switch(axis)
	{
		case AXIS_GANTRY:  // gantry
			duet_object[axis].min_position_limit = DUET_GANTRY_MIN_POS_LIMIT;
			duet_object[axis].max_position_limit = DUET_GANTRY_MAX_POS_LIMIT;
			duet_object[axis].home_speed = 53600;
			duet_object[axis].motion_stat = MOT_MOTION_COMPLETED;
			duet_object[axis].pos_acc = 107200;
			duet_object[axis].pos_dec = 107200;
			duet_object[axis].pos_speed = 53600;
			duet_object[axis].jog_speed = 53600;
		break;
		
//		case AXIS_SLIDER1:
		case AXIS_BED:  // carrello, OR AXIS_SLIDER1
			if(node==NODE_SLIDER1){
				duet_object[axis].min_position_limit = DUET_LIFTER_MIN_POS_LIMIT;
				duet_object[axis].max_position_limit = DUET_LIFTER_MAX_POS_LIMIT;
				duet_object[axis].home_speed = DUET_LIFTER_VEL;
				duet_object[axis].motion_stat = MOT_MOTION_COMPLETED;
				duet_object[axis].pos_acc = DUET_LIFTER_ACC;
				duet_object[axis].pos_dec = DUET_LIFTER_DEC;
				duet_object[axis].pos_speed = DUET_LIFTER_VEL;
				duet_object[axis].jog_speed = DUET_LIFTER_VEL;				
			}else{
				duet_object[axis].min_position_limit = DUET_BED_MIN_POS_LIMIT;
				duet_object[axis].max_position_limit = DUET_BED_MAX_POS_LIMIT;
				duet_object[axis].home_speed = 98304;
				duet_object[axis].motion_stat = MOT_MOTION_COMPLETED;
				duet_object[axis].pos_acc = 196608;
				duet_object[axis].pos_dec = 196608;
				duet_object[axis].pos_speed = 98304;
				duet_object[axis].jog_speed = 98304;
			}
		break;
		
		case AXIS_PANEL:  //flat panel
			duet_object[axis].min_position_limit = DUET_FLATP_MIN_POS_LIMIT;
			duet_object[axis].max_position_limit = DUET_FLATP_MAX_POS_LIMIT;
			duet_object[axis].home_speed = 163840;
			duet_object[axis].motion_stat = MOT_MOTION_COMPLETED;
			duet_object[axis].pos_acc = 546133;
			duet_object[axis].pos_dec = 546133;
			duet_object[axis].pos_speed = 163840;
			duet_object[axis].jog_speed = 163840;
		break;

		case AXIS_TILT:			
			duet_object[axis].min_position_limit = DUET_TILT_MIN_POS_LIMIT;
			duet_object[axis].max_position_limit = DUET_TILT_MAX_POS_LIMIT;
			duet_object[axis].home_speed = DUET_TILT_VEL;
			duet_object[axis].motion_stat = MOT_MOTION_COMPLETED;
			duet_object[axis].pos_acc = DUET_TILT_ACC;
			duet_object[axis].pos_dec = DUET_TILT_DEC;
			duet_object[axis].pos_speed = DUET_TILT_VEL;
			duet_object[axis].jog_speed = DUET_TILT_VEL;
		break;
		
		case AXIS_BACKLIFT:
			duet_object[axis].min_position_limit = DUET_BACKLIFT_MIN_POS_LIMIT;
			duet_object[axis].max_position_limit = DUET_BACKLIFT_MAX_POS_LIMIT;
			duet_object[axis].home_speed = DUET_BACKLIFT_VEL;
			duet_object[axis].motion_stat = MOT_MOTION_COMPLETED;
			duet_object[axis].pos_acc = DUET_BACKLIFT_ACC;
			duet_object[axis].pos_dec = DUET_BACKLIFT_DEC;
			duet_object[axis].pos_speed = DUET_BACKLIFT_VEL;
			duet_object[axis].jog_speed = DUET_BACKLIFT_VEL;
		break;
		
		case AXIS_FRONTLIFT_DX:
			duet_object[axis].min_position_limit = DUET_FRONTLIFTDX_MIN_POS_LIMIT;
			duet_object[axis].max_position_limit = DUET_FRONTLIFTDX_MAX_POS_LIMIT;
			duet_object[axis].home_speed = DUET_FRONTLIFTDX_VEL;
			duet_object[axis].motion_stat = MOT_MOTION_COMPLETED;
			duet_object[axis].pos_acc = DUET_FRONTLIFTDX_ACC;
			duet_object[axis].pos_dec = DUET_FRONTLIFTDX_DEC;
			duet_object[axis].pos_speed = DUET_FRONTLIFTDX_VEL;
			duet_object[axis].jog_speed = DUET_FRONTLIFTDX_VEL;
		break;
		
		case AXIS_FRONTLIFT_SX:
			duet_object[axis].min_position_limit = DUET_FRONTLIFTSX_MIN_POS_LIMIT;
			duet_object[axis].max_position_limit = DUET_FRONTLIFTSX_MAX_POS_LIMIT;
			duet_object[axis].home_speed = DUET_FRONTLIFTSX_VEL;
			duet_object[axis].motion_stat = MOT_MOTION_COMPLETED;
			duet_object[axis].pos_acc = DUET_FRONTLIFTSX_ACC;
			duet_object[axis].pos_dec = DUET_FRONTLIFTSX_DEC;
			duet_object[axis].pos_speed = DUET_FRONTLIFTSX_VEL;
			duet_object[axis].jog_speed = DUET_FRONTLIFTSX_VEL;
		break;
/*				
		case AXIS_SLIDER2:
			duet_object[axis].min_position_limit = DUET_LIFTER_MIN_POS_LIMIT;
			duet_object[axis].max_position_limit = DUET_LIFTER_MAX_POS_LIMIT;
			duet_object[axis].home_speed = DUET_LIFTER_VEL;
			duet_object[axis].motion_stat = MOT_MOTION_COMPLETED;
			duet_object[axis].pos_acc = DUET_LIFTER_ACC;
			duet_object[axis].pos_dec = DUET_LIFTER_DEC;
			duet_object[axis].pos_speed = DUET_LIFTER_VEL;
			duet_object[axis].jog_speed = DUET_LIFTER_VEL;
			break;
*/			
		default:
			//Default initialization
			duet_object[axis].min_position_limit = DUET_LIFTER_MIN_POS_LIMIT;
			duet_object[axis].max_position_limit = DUET_LIFTER_MAX_POS_LIMIT;
			duet_object[axis].home_speed = DUET_LIFTER_VEL;
			duet_object[axis].motion_stat = MOT_MOTION_COMPLETED;
			duet_object[axis].pos_acc = DUET_LIFTER_ACC;
			duet_object[axis].pos_dec = DUET_LIFTER_DEC;
			duet_object[axis].pos_speed = DUET_LIFTER_VEL;
			duet_object[axis].jog_speed = DUET_LIFTER_VEL;
		break;
	}
	
	#ifdef AGNOSTIC_AXIS
		if((node==NODE_SLIDER1)||(node==NODE_SLIDER2))
		{
			duet_object[axis].min_position_limit = DUET_LIFTER_MIN_POS_LIMIT;
			duet_object[axis].max_position_limit = DUET_LIFTER_MAX_POS_LIMIT;
			duet_object[axis].home_speed = DUET_LIFTER_VEL;
			duet_object[axis].motion_stat = MOT_MOTION_COMPLETED;
			duet_object[axis].pos_acc = DUET_LIFTER_ACC;
			duet_object[axis].pos_dec = DUET_LIFTER_DEC;
			duet_object[axis].pos_speed = DUET_LIFTER_VEL;
			duet_object[axis].jog_speed = DUET_LIFTER_VEL;
		}
	#endif
	
	// init dei parametri di movimento
	if(!duet_SetMaxPosition(axis, duet_object[axis].max_position_limit))
	{
		duet_object[axis].motor_stat = MOT_SET_PARAM_ERROR;
		DbgPrintf(DBG_DUET, "Init error: SetMaxPosition failed\r\n");
		return RES_FAIL;
	}

	if(!duet_SetMinPosition(axis, duet_object[axis].min_position_limit))
	{
		duet_object[axis].motor_stat = MOT_SET_PARAM_ERROR;
		DbgPrintf(DBG_DUET, "Init error: SetMinPosition failed\r\n");
		return RES_FAIL;
	}
	
	// set default acc.
	if(!duet_SetAcc(axis, duet_object[axis].pos_acc))
		duet_object[axis].motor_stat = MOT_SET_PARAM_ERROR;
	// set default dec.
	if(!duet_SetDec(axis, duet_object[axis].pos_dec))
		duet_object[axis].motor_stat = MOT_SET_PARAM_ERROR;

	duet_GetMotorActPos(axis, &duet_object[axis].actual_position);
	
	// read rated current
	duet_GetMotorRatedCurrent(axis);
	
	// set parametri di errore posizione
	// following error
	val = 1000;  // 1000 step
	duet_SetParameter(axis, 0x6065, 0, &val, 4);
	// following error time out
	val = 4;	// 1ms
	duet_SetParameter(axis, 0x6066, 0, &val, 2);
	
	// set timeout sulla trasmissione sincrona
	duet_object[axis].heardbeat_tout = MOT_SYNC_RX_TOUT;
	duet_object[axis].heartbeat_timer = timer_TimerSet(MOT_SYNC_RX_TOUT*6);			//Fix on Init Function to avoid heartbit error, from 2x to 4x
	
	// Initialize the callback structure for CAN interrupt
	//	assign node ID to the object
	duet_object[axis].node_id = node;
#ifdef	OLD_STYLE_COPROCESS
	icb.node_id = node;
	icb.intr_cback = duet_CbackManage;
	// and add it to the callback list.
	if(micro_AddCback(&icb)==0){
		DbgPrintf(DBG_DUET, "Init error: adding callback\r\n");
		return RES_FAIL;
	}
#else
	//Add canopen filter on motor
	uint32_t filetermask_id[]={PDO1TX_FCODE, PDO2TX_FCODE, EMCY_FCODE, NMTERR_FCODE};
	for(int index_mask=0;index_mask<sizeof(filetermask_id)/sizeof(uint32_t);index_mask++)
	{
		int filterid=get_freefilterId();
		FDCAN_FilterTypeDef sFilterConfig;
		sFilterConfig.IdType = FDCAN_STANDARD_ID;
		sFilterConfig.FilterIndex = filterid;
		sFilterConfig.FilterType = FDCAN_FILTER_MASK;
		sFilterConfig.FilterConfig = FDCAN_FILTER_TO_RXFIFO0;
//	sFilterConfig.FilterID1 = (uint32_t)node + (uint32_t)(ID_MASK & (PDO1TX_FCODE | PDO2TX_FCODE | PDO3TX_FCODE | EMCY_FCODE));
		sFilterConfig.FilterID1 = (uint32_t)ALL_ACCEPT_ID_MASK &
				((uint32_t)node + (uint32_t)(ID_MASK & filetermask_id[index_mask]));
		sFilterConfig.FilterID2 = ALL_ACCEPT_ID_MASK;
		if(filterid<0 || set_initfilterId(filterid, &sFilterConfig) == RES_FAIL){
			DbgPrintf(DBG_DUET, "Init error: no more available filterid\r\n");
			duet_object[axis].motor_stat = MOT_INIT_ERROR;
			return RES_FAIL;
		}
		icb.filter_id[index_mask]=filterid;
	}
	//Add callback to manage single message on CAN FIFO0 (HW)
	icb.node_id = node;
	icb.intr_cback = duet_Machine;
	// and add it to the callback list.
	if(micro_AddCback(&icb)==0){
		DbgPrintf(DBG_DUET, "Init error: adding callback\r\n");
		return RES_FAIL;
	}
#endif
	duet_object[axis].initialized = 1;
	duet_object[axis].motor_stat = MOT_READY;
#ifdef AGNOSTIC_AXIS	
	if(node==NODE_SLIDER1){
		motor_sync1.enable = 1;
		motor_sync1.axis = axis;
	}
	if(node==NODE_SLIDER2){
		motor_sync2.enable = 1;
		motor_sync2.axis = axis;
	}
#endif		
	// delay per leggere lo stato dei motori via can
	HAL_Delay(500);
		
	return RES_SUCCESS;
}

uint32_t duet_OperationEnable(int axis)
{
	uint32_t stat_res;
	stat_res = duet_WaitForStatus(axis, MOT_OPERATION_ENABLED, 10/*1000*2*/);
	if (stat_res!=0 && stat_res!=2)
	{
		return RES_SUCCESS;
	}

	// send "enable operation" command
	if(!duet_StateCommand(axis,MOT_CMD_ENABLE_OPERATION))
	{
		DbgPrintf(DBG_DUET, "ENABLE_OPERATION command failed\r\n");
		duet_object[axis].motor_stat = MOT_INIT_ERROR;
		return RES_FAIL;
	}
	// wait for "operation enabled" status
	stat_res = duet_WaitForStatus(axis, MOT_OPERATION_ENABLED, 250);
	if (stat_res==0)
	{
		DbgPrintf(DBG_DUET, "OPERATION_ENABLED failed, code %d\r\n", stat_res);
		duet_object[axis].motor_stat = MOT_INIT_ERROR;
		return RES_FAIL;
	}
	
	return RES_SUCCESS;
}

uint32_t duet_OperationDisable(int axis)
{
	uint32_t stat_res;
	stat_res = duet_WaitForStatus(axis, MOT_SWITCHED_ON, 10/*1000*2*/);
	if (stat_res!=0 && stat_res!=2)
	{
		return RES_SUCCESS;
	}

	// send "switch on" command
	if(!duet_StateCommand(axis, MOT_CMD_SWITCH_ON))
	{
		DbgPrintf(DBG_DUET, "Init error: SWITCH_ON command failed\r\n");
		duet_object[axis].motor_stat = MOT_INIT_ERROR;
		return RES_FAIL;
	}
	// wait for "switched on" status
	stat_res = duet_WaitForStatus(axis, MOT_SWITCHED_ON, 250);
	if (!stat_res)
	{
		DbgPrintf(DBG_DUET, "Init error: SWITCHED_ON failed, code %d\r\n",stat_res);
		duet_object[axis].motor_stat = MOT_INIT_ERROR;
		return RES_FAIL;
	}
	
	return RES_SUCCESS;
}

/**
* \fn uint32_t motor_SetMaxPosition(int axis, int32_t max_pos)
* \brief Set the max position limit for the axis
*
* This function perform the commands to set max position limit in step 
*
* \param int axis Axis number (index in the motor_object array)
* \param int32_t max_pos max position.
* \return  1 = success, 0 = error.
*
*/
uint32_t duet_SetMaxPosition(int axis, int32_t max_pos)
{
	uint32_t res = duet_SetParameter(axis, 0x607D, 2, (uint32_t *)&max_pos, sizeof(uint32_t));
	if(res){
		duet_object[axis].max_position_limit = max_pos;
	}
	return res;
}

/**
* \fn uint32_t motor_SetMinPosition(int axis, int32_t min_pos)
* \brief Set the min position limit for the axis
*
* This function perform the commands to set min position limit in step
*
* \param int axis Axis number (index in the motor_object array)
* \param int32_t min_pos min position.
* \return  1 = success, 0 = error.
*
*/
uint32_t duet_SetMinPosition(int axis, int32_t min_pos)
{
	uint32_t res = duet_SetParameter(axis, 0x607D, 1, (uint32_t *)&min_pos, sizeof(uint32_t));
	if(res){
		duet_object[axis].min_position_limit = min_pos;
	}
	return res;
}

#ifdef	OLD_STYLE_COPROCESS
/**
 * \fn void motor_CbackManage(void *param)
 * \brief Callback function called from CAN peripheral interrupt.
 *        Add an element to the message queue of the motor object.
 *  
 * \param 'void *param' pointer to can_mb_conf_t variable 
 * 
 */
void duet_CbackManage(void *param, uint8_t *data)
{
	FDCAN_RxHeaderTypeDef * rx_header = (FDCAN_RxHeaderTypeDef *)param;

	int node_id = rx_header->Identifier & CO_NODEMASK;

#warning "check it ibn microcallback"
	/*
		int f_code = p_mbx->ul_id >> CAN_MID_MIDvA_Pos;
		if(f_code == 0)
			f_code = p_mbx->ul_fid & 0x00000780;
	*/
	int f_code = rx_header->Identifier & CO_CODEMASK;

	// search axis number
	int i;
	for(i=0;i<MOTOR_MAXOBJ;i++)
	{
		if(duet_object[i].node_id == node_id)
			break;
	}
	if(i >= MOTOR_MAXOBJ) // error, axis number not found
	{
		DbgPrintf(DBG_DUET, "Msg from unknown node\r\n");
		return;
	}

	//Condizione di coda piena
	if((duet_object[i].rx_queue_write_p+1)%MOTOR_MSG_QUEUE_SIZE==duet_object[i].rx_queue_read_p){
		DbgPrintf(FORCE_DEBUG, "Fullnode%d t=%d\r\n",node_id,timer_TimerSet(0));
		return;
	}
	
	uint32_t datal = (uint32_t)*((uint32_t *)&data[0]);
	uint32_t datah = (uint32_t)*((uint32_t *)&data[4]);

//	duet_object[i].rx_queue[duet_object[i].rx_queue_write_p].datah = p_mbx->ul_datah;
//	duet_object[i].rx_queue[duet_object[i].rx_queue_write_p].datah = ( ((uint32_t)data[7])<<24 ) + ( ((uint32_t)data[6])<<16 ) + ( ((uint32_t)data[5])<<8 ) + ( ((uint32_t)data[4])<<0 );
	duet_object[i].rx_queue[duet_object[i].rx_queue_write_p].datah = datah;
//	duet_object[i].rx_queue[duet_object[i].rx_queue_write_p].datal = p_mbx->ul_datal;
//	duet_object[i].rx_queue[duet_object[i].rx_queue_write_p].datal = ( ((uint32_t)data[3])<<24 ) + ( ((uint32_t)data[2])<<16 ) + ( ((uint32_t)data[1])<<8 ) + ( ((uint32_t)data[0])<<0 );
	duet_object[i].rx_queue[duet_object[i].rx_queue_write_p].datal = datal;
	duet_object[i].rx_queue[duet_object[i].rx_queue_write_p].f_code = f_code;
	duet_object[i].rx_queue[duet_object[i].rx_queue_write_p].idx = rx_header->RxTimestamp;
	duet_object[i].rx_queue[duet_object[i].rx_queue_write_p].timestamp = timer_TimerSet(0);
	
	duet_object[i].rx_queue_write_p++;
	if(duet_object[i].rx_queue_write_p >= MOTOR_MSG_QUEUE_SIZE)
		duet_object[i].rx_queue_write_p = 0;
	
}
#endif

/**
* @fn 		uint32_t motor_StateCommand(int axis, uint32_t cmd)
* @brief 	Sends a change state to the node.
*
* 			Sends PDO1 message writing the controlword at index 0x6040, subindex 0
* 			of the node to command a change state.
*
* @param 	'uint32_t cmd' Command
* @param 	'int axis' Axis number (index in the motor_object array)
* @return 	1 if success, 0 if not.
*
*/
uint32_t duet_StateCommand(int axis, uint32_t cmd)
{
	uint32_t res = RES_SUCCESS;
//	uint8_t dt_out[8] = {0,0,0,0,0,0,0,0};
//	uint32_t * p_cw = (uint32_t *)dt_out;
	uint32_t p_cw = 0;
	uint8_t *dt_out = (uint8_t *)(&p_cw);
	
	switch(cmd)
	{
		case MOT_CMD_SHUTDOWN :
			p_cw = MOTOR_ENV_BIT | MOTOR_QSTOP_BIT;
		break;
		case MOT_CMD_SWITCH_ON :
			p_cw = MOTOR_SWON_BIT | MOTOR_ENV_BIT | MOTOR_QSTOP_BIT;
		break;
		case MOT_CMD_VOLT_DISABLE :
		break;
		case MOT_CMD_QUICK_STOP :
			p_cw = MOTOR_ENV_BIT;
		break;
		case MOT_CMD_DISABLE_OPERATION :
		break;
		case MOT_CMD_ENABLE_OPERATION :
			p_cw = MOTOR_SWON_BIT | MOTOR_ENV_BIT | MOTOR_QSTOP_BIT | MOTOR_ENOP_BIT;
		break;
		case MOT_CMD_FAULT_RESET :
		break;
		case MOT_CMD_STOP :
			p_cw = MOTOR_HALT_BIT | MOTOR_SWON_BIT | MOTOR_ENV_BIT | MOTOR_QSTOP_BIT;
		break;
	}

	co_pdo_Send(0, duet_object[axis].node_id, dt_out, 2);
	
	return res;
}

/**
 * \fn uint32_t motor_ReadStatusWord(int axis, uint32_t *sw)
 * \brief Read the statusword at index 0x6041, subindex 0
 *
 * Sends SDO upload message to read the statusword
 *  
 * \param uint32_t *sw Variable pointer to store the readed statusword
 * \param int axis Axis number (index in the motor_object array)  
 * \return 1 if success, 0 if error.
 * 
 */
uint32_t duet_ReadStatusWord(int axis, volatile uint32_t *sw)
{
	co_msg_t msg;
	uint32_t res = 0;
	uint8_t dt_out[8] = {0,0,0,0,0,0,0,0};
	uint8_t dt_in[8] = {0,0,0,0,0,0,0,0};

	memset(&msg, 0, sizeof(co_msg_t));	
	msg.node = duet_object[axis].node_id;
	msg.index = 0x6041;
	msg.subindex = 0;
	msg.r_w = 'r';
	msg.data_in = dt_in;
	msg.data_out = dt_out;
	
	res = co_sdo_Send(&msg);
	if(!res)
		*sw = 0;
	else
		memcpy((void *)sw, &msg.data_in[4], sizeof(uint32_t));
	
	return res;

}

/**
 * \fn uint32_t motor_ReadControlWord(int axis, uint32_t * cw)
 * \brief Read the controlword at index 0x6040, subindex 0
 *
 * Sends SDO upload message to read the controlword
 *  
 * \param uint32_t *cw Variable pointer to store the readed controlword
 * \param int axis Axis number (index in the motor_object array)
 * \return 1 if success, 0 if error.
 * 
 */
uint32_t duet_ReadControlWord(int axis, uint32_t * cw)
{
	co_msg_t msg;
	uint32_t res = 0;
	uint8_t dt_out[8] = {0,0,0,0,0,0,0,0};
	uint8_t dt_in[8] = {0,0,0,0,0,0,0,0};

	memset(&msg, 0, sizeof(co_msg_t));
	msg.node = duet_object[axis].node_id;
	msg.index = 0x6040;
	msg.subindex = 0;
	msg.r_w = 'r';
	
	msg.data_in = dt_in;
	msg.data_out = dt_out;
	
	res = co_sdo_Send(&msg);
	if(!res)
		*cw = 0;
	else
		memcpy(cw, &msg.data_in[4], sizeof(uint32_t));

	return res;
}

/**
 * \fn uint32_t motor_WriteControlWord(int axis, uint32_t cw)
 * \brief Write the controlword at index 0x6040, subindex 0
 *
 * Sends SDO download message to write the controlword
 *  
 * \param uint32_t cw New value for the controlword
 * \param int axis Axis number (index in the motor_object array)
 * \return 1 if success, 0 if error.
 * 
 */
uint32_t duet_WriteControlWord(int axis, uint32_t cw)
{
	co_msg_t msg;
	uint32_t res = 0;
	uint8_t dt_out[8] = {0,0,0,0,0,0,0,0};
	uint8_t dt_in[8] = {0,0,0,0,0,0,0,0};
	
	memset(&msg, 0, sizeof(co_msg_t));
	msg.node = duet_object[axis].node_id;
	msg.index = 0x6040;
	msg.subindex = 0;
	msg.r_w = 'w';
	
	memcpy(&dt_out[4], &cw, sizeof(uint32_t));
	msg.data_in = dt_in;
	msg.data_out = dt_out;
	
	res = co_sdo_Send(&msg);
	
	return res;
}

/**
 * \fn uint32_t motor_FaultReset(int axis)
 * \brief Recover a fault resetting the fault bit in the controlword
 *
 * Read and rewrite controlword with fault reset bit modified.
 *  
 * \param int axis Axis number (index in the motor_object array)
 * \return 1 if success, 0 if error.
 * 
 */
uint32_t duet_FaultReset(int axis)
{
	uint32_t cw;
	
	if(duet_ReadControlWord(axis, &cw))
		return duet_WriteControlWord(axis, cw | MOTOR_FRES_BIT);
	else
		return RES_FAIL;
}

/**
* \fn uint32_t motor_StopHere(int axis)
* \brief Command the motor stop setting the stop bit in the controlword
*
* Read and rewrite controlword with stop bit modified.
*
* \param int axis Axis number (index in the motor_object array)
* \return 1 if success, 0 if error.
*
*/
uint32_t duet_StopHere(int axis)
{
	uint32_t cw;
	uint32_t result = RES_SUCCESS;
	
	if(duet_ReadControlWord(axis, &cw)){
		result = duet_WriteControlWord(axis, cw | MOTOR_HALT_BIT);
		if(result==RES_FAIL){
			DbgPrintf(DBG_DUET, "StopHere - Status write FAIL\r\n");
		}
	}else 
	{
		DbgPrintf(DBG_DUET, "StopHere- Status read FAIL\r\n");
		result = RES_FAIL;
	}
		
	return result;
}

/**
* \fn static uint32_t motor_WaitForStatus(int axis, uint32_t status, uint32_t tout_ms)
* \brief Check the statusword to verify a new change state
*
* Check the statusword to verify a new change state with timeout.
*
* \param uint32_t status The status to verify
* \param uint32_t tout_ms Wait timeout
* \param int axis Axis number (index in the motor_object array)
* \return  1 = ok, 0 = timeout, 2 = motor fault
*
*/
#define	WAIT_TIME_SLICE_MS	5
static uint32_t duet_WaitForStatus(int axis, uint32_t status, uint32_t tout_ms)
{
	//uint32_t delay_timer = timer_TimerSet(5);
	uint32_t tout_timer = tout_ms / WAIT_TIME_SLICE_MS;
	uint32_t res = 0;

	//tout_timer = timer_TimerSet(tout_ms);
	while(tout_timer != 0)	
	{
		//HAL_Delay(WAIT_TIME_SLICE_MS);
		HAL_Delay(WAIT_TIME_SLICE_MS);
		// Read the status word
		uint32_t sw;
		duet_ReadStatusWord(axis, &sw /*&duet_object[axis].statusword*/);
		if(((/*duet_object[axis].statusword*/ sw & MOT_STATUS_MASK) ^ status) == 0)
		{
			res = WAIT_TIME_SLICE_MS+((tout_ms / WAIT_TIME_SLICE_MS)-tout_timer)*WAIT_TIME_SLICE_MS;//1;
			break;
		}
		else if(/*duet_object[axis].statusword*/ sw & MOT_FAULT)
		{
			res = 2;
			break;
		}
		tout_timer--;
	};
	
	return res;
}

/**
* \fn uint32_t motor_SetModeOperation(int axis, uint32_t mode)
* \brief Set the operational mode of the motor.
*
* Set the operational mode of the motor:
* profiled position,
* profiled velocity,
* torque profile,
* homing,
* interpolated position.
* At power on the motor drive set the operational mode as "no mode".
*
* \param int8_t mode New mode of operation.
* \param int axis Axis number (index in the motor_object array)
* \return  1 = mode set successfully, 0 = mode set error.
*
*/
uint32_t duet_SetModeOperation(int axis, uint32_t mode)
{
	return duet_SetParameter(axis, 0x6060, 0, &mode, sizeof(uint8_t));
}

/**
* \fn uint32_t duet_GetModeOperation(int axis, uint32_t mode)
* \brief Set the operational mode of the motor.
*
* Set the operational mode of the motor:
* profiled position,
* profiled velocity,
* torque profile,
* homing,
* interpolated position.
* At power on the motor drive set the operational mode as "no mode".
*
* \param int8_t mode New mode of operation.
* \param int axis Axis number (index in the motor_object array)
* \return  1 = mode set successfully, 0 = mode set error.
*
*/
uint32_t duet_GetModeOperation(int axis, uint32_t *mode)
{
	return duet_GetParameter(axis, 0x6061, 0, mode, sizeof(uint8_t));
}


/**
* \fn uint32_t motor_SetHome(int axis)
* \brief Set home position for the motor
*
* Before any operation, the motor drive must perform the homing 
* procedure. This function sends the sequence of commands to
* set the home position using the homing method n. 35:
* "homing on the current position".
*
* \param int axis Axis number (index in the motor_object array)
* \return  1 = home set success, 0 = home set error.
*
*/
uint32_t duet_SetHome(int axis)
{
	uint32_t res = RES_SUCCESS;
	
	duet_object[axis].home_position = duet_object[axis].actual_position;
	
	return res;
}

/** @name homing functions 
 *  Functions needed to set the homing parameters of the motor
 */
///@{
uint32_t duet_SetHomingMethod(int axis, uint32_t h_method)
{
	return duet_SetParameter(axis, 0x6098, 0, &h_method, sizeof(uint8_t));
}

uint32_t duet_SetHomingOffset(int axis, int32_t h_offset)
{
	return duet_SetParameter(axis, 0x607C, 0, (uint32_t *)&h_offset, sizeof(int32_t));
}

uint32_t duet_SetHomingSpeed(int axis, uint32_t h_speed)
{
	duet_object[axis].home_speed = h_speed;
	return duet_SetParameter(axis, 0x6099, 2, (uint32_t *)&h_speed, sizeof(int32_t));
}

uint32_t duet_SetHomingAcc(int axis, uint32_t h_acc)
{
	return duet_SetParameter(axis, 0x609A, 0, (uint32_t *)&h_acc, sizeof(int32_t));
}
///@}

/**
* \fn uint32_t motor_SetPosition(int axis, uint32_t new_position)
* \brief Set a new position for the motor and start it
*
* This function perform the commands to set a new absolute position,
* start the motor and wait for position reached, monitoring the 
* statusword.
*
* \param int axis Axis number (index in the motor_object array)
* \param uint32_t new_position New absolute position
* \return  1 = success, 0 = error.
*
*/
uint32_t duet_SetPosition(int axis, int32_t new_position)
{
	uint32_t result = RES_FAIL;
	
	// lifter si muovono insieme in modalit� interpolata
	#ifndef AGNOSTIC_AXIS	
	if( (axis == AXIS_SLIDER1 && duet_object[axis].node_id==NODE_SLIDER1) || (axis == AXIS_SLIDER2 && duet_object[axis].node_id==NODE_SLIDER2) )
	#else
	if( (axis == AXIS_SLIDER1) || (axis == AXIS_SLIDER2) )
	#endif
	{
		//Per evitare set position quando i motori interpolati sono in moto
		if(duet_object[AXIS_SLIDER1].motion_stat!=MOT_MOTION_COMPLETED || duet_object[AXIS_SLIDER2].motion_stat!=MOT_MOTION_COMPLETED)
			return RES_FAIL;	
		#ifdef SAME_AS_SYNCRO
			if(service_state)
			{
				if(!duet_SetParameter(axis, 0x6081, 0, (uint32_t *)&duet_object[axis].pos_speed, sizeof(uint32_t)))
					return RES_FAIL;
			}
			
			return duet_GoAbsolute(axis, new_position);
		#else
			if( (slider_enabled) && (!service_state) )
				result = duet_SetInterpolatePositionLinear(AXIS_SLIDER1, AXIS_SLIDER2, new_position);	//Abilito il set position contemporaneo
			//se ho inizializzato entrambi i due motori
			//sincroni e se non sono in service mode
			else if(service_state)
			{
				duet_OperationEnable(axis);
				//Modalit� nella quale posso muovere in maniera indipendente i motori che altrimenti
				//sarebbero sincroni (questo per facilitare eventuali manutenzioni)
				if(!duet_SetModeOperation(axis, OP_PROFILED_POSITION))
				{
					duet_object[axis].motor_stat = MOT_SET_PARAM_ERROR;
					return RES_FAIL;
				}
				if(!duet_SetParameter(axis, 0x6081, 0, (uint32_t *)&duet_object[axis].pos_speed, sizeof(uint32_t)))
					return RES_FAIL;

				result = duet_GoAbsolute(axis, new_position);
			}
			else
				result = RES_FAIL;	
		#endif
			
	}else{
		if(!duet_SetParameter(axis, 0x6081, 0, (uint32_t *)&duet_object[axis].pos_speed, sizeof(uint32_t)))
			return RES_FAIL;
		
		 result = duet_GoAbsolute(axis, new_position);
	}
	return result;
}

/**
* \fn static uint32_t motor_GoAbsolute(int axis, uint32_t new_position)
* \brief Set a new position for the motor and commands to start moving
*
* This function perform the commands to set a new absolute position,
* and start the motor.
*
* \param int axis Axis number (index in the motor_object array)
* \param uint32_t new_position New absolute position
* \return  1 = success, 0 = error.
*
*/
static uint32_t duet_GoAbsolute(int axis, int32_t new_position)
{
	uint32_t res = RES_SUCCESS;

	 if(!digio_GetPowerStatus())
		return RES_FAIL;

	if(axis == AXIS_GANTRY)  // il gantry da fermo non sta in coppia
	{
		if(duet_OperationEnable(axis) == 0)
			return RES_FAIL;
	}
	// gestione elettrofreno per motore lineare
	BrakeOff(axis);

	duet_SetParameter(axis, 0x607A, 0, (uint32_t*)&new_position, sizeof(uint32_t));
	
	duet_WriteControlWord(axis, 0x3f);
	
	duet_WriteControlWord(axis, 0x0f);

	return res;	
	
}

/**
* \fn static uint32_t motor_SetAcc(int axis, uint32_t acc)
* \brief Set the acceleration and deceleration rate for the motor.
*
* This function perform the commands to set motor acceleration and 
* deceleration rate, in step/s^2
*
* \param int axis Axis number (index in the motor_object array)
* \param uint32_t acc Acceleration rate.
* \return  1 = success, 0 = error.
*
*/
uint32_t duet_SetAcc(int axis, uint32_t acc)
{
	// per i lifter scrivo subito nel motore anche se non serve per la modalit� interpolata
	#ifndef AGNOSTIC_AXIS
	if( ((axis == AXIS_SLIDER1 && duet_object[axis].node_id==NODE_SLIDER1) || (axis == AXIS_SLIDER2 && duet_object[axis].node_id==NODE_SLIDER2)) && !service_state
		&& slider_enabled)
	#else
	if( ((axis == AXIS_SLIDER1) || (axis == AXIS_SLIDER2)) && !service_state
		&& slider_enabled)
	#endif
	{
		//Per evitare duet_SetAcc quando i motori interpolati sono in moto
		if(duet_object[AXIS_SLIDER1].motion_stat!=MOT_MOTION_COMPLETED || duet_object[AXIS_SLIDER2].motion_stat!=MOT_MOTION_COMPLETED)
			return RES_FAIL;
		duet_object[AXIS_SLIDER1].pos_acc=acc;
		if(!duet_SetParameter(AXIS_SLIDER1, 0x6083, 0, (uint32_t *)&duet_object[AXIS_SLIDER1].pos_acc, sizeof(uint32_t)))
			return RES_FAIL;
		duet_object[AXIS_SLIDER2].pos_acc=acc;
		if(!duet_SetParameter(AXIS_SLIDER2, 0x6083, 0, (uint32_t *)&duet_object[AXIS_SLIDER2].pos_acc, sizeof(uint32_t)))
			return RES_FAIL;
	}else{	
		duet_object[axis].pos_acc=acc;
		return duet_SetParameter(axis, 0x6083, 0, &acc, sizeof(uint32_t));
	}
	return RES_SUCCESS;	
}

/**
* \fn uint32_t motor_SetDec(int axis, uint32_t dec)
* \brief Set the deceleration rate for the motor
*
* This function perform the commands to set motor deceleration rate, in step/s^2
*
* \param int axis Axis number (index in the motor_object array)
* \param uint32_t dec Deceleration rate.
* \return  1 = success, 0 = error.
*
*/
uint32_t duet_SetDec(int axis, uint32_t dec)
{
	// per i lifter scrivo subito nel motore anche se non serve per la modalit� interpolata
	#ifndef AGNOSTIC_AXIS
	if( ((axis == AXIS_SLIDER1 && duet_object[axis].node_id==NODE_SLIDER1) || (axis == AXIS_SLIDER2 && duet_object[axis].node_id==NODE_SLIDER2)) && !service_state 
		&& slider_enabled)
	#else
	if( ((axis == AXIS_SLIDER1) || (axis == AXIS_SLIDER2)) && !service_state 
		&& slider_enabled)
	#endif
	{
		//Per evitare duet_SetDec quando i motori interpolati sono in moto
		if(duet_object[AXIS_SLIDER1].motion_stat!=MOT_MOTION_COMPLETED || duet_object[AXIS_SLIDER2].motion_stat!=MOT_MOTION_COMPLETED)
			return RES_FAIL;
		duet_object[AXIS_SLIDER1].pos_dec=dec;
		if(!duet_SetParameter(AXIS_SLIDER1, 0x6084, 0, (uint32_t *)&duet_object[AXIS_SLIDER1].pos_dec, sizeof(uint32_t)))
			return RES_FAIL;
		duet_object[AXIS_SLIDER2].pos_dec=dec;
		if(!duet_SetParameter(AXIS_SLIDER2, 0x6084, 0, (uint32_t *)&duet_object[AXIS_SLIDER2].pos_dec, sizeof(uint32_t)))
			return RES_FAIL;
	}else{	
		duet_object[axis].pos_dec=dec;
		return duet_SetParameter(axis, 0x6084, 0, &dec, sizeof(uint32_t));
	}
	return RES_SUCCESS;
}


/**
* \fn uint32_t motor_GetMotionStatus(int axis)
* \brief Return the motion status: moving-complete/moving
*
* \param int axis Axis number (index in the motor_object array)
* \return  motion status.
*
*/
uint32_t duet_GetMotionStatus(int axis)
{
	return duet_object[axis].motion_stat;
}

/**
* \fn uint32_t motor_GetMotorStatus(int axis)
* \brief Return the motor status.
*
* \param int axis Axis number (index in the motor_object array)
* \return  motor status.
*
*/
uint32_t duet_GetMotorStatus(int axis)
{
	if(axis>=0)	//To avoid request from sync motor not initialized
		return duet_object[axis].motor_stat;
	else
		return MOT_NOT_INIT;
}

bool duet_SetMotorStatus(int axis, int new_stat)
{
	if(axis>=0){	//To avoid request from sync motor not initialized
		duet_object[axis].motor_stat=new_stat;
		return duet_object[axis].motor_stat==new_stat;
	}else{
		return RES_FAIL;
	}
}

/**
* \fn uint32_t motor_SetJogSpeed(int axis, uint32_t j_speed)
* \brief Set the motor speed for jog mode in the motor object structure
*
* \param int axis Axis number (index in the motor_object array)
* \param uint32_t j_speed Jog mode speed
* \return  1 = success, 0 = error.
*
*/
uint32_t duet_SetJogSpeed(int axis, uint32_t j_speed)
{
	duet_object[axis].jog_speed = j_speed;
	
	return RES_SUCCESS;
}

/**
* \fn uint32_t motor_GoJogFwd(int axis, uint32_t n_step)
* \brief Set the motor in jog mode and start moving in forward direction. 
*
* \param int axis Axis number (index in the motor_object array)
* \return  1 = success, 0 = error.
*
*/
#define DEGREE4STEP				10720		//1 degrees on vimago for step/degree
#define OFFSET_90				((duet_object[axis].max_position_limit-5*(OFFSET_2DEGREE/2))/4)	//3900260
#define OFFSET_180				((duet_object[axis].max_position_limit-5*(OFFSET_2DEGREE/2))/2)
#define DEGREE2					duet_object[axis].min_position_limit+ OFFSET_2DEGREE
#define DEGREE92				(DEGREE4STEP*92) //duet_object[axis].min_position_limit+OFFSET_90+OFFSET_2DEGREE
#define DEGREE182				(DEGREE4STEP*182) //duet_object[axis].min_position_limit+OFFSET_180+OFFSET_2DEGREE
#define DEGREE272				(DEGREE4STEP*272) //DEGREE182+OFFSET_90
#define DEGREE362				duet_object[axis].max_position_limit - 10 - OFFSET_2DEGREE	//DEGREE182+duet_object[axis].max_position_limit/2

uint32_t duet_GoJogFwd(int axis)
{
	// lifter si muovono insieme in modalit� interpolata
	#ifndef AGNOSTIC_AXIS
	if( (axis == AXIS_SLIDER1 && duet_object[axis].node_id==NODE_SLIDER1) || (axis == AXIS_SLIDER2 && duet_object[axis].node_id==NODE_SLIDER2) )
	#else
	if( (axis == AXIS_SLIDER1) || (axis == AXIS_SLIDER2) )
	#endif
	{
		return RES_FAIL;
	}
	
	uint32_t res;
	int forward_pos=duet_object[axis].actual_position+duet_object[axis].max_position_limit/4;
	int checkpos=duet_object[axis].actual_position+2000;
	
	duet_SetParameter(axis, 0x6081, 0, (uint32_t *)&duet_object[axis].jog_speed, sizeof(uint32_t));

	if(checkpos<=DEGREE2){	//Minore di 2
		forward_pos=DEGREE2;	//Andare a 2
	}else if(checkpos<=DEGREE92){	//Minore di 90
		forward_pos=DEGREE92;	//Andare a 92 gradi
	}else if(checkpos<=DEGREE182){ //Tra 90 e 182
		forward_pos=DEGREE182;	//Andare a 182 gradi
	}else if(checkpos<=DEGREE272){ //Tra 182 e 272
		forward_pos=DEGREE272;	//Andare a 272 gradi
	}else { //Tra 272 e 362
		forward_pos=DEGREE362;	//Andare a 362 gradi
	}

	if(forward_pos>=duet_object[axis].max_position_limit - 10 - OFFSET_2DEGREE){
		forward_pos=duet_object[axis].max_position_limit - 10 - OFFSET_2DEGREE;
	}
	res = duet_GoAbsolute(axis, forward_pos);

	return res;
}

/**
* \fn uint32_t motor_GoJogRev(int axis, uint32_t n_step)
* \brief Set the motor in jog mode and start moving in reverse direction.
*
* \param int axis Axis number (index in the motor_object array)
* \return  1 = success, 0 = error.
*
*/
uint32_t duet_GoJogRev(int axis)
{
	// lifter si muovono insieme in modalit� interpolata
	#ifndef AGNOSTIC_AXIS
	if( (axis == AXIS_SLIDER1 && duet_object[axis].node_id==NODE_SLIDER1) || (axis == AXIS_SLIDER2 && duet_object[axis].node_id==NODE_SLIDER2) )
	#else
	if( (axis == AXIS_SLIDER1) || (axis == AXIS_SLIDER2) )
	#endif
	{
		return RES_FAIL;
	}
	
	uint32_t res;
	int reverse_pos=duet_object[axis].actual_position-duet_object[axis].max_position_limit/4;
	int checkpos=duet_object[axis].actual_position-2000;

	duet_SetParameter(axis, 0x6081, 0, (uint32_t *)&duet_object[axis].jog_speed, sizeof(uint32_t));

	if(checkpos<=DEGREE92){	//Minore di 92
		reverse_pos=DEGREE2;	//Andare a 2 gradi
	}else if(checkpos<=DEGREE182){ //Tra 90 e 182
		reverse_pos=DEGREE92;	//Andare a 92 gradi
	}else if(checkpos<=DEGREE272){ //Tra 182 e 272
		reverse_pos=DEGREE182;	//Andare a 182 gradi
	}else if(checkpos<=DEGREE362){ //Tra 272 e 362
		reverse_pos=DEGREE272;	//Andare a 272 gradi
	}else
		reverse_pos=DEGREE362;	//Andare a 362 gradi


	if(reverse_pos<=duet_object[axis].min_position_limit+ OFFSET_2DEGREE + 10){
		reverse_pos=duet_object[axis].min_position_limit+ OFFSET_2DEGREE + 10;
	}
	
	res=duet_GoAbsolute(axis, reverse_pos);
	

	return res;
}

uint32_t duet_ReadObj(int32_t axis, uint32_t index, uint32_t subindex, uint32_t *value)
{
	return duet_GetParameter(axis, index, subindex, value, 4);
	
}

uint32_t duet_WriteObj(int32_t axis, uint32_t index, uint32_t subindex, uint32_t value)
{
	return duet_SetParameter(axis, index, subindex, &value, 4);
}

/**
* \fn static uint32_t motor_SetParameter(int axis, uint32_t idx, uint8_t subidx, uint32_t * val, uint32_t size)
* \brief Upload an object parameter value to the server node
*
* Write (upload) a new value in the server object parameter and verify the operation
* reading back the value.
*
* \param uint32_t idx Object dictionary index
* \param uint8_t subidx Object subindex
* \param uint32_t * val Pointer to the variable containing the new value
* \param uint32_t size Size in byte of the parameter.
* \return  1 = parameter set successfully, 0 = parameter set error.
*
*/
uint32_t duet_SetParameter(int axis, uint32_t idx, uint8_t subidx, uint32_t * val, uint32_t size)
{
	co_msg_t msg;
	//uint32_t res = 0;
	uint8_t dt_out[8] = {0,0,0,0,0,0,0,0};
	uint8_t dt_in[8] = {0,0,0,0,0,0,0,0};
	
	// set parameter
	memset(&msg, 0, sizeof(co_msg_t));
	msg.node = duet_object[axis].node_id;
	msg.index = idx;
	msg.subindex = subidx;
	msg.r_w = 'w';
	memcpy(&dt_out[4], val, size);

	msg.data_in = dt_in;
	msg.data_out = dt_out;
	if(!co_sdo_Send(&msg))
		return RES_FAIL;
	
	// verify
	memset(&msg, 0, sizeof(co_msg_t));
	msg.node = duet_object[axis].node_id;
	msg.index = idx;
	msg.subindex = subidx;
	msg.r_w = 'r';

	msg.data_in = dt_in;
	msg.data_out = dt_out;
	if(!co_sdo_Send(&msg))
		return RES_FAIL;
	
	uint32_t tmpint;
	memcpy(&tmpint, &dt_in[4], sizeof(uint32_t));
	if(tmpint == *val)
		return RES_SUCCESS;
	else
		return RES_FAIL;
}

/**
* \fn static uint32_t motor_GetParameter(int axis, uint32_t idx, uint8_t subidx, uint32_t * val, uint32_t size)
* \brief Download an object parameter value from the server node
*
* read (download) an object dictionary entry value from the server node.
*
* \param uint32_t idx Object dictionary index
* \param uint8_t subidx Object subindex
* \param uint32_t * val Pointer to the variable containing the new value
* \param uint32_t size Size in byte of the parameter.
* \return  1 = parameter set successfully, 0 = parameter set error.
*
*/
static uint32_t duet_GetParameter(int axis, uint32_t idx, uint8_t subidx, uint32_t * val, uint32_t size)
{
	volatile co_msg_t msg;
	uint8_t dt_out[8] = {0,0,0,0,0,0,0,0};
	uint8_t dt_in[8] = {0,0,0,0,0,0,0,0};
	
	// read parameter
	memset((void *)&msg, 0, sizeof(co_msg_t));
	msg.node = duet_object[axis].node_id;
	msg.index = idx;
	msg.subindex = subidx;
	msg.r_w = 'r';

	msg.data_in = dt_in;
	msg.data_out = dt_out;
	if(!co_sdo_Send((co_msg_t *)&msg))
		return RES_FAIL;
	
	memcpy(val, &dt_in[4], sizeof(uint32_t));

	return RES_SUCCESS;
}
#ifdef	OLD_STYLE_COPROCESS
/**
* \fn void motor_Machine(void)
* \brief Main function for motors management.
*
* This function is added to the main CAN management list of functions and is called from the main CAN 
* routine, temporized by interrupt timer.
* Scans the motor_object list for queued messages to manage.
*
*
*/
static uint32_t head_pos=0, tail_pos=0;
void duet_Machine(int m_idx)
{
	int i=0;
	int len=message_on_circular_queue_idx(duet_object[m_idx].rx_queue_write_p,duet_object[m_idx].rx_queue_read_p, MOTOR_MSG_QUEUE_SIZE);
	
	// looks in the received message queue for new messages
//	if(duet_object[m_idx].rx_queue_write_p != duet_object[m_idx].rx_queue_read_p) 
	while(duet_object[m_idx].rx_queue_write_p != duet_object[m_idx].rx_queue_read_p && i<len) 
	{
		// there is new message to elaborate
		motor_in_mesg_t msg;
		memcpy(&msg, &duet_object[m_idx].rx_queue[duet_object[m_idx].rx_queue_read_p], sizeof(motor_in_mesg_t));
		duet_object[m_idx].rx_queue_read_p++;
		if(duet_object[m_idx].rx_queue_read_p >= MOTOR_MSG_QUEUE_SIZE)
			duet_object[m_idx].rx_queue_read_p = 0;
				
		// perfoms action depending the type of received message.
		switch (msg.f_code)
		{
		case PDO1TX_FCODE :
			// reset timeout heartbeat
			if(duet_object[m_idx].heardbeat_tout != 0){
				duet_object[m_idx].heartbeat_timer = timer_TimerSet(duet_object[m_idx].heardbeat_tout*2); 
			}

			duet_object[m_idx].statusword = msg.datal & 0x0000FFFF;
			
//			DbgPrintf(FORCE_DEBUG, "Status node%d=%d\r\n", duet_object[m_idx].node_id, duet_object[m_idx].statusword);
			duet_object[m_idx].actual_position = (int32_t)(msg.datah);
			duet_object[m_idx].mis_warn_bits = msg.timestamp;		//Used as timestamp for actual position
			//int pos_err = (duet_object[1].actual_position) - (duet_object[8].actual_position);//Forced debug for motorpower
			
			short tmp = ((msg.datal & 0xFFFF0000) >> 16);
			// conversione in mA in base alla "rated current"
			duet_object[m_idx].actual_current = (abs(tmp) * duet_object[m_idx].rated_current) / 1000;
			/*
			DbgPrintf(true, "nde%d Pos=%d Sts=%d T=%d\r\n", duet_object[m_idx].node_id, 
			duet_object[m_idx].actual_position,duet_object[m_idx].statusword,duet_object[m_idx].mis_warn_bits);
			*/
/*			DbgPrintf(FORCE_DEBUG, "nde%d Pos=%d Sts=%d C=%d Head=%d Tail=%d\r\n", duet_object[m_idx].node_id, 
			duet_object[m_idx].actual_position,duet_object[m_idx].statusword,tmp,head_pos,tail_pos);
*/			break;
		case PDO2TX_FCODE :
			// reset timeout heartbeat
			if(duet_object[m_idx].heardbeat_tout != 0){
				duet_object[m_idx].heartbeat_timer = timer_TimerSet(duet_object[m_idx].heardbeat_tout*2);
			}
//			DbgPrintf(FORCE_DEBUG, "node%d HeadPos=%d TailPos=%d\r\n", duet_object[m_idx].node_id, msg.datal & 0x0000FFFF, (msg.datal & 0xFFFF0000)>>16 );
			head_pos=msg.datal & 0x0000FFFF;
			tail_pos=(msg.datal & 0xFFFF0000)>>16;
			break;			
		case SDOTX_FCODE :
			// reset timeout heartbeat
			//Fix on Duet motor Init Function to avoid heartbit error
			if(duet_object[m_idx].heardbeat_tout != 0){
				duet_object[m_idx].heartbeat_timer = timer_TimerSet(duet_object[m_idx].heardbeat_tout*2);
			}
			break;
		case EMCY_FCODE :
			duet_object[m_idx].emergency_flag = 1;
			duet_object[m_idx].emergency_data_h = msg.datah;
			duet_object[m_idx].emergency_data_l = msg.datal;
			if(interp_end==0)
				interp_end=1;
			break;
		case NMT_FCODE :
			break;
		case NMTERR_FCODE :
			// reset heartbeat timeout
			if(duet_object[m_idx].heardbeat_tout != 0){
				duet_object[m_idx].heartbeat_timer = timer_TimerSet(duet_object[m_idx].heardbeat_tout*2); //aggiunta temporanea bm
			}
			break;			
		case TIME_FCODE :
			break;
		}  // switch (msg.f_code)			
	}  // if(motor_object[m_idx].rx_queue_write_p != motor_object[m_idx].rx_queue_read_p) 
}
#else
/**
  * @brief 	Main function for motor management.
  *
  * @param  int m_idx	Index of the motor in the global structure
  * @param  void * param: 	pointer of header CAN structure
  * @param  uint32_t RxFifo0ITs: 				type of activated interrupt
  * @return None
  */
void duet_Machine(void * param, uint8_t *data)
{
	uint32_t datal, datah;
	FDCAN_RxHeaderTypeDef * rx_header = (FDCAN_RxHeaderTypeDef *)param;

	// search axis number
	int m_idx;
	int node_id = rx_header->Identifier & CO_NODEMASK;
	for(m_idx=0;m_idx<MOTOR_MAXOBJ;m_idx++)
	{
		if(duet_object[m_idx].node_id == node_id)
			break;
	}
	if(m_idx >= MOTOR_MAXOBJ) // error, axis number not found
	{
		DbgPrintf(DBG_DUET, "Msg from unknown node\r\n");
		return;
	}

	// reset timeout heartbeat
	if(duet_object[m_idx].heardbeat_tout != 0){
		duet_object[m_idx].heartbeat_timer = timer_TimerSet(duet_object[m_idx].heardbeat_tout*2);
	}

	// perfoms action depending the type of received message.
	switch (rx_header->Identifier & CO_CODEMASK)
	{
	case PDO1TX_FCODE :
		datal = (uint32_t)*((uint32_t *)&data[0]);
		datah = (uint32_t)*((uint32_t *)&data[4]);
		/*
		// reset timeout heartbeat
		if(duet_object[m_idx].heardbeat_tout != 0){
			duet_object[m_idx].heartbeat_timer = timer_TimerSet(duet_object[m_idx].heardbeat_tout*2);
		}
		*/
		duet_object[m_idx].statusword = datal  & 0x0000FFFF;

//			DbgPrintf(FORCE_DEBUG, "Status node%d=%d\r\n", duet_object[m_idx].node_id, duet_object[m_idx].statusword);
		duet_object[m_idx].actual_position = (int32_t)datah;	//H data
		duet_object[m_idx].mis_warn_bits = rx_header->RxTimestamp;		//Used as timestamp for actual position
		//int pos_err = (duet_object[1].actual_position) - (duet_object[8].actual_position);//Forced debug for motorpower

		short tmp = ( (datal & 0xFFFF0000) >> 16);
		// conversione in mA in base alla "rated current"
		duet_object[m_idx].actual_current = (abs(tmp) * duet_object[m_idx].rated_current) / 1000;
		/*
		DbgPrintf(true, "nde%d Pos=%d Sts=%d T=%d\r\n", duet_object[m_idx].node_id,
		duet_object[m_idx].actual_position,duet_object[m_idx].statusword,duet_object[m_idx].mis_warn_bits);
		*/
/*			DbgPrintf(FORCE_DEBUG, "nde%d Pos=%d Sts=%d C=%d Head=%d Tail=%d\r\n", duet_object[m_idx].node_id,
		duet_object[m_idx].actual_position,duet_object[m_idx].statusword,tmp,head_pos,tail_pos);
*/			break;
	case PDO2TX_FCODE :
		datal = (uint32_t)*((uint32_t *)&data[0]);
		datah = (uint32_t)*((uint32_t *)&data[4]);
		/*
		// reset timeout heartbeat
		if(duet_object[m_idx].heardbeat_tout != 0){
			duet_object[m_idx].heartbeat_timer = timer_TimerSet(duet_object[m_idx].heardbeat_tout*2);
		}
		*/
//			DbgPrintf(FORCE_DEBUG, "node%d HeadPos=%d TailPos=%d\r\n", duet_object[m_idx].node_id, msg.datal & 0x0000FFFF, (msg.datal & 0xFFFF0000)>>16 );
		break;
	case SDOTX_FCODE :
		// reset timeout heartbeat
		/*
		//Fix on Duet motor Init Function to avoid heartbit error
		if(duet_object[m_idx].heardbeat_tout != 0){
			duet_object[m_idx].heartbeat_timer = timer_TimerSet(duet_object[m_idx].heardbeat_tout*2);
		}
		*/
		break;
	case EMCY_FCODE :
		datal = (uint32_t)*((uint32_t *)&data[0]);
		datah = (uint32_t)*((uint32_t *)&data[4]);
		duet_object[m_idx].emergency_flag = 1;
		duet_object[m_idx].emergency_data_h = datah;
		duet_object[m_idx].emergency_data_l = datal;
		if(interp_end==0)
			interp_end=1;
		break;
	case NMT_FCODE :
		break;
	case NMTERR_FCODE :
		/*
		// reset heartbeat timeout
		if(duet_object[m_idx].heardbeat_tout != 0){
			duet_object[m_idx].heartbeat_timer = timer_TimerSet(duet_object[m_idx].heardbeat_tout*2); //aggiunta temporanea bm
		}
		*/
		break;
	case TIME_FCODE :
		break;
	}  // switch (msg.f_code)
}
#endif

static uint32_t disable_timer;
#define DUET_DISABLE_TIME 500
#define DUET_MOVING_TIME 4000
void duet_Manage(int axis)
{
	static int one_shot_disable=0;
	// checks for max current limit
	// if threshold and window time are set: make the check, otherwise not.
	if( (duet_object[axis].limit_current_threshold != 0) && (duet_object[axis].limit_current_time_ms != 0))
	{
		// if actual current is over limit ...
		if(duet_object[axis].actual_current > duet_object[axis].limit_current_threshold)
		{
			// if it is the first time is over limit then set the window timer...
			if(duet_object[axis].max_current_timer == 0)
				duet_object[axis].max_current_timer = timer_TimerSet(duet_object[axis].limit_current_time_ms);
			// if the window timer is active, checks if the time window is elapsed ...
			else if(timer_TimerTest(duet_object[axis].max_current_timer))
			{
				// if the timer has expired => stop the motor and signal error
				digio_ResetRelay(REL_POWER_CHAIN);
				duet_object[axis].motor_stat = MOT_OVERCURRENT_ERROR;
				duet_object[axis].motion_stat = MOT_MOTION_COMPLETED;
				duet_object[axis].max_current_timer = 0;  // disable next check
				DbgPrintf(DBG_DUET, "Over current: %dmA on axis%d, node%d. ", duet_object[axis].actual_current, axis, duet_object[axis].node_id );
				DbgPrintf(DBG_DUET, "Limits: %dmA for %dms\r\n",  duet_object[axis].limit_current_threshold, duet_object[axis].limit_current_time_ms);
			}
		}
		// if the current is now below the threshold limit the reset the timer.
		else
			duet_object[axis].max_current_timer = 0;
	}
	
	// checks for heartbeat timeout
	if( (duet_object[axis].heardbeat_tout != 0) && (timer_TimerTest(duet_object[axis].heartbeat_timer)) ) //controllo heartbeat timeout
	{ //heartbeat scaduto
		// se un motore non invia heartbeat, non posso sapere neanche se prende i comandi,
		// quindi levo la corrente a tutti
		digio_ResetRelay(REL_POWER_CHAIN);
		duet_object[axis].motor_stat = MOT_HBT_ERROR;
		duet_object[axis].motion_stat = MOT_MOTION_COMPLETED;
		duet_object[axis].heardbeat_tout = 0;  // disable next check
		DbgPrintf(DBG_DUET, "Heartbeat timeout node%d\r\n", duet_object[axis].node_id);
	}
	
	if(duet_object[axis].statusword & MOTOR_FAULT_BIT)
	{
		duet_object[axis].motor_stat = MOT_FAULT_ERROR;
	}

	if(duet_object[axis].emergency_flag)
	{
//		DbgPrintf(FORCE_DEBUG, "Emergency msg node%d: ", duet_object[axis].node_id);
//		DbgPrintf(FORCE_DEBUG, "data: %x %x\r\n", duet_object[axis].emergency_data_h, duet_object[axis].emergency_data_l);
		// controllo che non sia un messaggio di fine buffer posizioni di interpolazione
		if(duet_object[axis].emergency_data_l != 0x21FF02)
		{
			duet_object[axis].motor_stat = MOT_EMERGENCY_ERROR;
		}
		duet_object[axis].emergency_flag = 0;
	}
			
	if(duet_object[axis].statusword & MOTOR_FOLLOWING_ERROR_BIT) //nel caso che un motore vada in errore
	{
		if(!digio_GetPowerStatus())
			digio_ResetRelay(REL_POWER_CHAIN); 		
		duet_object[axis].motor_stat = MOT_FOLLOWING_ERROR;
		duet_object[axis].motion_stat = MOT_STOPPING;
		DbgPrintf(DBG_DUET, "Following Error node%d\r\n", duet_object[axis].node_id);
	}
	
	if(duet_object[axis].motion_stat != MOT_MOTION_COMPLETED)
	{
		if(!digio_GetPowerStatus())
		{
			DbgPrintf(DBG_DUET, "Stop due to power down node%d\r\n", duet_object[axis].node_id);
			duet_StopHere(axis);  
			duet_object[axis].motion_stat = MOT_STOPPING;
		}
	}

	switch (duet_object[axis].motion_stat)
	{
		case MOT_MOTION_COMPLETED:  // sono fermo
			if(duet_object[axis].motor_stat == MOT_READY) // se non sono in errore
			{
				if ( ((duet_object[axis].statusword & MOTOR_TARGET_REACHED_BIT) == 0) 
					&& (((duet_object[axis].statusword & MOT_STATUS_MASK) ^ MOT_OPERATION_ENABLED) == 0)		
					&& digio_GetPowerStatus()==true )	// se sto muovendo e se macchina accesa
				{
					DbgPrintf(DBG_DUET, "Toward moving %X\r\n", duet_object[axis].statusword);
					duet_object[axis].motion_stat = MOT_MOVING;
					if(axis == AXIS_GANTRY)
						one_shot_disable=0;
				}else{
					// disattiva le potenza del motore (solo gantry)
					if(axis == AXIS_GANTRY  && one_shot_disable==0 && ((duet_object[axis].statusword & MOTOR_TARGET_REACHED_BIT) != 0)){
						duet_OperationDisable(axis);
						one_shot_disable=1;
					}
				}
				CheckBrakeOn(axis);
			}
			break;
		case MOT_MOVING:
		case MOT_STOPPING:
			if ( (duet_object[axis].statusword & MOTOR_TARGET_REACHED_BIT) ||  ((duet_object[axis].statusword & MOTOR_QUICK_STOP_BIT) == 0) 
				|| (((duet_object[axis].statusword & MOT_STATUS_MASK) ^ MOT_OPERATION_ENABLED) != 0))	//Aggiunta per verificare che il motore sia in OPERATION ENABLE state
			{
				if(duet_object[axis].statusword & MOTOR_QUICK_STOP_BIT)
				{
					DbgPrintf(DBG_DUET, "Toward wait to disable %X\r\n", duet_object[axis].statusword);
					duet_object[axis].motion_stat = MOT_DELAY_DISABLE;  // delay per far passare l'inerzia prima di mettere il motore in modo passivo
					disable_timer = timer_TimerSet(DUET_DISABLE_TIME);
				}
				else
				{
					DbgPrintf(DBG_DUET, "Toward wait to MOT_MOTION_COMPLETED %X\r\n", duet_object[axis].statusword);
					duet_object[axis].motion_stat = MOT_MOTION_COMPLETED;
					duet_object[axis].motor_stat = MOT_NOT_INIT;
				}
			}
		break;
		case MOT_DELAY_DISABLE:
		if ( timer_TimerTest(disable_timer))
		{
			DbgPrintf(DBG_DUET, "Toward complete %X\r\n", duet_object[axis].statusword);
			duet_object[axis].motion_stat = MOT_MOTION_COMPLETED;
			BrakeOn(axis);
			/*
			// disattiva le potenza del motore (solo gantry)
			if(axis == AXIS_GANTRY  && one_shot_disable==0){
				duet_OperationDisable(axis);
				one_shot_disable=1;
			}
			*/
		}
		joinedWdt_restart();
		break;
	}
	
}

#define LIFTER_MAX_ERROR	 2*STEP_TO_MM	// massimo errore di sincronismo tra i due motori del front lifter
void duet_ManageSlider_same_as_syncro(void)
{
	// controllo subito che lo stato dei due motori sia lo stesso
	if(duet_object[AXIS_SLIDER1].motion_stat != duet_object[AXIS_SLIDER2].motion_stat)
	{
		duet_object[AXIS_SLIDER1].motion_stat = MOT_STOPPING;
		duet_object[AXIS_SLIDER2].motion_stat = MOT_STOPPING;
	}
	
	switch (duet_object[AXIS_SLIDER1].motion_stat)
	{
		case MOT_MOTION_COMPLETED:  // sono fermo
		if(duet_object[AXIS_SLIDER1].motor_stat == MOT_READY && duet_object[AXIS_SLIDER2].motor_stat == MOT_READY) // se non sono in errore
		{
			// se uno dei due motori associato al lifter anteriore inizia a muovere cambio stato
			bool condition1 = ((duet_object[AXIS_SLIDER1].statusword & MOTOR_TARGET_REACHED_BIT) == 0) && (((duet_object[AXIS_SLIDER1].statusword & MOT_STATUS_MASK) ^ MOT_OPERATION_ENABLED) == 0);
			bool condition2 = ((duet_object[AXIS_SLIDER2].statusword & MOTOR_TARGET_REACHED_BIT) == 0) && (((duet_object[AXIS_SLIDER2].statusword & MOT_STATUS_MASK) ^ MOT_OPERATION_ENABLED) == 0);
			if (condition1 || condition2) // si muovono
			{
				DbgPrintf(DBG_DUET, "Toward moving %X\r\n", duet_object[AXIS_SLIDER1].statusword);
				duet_object[AXIS_SLIDER1].motion_stat = MOT_MOVING;
				duet_object[AXIS_SLIDER2].motion_stat = MOT_MOVING;
			}
			CheckBrakeOn(AXIS_SLIDER1);
			CheckBrakeOn(AXIS_SLIDER2);
			
		}
		break;
		
		case MOT_MOVING:
		{
			// controllo che il movimento sia sincrono con una certa tolleranza
			int32_t diff_pos = abs(duet_object[AXIS_SLIDER1].actual_position - duet_object[AXIS_SLIDER2].actual_position);
			if(diff_pos > LIFTER_MAX_ERROR)
			{
				duet_object[AXIS_SLIDER1].motion_stat = MOT_STOPPING;
				duet_object[AXIS_SLIDER2].motion_stat = MOT_STOPPING;
				DbgPrintf(DBG_DUET, "Stop: sync error %d\r\n", diff_pos);
			}
			
			// quando entrambi i motori arrivano a destinazione
			bool condition1 = (duet_object[AXIS_SLIDER1].statusword & MOTOR_TARGET_REACHED_BIT) != 0;
			bool condition2 = (duet_object[AXIS_SLIDER2].statusword & MOTOR_TARGET_REACHED_BIT) != 0;
			if(condition1 && condition2)
			{
				duet_object[AXIS_SLIDER1].motion_stat = MOT_DELAY_DISABLE;
				duet_object[AXIS_SLIDER2].motion_stat = MOT_DELAY_DISABLE;
				DbgPrintf(DBG_DUET, "Toward wait to disable\r\n");
			}
			// se uno dei due va in quickstop fermo tutto
			else if( ((duet_object[AXIS_SLIDER1].statusword & MOTOR_QUICK_STOP_BIT) == 0) ||  ((duet_object[AXIS_SLIDER2].statusword & MOTOR_QUICK_STOP_BIT) == 0) )
			{
				DbgPrintf(DBG_DUET, "Quick stop: Toward wait to disable\r\n");
				duet_object[AXIS_SLIDER1].motion_stat = MOT_DELAY_DISABLE;
				duet_object[AXIS_SLIDER2].motion_stat = MOT_DELAY_DISABLE;
				duet_object[AXIS_SLIDER1].disable_timer = timer_TimerSet(DUET_DISABLE_TIME);
			}
		}
		break;
				
		case MOT_STOPPING:
			motor_StopHere(AXIS_SLIDER1);
			motor_StopHere(AXIS_SLIDER2);
			duet_object[AXIS_SLIDER1].motion_stat = MOT_DELAY_DISABLE;
			duet_object[AXIS_SLIDER2].motion_stat = MOT_DELAY_DISABLE;
			duet_object[AXIS_SLIDER1].disable_timer = timer_TimerSet(DUET_DISABLE_TIME);
		break;
		
		case MOT_DELAY_DISABLE:
		if ( timer_TimerTest(duet_object[AXIS_SLIDER1].disable_timer))
		{
			DbgPrintf(DBG_DUET, "Toward complete\r\n" );
			duet_object[AXIS_SLIDER1].motion_stat = MOT_MOTION_COMPLETED;
			duet_object[AXIS_SLIDER2].motion_stat = MOT_MOTION_COMPLETED;
			BrakeOn(AXIS_SLIDER1);
			BrakeOn(AXIS_SLIDER2);
			duet_OperationDisable(AXIS_SLIDER1);
			duet_OperationDisable(AXIS_SLIDER2);
		}
		break;
	}
}

/************************************************************************/
/* Function to check electric current limit on slider motors            */
/************************************************************************/
static void check_slider_currentLimit(void){
		int axis_index[2]={AXIS_SLIDER1,AXIS_SLIDER2};
		for (int i=0;i<2;i++){
			// checks for max current limit
			// if threshold and window time are set: make the check, otherwise not.
			if( (duet_object[axis_index[i]].limit_current_threshold != 0) && (duet_object[axis_index[i]].limit_current_time_ms != 0))
			{
				// if actual current is over limit ...
				if(duet_object[axis_index[i]].actual_current > duet_object[axis_index[i]].limit_current_threshold)
				{
					// if it is the first time is over limit then set the window timer...
					if(duet_object[axis_index[i]].max_current_timer == 0)
						duet_object[axis_index[i]].max_current_timer = timer_TimerSet(duet_object[axis_index[i]].limit_current_time_ms);
					// if the window timer is active, checks if the time window is elapsed ...
					else if(timer_TimerTest(duet_object[axis_index[i]].max_current_timer))
					{
						digio_ResetRelay(REL_POWER_CHAIN);
						duet_object[axis_index[i]].motor_stat = MOT_OVERCURRENT_ERROR;
						duet_object[axis_index[i]].motion_stat = MOT_STOPPING;
						duet_object[axis_index[i]].max_current_timer = 0;  // disable next check
						// if the timer has expired => stop the motor and signal error
						/*
						duet_object[AXIS_SLIDER1].motor_stat = MOT_OVERCURRENT_ERROR;
						duet_object[AXIS_SLIDER1].motion_stat = MOT_STOPPING;
						duet_object[AXIS_SLIDER1].max_current_timer = 0;  // disable next check
						duet_object[AXIS_SLIDER2].motor_stat = MOT_OVERCURRENT_ERROR;
						duet_object[AXIS_SLIDER2].motion_stat = MOT_STOPPING;
						duet_object[AXIS_SLIDER2].max_current_timer = 0;  // disable next check						
						*/
						DbgPrintf(DBG_DUET, "Over current: %dmA on axis%d, node%d. ",duet_object[axis_index[i]].actual_current, axis_index[i], duet_object[axis_index[i]].node_id );
						DbgPrintf(DBG_DUET, "Limits: %dmA for %dms\r\n",  duet_object[axis_index[i]].limit_current_threshold, duet_object[axis_index[i]].limit_current_time_ms);
					}
				}
				// if the current is now below the threshold limit the reset the timer.
				else
					duet_object[axis_index[i]].max_current_timer = 0;
			}
		}
}


void duet_ManageSlider(void)
{
	check_slider_currentLimit();
	
	// controllo subito che lo stato dei due motori sia lo stesso
	#ifndef AGNOSTIC_AXIS
	if(duet_object[AXIS_SLIDER1].motion_stat != duet_object[AXIS_SLIDER2].motion_stat
		&& duet_object[AXIS_SLIDER1].node_id==NODE_SLIDER1 && duet_object[AXIS_SLIDER2].node_id==NODE_SLIDER2)
	#else
	if(duet_object[AXIS_SLIDER1].motion_stat != duet_object[AXIS_SLIDER2].motion_stat)
	#endif
	{
		duet_object[AXIS_SLIDER1].motion_stat = MOT_STOPPING;
		duet_object[AXIS_SLIDER2].motion_stat = MOT_STOPPING;
	}

	#ifndef AGNOSTIC_AXIS
	if(duet_object[AXIS_SLIDER1].emergency_flag && duet_object[AXIS_SLIDER1].node_id==NODE_SLIDER1)
	#else
	if(duet_object[AXIS_SLIDER1].emergency_flag)
	#endif
	{
		DbgPrintf(FORCE_DEBUG, "Slider Emergency msg node%d - ", duet_object[AXIS_SLIDER1].node_id);
		DbgPrintf(FORCE_DEBUG, "data: %x %x\r\n", duet_object[AXIS_SLIDER1].emergency_data_h, duet_object[AXIS_SLIDER1].emergency_data_l);
		// controllo che non sia un messaggio di fine buffer posizioni di interpolazione
		if(duet_object[AXIS_SLIDER1].emergency_data_l != 0x21FF02)
		{
			duet_object[AXIS_SLIDER1].motor_stat = MOT_EMERGENCY_ERROR;
		}
		duet_object[AXIS_SLIDER1].emergency_flag = 0;
	}

	#ifndef AGNOSTIC_AXIS
	if(duet_object[AXIS_SLIDER2].emergency_flag && duet_object[AXIS_SLIDER2].node_id==NODE_SLIDER2)
	#else
	if(duet_object[AXIS_SLIDER2].emergency_flag)
	#endif
	{
		DbgPrintf(FORCE_DEBUG, "Slider Emergency msg node%d - ", duet_object[AXIS_SLIDER2].node_id);
		DbgPrintf(FORCE_DEBUG, "data: %x %x\r\n", duet_object[AXIS_SLIDER2].emergency_data_h, duet_object[AXIS_SLIDER2].emergency_data_l);
		if(duet_object[AXIS_SLIDER2].emergency_data_l != 0x21FF02)
		{
			duet_object[AXIS_SLIDER2].motor_stat = MOT_EMERGENCY_ERROR;
		}
		duet_object[AXIS_SLIDER2].emergency_flag = 0;
	}
	
	// checks for heartbeat timeout
	if( ( (duet_object[AXIS_SLIDER2].heardbeat_tout != 0) && (timer_TimerTest(duet_object[AXIS_SLIDER2].heartbeat_timer)) ) ||
		( (duet_object[AXIS_SLIDER1].heardbeat_tout != 0) && (timer_TimerTest(duet_object[AXIS_SLIDER1].heartbeat_timer)) ) ) //controllo heartbeat timeout
	{ //heartbeat scaduto
		// se un motore non invia heartbeat, non posso sapere neanche se prende i comandi,
		// quindi levo la corrente a tutti
		digio_ResetRelay(REL_POWER_CHAIN);
		if(timer_TimerTest(duet_object[AXIS_SLIDER1].heartbeat_timer)){
			DbgPrintf(true/*DBG_DUET*/, "Heartbeat timeout node%d\r\n", duet_object[AXIS_SLIDER1].node_id);
			duet_object[AXIS_SLIDER1].motor_stat = MOT_HBT_ERROR;
			duet_object[AXIS_SLIDER1].motion_stat = MOT_MOTION_COMPLETED;
			duet_object[AXIS_SLIDER1].heardbeat_tout = 0;  // disable next check
		}
		if(timer_TimerTest(duet_object[AXIS_SLIDER2].heartbeat_timer)){
			DbgPrintf(true/*DBG_DUET*/, "Heartbeat timeout node%d\r\n", duet_object[AXIS_SLIDER2].node_id);
			duet_object[AXIS_SLIDER2].motor_stat = MOT_HBT_ERROR;
			duet_object[AXIS_SLIDER2].motion_stat = MOT_MOTION_COMPLETED;
			duet_object[AXIS_SLIDER2].heardbeat_tout = 0;  // disable next check
		}
	}
	
	//Manage The motion status, basing on the motor status
	switch (duet_object[AXIS_SLIDER1].motion_stat)
	{
		case MOT_MOTION_COMPLETED:  // sono fermo
		if(duet_object[AXIS_SLIDER1].motor_stat == MOT_READY || duet_object[AXIS_SLIDER1].motor_stat == MOT_CRUSHING_ERROR) // se non sono in errore, oppure devo liberare l'anticrushing
		{
			// se uno dei due motori associato al lifter anteriore inizia a muovere cambio stato
			bool condition1 = ((duet_object[AXIS_SLIDER1].statusword & MOTOR_TARGET_REACHED_BIT) == 0) && (((duet_object[AXIS_SLIDER1].statusword & MOT_STATUS_MASK) ^ MOT_OPERATION_ENABLED) == 0);
			bool condition2 = ((duet_object[AXIS_SLIDER2].statusword & MOTOR_TARGET_REACHED_BIT) == 0) && (((duet_object[AXIS_SLIDER2].statusword & MOT_STATUS_MASK) ^ MOT_OPERATION_ENABLED) == 0);
			if (condition1 || condition2) // si muovono
			{
				DbgPrintf(FORCE_DEBUG, "Slider Toward moving (status %X)\r\n", duet_object[AXIS_SLIDER1].statusword);
				duet_object[AXIS_SLIDER1].motion_stat = MOT_DELAY_MOVING;//MOT_MOVING;
				duet_object[AXIS_SLIDER2].motion_stat = MOT_DELAY_MOVING;//MOT_MOVING;
				disable_timer = timer_TimerSet(DUET_MOVING_TIME);
			}
			CheckBrakeOn(AXIS_SLIDER1);
			CheckBrakeOn(AXIS_SLIDER2);
		}
		break;
		
		case MOT_DELAY_MOVING:
		{
			bool condition1 = (duet_object[AXIS_SLIDER1].statusword & MOTOR_TARGET_REACHED_BIT) == 0 && ((duet_object[AXIS_SLIDER1].statusword & 0x1000) !=0);
			bool condition2 = (duet_object[AXIS_SLIDER2].statusword & MOTOR_TARGET_REACHED_BIT) == 0  && ((duet_object[AXIS_SLIDER2].statusword & 0x1000) !=0);
			if(condition1 && condition2){
				DbgPrintf(FORCE_DEBUG, "Slider Toward MOVING (status %X,%X)\r\n", duet_object[AXIS_SLIDER1].statusword,duet_object[AXIS_SLIDER2].statusword);
				duet_object[AXIS_SLIDER1].motion_stat = MOT_MOVING;
				duet_object[AXIS_SLIDER2].motion_stat = MOT_MOVING;			
			}else{
				if ( timer_TimerTest(disable_timer))
				{
					DbgPrintf(FORCE_DEBUG, "Slider Toward MOVING for timeout\r\n" );
					duet_object[AXIS_SLIDER1].motion_stat = MOT_MOVING;
					duet_object[AXIS_SLIDER2].motion_stat = MOT_MOVING;
				}
			}
		}
		break;
		
		case MOT_MOVING:
		{
			// quando entrambi i motori arrivano a destinazione
			bool condition1 = (duet_object[AXIS_SLIDER1].statusword & MOTOR_TARGET_REACHED_BIT) != 0;
			bool condition2 = (duet_object[AXIS_SLIDER2].statusword & MOTOR_TARGET_REACHED_BIT) != 0;
			bool condition3 = (((duet_object[AXIS_SLIDER1].statusword & MOT_STATUS_MASK) ^ MOT_OPERATION_ENABLED) != 0);
			bool condition4 = (((duet_object[AXIS_SLIDER2].statusword & MOT_STATUS_MASK) ^ MOT_OPERATION_ENABLED) != 0);
			if((condition1 && condition2) || (condition3 || condition4))
			{
				duet_object[AXIS_SLIDER1].motion_stat = MOT_DELAY_DISABLE;
				duet_object[AXIS_SLIDER2].motion_stat = MOT_DELAY_DISABLE;
				DbgPrintf(FORCE_DEBUG, "Slider Toward wait to disable %d %d %d %d\r\n",condition1,condition2,condition3,condition4);
			}
			// se uno dei due va in quickstop fermo tutto
			else if( ((duet_object[AXIS_SLIDER1].statusword & MOTOR_QUICK_STOP_BIT) == 0) ||  ((duet_object[AXIS_SLIDER2].statusword & MOTOR_QUICK_STOP_BIT) == 0) 
				|| interp_end==1)
			{
				DbgPrintf(FORCE_DEBUG, "Slider quick stop: toward wait to disable\r\n");
				duet_object[AXIS_SLIDER1].motion_stat = MOT_STOPPING;
				duet_object[AXIS_SLIDER2].motion_stat = MOT_STOPPING;
				disable_timer = timer_TimerSet(DUET_DISABLE_TIME);
			}
		}
		break;
		
		case MOT_STOPPING:
			duet_object[AXIS_SLIDER1].motion_stat = MOT_DELAY_DISABLE;
			duet_object[AXIS_SLIDER2].motion_stat = MOT_DELAY_DISABLE;
			disable_timer = timer_TimerSet(DUET_DISABLE_TIME);
			DbgPrintf(FORCE_DEBUG, "MOT_STOPPING\r\n");
			break;
		
		case MOT_DELAY_DISABLE:
		if ( timer_TimerTest(disable_timer))
		{
			DbgPrintf(FORCE_DEBUG, "Slider Toward complete \r\n" );
			duet_object[AXIS_SLIDER1].motion_stat = MOT_MOTION_COMPLETED;
			duet_object[AXIS_SLIDER2].motion_stat = MOT_MOTION_COMPLETED;
			BrakeOn(AXIS_SLIDER1);
			BrakeOn(AXIS_SLIDER2);
			duet_OperationDisable(AXIS_SLIDER1);
			duet_OperationDisable(AXIS_SLIDER2);
		}
		break;
	}
}



/**
* \fn uint32_t motor_Home(int axis)
* \brief Performs homing of the motor
*
* Set the home speed and start moving towards the position 0
*
* \param int axis Axis number (index in the motor_object array)
* \return  1 = success, 0 = error.
*
*/
uint32_t duet_Home(int axis)
{
	if(!duet_SetParameter(axis, 0x6081, 0, (uint32_t *)&duet_object[axis].home_speed, sizeof(uint32_t)))
		return RES_FAIL;
	
	return duet_GoAbsolute(axis, (uint32_t)duet_object[axis].home_position);
}

/**
* \fn uint32_t motor_SetSpeed(int axis, uint32_t speed)
* \brief Set the motor speed in the motor object structure.
*
* \param int axis Axis number (index in the motor_object array)
* \speed uint32_t speed Motor speed value in step/s
* \return  1 = success, 0 = error.
*
*/
uint32_t duet_SetSpeed(int axis, uint32_t speed)
{	
	// per i lifter scrivo subito nel motore anche se non serve per la modalit� interpolata
	#ifndef AGNOSTIC_AXIS
	if( ((axis == AXIS_SLIDER1 && duet_object[axis].node_id==NODE_SLIDER1) || (axis == AXIS_SLIDER2 && duet_object[axis].node_id==NODE_SLIDER2)) && !service_state
		&& slider_enabled)
	#else
	if( ((axis == AXIS_SLIDER1) || (axis == AXIS_SLIDER2)) && !service_state
		&& slider_enabled)
	#endif
	{
		//Per evitare duet_Set Speed quando i motori interpolati sono in moto
		if(duet_object[AXIS_SLIDER1].motion_stat!=MOT_MOTION_COMPLETED || duet_object[AXIS_SLIDER2].motion_stat!=MOT_MOTION_COMPLETED)
			return RES_FAIL;

		duet_object[AXIS_SLIDER1].pos_speed = speed;
		if(!duet_SetParameter(AXIS_SLIDER1, 0x6081, 0, (uint32_t *)&duet_object[AXIS_SLIDER1].pos_speed, sizeof(uint32_t)))
			return RES_FAIL;
		duet_object[AXIS_SLIDER2].pos_speed = speed;
		if(!duet_SetParameter(AXIS_SLIDER2, 0x6081, 0, (uint32_t *)&duet_object[AXIS_SLIDER2].pos_speed, sizeof(uint32_t)))
			return RES_FAIL;
	}else{
		duet_object[axis].pos_speed = speed;
	/*
		if(!duet_SetParameter(axis, 0x6081, 0, (uint32_t *)&duet_object[axis].pos_speed, sizeof(uint32_t)))
			return RES_FAIL;
		Made at GoAbsolutePos*/
	}
	
	return RES_SUCCESS;
}

/**
* \fn uint32_t motor_SetHomeSpeed(int axis, uint32_t h_speed)
* \brief Set the motor speed for homing
*
* \param int axis Axis number (index in the motor_object array)
* \speed uint32_t h_speed Motor homing speed value in step/s
* \return  1 = success, 0 = error.
*
*/
uint32_t duet_SetHomeSpeed(int axis, uint32_t h_speed)
{
	duet_object[axis].home_speed = h_speed;
	
	return RES_SUCCESS;
}

/**
* \fn uint32_t duet_QuickStop(int axis)
* \brief Commands the 'quick stop' of the motor.
*
* Command the 'quick stop' of the motor setting the bit in the control word.
* After this command the motor must be reinitialized.
*
* \param int axis Axis number (index in the motor_object array)
* \return  1 = success, 0 = error.
*
*/
uint32_t duet_QuickStop(int axis)
{
	uint32_t cw;
	
	if(duet_ReadControlWord(axis, &cw))
		return duet_WriteControlWord(axis, cw & ~MOTOR_QSTOP_BIT);
	else
		return RES_FAIL;
	
	//	duet_object[axis].motion_stat = MOT_MOTION_COMPLETED;
}

/**
* \fn uint32_t motor_GetMotorActPos(int axis, uint32_t* pos)
* \brief Read the actual position from the motor
*
* \param int axis Axis number (index in the motor_object array)
* \return  the actual absolute position of the motor in step.
*
*/
uint32_t duet_GetMotorActPos(int axis, int32_t* pos)
{
	uint32_t result;

	if(duet_object[axis].node_id != 0)  // se � inizializzato
	{
		*pos = duet_object[axis].actual_position;
		result = RES_SUCCESS;
	}
	else
	{
		*pos = 0;
		result = RES_FAIL;
	}

	return result;
}

/**
* \fn uint32_t motor_GetMotorActualCurrent(int axis)
* \brief Read the actual current from the motor
*
* \param int axis Axis number (index in the motor_object array)
* \return  the actual current in the motor in mA.
*
*/
uint32_t duet_GetMotorActualCurrent(int axis)
{
	return duet_object[axis].actual_current;
}

/**
* \fn uint32_t motor_GetMotorRatedCurrent(int axis)
* \brief Read the nominal rated current the motor
*
* \param int axis Axis number (index in the motor_object array)
* \return  the rated motor current in mA.
*
*/
uint32_t duet_GetMotorRatedCurrent(int axis)
{
	uint32_t result = RES_SUCCESS;
	
	uint32_t val;
	if(!duet_GetParameter(axis, 0x6075, 0, &val, 4))
	{
		duet_object[axis].motor_stat = MOT_GET_PARAM_ERROR;
		return RES_FAIL;
	}
	
	duet_object[axis].rated_current = val;

	return result;
}

uint32_t duet_SetMotorRatedCurrent(int axis, uint32_t val)
{
	uint32_t result = RES_SUCCESS;
	
	if(!duet_SetParameter(axis, 0x6075, 0, &val, 4))
	{
		duet_object[axis].motor_stat = MOT_GET_PARAM_ERROR;
		return RES_FAIL;
	}
	
	duet_object[axis].rated_current = val;

	return result;
}

/**
* \fn uint32_t motor_SetCurrentLimit(int axis)
* \brief Sets the max current limit beyond which the motor must stop 
*
* \param int axis Axis number (index in the motor_object array)
* \param int max_current limit in mA.
*
*/
uint32_t duet_SetCurrentLimit(int axis, int max_current)
{
	duet_object[axis].limit_current_threshold = max_current;
	DbgPrintf(DBG_DUET, "Current: Rated %d, Max %d, Thrs %d\r\n", duet_object[axis].rated_current, max_current, duet_object[axis].limit_current_threshold);
	return RES_SUCCESS;
}

/**
* \fn uint32_t motor_SetCurrentLimitTime(int axis)
* \brief Sets the maximum time during which the current can exceed the limit
*
* \param int axis Axis number (index in the motor_object array)
* \param uint32_t limit_time in ms
*
*/
uint32_t duet_SetCurrentLimitTime(int axis, uint32_t limit_time)
{
	duet_object[axis].limit_current_time_ms = limit_time;
	
	return RES_SUCCESS;
}

/**
* \fn uint32_t duet_SetZero(int axis)
* \brief Set the actual position as position 0
*
* This function write 0 in the registers P_NEW, P_IST and P_SOLL
* using fast command register 24
*
* \param int axis Axis number (index in the motor_object array)
* \return  1 = success, !=1 = error.
*
*/
uint32_t duet_SetZero(int axis)
{
	uint32_t res = RES_SUCCESS;

	// set homing method (35)
	duet_SetHomingMethod(axis, 35);
	// set mode of operation to homing
	duet_SetModeOperation(axis, OP_HOMING);
	// start homing
	duet_WriteControlWord(axis, 0x1f);
	// deassert start homing
	duet_WriteControlWord(axis, 0x0f);
	
	// wait for target reached with timeout
	//	uint32_t delay_timer = timer_TimerSet(5);
	int32_t tout_timer = 2000 / 5;
	do
	{
		HAL_Delay(5);
	
		if(duet_object[axis].statusword & MOTOR_FAULT_BIT)
		{
			res = 2;
			break;;
		}
		else if(tout_timer < 0)
		{
			res = RES_FAIL;
			break;;
		}
	
		tout_timer--;
	} while (!(duet_object[axis].statusword & MOTOR_TARGET_REACHED_BIT));
	
	// restore mode of operation to profiled position
	if(!duet_SetModeOperation(axis, OP_PROFILED_POSITION))
	{
		duet_object[axis].motor_stat = MOT_INIT_ERROR;
		res = RES_FAIL;
	}
		
	return res;
}
