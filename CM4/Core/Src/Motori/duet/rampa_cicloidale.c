/*
 * rampa_cicloidale.c
 *
 * Created: 01/06/2020 09:17:32
 *  Author: alessandro.rossi
 */ 

#include <AntiCrushing.h>
#include "main.h"
#include "duet.h"
#include "co_main.h"
#include "co_microdep.h"
#include "co_nmt.h"
#include "co_sdo.h"
#include "co_pdo.h"
#include "dig_io.h"
#include "manage_debug.h"
#include "joinedWdt.h"
#include "interpolate.h"
#include "BrakeManagement.h"
#include <string.h>
#include <math.h>

#define	PERIOD_INTERP_MS			5										//10	//5
#define PERIOD_INTERP_S				(((float)PERIOD_INTERP_MS)/1000.0)		//0.01	//0.005
#define DUET_INTERP_TIMING_FREQ		(1.0/(PERIOD_INTERP_S/2.0))				//200	//400

#define	PI							3.1416

extern bool slider_enabled;
extern bool service_state;
int  interp_end=1;



float f_delta_T;
int delta_T;
//Parametri cicloidale Asse 1
float VEL;	
float ACCEL;
float s_totale;
float T;
float S;
float V;
float A;
int i = 0;
float s_0, t_0, s_1, t_1, v_0, v_1, tt;
int segno;
float Sa, Ta;
int step_position;

//Parametri cicloidale Asse 2
float VEL2;
float ACCEL2;
float s_totale2;
float T2;
float S2;
float V2;
float A2;
float s_02, t_02, s_12, t_12, v_02, v_12, tt2;
int segno2;
float Sa2, Ta2;
int step_position2;


int asse1, asse2;
int start_position;
int start_position2;
uint32_t start_timer, stop_timer;

bool insieme;
float t_v, t_a;

int StartMotion2=0 ,StartInterpolatedSync=0,fix_target=0,static_target=0;

/// Array of motor object structures
extern motor_obj_t * duet_object;
syncro_stop_t syncro_stop = {false,1,0};

#define		FORCE_DEBUG					DBG_INTERP	//DBG_DUET		//true
#define		PRELIMINARY_SAMPLES			0	//0		//30
#define		SAMPLECOUPLE_BEFORE_SYNC	0	//0		//10
#define		FIX_TARGET_TIMES			30	//10
#define		MAX_ERROR					6*STEP_TO_MM

int max_err = MAX_ERROR;


static void restart_sync(void){
	duet_object[AXIS_SLIDER1].heardbeat_tout = MOT_SYNC_RX_TOUT;
	duet_object[AXIS_SLIDER1].heartbeat_timer = timer_TimerSet(MOT_SYNC_RX_TOUT*6);	
	duet_object[AXIS_SLIDER2].heardbeat_tout = MOT_SYNC_RX_TOUT;
	duet_object[AXIS_SLIDER2].heartbeat_timer = timer_TimerSet(MOT_SYNC_RX_TOUT*6);	
	motor_ResetSync();
	co_main_StartSync(MOT_SYNC_RX_TOUT);		//can PDO message
}

static void timerIP_config(uint32_t duet_interp_freq);
static void timerIP_config(uint32_t duet_interp_freq){
/*
	uint32_t ul_div;
	uint32_t ul_tcclks;

	//To avoid Bad code
	tc_stop(TC0, 1);
	pmc_disable_periph_clk(ID_TC1);

	// Configure PMC.
	// timer 1 per contatore invii posizioni
	pmc_enable_periph_clk(ID_TC1);

	// Configure TC for a 100Hz frequency and trigger on RC compare.
	tc_find_mck_divisor(duet_interp_freq, sysclk_get_main_hz(), &ul_div, &ul_tcclks, sysclk_get_main_hz());

	tc_init(TC0, 1, ul_tcclks | TC_CMR_CPCTRG);
	tc_write_rc(TC0, 1, (sysclk_get_main_hz() / ul_div) / duet_interp_freq);
	TC0->TC_CHANNEL[1].TC_CV=0;		//Reset to 0

	NVIC_SetPriority((IRQn_Type)ID_TC1, 1);
	// Configure and enable interrupt on RC compare.
	NVIC_EnableIRQ((IRQn_Type)ID_TC1);
	tc_enable_interrupt(TC0, 1, TC_IER_CPCS);
*/
}

void setMaxutOfSyncErr(int par){
	max_err = par * STEP_TO_MM;
}

uint32_t duet_SetInterpolatePositionLinear(int axis1, int axis2, int32_t new_position)
{
	uint32_t tmpuint;
	
	// stop sync
	co_main_StopSync();
	//To avoid Bad code
//	tc_stop(TC0, 1);
	HAL_TIM_Base_Stop_IT(&htim_ip);
	
	asse1 = axis1;
	asse2 = axis2;

	// set delta T in ms
	uint32_t duet_interp_freq;

	delta_T = PERIOD_INTERP_MS;						//Delta T di refresh ms
	f_delta_T = PERIOD_INTERP_S;					//Delta T (float) in s
	duet_interp_freq = DUET_INTERP_TIMING_FREQ;		//Frequenza 400Hz, period 2.5ms (per come � settato il timer questo genera interrupt ogni 5ms)

	if ( (duet_object[AXIS_SLIDER1].motor_stat != MOT_READY || duet_object[AXIS_SLIDER1].motor_stat == MOT_CRUSHING_ERROR) ||  
			(duet_object[AXIS_SLIDER2].motor_stat != MOT_READY || duet_object[AXIS_SLIDER2].motor_stat == MOT_CRUSHING_ERROR) )
	{
		if (duet_object[AXIS_SLIDER1].motor_stat == MOT_CRUSHING_ERROR || duet_object[AXIS_SLIDER2].motor_stat == MOT_CRUSHING_ERROR){
			if(duet_object[axis1].actual_position >= new_position){
				restart_sync();
				return RES_FAIL;
			}
		}else if(duet_object[AXIS_SLIDER1].motor_stat != MOT_READY || duet_object[AXIS_SLIDER2].motor_stat != MOT_READY){
			restart_sync();
			return RES_FAIL;
		} 
	}
	if(!duet_OperationEnable(axis1)){
		duet_object[axis1].motor_stat = MOT_SET_PARAM_ERROR;
		restart_sync();
		return RES_FAIL;
	}

	if(!duet_OperationEnable(axis2)){
		duet_object[axis2].motor_stat = MOT_SET_PARAM_ERROR;
		restart_sync();
		return RES_FAIL;
	}
	uint32_t op_mode=0;
	op_mode=0;
	// clear buffer
	tmpuint = 1;
	//DbgPrintf(FORCE_DEBUG, "SetPar 0x60C4 axis%d val%d\r\n", axis1,tmpuint);
	if(duet_SetParameter(axis1, 0x60C4, 6, &tmpuint, 1) == RES_FAIL){
		//DbgPrintf(FORCE_DEBUG, "Buffer clear error axis%d\r\n", axis1);
		restart_sync();
		return RES_FAIL;
	}

	//DbgPrintf(FORCE_DEBUG, "SetPar 0x60C4 axis%d val%d\r\n", axis2,tmpuint);
	if(duet_SetParameter(axis2, 0x60C4, 6, &tmpuint, 1) == RES_FAIL){
		//DbgPrintf(FORCE_DEBUG, "Buffer clear error axis%d\r\n", axis2);
		restart_sync();
		return RES_FAIL;
	}

	//DbgPrintf(FORCE_DEBUG, "Set INTERPOLATE_POSITION axis%d\r\n", axis1);
	if(!duet_SetModeOperation(axis1, OP_INTERPOLATE_POSITION))		
	{
		duet_object[axis1].motor_stat = MOT_SET_PARAM_ERROR;
		restart_sync();
		return RES_FAIL;
	}
	//DbgPrintf(FORCE_DEBUG, "Set INTERPOLATE_POSITION axis%d\r\n", axis2);
	if(!duet_SetModeOperation(axis2, OP_INTERPOLATE_POSITION))
	{
		duet_object[axis2].motor_stat = MOT_SET_PARAM_ERROR;
		restart_sync();
		return RES_FAIL;
	}

	op_mode=0;
	uint32_t tout_timer = timer_TimerSet(1000);	
	do{
		duet_GetModeOperation(axis1,&op_mode);
	}while(op_mode!=OP_INTERPOLATE_POSITION && timer_TimerTest(tout_timer) == false);	
	if(timer_TimerTest(tout_timer))
		return RES_FAIL;
	op_mode=0;
	tout_timer = timer_TimerSet(1000);
	do{
		duet_GetModeOperation(axis2,&op_mode);
	}while(op_mode!=OP_INTERPOLATE_POSITION && timer_TimerTest(tout_timer) == false);
	if(timer_TimerTest(tout_timer))
		return RES_FAIL;
	
	//DbgPrintf(FORCE_DEBUG, "SetPar 0x60C2 axis%d val%d\r\n", axis1,delta_T);
	if(duet_SetParameter(axis1, 0x60C2, 1, (uint32_t *) &delta_T, 1) == RES_FAIL){
		//DbgPrintf(FORCE_DEBUG, "Set delay error axis%d\r\n", axis1);
		restart_sync();
		return RES_FAIL;
	}

	//DbgPrintf(FORCE_DEBUG, "SetPar 0x60C2 axis%d val%d\r\n", axis2,delta_T);
	if(duet_SetParameter(axis2, 0x60C2, 1, (uint32_t *)&delta_T, 1) == RES_FAIL){
		//DbgPrintf(FORCE_DEBUG, "Set delay error axis%d\r\n", axis2);
		restart_sync();
		return RES_FAIL;
	}

	// accelerazione e velocit� le prendo dal primo, perch� devono essere uguali
	ACCEL = duet_object[axis1].pos_acc;
	VEL = duet_object[axis1].pos_speed;
	ACCEL2 = duet_object[axis2].pos_acc;
	VEL2 = duet_object[axis2].pos_speed;
	// definisco l'errore massimo tra le posizioni dei motori come i passi tra due invii di posizione alla massima velocit�
//	max_err = (int)(VEL * f_delta_T) + 100;
//	max_err = MAX_ERROR;
	
	// calcolo lo spazio da percorrere
	s_totale = abs(duet_object[axis1].actual_position - new_position);
	s_totale2 = abs(duet_object[axis2].actual_position - new_position);
	//DbgPrintf(FORCE_DEBUG, "Total steps(1) %d\r\n", (int)s_totale);
	//DbgPrintf(FORCE_DEBUG, "Total steps(2) %d\r\n", (int)s_totale2);
	if(duet_object[axis1].actual_position > new_position)
		segno = -1;			//Verso motore
	else
		segno = 1;
	if(duet_object[axis2].actual_position > new_position)
		segno2 = -1;			//Verso motore
	else
		segno2 = 1;

	start_position = duet_object[axis1].actual_position;
	start_position2 = duet_object[axis2].actual_position;
	//DbgPrintf(FORCE_DEBUG, "Initial Pos axis%d = %d\r\n", axis1,(int)start_position);
	//DbgPrintf(FORCE_DEBUG, "Initial Pos axis%d = %d\r\n", axis2,(int)start_position2);
	

	// Acceration time
	Ta = (PI*VEL)/(2.0*ACCEL);
	Ta2 = (PI*VEL2)/(2.0*ACCEL2);
	// Acceleration spaxe
//	Sa = (VEL*Ta)*(Ta/(2*Ta) - (1/(2*PI)) * sin((2*PI*Ta)/(2*Ta)));
	Sa = (VEL*Ta)/2.0;
	Sa2 = (VEL2*Ta2)/2.0;
	// If Sa < s_totale/2 slow down max velocity
	if(Sa > (s_totale/2.0) )
	{
		VEL = s_totale/Ta;
	}
	// If Sa2 < s_totale2/2 slow down max velocity
	if(Sa2 > (s_totale2/2.0) )
	{
		VEL2 = s_totale2/Ta2;
	}
	//DbgPrintf(FORCE_DEBUG, "Ta %d, Sa %d\r\n", (int)Ta, (int)Sa);
	//DbgPrintf(FORCE_DEBUG, "Ta2 %d, Sa2 %d\r\n", (int)Ta2, (int)Sa2);

	//Initialization step variables
	T = 0.0;
	T2 = 0.0;
	i = 0;
	S = 0;
	S2 = 0;
	s_0 = 0;
	s_02 = 0;
	
	// abilito modalit�
	uint32_t cw=0;

	duet_ReadObj(axis1, 0x60C4, 1, &cw);
	//DbgPrintf(FORCE_DEBUG, "Maximum buffer size %d axis%d\r\n",cw, axis1);
	cw=0;
	if(duet_SetParameter(axis1, 0x60C4, 2, &cw, 4)==0){
		//DbgPrintf(FORCE_DEBUG, "Error setting Max Buffer size axis%d\r\n", axis1);
	}
	duet_ReadObj(axis1, 0x60C4, 2, &cw);
	//DbgPrintf(FORCE_DEBUG, "Actual buffer size %d axis%d\r\n", cw, axis1);
	duet_ReadObj(axis1, 0x60C4, 3, &cw);
	//DbgPrintf(FORCE_DEBUG, "buffer org %d axis%d\r\n",cw,axis1);
	duet_ReadObj(axis2, 0x60C4, 1, &cw);
	//DbgPrintf(FORCE_DEBUG, "Maximum buffer size %d axis%d\r\n",cw, axis2);
	cw=0;
	if(duet_SetParameter(axis2, 0x60C4, 2, &cw, 4)==0){
		//DbgPrintf(FORCE_DEBUG, "Error setting Max Buffer size axis%d\r\n", axis2);
	}
	duet_ReadObj(axis2, 0x60C4, 2, &cw);
	//DbgPrintf(FORCE_DEBUG, "Actual buffer size %d axis%d\r\n",cw, axis2);
	duet_ReadObj(axis2, 0x60C4, 3, &cw);
	//DbgPrintf(FORCE_DEBUG, "buffer org %d axis%d\r\n", cw, axis2);	
	
	if(duet_ReadControlWord(axis1, &cw) == RES_FAIL){
		//DbgPrintf(FORCE_DEBUG, "Read cw error axis%d\r\n", axis1);
		restart_sync();
		return RES_FAIL;
	}

	cw |= MOTOR_INTERP_BIT;
	//DbgPrintf(FORCE_DEBUG, "Write cw axis%d val0x%x\r\n", axis1,cw);
	if(duet_WriteControlWord(axis1, cw) == RES_FAIL){
		//DbgPrintf(FORCE_DEBUG, "Write cw error axis%d\r\n", axis1);
		restart_sync();
		return RES_FAIL;
	}

	if(duet_ReadControlWord(axis2, &cw) == RES_FAIL){
		//DbgPrintf(FORCE_DEBUG, "Read cw error axis%d\r\n", axis2);
		restart_sync();
		return RES_FAIL;
	}
	
	cw |= MOTOR_INTERP_BIT;
	//DbgPrintf(FORCE_DEBUG, "Write cw axis%d val0x%x\r\n", axis2,cw);
	if(duet_WriteControlWord(axis2, cw) == RES_FAIL){
		//DbgPrintf(FORCE_DEBUG, "Write cw error axis%d\r\n", axis2);
		restart_sync();
		return RES_FAIL;
	}
		
	duet_ReadStatusWord(axis1, &cw);
	//DbgPrintf(FORCE_DEBUG, "Sw 0x%x axis%d\r\n",cw, axis1);
	/*
	uint32_t tout_timer = timer_TimerSet(3000);
	while(timer_TimerTest(tout_timer) == false)	
	{
		timer_Delay(5);
		uint32_t sw;
		duet_ReadStatusWord(axis1, &sw);
		if(((sw & 0xFF00) ^ 0x1200) == 0)
		{
			break;
		}
	};
	if(timer_TimerTest(tout_timer))
		return RES_FAIL;
		
	duet_ReadStatusWord(axis2, &cw);
	//DbgPrintf(FORCE_DEBUG, "Sw 0x%x axis%d\r\n",cw, axis2);
	
	tout_timer = timer_TimerSet(3000);
	while(timer_TimerTest(tout_timer) == false)	
	{
		timer_Delay(5);
		// Read the status word
		uint32_t sw;
		duet_ReadStatusWord(axis2, &sw);
		if((( sw & 0xFF00) ^ 0x1200) == 0)
		{
			break;
		}
	};
	if(timer_TimerTest(tout_timer))
		return RES_FAIL;
	*/
	start_timer = timer_TimerSet(0);
		
	// attivo il timer
	StartMotion2=0;
	StartInterpolatedSync=0;
	fix_target=0;
	last_syncro_Sendtimer=timer_TimerSet(0);
	interp_end=0;
	// Waiting to load n dati on MotorPower FIFO
	//First SYNC
	BrakeOff(asse1);
	BrakeOff(asse2);
	static_target=PRELIMINARY_SAMPLES;
	syncro_stop.enable=false;
	syncro_stop.ratio=1;
	syncro_stop.count = 1000/delta_T;//100;@50ms->5000ms
	timerIP_config(duet_interp_freq);
	duet_object[AXIS_SLIDER1].heardbeat_tout = MOT_SYNC_RX_TOUT;
	duet_object[AXIS_SLIDER1].heartbeat_timer = timer_TimerSet(MOT_SYNC_RX_TOUT*6);	
	duet_object[AXIS_SLIDER2].heardbeat_tout = MOT_SYNC_RX_TOUT;
	duet_object[AXIS_SLIDER2].heartbeat_timer = timer_TimerSet(MOT_SYNC_RX_TOUT*6);	
//	tc_start(TC0, 1);
	HAL_TIM_Base_Start_IT(&htim_ip);
//	DbgPrintf(FORCE_DEBUG, "Inizio davvero\r\n");
//	while(StartMotion2<SAMPLECOUPLE_BEFORE_SYNC);
	motor_ResetSync();		
	return RES_SUCCESS;
}

uint32_t index_sample=0;
typedef struct{
	uint32_t time;
	int sample;
}sample_t;
sample_t last_sample[10]; 
uint32_t last_syncro_Sendtimer=0;
#if 1
void IP_Handler(void)
{
	volatile uint32_t ul_dummy;

//	ioport_set_pin_level(LED1_GPIO, LEDS_ACTIVE_LEVEL);
	HAL_GPIO_WritePin(LED1_PORT, LED1_GPIO, LEDS_ACTIVE_LEVEL);

	/* Remove warnings. */
	ul_dummy++;

	//Se la macchina non � in potenza 
	if(!digio_GetPowerStatus())
	{
//		tc_stop(TC0, 1);
		HAL_TIM_Base_Stop_IT(&htim_ip);
		ReloadBrakeOn(asse1);//BrakeOn(asse1);
		ReloadBrakeOn(asse2);//BrakeOn(asse2);
		stop_timer = timer_TimerSet(0);
		//DbgPrintf(FORCE_DEBUG, "Power off error: m1 %d, m2 %d\r\n", duet_object[AXIS_SLIDER1].motor_stat, duet_object[AXIS_SLIDER2].motor_stat );
		duet_object[AXIS_SLIDER1].motion_stat = MOT_STOPPING;
		duet_object[AXIS_SLIDER2].motion_stat = MOT_STOPPING;
		restart_sync();
		if(StartMotion2<SAMPLECOUPLE_BEFORE_SYNC)
			StartMotion2=SAMPLECOUPLE_BEFORE_SYNC;
		return;
	}

	//Controllo che lo stato del motore sia ready, or if in MOT_CRUSHING_ERROR -> deceleration
	if(!service_state)
	{
		//If there are a collision, stop motor
		if(duet_object[AXIS_SLIDER1].motor_stat == MOT_CRUSHING_ERROR || duet_object[AXIS_SLIDER2].motor_stat == MOT_CRUSHING_ERROR){
			
		} else if( (duet_object[AXIS_SLIDER1].motor_stat != MOT_READY) ||  (duet_object[AXIS_SLIDER2].motor_stat != MOT_READY) )
		{
			//tc_stop(TC0, 1);
			HAL_TIM_Base_Stop_IT(&htim_ip);
			ReloadBrakeOn(asse1);//BrakeOn(asse1);
			ReloadBrakeOn(asse2);//BrakeOn(asse2);
			stop_timer = timer_TimerSet(0);
			//DbgPrintf(FORCE_DEBUG, "Motor state error: m1 %d, m2 %d\r\n", duet_object[AXIS_SLIDER1].motor_stat, duet_object[AXIS_SLIDER2].motor_stat );
			duet_object[AXIS_SLIDER1].motion_stat = MOT_STOPPING;
			duet_object[AXIS_SLIDER2].motion_stat = MOT_STOPPING;
			restart_sync();
			if(StartMotion2<SAMPLECOUPLE_BEFORE_SYNC)
				StartMotion2=SAMPLECOUPLE_BEFORE_SYNC;
			return;
		}
	}
	
	if(syncro_stop.enable){
					float new_Ta=Ta/(float)syncro_stop.ratio;
					float new_Ta2=Ta2/(float)syncro_stop.ratio;
					tt = T - t_1;
					S = s_1 + (v_1*new_Ta)*(tt/(2.0*new_Ta) - (1/(2.0*PI)) * sin((2.0*PI*(tt+new_Ta))/(2.0*new_Ta)));
					V = v_1 - (v_1/2.0) *(1-cos( (2.0*PI*tt)/(2.0*new_Ta) ) );
					A = -((2.0*PI*v_1*new_Ta)/(pow((2.0*new_Ta),2.0))) * sin((2.0*PI*tt)/(2.0*new_Ta) );

					T += f_delta_T;
					s_0 = S;
					step_position = (int)S * segno;
					
					tt2 = T2 - t_12;
					S2 = s_12 + (v_12*new_Ta2)*(tt2/(2.0*new_Ta2) - (1/(2.0*PI)) * sin((2.0*PI*(tt2+new_Ta2))/(2.0*new_Ta2)));
					V2 = v_12 - (v_12/2.0) *(1-cos( (2.0*PI*tt2)/(2.0*new_Ta2) ) );
					A2 = -((2.0*PI*v_12*new_Ta2)/(pow((2.0*new_Ta2),2.0))) * sin((2.0*PI*tt2)/(2.0*new_Ta2) );

					T2 += f_delta_T;
					s_02 = S2;
					step_position2 = (int)S2 * segno2;
					syncro_stop.count--;
					if(/*syncro_stop.count==0 || */(V<=3000 && V2<=3000)){
//						tc_stop(TC0, 1);
						HAL_TIM_Base_Stop_IT(&htim_ip);
						stop_timer = timer_TimerSet(0);
						//DbgPrintf(FORCE_DEBUG, "Stop Time %d Error %d\r\n", stop_timer - start_timer,(duet_object[asse1].actual_position - start_position) - (duet_object[asse2].actual_position - start_position2) );
						ReloadBrakeOn(asse1);//BrakeOn(asse1);
						ReloadBrakeOn(asse2);//BrakeOn(asse2);
						restart_sync();
						if(StartMotion2<SAMPLECOUPLE_BEFORE_SYNC)
							StartMotion2=SAMPLECOUPLE_BEFORE_SYNC;
					}
	}else{	
		static_target--;
		// traiettoria con rampe do accelerazione sinusoidali
		if(static_target<0){
			static_target=-1;
			////////////////////Target 1
			if(S < Sa)					//Accelerazione
			{
				t_0 = T;
				t_1 = T;
				S = (VEL*Ta)*(T/(2.0*Ta) - (1/(2.0*PI)) * sin((2.0*PI*T)/(2.0*Ta)));
				V = (VEL/2.0) *(1-cos( (2.0*PI*T)/(2.0*Ta) ) );
				A = ((2.0*PI*VEL*Ta)/(pow((2.0*Ta),2))) * sin((2.0*PI*T)/(2.0*Ta) );
				s_0 = S;
			  s_1 = S;
			  v_1 = V;
				T += f_delta_T;
			}
			else if(S <= (s_totale - Sa))		//Velocita costante
			{
			  t_1 = T;
			  S = s_0 + V * (T - t_0);
			  T += f_delta_T;
			  s_1 = S;
			  v_1 = V;
			}
			else if(S < s_totale)				//Decelerazione
			{
				tt = T - t_1;
				S = s_1 + (v_1*Ta)*(tt/(2.0*Ta) - (1/(2.0*PI)) * sin((2.0*PI*(tt+Ta))/(2.0*Ta)));
				V = v_1 - (v_1/2.0) *(1-cos( (2.0*PI*tt)/(2.0*Ta) ) );
				A = -((2.0*PI*v_1*Ta)/(pow((2.0*Ta),2.0))) * sin((2.0*PI*tt)/(2.0*Ta) );

				T += f_delta_T;
				s_0 = S;
			}
			////////////////////Target 2
			if(S2 < Sa2)					//Accelerazione
			{
				t_02 = T2;
				t_12 = T2;
				S2 = (VEL2*Ta2)*(T2/(2.0*Ta2) - (1/(2.0*PI)) * sin((2.0*PI*T2)/(2.0*Ta2)));
				V2 = (VEL2/2.0) *(1-cos( (2.0*PI*T2)/(2.0*Ta2) ) );
				A2 = ((2.0*PI*VEL2*Ta2)/(pow((2.0*Ta2),2))) * sin((2.0*PI*T2)/(2.0*Ta2) );
				s_02 = S2;
				s_12 = S2;
				v_12 = V2;
				T2 += f_delta_T;
			}
			else if(S2 <= (s_totale2 - Sa2))		//Velocita costante
			{
				t_12 = T2;
				S2 = s_02 + V2 * (T2 - t_02);
				T2 += f_delta_T;
				s_12 = S2;
				v_12 = V2;
			}
			else if(S2 < s_totale2)				//Decelerazione
			{
				tt2 = T2 - t_12;
				S2 = s_12 + (v_12*Ta2)*(tt2/(2.0*Ta2) - (1/(2.0*PI)) * sin((2.0*PI*(tt2+Ta2))/(2.0*Ta2)));
				V2 = v_12 - (v_12/2.0) *(1-cos( (2.0*PI*tt2)/(2.0*Ta2) ) );
				A2 = -((2.0*PI*v_12*Ta2)/(pow((2.0*Ta2),2.0))) * sin((2.0*PI*tt2)/(2.0*Ta2) );

				T2 += f_delta_T;
				s_02 = S2;
			}
			

			// stop per fine movimento
			if( (S >= s_totale && S2 >= s_totale2) || (S < s_0 || S2 < s_02) )
			{
				S = s_totale;
				S2 = s_totale2;
				fix_target++;
				if(fix_target>FIX_TARGET_TIMES){
//					tc_stop(TC0, 1);
					HAL_TIM_Base_Stop_IT(&htim_ip);
					ReloadBrakeOn(asse1);//BrakeOn(asse1);
					ReloadBrakeOn(asse2);//BrakeOn(asse2);
					fix_target=FIX_TARGET_TIMES;
					stop_timer = timer_TimerSet(0);
					//DbgPrintf(FORCE_DEBUG, "Time %d Error %d\r\n", stop_timer - start_timer,(duet_object[asse1].actual_position - start_position) - (duet_object[asse2].actual_position - start_position2) );
					restart_sync();
					interp_end=1;
					if(StartMotion2<SAMPLECOUPLE_BEFORE_SYNC)
						StartMotion2=SAMPLECOUPLE_BEFORE_SYNC;
				}
			}

			i++;
			step_position = (int)S * segno;
			step_position2 = (int)S2 * segno2;
		}else{
			step_position=0;	//Rimani fisso per PRELIMINARY_SAMPLES campioni
			step_position2=0;	//Rimani fisso per PRELIMINARY_SAMPLES campioni
		}
	}
	//Controllo errori
	{
//		int32_t pos_err = (duet_object[asse1].actual_position - start_position) - (duet_object[asse2].actual_position - start_position2);
		int32_t pos_err = (duet_object[asse1].actual_position) - (duet_object[asse2].actual_position);
//		int32_t pos_start_diff = abs(start_position-start_position2);
//		int32_t pos_err = (duet_object[asse1].actual_position) - (duet_object[asse2].actual_position);
		int32_t diff_time = abs(duet_object[asse1].mis_warn_bits - duet_object[asse2].mis_warn_bits);
		
		////DbgPrintf(true, "Diff Pos = %d asse1=%d asse2=%d\r\n", pos_err, duet_object[asse1].actual_position,duet_object[asse2].actual_position);
		if(((pos_err > max_err) || (pos_err < -max_err)) && diff_time<=4*(delta_T))
		{
//			if(pos_start_diff<max_err){
				duet_object[AXIS_SLIDER1].motion_stat = MOT_STOPPING;
				duet_object[AXIS_SLIDER2].motion_stat = MOT_STOPPING;
				duet_object[AXIS_SLIDER1].motor_stat = MOT_SLIDER_NOT_ALLIGN;
				duet_object[AXIS_SLIDER2].motor_stat = MOT_SLIDER_NOT_ALLIGN;
				
				//DbgPrintf(FORCE_DEBUG, "Initial Pos axis%d = %d\r\n", asse1,(int)start_position);
				//DbgPrintf(FORCE_DEBUG, "Initial Pos axis%d = %d\r\n", asse2,(int)start_position2);
				//DbgPrintf(FORCE_DEBUG, "Actual Pos axis%d = %d\r\n", asse1,duet_object[asse1].actual_position);
				//DbgPrintf(FORCE_DEBUG, "Actual Pos axis%d = %d\r\n", asse2,duet_object[asse2].actual_position);

				//DbgPrintf(FORCE_DEBUG, "Stop: sync error %d diff_time=%d\r\n", pos_err,diff_time);
				restart_sync();
				return;
//			}
		}
		
	}
	
	//Invio valori
	{
		volatile int tmp = step_position + start_position;
		if((int)((int)timer_TimerSet(0)-(int)last_syncro_Sendtimer)>(delta_T+3))
			last_syncro_Sendtimer++;
		last_syncro_Sendtimer=timer_TimerSet(0);
		last_sample[index_sample].time=last_syncro_Sendtimer;
		last_sample[index_sample].sample= step_position + start_position;;
		index_sample=(index_sample+1)%10;
		micro_SendSync();
		//micro_SendSync();
		if(StartMotion2<SAMPLECOUPLE_BEFORE_SYNC){
			StartMotion2++;
		}else{
			StartMotion2++;
			StartMotion2--;			
		}
		co_pdo_Send(2, duet_object[asse1].node_id, (uint8_t*)&tmp, 4);

		volatile int tmp2 = step_position2 + start_position2;
		co_pdo_Send1(2, duet_object[asse2].node_id, (uint8_t*)&tmp2, 4);

		//DbgPrintf(FORCE_DEBUG, "axis%d new_position %d tmp%d\r\n",asse1, (int)tmp,timer_TimerSet(0));
		//DbgPrintf(FORCE_DEBUG, "axis%d new_position %d tmp%d\r\n", asse2, (int)tmp2,timer_TimerSet(0));
	}

//	ioport_set_pin_level(LED1_GPIO, LEDS_INACTIVE_LEVEL);
	HAL_GPIO_WritePin(LED1_PORT, LED1_GPIO, LEDS_INACTIVE_LEVEL);
}
#else
static volatile int tmp = 0;
void IP_Handler(void)
{
	micro_SendSync();
	co_pdo_Send(2, 1, (uint8_t*)&tmp, 4);
	tmp++;
	co_pdo_Send1(2, 9, (uint8_t*)&tmp, 4);
	tmp++;
}
#endif
