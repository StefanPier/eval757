/*
 * AntiCrushing.c
 *
 * Created: 25/01/2021 10:37:32
 *  Author: stefano
 */ 
#include "main.h"
#include "AntiCrushing.h"
#include <string.h>
#include "motors.h"
#include "timer.h"
#include "manage_debug.h"
#include "dig_io.h"
#include "duet.h"
#include "stm32h7xx_it.h"


anticrush_t anticrush_state={false,false};


void AntiCrushingInit(void){
	if( ioport_get_pin_level(IMGLS0024_CHECK_GPIO) == false){
			anticrush_state.presence=true;
			anticrush_state.enable=true;
			AntiCrushingInterruptEnable();
	}else{
			anticrush_state.enable=false;
			anticrush_state.presence=false;		
	}
}


#define RESET_CRUSH_STATE_COUNT	5
void AntiCrushingCheck(void){
	static bool first_stop=true;			//to send stop during condition occur once
	static int anti_returning_count=0;		//to count up to RESET_CRUSH_STATE_COUNT, resetting the crush state
	if( ioport_get_pin_level(IMGLS0024_ERROR1_GPIO) == false || 
			ioport_get_pin_level(IMGLS0024_ERROR2_GPIO) == false ){
		anticrush_state.alarm=true;
		duet_SetMotorStatus(AXIS_SLIDER1, MOT_CRUSHING_ERROR);
		duet_SetMotorStatus(AXIS_SLIDER2, MOT_CRUSHING_ERROR);
		if(first_stop){
			//Se sono durante un movimento dei motori sincroni mi preoccupo che si fermino 
			//con una decelerazione pi� del normale (syncro_stop.ratio=3)
			syncro_stop.enable=true;
			syncro_stop.ratio=3;
			first_stop=false;
		}
		anti_returning_count=0;
	}else if ( ioport_get_pin_level(IMGLS0024_ERROR1_GPIO) == true &&
				ioport_get_pin_level(IMGLS0024_ERROR2_GPIO) == true ){
		//Quando mi si disingaggia il sensore anticrushing disabilito l'allarme stesso
		if(anti_returning_count++>=RESET_CRUSH_STATE_COUNT){
			anti_returning_count=RESET_CRUSH_STATE_COUNT+1;
			if(anticrush_state.alarm==true)
				anticrush_state.alarm=false;
			if(first_stop==false)
				first_stop=true;	//Reset condition for a new crushing detect
		}
	}
}


//Enable / Disable Anti crushing error 
void CrushingEnable(uint8_t en){
	if(en){
		anticrush_state.enable=true;
	}else{
		AntiCrushingInterruptDisable();
		anticrush_state.enable=false;	
		anticrush_state.alarm=false;
	}
}

static void CrushingInt (uint32_t a, uint32_t b)
{
	AntiCrushingCheck();
}

void AntiCrushingInterruptDisable (void)
{	
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	IMGLS0024_ERROR1_CLK;
    GPIO_InitStruct.Pin = IMGLS0024_ERROR1_GPIO;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(IMGLS0024_ERROR1_PORT, &GPIO_InitStruct);

    DisableEXTI(EXTI4_IRQn);

    /*
	pio_disable_interrupt(PIOA, (1<<IMGLS0024_ERROR1_GPIO) | (1<<IMGLS0024_ERROR2_GPIO));
	
	NVIC_DisableIRQ(PIOA_IRQn);	
	*/
}

uint8_t AntiCrushingInterruptEnable (void)
{
	volatile uint8_t res=RES_FAIL, res1 = RES_FAIL;
/*
	ioport_set_pin_dir(IMGLS0024_ERROR1_GPIO, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(IMGLS0024_ERROR1_GPIO, PIO_DEGLITCH | PIO_DEBOUNCE);
	ioport_set_pin_sense_mode(IMGLS0024_ERROR1_GPIO, IOPORT_SENSE_BOTHEDGES);
*/
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	//PB5
	IMGLS0024_ERROR1_CLK;
    GPIO_InitStruct.Pin = IMGLS0024_ERROR1_GPIO;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(IMGLS0024_ERROR1_PORT, &GPIO_InitStruct);

    /*
	ioport_set_pin_dir(IMGLS0024_ERROR2_GPIO, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(IMGLS0024_ERROR2_GPIO, PIO_DEGLITCH | PIO_DEBOUNCE);
	ioport_set_pin_sense_mode(IMGLS0024_ERROR2_GPIO, IOPORT_SENSE_BOTHEDGES);
	*/
    //PG11
	IMGLS0024_ERROR2_CLK;
    GPIO_InitStruct.Pin = IMGLS0024_ERROR2_GPIO;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(IMGLS0024_ERROR2_PORT, &GPIO_InitStruct);

    /*
	res=pio_handler_set(PIOA, ID_PIOA, (1<<IMGLS0024_ERROR1_GPIO), INTERRUPT_EDGE_SENSE_T, CrushingInt)?RES_FAIL:RES_SUCCESS;
	res1=pio_handler_set(PIOA, ID_PIOA, (1<<IMGLS0024_ERROR2_GPIO), INTERRUPT_EDGE_SENSE_T, CrushingInt)?RES_FAIL:RES_SUCCESS;
	pio_enable_interrupt(PIOA, (1<<IMGLS0024_ERROR1_GPIO) | (1<<IMGLS0024_ERROR2_GPIO));	
	*/


    //	NVIC_EnableIRQ(PIOA_IRQn);

    HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);	//PB5	IMGLS0024_ERROR2_GPIO
    HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);	//PG11	IMGLS0024_ERROR2_GPIO


	//Nessun errore
	return RES_SUCCESS;
}


