/*
 * linact.c
 *
 * Created: 04/02/2018 15:53:46
 *  Author: Alessandro
 */ 
#include "main.h"
#include "linact.h"
#include "timer.h"
#include "manage_debug.h"
//#include "adconv.h"
#include "dig_io.h"

#define LINACT_HALL_TIMEOUT	500

#define LINACT_ACTIVE_IO	GPIO_PIN_SET
#define LINACT_INACTIVE_IO	GPIO_PIN_RESET

// stati di movimento
#define LINACT_STOP		0
#define LINACT_STARTING	1
#define LINACT_FOREWARD	2
#define LINACT_REVERSE	3

/// Array of motor object structures
static motor_obj_t * linact_object;

static void linact_Stop(int m_idx);
static void linact_InitTiming(void);

void linact_InitData(motor_obj_t * m_obj)
{
	linact_object = m_obj;
	linact_InitTiming();
}

uint32_t linact_Init(int axis, uint8_t node)
{
	
	linact_object[axis].initialized = 1;
	linact_object[axis].motion_stat = LINACT_STOP;
	linact_object[axis].pulse_counter = 0;
	linact_object[axis].motor_stat = MOT_READY;
	linact_object[axis].actual_position = 0;

	return RES_SUCCESS;
}

uint32_t linact_Home(int axis)
{
	linact_SetPosition(axis, 0);
	
	return RES_SUCCESS;
}

uint32_t linact_ResetPosition(int axis, int position)
{
#ifdef LINACT_HALL
	linact_object[axis].pulse_counter = position;
	linact_object[axis].actual_position = linact_object[axis].pulse_counter;
	
	return RES_SUCCESS;
#else	
	return RES_FAIL;
#endif	
}

void linact_Count(void)
{
	switch(linact_object[AXIS_ACT1].motion_stat)
	{
		case 2: // avanti
		linact_object[AXIS_ACT1].actual_position = linact_object[AXIS_ACT1].pulse_counter++;
		//DbgPrintf(DBG_LINACT, "request %d, actual %d\n\r",  linact_object[m_idx].requested_position, linact_object[m_idx].actual_position);
		if((linact_object[AXIS_ACT1].actual_position >= linact_object[AXIS_ACT1].requested_position) || (!digio_GetPowerStatus()) )
		{
			linact_Stop(AXIS_ACT1);
			//DbgPrintf(DBG_LINACT, "act %d %d\n\r",  AXIS_ACT1, linact_object[AXIS_ACT1].actual_position);
		}
		linact_object[AXIS_ACT1].heartbeat_timer = timer_TimerSet(LINACT_HALL_TIMEOUT);  // timeout movimento
		break;
		case 3: // indietro
		linact_object[AXIS_ACT1].actual_position = linact_object[AXIS_ACT1].pulse_counter--;
		//DbgPrintf(DBG_LINACT, "request %d, actual %d\n\r",  linact_object[m_idx].requested_position, linact_object[m_idx].actual_position);
		if( (linact_object[AXIS_ACT1].actual_position <= linact_object[AXIS_ACT1].requested_position) || (!digio_GetPowerStatus()) )
		{
			linact_Stop(AXIS_ACT1);
			//DbgPrintf(DBG_LINACT, "act %d %d\n\r",  AXIS_ACT1, linact_object[AXIS_ACT1].actual_position);
		}
		linact_object[AXIS_ACT1].heartbeat_timer = timer_TimerSet(LINACT_HALL_TIMEOUT);  // timeout movimento
		break;
	}


	switch(linact_object[AXIS_ACT2].motion_stat)
	{
		case 2: // avanti
		linact_object[AXIS_ACT2].actual_position = linact_object[AXIS_ACT2].pulse_counter++;
		//DbgPrintf(DBG_LINACT, "request %d, actual %d\n\r",  linact_object[m_idx].requested_position, linact_object[m_idx].actual_position);
		if( (linact_object[AXIS_ACT2].actual_position >= linact_object[AXIS_ACT2].requested_position) || (!digio_GetPowerStatus()) )
		{
			linact_Stop(AXIS_ACT2);
			//DbgPrintf(DBG_LINACT, "act %d %d\n\r",  AXIS_ACT2, linact_object[AXIS_ACT2].actual_position);
		}
		linact_object[AXIS_ACT2].heartbeat_timer = timer_TimerSet(LINACT_HALL_TIMEOUT);  // timeout movimento
		break;
		case 3: // indietro
		linact_object[AXIS_ACT2].actual_position = linact_object[AXIS_ACT2].pulse_counter--;
		//DbgPrintf(DBG_LINACT, "request %d, actual %d\n\r",  linact_object[m_idx].requested_position, linact_object[m_idx].actual_position);
		if( (linact_object[AXIS_ACT2].actual_position <= linact_object[AXIS_ACT2].requested_position) || (!digio_GetPowerStatus()) )
		{
			linact_Stop(AXIS_ACT2);
			//DbgPrintf(DBG_LINACT, "act %d %d\n\r",  AXIS_ACT2, linact_object[AXIS_ACT2].actual_position);
		}
		linact_object[AXIS_ACT2].heartbeat_timer = timer_TimerSet(LINACT_HALL_TIMEOUT);  // timeout movimento
		break;
	}
		
}

// utilizzata nel main loop per leggere la posizione quando i motori sono fermi
void linact_GetPosition(void)
{
	// se questo asse ha un motore Linak ...
	if(linact_object[AXIS_ACT1].motor_model == MOT_LINAK)
	{
		if(linact_object[AXIS_ACT1].motion_stat == 0)
#ifdef LINACT_0_10
			linact_object[AXIS_ACT1].actual_position = adconv_GetMedia(IN_0_10_0);
#endif		
#ifdef LINACT_4_20
			linact_object[AXIS_ACT1].actual_position = adconv_GetMedia(IN_4_20_0);
#endif
#ifdef LINACT_HALL
			linact_object[AXIS_ACT1].actual_position = linact_object[AXIS_ACT1].pulse_counter;
#endif		
		if(linact_object[AXIS_ACT1].heartbeat_timer != 0)
		{
			if(timer_TimerTest(linact_object[AXIS_ACT1].heartbeat_timer))
			{
				linact_object[AXIS_ACT1].motion_stat = 0;
				linact_object[AXIS_ACT1].heartbeat_timer = 0;
				linact_Stop(AXIS_ACT1);
			}
		}
	}

	// se questo asse ha un motore Linak ...
	if(linact_object[AXIS_ACT2].motor_model == MOT_LINAK)
	{
		if(linact_object[AXIS_ACT2].motion_stat == 0)
#ifdef LINACT_0_10
			linact_object[AXIS_ACT2].actual_position = adconv_GetMedia(IN_0_10_1);
#endif
#ifdef LINACT_4_20
			linact_object[AXIS_ACT2].actual_position = adconv_GetMedia(IN_4_20_1);
#endif
#ifdef LINACT_HALL
			linact_object[AXIS_ACT2].actual_position = linact_object[AXIS_ACT2].pulse_counter;
#endif

		if(linact_object[AXIS_ACT2].heartbeat_timer != 0)
		{
			if(timer_TimerTest(linact_object[AXIS_ACT2].heartbeat_timer))
			{
				linact_object[AXIS_ACT2].motion_stat = 0;
				linact_object[AXIS_ACT2].heartbeat_timer = 0;
				linact_Stop(AXIS_ACT2);
			}
		}
	}

#ifdef LINACT_0_10
	adconv_ConvertAfec0();	
#endif		

#ifdef LINACT_4_20
	adconv_ConvertAfec1();
#endif
}

uint32_t linact_SetPosition(int axis, uint32_t new_position)
{
	if(!digio_GetPowerStatus())
		return RES_FAIL;
		
	if(axis == AXIS_ACT1)
	{
		linact_object[AXIS_ACT1].requested_position = new_position;
		if(linact_object[AXIS_ACT1].requested_position < linact_object[AXIS_ACT1].actual_position)
		{
			// vado indietro
			//DbgPrintf(DBG_LINACT, "Vado indietro \r\n");
			linact_object[AXIS_ACT1].motion_stat = 3;

			linact_object[AXIS_ACT1].heartbeat_timer = timer_TimerSet(LINACT_HALL_TIMEOUT);  // timeout movimento	

//			ioport_set_pin_level(ACT1_FWD_IO, LINACT_INACTIVE_IO);
//			ioport_set_pin_level(ACT1_REV_IO, LINACT_ACTIVE_IO);
			HAL_GPIO_WritePin(ACT1_FWD_PORT, ACT1_FWD_GPIO, LINACT_INACTIVE_IO);
			HAL_GPIO_WritePin(ACT1_REV_PORT, ACT1_REV_GPIO, LINACT_ACTIVE_IO);
		}
		else if(linact_object[AXIS_ACT1].requested_position > linact_object[AXIS_ACT1].actual_position)
		{
			// vado avanti
			//DbgPrintf(DBG_LINACT, "Vado avanti \r\n");
			linact_object[AXIS_ACT1].motion_stat = 2;

			linact_object[AXIS_ACT1].heartbeat_timer = timer_TimerSet(LINACT_HALL_TIMEOUT);  // timeout movimento	

//			ioport_set_pin_level(ACT1_REV_IO, LINACT_INACTIVE_IO);
//			ioport_set_pin_level(ACT1_FWD_IO, LINACT_ACTIVE_IO);
			HAL_GPIO_WritePin(ACT1_FWD_PORT, ACT1_FWD_GPIO, LINACT_ACTIVE_IO);
			HAL_GPIO_WritePin(ACT1_REV_PORT, ACT1_REV_GPIO, LINACT_INACTIVE_IO);
		}
	}
	else if(axis == AXIS_ACT2)
	{
		linact_object[AXIS_ACT2].requested_position = new_position;
		if(linact_object[AXIS_ACT2].requested_position < linact_object[AXIS_ACT2].actual_position)
		{
			// vado indietro
			//DbgPrintf(DBG_LINACT, "Vado indietro \r\n");
			linact_object[AXIS_ACT2].motion_stat = 3;

//			ioport_set_pin_level(ACT2_FWD_IO, LINACT_INACTIVE_IO);
//			ioport_set_pin_level(ACT2_REV_IO, LINACT_ACTIVE_IO);
			HAL_GPIO_WritePin(ACT2_FWD_PORT, ACT2_FWD_GPIO, LINACT_INACTIVE_IO);
			HAL_GPIO_WritePin(ACT2_REV_PORT, ACT2_REV_GPIO, LINACT_ACTIVE_IO);

			linact_object[AXIS_ACT2].heartbeat_timer = timer_TimerSet(LINACT_HALL_TIMEOUT);  // timeout movimento
		}
		else if(linact_object[AXIS_ACT2].requested_position > linact_object[AXIS_ACT2].actual_position)
		{
			// vado avanti
			//DbgPrintf(DBG_LINACT, "Vado avanti \r\n");
			linact_object[AXIS_ACT2].motion_stat = 2;

//			ioport_set_pin_level(ACT2_REV_IO, LINACT_INACTIVE_IO);
//			ioport_set_pin_level(ACT2_FWD_IO, LINACT_ACTIVE_IO);
			HAL_GPIO_WritePin(ACT2_FWD_PORT, ACT2_FWD_GPIO, LINACT_ACTIVE_IO);
			HAL_GPIO_WritePin(ACT2_REV_PORT, ACT2_REV_GPIO, LINACT_INACTIVE_IO);

			linact_object[AXIS_ACT2].heartbeat_timer = timer_TimerSet(LINACT_HALL_TIMEOUT);  // timeout movimento
		}
	}
	else
		return RES_FAIL;

//	linact_object[axis].motion_stat = 1;

#ifndef LINACT_HALL
	/* Start timer. */
	if(axis == AXIS_ACT1)
		tc_start(TC2, 0);
	else
		tc_start(TC2, 1);
#endif
	
	return RES_SUCCESS;
}

uint32_t linact_GoJogFwd(int axis)
{
	return linact_SetPosition(axis, LINACT_MAX_POS);
}

uint32_t linact_GoJogRev(int axis)
{
	return linact_SetPosition(axis, LINACT_MIN_POS);
}

uint32_t linact_GetMotorActPos(int axis, int32_t* pos)
{
	uint32_t result;
	
	if(linact_object[axis].node_id != 0)  // se � inizializzato
	{
		*pos = linact_object[axis].actual_position;
		result = RES_SUCCESS;
	}
	else
	{
		*pos = 0;
		result = RES_FAIL;
	}
	
	return result;
}

uint32_t linact_StopHere(int axis)
{
	linact_Stop(axis);

	return RES_SUCCESS;
}

// AXIS_ACT1, lettino
void TC6_Handler(void)
{
	volatile uint32_t ul_dummy;

//	ioport_set_pin_level(LED1_GPIO, LEDS_ACTIVE_LEVEL);
	HAL_GPIO_WritePin(LED1_PORT, LED1_GPIO, LEDS_ACTIVE_LEVEL);

	/* Remove warnings. */
	ul_dummy++;

#ifdef LINACT_0_10
	linact_object[AXIS_ACT1].actual_position = adconv_GetMedia(IN_0_10_0);
#endif
#ifdef LINACT_4_20
	linact_object[AXIS_ACT1].actual_position = adconv_GetMedia(IN_4_20_0);
#endif
#ifdef LINACT_HALL
	linact_object[AXIS_ACT1].actual_position = linact_object[AXIS_ACT1].pulse_counter;
#endif


	switch(linact_object[AXIS_ACT1].motion_stat)
	{
		case 1:  // stato di decisione della direzione
			DbgPrintf(DBG_LINACT, "act %d %d\n\r",  AXIS_ACT1, linact_object[AXIS_ACT1].actual_position);

			if(linact_object[AXIS_ACT1].requested_position < linact_object[AXIS_ACT1].actual_position)
			{
				// vado indietro
				DbgPrintf(DBG_LINACT, "Go Back\r\n");
				linact_object[AXIS_ACT1].motion_stat = 3;
	
//				ioport_set_pin_level(ACT1_FWD_IO, LINACT_INACTIVE_IO);
//				ioport_set_pin_level(ACT1_REV_IO, LINACT_ACTIVE_IO);
				HAL_GPIO_WritePin(ACT1_FWD_PORT, ACT1_FWD_GPIO, LINACT_INACTIVE_IO);
				HAL_GPIO_WritePin(ACT1_REV_PORT, ACT1_REV_GPIO, LINACT_ACTIVE_IO);
			}
			else if(linact_object[AXIS_ACT1].requested_position > linact_object[AXIS_ACT1].actual_position)
			{
				// vado avanti
				DbgPrintf(DBG_LINACT, "Go Forward\r\n");
				linact_object[AXIS_ACT1].motion_stat = 2;

//				ioport_set_pin_level(ACT1_REV_IO, LINACT_INACTIVE_IO);
//				ioport_set_pin_level(ACT1_FWD_IO, LINACT_ACTIVE_IO);
				HAL_GPIO_WritePin(ACT1_FWD_PORT, ACT1_FWD_GPIO, LINACT_ACTIVE_IO);
				HAL_GPIO_WritePin(ACT1_REV_PORT, ACT1_REV_GPIO, LINACT_INACTIVE_IO);
			}
			break;
		case 2: // avanti
			//DbgPrintf(DBG_LINACT, "request %d, actual %d\n\r",  linact_object[m_idx].requested_position, linact_object[m_idx].actual_position);
			if((linact_object[AXIS_ACT1].actual_position >= linact_object[AXIS_ACT1].requested_position) || (!digio_GetPowerStatus()) )
			{
				linact_Stop(AXIS_ACT1);
				DbgPrintf(DBG_LINACT, "act %d %d\n\r",  AXIS_ACT1, linact_object[AXIS_ACT1].actual_position);
			}
			break;
		case 3: // indietro
			//DbgPrintf(DBG_LINACT, "request %d, actual %d\n\r",  linact_object[m_idx].requested_position, linact_object[m_idx].actual_position);
			if( (linact_object[AXIS_ACT1].actual_position <= linact_object[AXIS_ACT1].requested_position) || (!digio_GetPowerStatus()) )
			{
				linact_Stop(AXIS_ACT1);
				DbgPrintf(DBG_LINACT, "act %d %d\n\r",  AXIS_ACT1, linact_object[AXIS_ACT1].actual_position);
			}
//#ifdef LINACT_HALL
			//else if(timer_TimerTest(linak_tout))
			//{
				//linact_Stop(AXIS_ACT1);
				//DbgPrintf(DBG_LINACT, "act %d timeout stop, %d\n\r",  AXIS_ACT1, linact_object[AXIS_ACT1].actual_position);
			//}
//#endif			
			break;
	}

	ioport_set_pin_level(LED1_GPIO, LEDS_INACTIVE_LEVEL);
}

// AXIS_ACT2, pannello
void TC7_Handler(void)
{
	volatile uint32_t ul_dummy;

	/* Remove warnings. */
	ul_dummy++;

	//ioport_toggle_pin_level(LED1_GPIO);
	ioport_set_pin_level(LED1_GPIO, LEDS_ACTIVE_LEVEL);

#ifdef LINACT_0_10
	linact_object[AXIS_ACT2].actual_position = adconv_GetMedia(IN_0_10_1);
#endif
#ifdef LINACT_4_20
	linact_object[AXIS_ACT2].actual_position = adconv_GetMedia(IN_4_20_1);
#endif
#ifdef LINACT_HALL
	linact_object[AXIS_ACT2].actual_position = linact_object[AXIS_ACT2].pulse_counter;
#endif

	
	switch(linact_object[AXIS_ACT2].motion_stat)
	{
		case 1:  // stato di decisione della direzione
		DbgPrintf(DBG_LINACT, "act %d %d\n\r",  AXIS_ACT2, linact_object[AXIS_ACT2].actual_position);

		if(linact_object[AXIS_ACT2].requested_position < linact_object[AXIS_ACT2].actual_position)
		{
			// vado indietro
			DbgPrintf(DBG_LINACT, "Go Back\r\n");
			linact_object[AXIS_ACT2].motion_stat = 3;

//			ioport_set_pin_level(ACT2_FWD_IO, LINACT_INACTIVE_IO);
//			ioport_set_pin_level(ACT2_REV_IO, LINACT_ACTIVE_IO);
			HAL_GPIO_WritePin(ACT2_FWD_PORT, ACT2_FWD_GPIO, LINACT_INACTIVE_IO);
			HAL_GPIO_WritePin(ACT2_REV_PORT, ACT2_REV_GPIO, LINACT_ACTIVE_IO);
		}
		else if(linact_object[AXIS_ACT2].requested_position > linact_object[AXIS_ACT2].actual_position)
		{
			// vado avanti
			DbgPrintf(DBG_LINACT, "Go Forward\r\n");
			linact_object[AXIS_ACT2].motion_stat = 2;

//			ioport_set_pin_level(ACT2_REV_IO, LINACT_INACTIVE_IO);
//			ioport_set_pin_level(ACT2_FWD_IO, LINACT_ACTIVE_IO);
			HAL_GPIO_WritePin(ACT2_FWD_PORT, ACT2_FWD_GPIO, LINACT_ACTIVE_IO);
			HAL_GPIO_WritePin(ACT2_REV_PORT, ACT2_REV_GPIO, LINACT_INACTIVE_IO);
		}
		break;
		case 2: // avanti
		//DbgPrintf(DBG_LINACT, "request %d, actual %d\n\r",  linact_object[m_idx].requested_position, linact_object[m_idx].actual_position);
		if( (linact_object[AXIS_ACT2].actual_position >= linact_object[AXIS_ACT2].requested_position) || (!digio_GetPowerStatus()) )
		{
			linact_Stop(AXIS_ACT2);
			DbgPrintf(DBG_LINACT, "act %d %d\n\r",  AXIS_ACT2, linact_object[AXIS_ACT2].actual_position);
		}
		break;
		case 3: // indietro
		//DbgPrintf(DBG_LINACT, "request %d, actual %d\n\r",  linact_object[m_idx].requested_position, linact_object[m_idx].actual_position);
		if( (linact_object[AXIS_ACT2].actual_position <= linact_object[AXIS_ACT2].requested_position) || (!digio_GetPowerStatus()) )
		{
			linact_Stop(AXIS_ACT2);
			DbgPrintf(DBG_LINACT, "act %d %d\n\r",  AXIS_ACT2, linact_object[AXIS_ACT2].actual_position);
		}
//#ifdef LINACT_HALL
			//else if(timer_TimerTest(linak_tout))
			//{
				//linact_Stop(AXIS_ACT2);
				//DbgPrintf(DBG_LINACT, "act %d timeout stop, %d\n\r",  AXIS_ACT2, linact_object[AXIS_ACT2].actual_position);
			//}
//#endif			
		
		break;
	}
	ioport_set_pin_level(LED1_GPIO, LEDS_INACTIVE_LEVEL);
}

static void linact_Stop(int m_idx)
{
#ifndef LINACT_HALL	
	/* Stop timer. */
	tc_stop(TC2, 0);
	tc_stop(TC2, 1);
#endif	

//	ioport_set_pin_level(LED1_GPIO, LEDS_INACTIVE_LEVEL);
	HAL_GPIO_WritePin(LED1_PORT, LED1_GPIO, LEDS_INACTIVE_LEVEL);
	
	if(m_idx == AXIS_ACT1)
	{
//		ioport_set_pin_level(ACT1_FWD_IO, LINACT_INACTIVE_IO);
//		ioport_set_pin_level(ACT1_REV_IO, LINACT_INACTIVE_IO);
		HAL_GPIO_WritePin(ACT1_FWD_PORT, ACT1_FWD_GPIO, LINACT_INACTIVE_IO);
		HAL_GPIO_WritePin(ACT1_REV_PORT, ACT1_REV_GPIO, LINACT_INACTIVE_IO);
	}
	else
	{
//		ioport_set_pin_level(ACT2_FWD_IO, LINACT_INACTIVE_IO);
//		ioport_set_pin_level(ACT2_REV_IO, LINACT_INACTIVE_IO);
		HAL_GPIO_WritePin(ACT2_FWD_PORT, ACT2_FWD_GPIO, LINACT_INACTIVE_IO);
		HAL_GPIO_WritePin(ACT2_REV_PORT, ACT2_REV_GPIO, LINACT_INACTIVE_IO);
	}
	//DbgPrintf(DBG_LINACT, "Stop \r\n");
	//DbgPrintf(DBG_LINACT, "act %d %d\n\r", m_idx, linact_object[m_idx].actual_position);
//#ifndef LINACT_HALL
	//linact_object[m_idx].motion_stat = 0;
//#endif	

}

#define LINACT_TIMING_FREQ	100		
static void linact_InitTiming(void)
{
#ifdef LINACT_HALL
	/*
	// interrup ingresso feedback motore Linak
	pio_set_input(PIOD, PIO_PD27, PIO_DEGLITCH | PIO_DEBOUNCE);
	pio_handler_set(PIOD, ID_PIOD, PIO_PD27, PIO_IT_EDGE, linact_Count);
	pio_enable_interrupt(PIOD, PIO_PD27);

	NVIC_EnableIRQ(PIOD_IRQn);
	*/
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	//DIGITIN_8, PJ15
	LINACT_HALL_IN_CLK;
    GPIO_InitStruct.Pin = LINACT_HALL_IN_GPIO;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(LINACT_HALL_IN_PORT, &GPIO_InitStruct);

#else
	
	uint32_t ul_div;
	uint32_t ul_tcclks;

	/* Configure PMC. */
	// timer 6 per il primo asse, time 7 per il secondo asse
	pmc_enable_periph_clk(ID_TC6);
	pmc_enable_periph_clk(ID_TC7);

	/* Configure TC for a 1000Hz frequency and trigger on RC compare. */
	tc_find_mck_divisor(LINACT_TIMING_FREQ, sysclk_get_main_hz(), &ul_div, &ul_tcclks, sysclk_get_main_hz());
	
	tc_init(TC2, 0, ul_tcclks | TC_CMR_CPCTRG);
	tc_write_rc(TC2, 0, (sysclk_get_main_hz() / ul_div) / LINACT_TIMING_FREQ);

	tc_init(TC2, 1, ul_tcclks | TC_CMR_CPCTRG);
	tc_write_rc(TC2, 1, (sysclk_get_main_hz() / ul_div) / LINACT_TIMING_FREQ);

	/* Configure and enable interrupt on RC compare. */
	NVIC_EnableIRQ((IRQn_Type)ID_TC6);
	NVIC_EnableIRQ((IRQn_Type)ID_TC7);
	tc_enable_interrupt(TC2, 0, TC_IER_CPCS);
	tc_enable_interrupt(TC2, 1, TC_IER_CPCS);

#endif	
}

uint32_t linact_GetMotionStatus(int axis)
{
	if(linact_object[axis].motion_stat > 0)
		return 1;
	else
		return 0;
}

uint32_t linact_GetMotorStatus(int axis)
{
	return linact_object[axis].motor_stat;
}
