/*! \file duet.c \brief General motors control module */
//*****************************************************************************
//
// File Name	: 'motors.c'
// Title		: General motors control module.
// Author		: Alessandro
// Company		: Qprel s.r.l.
// Created		: 14/10/2017
// Revised		: -
// Version		: 1.0
//
///
/// \code #include "motors.h" \endcode
/// \par Overview
///		This module provide wrapper functions to control specific motor in position profile mode 
///
//*****************************************************************************
//@{ 
#include "motors.h"
#include "duet.h"
#include "interpolate.h"
#include "mis23x.h"
/*
#include "LinAct/linact.h"
#include "Lettino/Stille.h"
*/
#include "co_main.h"
#include "co_microdep.h"
#include "co_nmt.h"
#include "co_sdo.h"
#include "co_pdo.h"
#include "dig_io.h"
#include "manage_debug.h"
#include "BrakeManagement.h"
#include "AntiCrushing.h"
#include <string.h>

/// Array of motor object structures
static motor_obj_t motor_object[MOTOR_MAXOBJ];
bool slider_enabled=false;
bool service_state = false;


void reset_Motor_HeartBeat(void){
	for (int axis=0;axis<MOTOR_MAXOBJ;axis++){
		if(motor_object[axis].initialized==1){
			motor_object[axis].heartbeat_timer = timer_TimerSet(20*MOT_MIS23x_HTB_TOUT);
		}
	}
}

/**
 * \fn void motor_InitData(void)
 * \brief Initialize the motor objects array and
 *        add the motor_Machine() function to the co_main calling list
 *
 */
void motors_InitData(void)
{
	for(int i=0;i<MOTOR_MAXOBJ;i++)
		memset(&motor_object[i], 0 , sizeof(motor_obj_t));
	
	duet_InitData(motor_object);
	mis23x_InitData(motor_object);
	linact_InitData(motor_object);	
	stille_InitData(motor_object);
	
#ifdef OLD_STYLE_COPROCESS
	p_cback_t dummyMotor;
	dummyMotor.p_cback = motor_Machine;
	dummyMotor.tipo = MOTOR_TYPE;
	co_main_AddCback(dummyMotor);
#endif
}

/**
 * \fn uint32_t motor_Init(int axis, uint8_t node)
 * \brief Initialize the motor drive
 *
 * Send the command sequence to make the motor drive in the operative state,
 * as requested by the CanOpen specifications.
 * - reset the node, and sent command to start it.
 * - sends "shutdown" command to put node in "ready to switch on" state
 * - sends "switch on" command to put node in "switched on" state
 * - sends "enable operation" command to put node in "operation enable" state
 * If the command sequence fails return 0, 1 if success
 *  
 * \param 'uint8_t node' Node id to initialize.
 * \param 'int axis' Axis (index in the motor_object array)
 * \return 1 if success, 0 if fails.
 * 
 */
uint32_t motor_Init(int axis, uint8_t node)
{
	// reset the node structure
	memset(&motor_object[axis],0,sizeof(motor_obj_t));

	// assign node ID to the object
	if(node > 0)
	{//Asse 0 gantry, , 2 lettino
		motor_object[axis].node_id = node;
		if(node > 127 && node <200) // � un motore lineare
		{
			motor_object[axis].motor_model = MOT_LINAK;
			if(linact_Init(axis, node) == 0)  // if failed
			{
				/// reset the node structure
				memset(&motor_object[axis],0,sizeof(motor_obj_t));
				return RES_FAIL;
			}
			else
				return RES_SUCCESS;
		}else if(node >=200){
			//Node 200-205,	0-5 Motori comandati dal lifter
			if(stille_Init(axis, node) == 0)  // if failed
			{
				/// reset the node structure
				memset(&motor_object[axis],0,sizeof(motor_obj_t));
				return RES_FAIL;
			}
			else
				return RES_SUCCESS;
		}
	}
	else
		return RES_FAIL;
	
	// read manufacturer code at 0x1018 sub 1
	co_msg_t msg;
	//uint32_t res = 0;
	uint8_t dt_out[8] = {0,0,0,0,0,0,0,0};
	uint8_t dt_in[8] = {0,0,0,0,0,0,0,0};
	
	// read parameter
	memset(&msg, 0, sizeof(co_msg_t));
	msg.node = node;
	msg.index = 0x1018;
	msg.subindex = 1;
	msg.r_w = 'r';

	msg.data_in = dt_in;
	msg.data_out = dt_out;
	if(!co_sdo_Send(&msg))
		return RES_FAIL;
	uint32_t val;
	memcpy(&val, &dt_in[4], sizeof(uint32_t));
	
	if(val == 0x9A)  // DUET Motorpower motor with Elmo controller
	{
		motor_object[axis].motor_model = MOT_DUET;
		if(duet_Init(axis, node) == 0)  // if failed
		{
			/// reset the node structure
			memset(&motor_object[axis],0,sizeof(motor_obj_t));
			return RES_FAIL;
		}
		else{
			// if axis is front lift dx or sx, check if both are initialized
			#ifndef AGNOSTIC_AXIS
			if((axis == AXIS_SLIDER1 && motor_object[axis].node_id == NODE_SLIDER1) 
				|| (axis == AXIS_SLIDER2 && motor_object[axis].node_id == NODE_SLIDER2))
			#else
			if((axis == AXIS_SLIDER1) 
				|| (axis == AXIS_SLIDER2))
			#endif
			{
				if(anticrush_state.alarm)
					motor_object[axis].motor_stat=MOT_CRUSHING_ERROR;
				#ifndef AGNOSTIC_AXIS
				if( (motor_object[AXIS_SLIDER1].initialized) && (motor_object[AXIS_SLIDER2].initialized) )
				#else
				if( (motor_sync1.enable) && (motor_sync2.enable) )
				#endif
					slider_enabled = true;
			}
			return RES_SUCCESS;
		}
	}
	else if(val == 0x117)  // JVL motor
	{
		motor_object[axis].motor_model = MOT_JVL;
		if(mis23x_Init(axis, node) == 0)  // if failed
		{
			/// reset the node structure
			memset(&motor_object[axis],0,sizeof(motor_obj_t));
			return RES_FAIL;
		}
		else
			return RES_SUCCESS;
	}
	else
		return RES_FAIL;
}

/**
* \fn uint32_t motor_SetMaxPosition(int axis, int32_t max_pos)
* \brief Set the max position limit for the axis
*
* This function perform the commands to set max position limit in step 
*
* \param int axis Axis number (index in the motor_object array)
* \param int32_t max_pos max position.
* \return  1 = success, 0 = error.
*
*/
uint32_t motor_SetMaxPosition(int axis, int32_t max_pos)
{
	uint32_t result;

	if(motor_object[axis].motor_model == MOT_DUET){
		#ifdef SAME_AS_SYNCRO
			if(slider_enabled && !service_state){
				result = duet_SetMaxPosition(AXIS_SLIDER1, max_pos);
				result |= duet_SetMaxPosition(AXIS_SLIDER2, max_pos);
			}else{
				result = duet_SetMaxPosition(axis, max_pos);
			}
		#else
			result = duet_SetMaxPosition(axis, max_pos);
		#endif
	}else if(motor_object[axis].motor_model == MOT_JVL)
		result = mis23x_SetMaxPosition(axis, max_pos);
	else if(motor_object[axis].motor_model == MOT_STILLE)
		result = stille_SetMaxPosition(axis, max_pos);
	else
		result = RES_FAIL;
	
	return result;
}

/**
* \fn uint32_t motor_SetMinPosition(int axis, int32_t min_pos)
* \brief Set the min position limit for the axis
*
* This function perform the commands to set min position limit in step
*
* \param int axis Axis number (index in the motor_object array)
* \param int32_t min_pos min position.
* \return  1 = success, 0 = error.
*
*/
uint32_t motor_SetMinPosition(int axis, int32_t min_pos)
{
	uint32_t result;

	if(motor_object[axis].motor_model == MOT_DUET){
		#ifdef SAME_AS_SYNCRO
			if(slider_enabled && !service_state){
				result = duet_SetMinPosition(AXIS_SLIDER1, min_pos);
				result |= duet_SetMinPosition(AXIS_SLIDER2, min_pos);
			}else{
				result = duet_SetMinPosition(axis, min_pos);			
			}
		#else
			result = duet_SetMinPosition(axis, min_pos);
		#endif
	
	}else if(motor_object[axis].motor_model == MOT_JVL)
		result = mis23x_SetMinPosition(axis, min_pos);
	else if(motor_object[axis].motor_model == MOT_STILLE)
		result = stille_SetMinPosition(axis, min_pos);
	else
		result = RES_FAIL;
		
	return result;
}


/**
 * \fn uint32_t motor_FaultReset(int axis)
 * \brief Recover a fault resetting the fault bit in the controlword
 *
 * Read and rewrite controlword with fault reset bit modified.
 *  
 * \param int axis Axis number (index in the motor_object array)
 * \return 1 if success, 0 if error.
 * 
 */
uint32_t motor_FaultReset(int axis)
{
	uint32_t result;
	
	if(motor_object[axis].motor_model == MOT_DUET)
		result = duet_FaultReset(axis);
	else if(motor_object[axis].motor_model == MOT_JVL)
		result = mis23x_FaultReset(axis);
	else
		result = RES_FAIL;
	
	return result;
}

/**
* \fn uint32_t motor_StopHere(int axis)
* \brief Command the motor stop setting the stop bit in the controlword
*
* Read and rewrite controlword with stop bit modified.
*
* \param int axis Axis number (index in the motor_object array)
* \return 1 if success, 0 if error.
*
*/
uint32_t motor_StopHere(int axis)
{
	uint32_t result= RES_FAIL;
	
	if(motor_object[axis].motor_model == MOT_DUET)
	{
		#ifndef AGNOSTIC_AXIS
		if( ((axis == AXIS_SLIDER1 && motor_object[axis].node_id == NODE_SLIDER1) 
			|| (axis == AXIS_SLIDER2 && motor_object[axis].node_id == NODE_SLIDER2)) && !service_state)
		#else
		if( ((axis == AXIS_SLIDER1) 
			|| (axis == AXIS_SLIDER2)) && !service_state)
		#endif
		{
			if(slider_enabled)
			{
//				tc_stop(TC0, 1);
				syncro_stop.enable=true;
				result = RES_SUCCESS;
			}
			else{
				result = RES_FAIL;
			}
		}
		else
		{
			result = duet_StopHere(axis);
			BrakeOn(axis);
		}
		return result;
	}
	else if(motor_object[axis].motor_model == MOT_JVL)
		result = mis23x_StopHere(axis);
	else if(motor_object[axis].motor_model == MOT_LINAK)
		result = linact_StopHere(axis);
	else if(motor_object[axis].motor_model == MOT_STILLE)
		result = stille_StopHere(axis);
	else
		result = RES_FAIL;
	
	BrakeOn(axis);
	return result;
}


/**
* \fn uint32_t motor_SetModeOperation(int axis, uint32_t mode)
* \brief Set the operational mode of the motor.
*
* Set the operational mode of the motor:
* profiled position,
* profiled velocity,
* torque profile,
* homing,
* interpolated position.
* At power on the motor drive set the operational mode as "no mode".
*
* \param int8_t mode New mode of operation.
* \param int axis Axis number (index in the motor_object array)
* \return  1 = mode set successfully, 0 = mode set error.
*
*/
uint32_t motor_SetModeOperation(int axis, uint32_t mode)
{
	uint32_t result;
	
	if(motor_object[axis].motor_model == MOT_DUET)
		result = duet_SetModeOperation(axis, mode);
	else if(motor_object[axis].motor_model == MOT_JVL)
		result = mis23x_SetModeOperation(axis, mode);
	else
		result = RES_FAIL;
	
	return result;
	
}

/**
* \fn uint32_t motor_SetHome(int axis)
* \brief Set home position for the motor
*
* Before any operation, the motor drive must perform the homing 
* procedure. This function sends the sequence of commands to
* set the home position using the homing method n. 35:
* "homing on the current position".
*
* \param int axis Axis number (index in the motor_object array)
* \return  1 = home set success, 0 = home set error.
*
*/
uint32_t motor_SetHome(int axis)
{
	uint32_t result;
	
	if(motor_object[axis].motor_model == MOT_DUET)
		result = duet_SetHome(axis);
	else if(motor_object[axis].motor_model == MOT_JVL)
		result = mis23x_SetHome(axis);
	else if(motor_object[axis].motor_model == MOT_STILLE)
		result = stille_SetHome(axis);
	else
		result = RES_FAIL;
	
	return result;
}


/**
* \fn uint32_t motor_SetPosition(int axis, uint32_t new_position)
* \brief Set a new position for the motor and start it
*
* This function perform the commands to set a new absolute position,
* start the motor and wait for position reached, monitoring the 
* statusword.
*
* \param int axis Axis number (index in the motor_object array)
* \param uint32_t new_position New absolute position
* \return  1 = success, 0 = error.
*
*/
uint32_t motor_SetPosition(int axis, int32_t new_position)
{
	uint32_t result;

	if(!digio_GetPowerStatus()){
		result=RES_FAIL;			//Se la macchina non � in potenza rispondi NAK
	}else{
		if(motor_object[axis].motion_stat!=MOT_MOTION_COMPLETED && motor_object[axis].motor_model != MOT_DUET) {
			result=RES_FAIL;			//Se il motore non � fermo NAK
		} else {
			if(motor_object[axis].motor_model == MOT_DUET)	{
				#ifdef SAME_AS_SYNCRO
					if(slider_enabled && !service_state && (axis==AXIS_SLIDER1 || axis==AXIS_SLIDER2)){
						result = duet_SetPosition(AXIS_SLIDER1, new_position);
						result |= duet_SetPosition(AXIS_SLIDER2, new_position);
					}else{
						result = duet_SetPosition(axis, new_position);			
					}
				#else
					result = duet_SetPosition(axis, new_position);
				#endif
			}else if(motor_object[axis].motor_model == MOT_JVL)	
				result = mis23x_SetPosition(axis, (uint32_t)new_position);
			else if(motor_object[axis].motor_model == MOT_LINAK)
				result = linact_SetPosition(axis, (uint32_t)new_position);
			else if(motor_object[axis].motor_model == MOT_STILLE)
				result = stille_SetPosition(axis, (uint32_t)new_position);
			else{
				result = RES_FAIL;
			}
		}
	}
	
	if(result == RES_FAIL){
		/*
		uint32_t tmptimer = timer_TimerSet(BRAKE_WAIT_ON);
		while(!timer_TimerTest(tmptimer));
		*/
		BrakeOn(axis);
	}
	

	return result;
}

uint32_t motor_ResetPosition(int axis, uint32_t new_position)
{
	uint32_t result;

	if(motor_object[axis].motor_model == MOT_LINAK)
		result = linact_ResetPosition(axis, new_position);
	else
		result = RES_FAIL;

	return result;
}

/**
* \fn static uint32_t motor_SetAcc(int axis, uint32_t acc)
* \brief Set the acceleration and deceleration rate for the motor.
*
* This function perform the commands to set motor acceleration and 
* deceleration rate, in step/s^2
*
* \param int axis Axis number (index in the motor_object array)
* \param uint32_t acc Acceleration rate.
* \return  1 = success, 0 = error.
*
*/
uint32_t motor_SetAcc(int axis, uint32_t acc)
{
	uint32_t result;

	if(motor_object[axis].motion_stat!=MOT_MOTION_COMPLETED && motor_object[axis].motor_model != MOT_DUET)
			result=RES_FAIL;			//Se il motore non � fermo NAK
	else if(motor_object[axis].motor_model == MOT_DUET)
		result = duet_SetAcc(axis, acc);
	else if(motor_object[axis].motor_model == MOT_JVL)
		result = mis23x_SetAcc(axis, acc);
	else
		result = RES_FAIL;

	return result;
}

/**
* \fn uint32_t motor_SetDec(int axis, uint32_t dec)
* \brief Set the deceleration rate for the motor
*
* This function perform the commands to set motor deceleration rate, in step/s^2
*
* \param int axis Axis number (index in the motor_object array)
* \param uint32_t dec Deceleration rate.
* \return  1 = success, 0 = error.
*
*/
uint32_t motor_SetDec(int axis, uint32_t dec)
{
	uint32_t result;

	if(motor_object[axis].motion_stat!=MOT_MOTION_COMPLETED && motor_object[axis].motor_model != MOT_DUET)
			result=RES_FAIL;			//Se il motore non � fermo NAK
	else if(motor_object[axis].motor_model == MOT_DUET)
		result = duet_SetDec(axis, dec);
	else if(motor_object[axis].motor_model == MOT_JVL)
		result = mis23x_SetDec(axis, dec);
	else
		result = RES_FAIL;

	return result;

}


/**
* \fn uint32_t motor_GetMotionStatus(int axis)
* \brief Return the motion status: moving-complete/moving
*
* \param int axis Axis number (index in the motor_object array)
* \return  motion status.
*
*/
uint32_t motor_GetMotionStatus(int axis)
{
	uint32_t res = RES_FAIL;

	if(motor_object[axis].motor_model == MOT_DUET)
		return duet_GetMotionStatus(axis);
	else if(motor_object[axis].motor_model == MOT_JVL)
		return mis23x_GetMotionStatus(axis);
	else if(motor_object[axis].motor_model == MOT_LINAK)
		return linact_GetMotionStatus(axis);
	else if(motor_object[axis].motor_model == MOT_STILLE)
		return stille_GetMotionStatus(axis);
		
		
	return res;
}

/**
* \fn uint32_t motor_GetMotorStatus(int axis)
* \brief Return the motor status.
*
* \param int axis Axis number (index in the motor_object array)
* \return  motor status.
*
*/
uint32_t motor_GetMotorStatus(int axis)
{
	uint32_t result = RES_FAIL;

	if(motor_object[axis].motor_model == MOT_DUET)
		result = duet_GetMotorStatus(axis);
	else if(motor_object[axis].motor_model == MOT_JVL)
		result = mis23x_GetMotorStatus(axis);
	else if(motor_object[axis].motor_model == MOT_LINAK)
		result = linact_GetMotorStatus(axis);
	else if(motor_object[axis].motor_model == MOT_STILLE)
		result = stille_GetMotorStatus(axis);

	return result;

}

/**
* \fn uint32_t motor_SetJogSpeed(int axis, uint32_t j_speed)
* \brief Set the motor speed for jog mode in the motor object structure
*
* \param int axis Axis number (index in the motor_object array)
* \param uint32_t j_speed Jog mode speed
* \return  1 = success, 0 = error.
*
*/
uint32_t motor_SetJogSpeed(int axis, uint32_t j_speed)
{
	uint32_t result=RES_FAIL;
	
	if(motor_object[axis].motor_model == MOT_DUET)
		result=duet_SetJogSpeed(axis, j_speed);
	else if(motor_object[axis].motor_model == MOT_JVL)
		result=mis23x_SetJogSpeed(axis, j_speed);	
	
	return result;
}

/**
* \fn uint32_t motor_GoJogFwd(int axis)
* \brief Set the motor in jog mode and start moving in forward direction. 
*
* \param int axis Axis number (index in the motor_object array)
* \return  1 = success, 0 = error.
*
*/
uint32_t motor_GoJogFwd(int axis)
{
	uint32_t res = RES_FAIL;

	if(motor_object[axis].motor_model == MOT_DUET)
		res = duet_GoJogFwd(axis);
	else if(motor_object[axis].motor_model == MOT_JVL)
		res = mis23x_GoJogFwd(axis);

	return res;
}

/**
* \fn uint32_t motor_GoJogRev(int axis)
* \brief Set the motor in jog mode and start moving in reverse direction.
*
* \param int axis Axis number (index in the motor_object array)
* \return  1 = success, 0 = error.
*
*/
uint32_t motor_GoJogRev(int axis)
{
	uint32_t res = RES_FAIL;

	if(motor_object[axis].motor_model == MOT_DUET)
		res = duet_GoJogRev(axis);
	else if(motor_object[axis].motor_model == MOT_JVL)
		res = mis23x_GoJogRev(axis);

	return res;
}

/**
* \fn void motor_Machine(void)
* \brief Main function for motors management.
*
* This function is added to the main CAN management list of functions and is called from the main CAN 
* routine, temporized by interrupt timer.
* Scans the motor_object list for queued messages to manage.
*
*
*/
#ifdef OLD_STYLE_COPROCESS
void motor_Machine(void)
{
	// iterate the motor object instances
	for(int i=0; i<MOTOR_MAXOBJ; i++)
	{
		if(motor_object[i].node_id != 0)  // if the object is initialized)
		{
			if(motor_object[i].motor_model == MOT_DUET)
				duet_Machine(i);
			else if(motor_object[i].motor_model == MOT_JVL)
				mis23x_Machine(i);
			//else if(motor_object[i].motor_model == MOT_LINAK)
				//linact_InMachine(i);
		}
	}

}
#endif

/**
* \fn uint32_t motor_Home(int axis)
* \brief Performs homing of the motor
*
* Set the home speed and start moving towards the position 0
*
* \param int axis Axis number (index in the motor_object array)
* \return  1 = success, 0 = error.
*
*/
uint32_t motor_Home(int axis)
{
	uint32_t result;
	
	if(motor_object[axis].motor_model == MOT_DUET)
		result = duet_Home(axis);
	else if(motor_object[axis].motor_model == MOT_JVL)
		result = mis23x_Home(axis);
	else if(motor_object[axis].motor_model == MOT_LINAK)
		result = linact_Home(axis);
	else if(motor_object[axis].motor_model == MOT_STILLE)
		result = stille_Home(axis);
	else
		result = RES_FAIL;
		
	return result;
}

/**
* \fn uint32_t motor_SetSpeed(int axis, uint32_t speed)
* \brief Set the motor speed in the motor object structure.
*
* \param int axis Axis number (index in the motor_object array)
* \speed uint32_t speed Motor speed value in step/s
* \return  1 = success, 0 = error.
*
*/	
uint32_t motor_SetSpeed(int axis, uint32_t speed)
{
	uint32_t result;

	if(motor_object[axis].motion_stat!=MOT_MOTION_COMPLETED && motor_object[axis].motor_model != MOT_DUET)
			result=RES_FAIL;			//Se il motore non � fermo NAK
	else if(motor_object[axis].motor_model == MOT_DUET)
		result = duet_SetSpeed(axis, speed);
	else if(motor_object[axis].motor_model == MOT_JVL)
		result = mis23x_SetSpeed(axis, speed);
	else if(motor_object[axis].motor_model == MOT_STILLE)
		result = stille_SetSpeed(axis, speed);
	else
		result = RES_FAIL;

	return result;
}

/**
* \fn uint32_t motor_SetHomeSpeed(int axis, uint32_t h_speed)
* \brief Set the motor speed for homing
*
* \param int axis Axis number (index in the motor_object array)
* \speed uint32_t h_speed Motor homing speed value in step/s
* \return  1 = success, 0 = error.
*
*/
uint32_t motor_SetHomeSpeed(int axis, uint32_t h_speed)
{
	uint32_t result;

	if(motor_object[axis].motion_stat!=MOT_MOTION_COMPLETED)
			result=RES_FAIL;			//Se il motore non � fermo NAK
	else if(motor_object[axis].motor_model == MOT_DUET)
		result = duet_SetHomeSpeed(axis, h_speed);
	else if(motor_object[axis].motor_model == MOT_JVL)
		result = mis23x_SetHomeSpeed(axis, h_speed);
	else
		result = RES_FAIL;

	return result;
}

/**
* \fn uint32_t motor_GetMotorActPos(int axis, uint* pos)
* \brief Read the actual position from the motor
*
* \param int axis Axis number (index in the motor_object array)
* \return  the actual absolute position of the motor in step.
*
*/
uint32_t motor_GetMotorActPos(int axis, int32_t* pos)
{
	uint32_t result;
	
	if(motor_object[axis].motor_model == MOT_DUET)
		result = duet_GetMotorActPos(axis, pos);
	else if(motor_object[axis].motor_model == MOT_JVL)
		result = mis23x_GetMotorActPos(axis, pos);
	else if(motor_object[axis].motor_model == MOT_LINAK)
		result = linact_GetMotorActPos(axis, pos);
	else if(motor_object[axis].motor_model == MOT_STILLE)
		result = stille_GetMotorActPos(axis, pos);
	else
		result = RES_FAIL;

	return result;
}

/**
* \fn uint32_t motor_GetMotorActualCurrent(int axis)
* \brief Read the actual current from the motor
*
* \param int axis Axis number (index in the motor_object array)
* \return  the actual current in the motor in mA.
*
*/
uint32_t motor_GetMotorActualCurrent(int axis)
{
	uint32_t result;
	
	if(motor_object[axis].motor_model == MOT_DUET)
		result = duet_GetMotorActualCurrent(axis);
	else if(motor_object[axis].motor_model == MOT_JVL)
		result = mis23x_GetMotorActualCurrent(axis);
	else
		result = RES_FAIL;

	return result;
}

/**
* \fn uint32_t motor_GetMotorRatedCurrent(int axis)
* \brief Read the nominal rated current the motor
*
* \param int axis Axis number (index in the motor_object array)
* \return  the rated motor current in mA.
*
*/
uint32_t motor_GetMotorRatedCurrent(int axis)
{
	uint32_t result;
	
	if(motor_object[axis].motor_model == MOT_DUET)
		result = motor_object[axis].rated_current;
	else if(motor_object[axis].motor_model == MOT_JVL)
		result = mis23x_GetMotorActualCurrent(axis);
	else
		result = RES_FAIL;

	return result;
}

/**
* \fn uint32_t motor_SetCurrentLimit(int axis)
* \brief Sets the max current limit beyond which the motor must stop 
*
* \param int axis Axis number (index in the motor_object array)
* \param int max_current limit in mA.
*
*/
uint32_t motor_SetCurrentLimit(int axis, int max_current)
{
	uint32_t result;
	
	if(motor_object[axis].motor_model == MOT_DUET){		
		#ifndef AGNOSTIC_AXIS
		if( ((axis == AXIS_SLIDER1 && motor_object[axis].node_id == NODE_SLIDER1)
			|| (axis == AXIS_SLIDER2 && motor_object[axis].node_id == NODE_SLIDER2) ) && slider_enabled && !service_state)
		#else
		if( ((axis == AXIS_SLIDER1)
			|| (axis == AXIS_SLIDER2) ) && slider_enabled && !service_state)
		#endif
		{	
			result = duet_SetCurrentLimit(axis, max_current);
		}else{
			result = duet_SetCurrentLimit(axis, max_current);
		}
	}else if(motor_object[axis].motor_model == MOT_JVL)
		result = mis23x_SetCurrentLimit(axis, max_current);
	else
		result = RES_FAIL;

	return result;
}

/**
* \fn uint32_t motor_SetCurrentLimitTime(int axis)
* \brief Sets the maximum time during which the current can exceed the limit
*
* \param int axis Axis number (index in the motor_object array)
* \param uint32_t limit_time in ms
*
*/
uint32_t motor_SetCurrentLimitTime(int axis, uint32_t limit_time)
{
	uint32_t result;
	
	if(motor_object[axis].motor_model == MOT_DUET){
		#ifndef AGNOSTIC_AXIS
		if( ((axis == AXIS_SLIDER1 && motor_object[axis].node_id == NODE_SLIDER1)
				|| (axis == AXIS_SLIDER2 && motor_object[axis].node_id == NODE_SLIDER2) ) && slider_enabled && !service_state)
		#else
		if( ((axis == AXIS_SLIDER1)
				|| (axis == AXIS_SLIDER2) ) && slider_enabled && !service_state)
		#endif
		{
			result = duet_SetCurrentLimitTime(axis, limit_time);
		}else{
			result = duet_SetCurrentLimitTime(axis, limit_time);
		}	
	}else if(motor_object[axis].motor_model == MOT_JVL)
		result = mis23x_SetCurrentLimitTime(axis, limit_time);
	else
		result = RES_FAIL;

	return result;
}

/**
* \fn uint32_t motor_QuickStop(int axis)
* \brief Commands the 'quick stop' of the motor.
*
* Command the 'quick stop' of the motor setting the bit in the control word.
* After this command the motor must be reinitialized.
*
* \param int axis Axis number (index in the motor_object array)
* \return  1 = success, 0 = error.
*
*/
uint32_t motor_QuickStop(int axis)
{
	uint32_t result;
	
	if(motor_object[axis].motor_model == MOT_DUET)
	{
		#ifndef AGNOSTIC_AXIS
		if( ((axis == AXIS_SLIDER1 && motor_object[axis].node_id == NODE_SLIDER1) 
			|| (axis == AXIS_SLIDER2 && motor_object[axis].node_id == NODE_SLIDER2)) && !service_state)
		#else
		if( ((axis == AXIS_SLIDER1) 
			|| (axis == AXIS_SLIDER2)) && !service_state)
		#endif			
		{
			if(slider_enabled)
			{
//				tc_stop(TC0, 1);
				syncro_stop.ratio=2;
				syncro_stop.enable=true;
				result = RES_SUCCESS;
			}
			else{
				result = RES_FAIL;
			}
		}else{
			result =  duet_QuickStop(axis);
			BrakeOn(axis);
		}
		return result;
	}
	else if(motor_object[axis].motor_model == MOT_JVL){
		result = mis23x_QuickStop(axis);
	}else{
		result = RES_FAIL;
	}
	BrakeOn(axis);
	return result;
}

//questa resetta in fw i primi 3 assi
void motorStopNoPower (void)
{
	for(uint8_t axis=0; axis<3; axis++) //fermo i primi 3 assi
		motor_object[axis].motion_stat = MOT_MOTION_COMPLETED;
}



/**
* \fn uint32_t motor_SetZero(int axis)
* \brief Set the zero position of the motor. For JVL motors only.
*
* \param int axis Axis number (index in the motor_object array)
* \return  1 = success, 0 = error.
*
*/
uint32_t motor_SetZero(int axis)
{
	uint32_t result;
	
	if(motor_object[axis].motor_model == MOT_DUET)
		//result = RES_FAIL;
		result = duet_SetZero(axis);
	else if(motor_object[axis].motor_model == MOT_JVL)
		result = mis23x_SetZero(axis);
	else
		result = RES_FAIL;

	return result;
	
}

//uint32_t motor_ResetError(int axis)
//{
	//uint32_t result;
	//
	//if(motor_object[axis].motor_model == MOT_DUET)
		//result = RES_FAIL;
	//else if(motor_object[axis].motor_model == MOT_JVL)
		//result = mis23x_ResetErrors(axis);
	//else
		//result = RES_FAIL;
//
	//return result;
//}

uint32_t motor_GetErrorsBits(uint32_t axis, uint32_t* errors)
{
	uint32_t result = RES_SUCCESS;
	
	if(motor_object[axis].motor_model == MOT_DUET)
		result = RES_FAIL;
	else if(motor_object[axis].motor_model == MOT_JVL)
		*errors = motor_object[axis].mis_err_bits;
	else
		result = RES_FAIL;

	return result;
}

uint32_t motor_GetWarnsBits(uint32_t axis, uint32_t* warnings)
{
	uint32_t result = RES_SUCCESS;
	
	if(motor_object[axis].motor_model == MOT_DUET)
		result = RES_FAIL;
	else if(motor_object[axis].motor_model == MOT_JVL)
		*warnings = motor_object[axis].mis_warn_bits;
	else
		result = RES_FAIL;

	return result;
}

uint32_t motor_ReadObject(uint32_t axis, uint32_t index, uint32_t subindex, uint32_t *value)
{
	uint32_t result = RES_SUCCESS;
	
	if(motor_object[axis].motor_model == MOT_DUET)
		result = duet_ReadObj(axis, index, subindex, value);
	else if(motor_object[axis].motor_model == MOT_JVL)
		result = mis23x_ReadObj(axis, index, subindex, value);
	else
		result = RES_FAIL;

	return result;

}

uint32_t motor_WriteObject(uint32_t axis, uint32_t index, uint32_t subindex, uint32_t value)
{
	uint32_t result = RES_SUCCESS;
	
	if(motor_object[axis].motor_model == MOT_DUET)
		result = duet_WriteObj(axis, index, subindex, value);
	else if(motor_object[axis].motor_model == MOT_JVL)
		result = mis23x_WriteObj(axis, index, subindex, value);
	else
		result = RES_FAIL;

	return result;
}

/*   FUNZIONI PER LA GESTIONE DELLA MODALITA' JOG DA PULSANTIERA  */
#define MOTOR_JOG_IDLE		0
#define MOTOR_JOG_MOVING	1
#define MOTOR_JOG_STOPPED	2
#define JOG_STOP_DELAY	2000 // 300 ms delay dopo aver rilasciato il tasto

int motor_gantry_jog_rotation_state = MOTOR_JOG_IDLE;
int motor_gantry_jog_traslation_state = MOTOR_JOG_IDLE;
uint32_t rotation_stop_timer;
uint32_t traslation_stop_timer;

void motor_JogFunction(void)
{
	switch(motor_gantry_jog_rotation_state)
	{
		case MOTOR_JOG_IDLE:
			if( ioport_get_pin_level(GANTRY_CW_GPIO) == false )  // tasto premuto antiorario
			{
				DbgPrintf(DBG_MOTOR, "Gantry CW pressed\r\n");
				// set motor for moving clock wise
				if(motor_object[0].motor_model == MOT_DUET)
				{
					duet_GoJogFwd(0);
					//duet_SetPosition(0, motor_object[0].max_position_limit - 10);
					motor_gantry_jog_rotation_state = MOTOR_JOG_MOVING;
				}
				else if(motor_object[0].motor_model == MOT_JVL)
				{
					mis23x_GoJogFwd(0);
					motor_gantry_jog_rotation_state = MOTOR_JOG_MOVING;
				}
			}
			else if( ioport_get_pin_level(GANTRY_CCW_GPIO) == false )  // tasto premuto orario
			{
				DbgPrintf(DBG_MOTOR, "Gantry CCW pressed\r\n");
				// set motor for moving counter clock wise
				if(motor_object[0].motor_model == MOT_DUET)
				{
					duet_GoJogRev(0);
					motor_gantry_jog_rotation_state = MOTOR_JOG_MOVING;
				}
				else if(motor_object[0].motor_model == MOT_JVL)
				{
					mis23x_GoJogRev(0);
					motor_gantry_jog_rotation_state = MOTOR_JOG_MOVING;
				}

			}
		break;

		case MOTOR_JOG_MOVING :
			if( (ioport_get_pin_level(GANTRY_CW_GPIO) == true) &&  (ioport_get_pin_level(GANTRY_CCW_GPIO) == true) )	// tasto rilascito
			{
				DbgPrintf(DBG_MOTOR, "Gantry released\r\n");
				// stop motor
				if(motor_object[0].motor_model == MOT_DUET)
					duet_StopHere(0);
					//duet_QuickStop(0);
				else if(motor_object[0].motor_model == MOT_JVL)
					mis23x_QuickStop(0);

				//rotation_stop_timer = timer_TimerSet(JOG_STOP_DELAY);
				motor_gantry_jog_rotation_state = MOTOR_JOG_STOPPED;
			}
		break;
		
		case MOTOR_JOG_STOPPED :
			if(motor_object[0].motion_stat == MOT_MOTION_COMPLETED)
			//if(timer_TimerTest(rotation_stop_timer))
				motor_gantry_jog_rotation_state = MOTOR_JOG_IDLE;
		break;
	}

	switch(motor_gantry_jog_traslation_state)
	{
		case MOTOR_JOG_IDLE :
		if( ioport_get_pin_level(GANTRY_FW_GPIO) == false )  // tasto premuto
		{
			DbgPrintf(DBG_MOTOR, "Cart FW pressed\r\n");
			// set motor for moving forward
			if(motor_object[1].motor_model == MOT_DUET)
				duet_GoJogFwd(1);
			else if(motor_object[1].motor_model == MOT_JVL)
				mis23x_GoJogFwd(1);
			else
				linact_GoJogFwd(1);

			motor_gantry_jog_traslation_state = MOTOR_JOG_MOVING;
		}
		else if( ioport_get_pin_level(GANTRY_BW_GPIO) == false)  // tasto premuto
		{
			DbgPrintf(DBG_MOTOR, "Cart BW pressed\r\n");
			// set motor for moving backward
			if(motor_object[1].motor_model == MOT_DUET)
				duet_GoJogRev(1);
			else if(motor_object[1].motor_model == MOT_JVL)
				mis23x_GoJogRev(1);
			else
				linact_GoJogRev(1);

			motor_gantry_jog_traslation_state = MOTOR_JOG_MOVING;
		}
		break;

		case MOTOR_JOG_MOVING :
		if( (ioport_get_pin_level(GANTRY_FW_GPIO) == true) &&  (ioport_get_pin_level(GANTRY_BW_GPIO) == true) )	// tasto rilascito
		{
			DbgPrintf(DBG_MOTOR, "Cart released\r\n");
			// stop motor
			if(motor_object[1].motor_model == MOT_DUET)
				duet_StopHere(1);
			else if(motor_object[1].motor_model == MOT_JVL)
				mis23x_QuickStop(1);
			else
				linact_StopHere(1);
				
			traslation_stop_timer = timer_TimerSet(JOG_STOP_DELAY);
			motor_gantry_jog_traslation_state = MOTOR_JOG_IDLE;
		}
		break;
		
		case MOTOR_JOG_STOPPED :
			if(motor_object[1].motion_stat == MOT_MOTION_COMPLETED)
			//if(timer_TimerTest(traslation_stop_timer))
				motor_gantry_jog_rotation_state = MOTOR_JOG_IDLE;
		break;

	}

}


#ifdef	TEST_ONLY_SYNC_MOTOR
	enum{
		FIRST_STEP,	//If motors are in rest state Move Forward: Motion state Complete, Motor Ready -> go to 300mm
		SECOND_STEP,	//Waiting to start to move
		THIRD_STEP,	//If motors are in rest state Move Backward: Motion state Complete, Motor Ready -> go to 0mm
		FOURTH_STEP,	//Waiting to start to move
		ERROR_STATE,	//Error State
	};

	void test_only_sync(){
		static int first=0;
		static int state=FIRST_STEP;
		uint32_t res;
		if(first==0){
			//ASSE 1 node 8
//			motor_Init(1, 8);
			//ASSE 8 node 9
//			motor_Init(8, 9);
			first=1;
		}else{
			//Check whe motor 1 and 2 are ok
			if(motor_object[AXIS_SLIDER1].initialized && motor_object[AXIS_SLIDER2].initialized){
				//Machine state to motor control
				switch (state)
				{
					case FIRST_STEP:
						if(motor_object[AXIS_SLIDER1].motion_stat==MOT_MOTION_COMPLETED && motor_object[AXIS_SLIDER1].motor_stat==MOT_READY &&
							motor_object[AXIS_SLIDER2].motion_stat==MOT_MOTION_COMPLETED && motor_object[AXIS_SLIDER2].motor_stat==MOT_READY){
							res=duet_SetInterpolatePositionLinear(AXIS_SLIDER1, AXIS_SLIDER2, STEP_TO_MM*300);
							if(res==RES_FAIL){
								state=ERROR_STATE;
							}else{
								state=SECOND_STEP;
							}
						}
					break;
					case SECOND_STEP:						
						if(motor_object[AXIS_SLIDER1].motion_stat!=MOT_MOTION_COMPLETED && motor_object[AXIS_SLIDER1].motion_stat!=MOT_MOTION_COMPLETED)
							state=THIRD_STEP;
					break;
					case THIRD_STEP:
					if(motor_object[AXIS_SLIDER1].motion_stat==MOT_MOTION_COMPLETED && motor_object[AXIS_SLIDER1].motor_stat==MOT_READY &&
						motor_object[AXIS_SLIDER2].motion_stat==MOT_MOTION_COMPLETED && motor_object[AXIS_SLIDER2].motor_stat==MOT_READY){
						res=duet_SetInterpolatePositionLinear(AXIS_SLIDER1, AXIS_SLIDER2, STEP_TO_MM*0);
						if(res==RES_FAIL){
							state=ERROR_STATE;
						}else{
							state=FOURTH_STEP;
						}
					}
					break;
					case FOURTH_STEP:
						if(motor_object[AXIS_SLIDER1].motion_stat!=MOT_MOTION_COMPLETED && motor_object[AXIS_SLIDER1].motion_stat!=MOT_MOTION_COMPLETED)
							state=FIRST_STEP;
					break;

					case ERROR_STATE:
					break;
				}
				
			}
		}
	}
#endif

void motor_ManageEmergency(void)
{
	for(int i=0; i<MOTOR_MAXOBJ; i++ )
	{
		if(motor_object[i].initialized != 0)
		{
			if(motor_object[i].motor_model == MOT_DUET){
				if(!service_state)
				{
					#ifndef AGNOSTIC_AXIS
					if( (i == AXIS_SLIDER1 && motor_object[i].node_id == NODE_SLIDER1) 
						|| (i == AXIS_SLIDER2 && motor_object[i].node_id == NODE_SLIDER2) )
					#else
					if( (i == AXIS_SLIDER1) 
						|| (i == AXIS_SLIDER2) )
					#endif
					{
						if( slider_enabled ){
							#ifdef SAME_AS_SYNCRO
								duet_ManageSlider_same_as_syncro();
							#else
								duet_ManageSlider();
							#endif
						}
					}else
						duet_Manage(i);
				}else
				{
					 // se sono in stato service vengono comunque gestiti separati
					 duet_Manage(i);
				}
			}
			else if(motor_object[i].motor_model == MOT_JVL)
				mis23x_Manage(i);
		}
	}
}

//Modalit� nella quale posso muovere in maniera indipendente i motori che altrimenti
//sarebbero sincroni (questo per facilitare eventuali manutenzioni)
void motor_EnableServiceState(bool enabled)
{
	service_state = enabled;
}
	
// resetta i timers di tutti i motori e riattiva l'invio dei sync
void motor_ResetSync(void)
{
	for(int i=0; i<MOTOR_MAXOBJ; i++ )
	{
		if(motor_object[i].initialized != 0)
		{
			motor_object[i].heartbeat_timer = timer_TimerSet(motor_object[i].heardbeat_tout*2);
		}
	}
}


