/*
 * BrakeManagement.c
 *
 * Created: 24/11/2020 14:46:04
 *  Author: stefano
 */ 
#include "main.h"
#include "BrakeManagement.h"
#include <string.h>
#include "motors.h"
#include "timer.h"
#include "dig_io.h"
#include "manage_debug.h"


brake_t brake_manage_v[MAX_AXIS_NUMBER];


void BrakeInit(void){
	memset(brake_manage_v,0,sizeof(brake_t)*MAX_AXIS_NUMBER);
}

// funzioni per attivazione/disattivazione elettrofreno su asse lineare lettino
// il freno e normalmente chiuso (frenato):
// per togliere il freno si attiva il rel�
// per mettere io freno si disattiva il rel�
void BrakeOn(uint8_t axis)
{
	if(axis>=MAX_AXIS_NUMBER)
		return;
	if(brake_manage_v[axis].enable){
		uint32_t tmptimer = timer_TimerSet(BRAKE_WAIT_ON);
		while(!timer_TimerTest(tmptimer));
		if(brake_manage_v[axis].brakeLogic){
			digio_ResetRelay(brake_manage_v[axis].relay);
			DbgPrintf(DBG_BRAKE, "BrakeOn digio_ResetRelay axis: %d\r\n", axis);
		}else{
			digio_SetRelay(brake_manage_v[axis].relay);
			DbgPrintf(DBG_BRAKE, "BrakeOn digio_SetRelay axis: %d\r\n", axis);
		}
		brake_manage_v[axis].timeOff=0;
	}
}

void ReloadBrakeOn(uint8_t axis)
{
	if(axis>=MAX_AXIS_NUMBER)
		return;
	DbgPrintf(DBG_BRAKE, "ReloadBrakeOn axis: %d\r\n", axis);
	if(brake_manage_v[axis].enable){
		brake_manage_v[axis].timeOff=timer_TimerSet(0)+BRAKE_WAIT_ON;
	}
}


void BrakeOff(uint8_t axis)
{
	if(axis>=MAX_AXIS_NUMBER)
		return;
	if(brake_manage_v[axis].enable){
		brake_manage_v[axis].timeOff=timer_TimerSet(0)+TIME_TO_START_MOVEMENT;
		if(brake_manage_v[axis].brakeLogic){
			DbgPrintf(DBG_BRAKE, "BrakeOff digio_SetRelay axis: %d\r\n", axis);
			digio_SetRelay(brake_manage_v[axis].relay);
		}else{
			DbgPrintf(DBG_BRAKE, "BrakeOff digio_ResetRelay axis: %d\r\n", axis);
			digio_ResetRelay(brake_manage_v[axis].relay);
		}
	}
}

void CheckBrakeOn(uint8_t axis)
{
	if(axis>=MAX_AXIS_NUMBER)
		return;
	if(brake_manage_v[axis].enable){
		if(brake_manage_v[axis].timeOff!=0){
			if(timer_TimerTest(brake_manage_v[axis].timeOff)){
				DbgPrintf(DBG_BRAKE, "CheckBrakeOn axis: %d\r\n", axis);
				BrakeOn(axis);
			}
		}
	}
}
