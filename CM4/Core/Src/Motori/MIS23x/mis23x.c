/*! \file mis23.c \brief Control module for JVL MIS23x motors */
//*****************************************************************************
//
// File Name	: 'mis23x.c'
// Title		: Control module for JVL MIS23x stepper motors.
// Author		: Alessandro
// Company		: Qprel s.r.l.
// Created		: 14/10/2017
// Revised		: -
// Version		: 1.0
//
///
/// \code #include "mis23x.h" \endcode
/// \par Overview
///		This module provide the functions to control the motor in position profile mode 
///
//*****************************************************************************
//@{ 
#include <mis23x.h>
#include <string.h>
#include "co_main.h"
#include "co_microdep.h"
#include "co_nmt.h"
#include "co_sdo.h"
#include "co_pdo.h"
#include "dig_io.h"
#include "manage_debug.h"
#include "BrakeManagement.h"


/// Array of motor object structures
static motor_obj_t * mis23x_object;

/// Private function waiting for motor node status change with timeout
//static uint32_t duet_WaitForStatus(int axis, uint32_t status, uint32_t tout_ms);
/// Private function to set parameter in the motor node object dictionary
static uint32_t mis23x_SetParameter(int axis, uint32_t idx, uint8_t subidx, uint32_t * val, uint32_t size);
/// Private function to get parameter from the motor node object dictionary
static uint32_t mis23x_GetParameter(int axis, uint32_t idx, uint8_t subidx, uint32_t * val, uint32_t size);
#ifdef	OLD_STYLE_COPROCESS
/// Private function for the callback array of co_main, to manage motor nodes.
static void mis23x_CbackManage(void *param, uint8_t *data)
#endif
/// Private function to command new absolute position to the motor node.
static uint32_t mis23x_GoAbsolute(int axis, uint32_t new_position);
//static uint32_t mis23x_GoRelative(int axis, int32_t new_position);


/**
 * \fn void mis23x_InitData(void)
 * \brief Initialize the motor objects array and
 *        add the motor_Machine() function to the co_main calling list
 *
 */
void mis23x_InitData(motor_obj_t * m_obj)
{
	
	mis23x_object = m_obj;

}

/**
 * \fn uint32_t motor_Init(int axis, uint8_t node)
 * \brief Initialize the motor drive
 *
 * Send the command sequence to make the motor drive in the operative state,
 * as requested by the CanOpen specifications.
 * - reset the node, and sent command to start it.
 * - sends "shutdown" command to put node in "ready to switch on" state
 * - sends "switch on" command to put node in "switched on" state
 * - sends "enable operation" command to put node in "operation enable" state
 * If the command sequence fails return 0, 1 if success
 *  
 * \param 'uint8_t node' Node id to initialize.
 * \param 'int axis' Axis (index in the motor_object array)
 * \return 1 if success, 0 if fails.
 * 
 */
uint8_t ERROR_INIT = 0;
uint32_t mis23x_Init(int axis, uint8_t node)
{
	intr_cback_t icb;
	uint32_t val;	
	reset_co_icb(&icb);
	
	//To avoid shut down power on reinitialization
	reset_Motor_HeartBeat();
	
#ifdef	OLD_STYLE_COPROCESS
	// initialize the message queue
	mis23x_object[axis].rx_queue_read_p = 0;
	mis23x_object[axis].rx_queue_write_p = 0;
#endif

	mis23x_object[axis].motor_stat = MOT_NOT_INIT;
	
	
	// send NMT message to reset node
	/*
	co_nmt_Send(mis23x_object[axis].node_id, NMT_RESET);
	uint32_t tout_reset=timer_TimerSet(700);
	while(timer_TimerTest(tout_reset)==false);
	*/
	
	// reset registro errori
	uint32_t tmp_uint = 0;
	if(mis23x_SetParameter(axis, MIS23_REG_OBJ, MIS23_ERR_BITS, &tmp_uint, 4) == RES_FAIL)
	{
		mis23x_object[axis].motor_stat = MOT_SET_PARAM_ERROR;
		ERROR_INIT = 1;
		return RES_FAIL;
	}

		if(!mis23x_SetModeOperation(axis, MIS23_OP_PASSIVE_MODE))
			mis23x_object[axis].motor_stat = MOT_INIT_ERROR;


	uint32_t tout_reset=timer_TimerSet(700);
	while(timer_TimerTest(tout_reset)==false);
	
	/*
		if(!mis23x_SetModeOperation(axis, MIS23_OP_PASSIVE_MODE))
			mis23x_object[axis].motor_stat = MOT_INIT_ERROR;
	*/
	
	// set run current 
	switch(axis)
	{
		case AXIS_GANTRY:
			mis23x_object[axis].rated_current = MIS23_GANTRY_MAX_CURRENT;
		break;
		case AXIS_BED:
			mis23x_object[axis].rated_current =MIS23_BED_MAX_CURRENT;
		break;
		case AXIS_PANEL:
			mis23x_object[axis].rated_current =MIS23_FLATP_MAX_CURRENT;
		break;
		case AXIS_BED_LIFT:
			mis23x_object[axis].rated_current =MIS23_LIFT_MAX_CURRENT;
		break;
		default:
			mis23x_object[axis].rated_current =1000;  // default corrente bassa: 1 Amp
		break;
	}
	
	tmp_uint = AMP_TO_BIT(mis23x_object[axis].rated_current);
	if(mis23x_SetParameter(axis, MIS23_REG_OBJ, MIS23_RUN_CURRENT, &tmp_uint, 4) == RES_FAIL)
	{
		mis23x_object[axis].motor_stat = MOT_SET_PARAM_ERROR;
				ERROR_INIT = 2;
		return RES_FAIL;
	}
	
	/**** set the bits in the setup register n.124  ****/
	// set up the motor in "closed loop operation" whit dynamic current control
	if(mis23x_GetParameter(axis, MIS23_REG_OBJ, MIS23_SETUP_BITS, &tmp_uint, 4) == RES_FAIL)
	{
		mis23x_object[axis].motor_stat = MOT_GET_PARAM_ERROR;
				ERROR_INIT = 2;
		return RES_FAIL;
	}
	tmp_uint |= (Enable_closed_loop | Enable_closed_loop_current_control);
	if(mis23x_SetParameter(axis, MIS23_REG_OBJ, MIS23_SETUP_BITS, &tmp_uint, 4) == RES_FAIL)
	{
		mis23x_object[axis].motor_stat = MOT_SET_PARAM_ERROR;
				ERROR_INIT = 3;

		return RES_FAIL;
	}
	
	// mapping TPDO1
	// disabilita TPDO1
	uint32_t stat_res = RES_SUCCESS;
	tmp_uint = 0x80000180 | node;
	stat_res &= mis23x_SetParameter(axis, 0x1800, 1, &tmp_uint, 4);
	// set trasmissione ad ogni sync ricevuto
	tmp_uint = 0x01;
	stat_res &= mis23x_SetParameter(axis, 0x1800, 2, &tmp_uint, 1);
	tmp_uint = 0x00000;
	stat_res &= mis23x_SetParameter(axis, 0x1A00, 0, &tmp_uint, 1);
	// mappa entry 1 come 0x2012 sub 0x0A lunghezza 4 bytes (posizione attuale)
	tmp_uint = 0x20120A20;
	stat_res &= mis23x_SetParameter(axis, 0x1A00, 1, &tmp_uint, 4);
	// mappa entry 2 come 0x2014 sub 0x23 lunghezza 2 bytes (error bits)
	tmp_uint = 0x20142310;
	stat_res &= mis23x_SetParameter(axis, 0x1A00, 2, &tmp_uint, 4);
	// set di totale 2 entry
	tmp_uint = 0x00002;
	stat_res &= mis23x_SetParameter(axis, 0x1A00, 0, &tmp_uint, 1);
	// attivazione TPDO1
	tmp_uint = 0x00000180 | node;
	stat_res &= mis23x_SetParameter(axis, 0x1800, 1, &tmp_uint, 4);

	// mapping TPDO2
	// disabilita TPDO2
	tmp_uint = 0x80000280 | node;
	stat_res &= mis23x_SetParameter(axis, 0x1801, 1, &tmp_uint, 4);
	// set trasmissione ad ogni sync ricevuto
	tmp_uint = 0x01;
	stat_res &= mis23x_SetParameter(axis, 0x1801, 2, &tmp_uint, 1);
	// reset numero di entry
	tmp_uint = 0x00000;
	stat_res &= mis23x_SetParameter(axis, 0x1A01, 0, &tmp_uint, 1);
	// mappa entry 1 come 0x2012 sub 0x19 lunghezza 4 bytes (status bits)
	tmp_uint = 0x20121920;
	stat_res &= mis23x_SetParameter(axis, 0x1A01, 1, &tmp_uint, 4);
	// mappa entry 2 come 0x2014 sub 0x24 lunghezza 2 bytes (warning bits)
	tmp_uint = 0x20142410;
	stat_res &= mis23x_SetParameter(axis, 0x1A01, 2, &tmp_uint, 4);
	// mappa entry 3 come 0x2014 sub 0xD9 (217) lunghezza 2 bytes (ACTUAL_TORQUE)
	tmp_uint = 0x2014D910;
	stat_res &= mis23x_SetParameter(axis, 0x1A01, 3, &tmp_uint, 4);
	// set di totale 3 entry
	tmp_uint = 0x00003;
	stat_res &= mis23x_SetParameter(axis, 0x1A01, 0, &tmp_uint, 1);
	// attivazione TPDO2
	tmp_uint = 0x00000280 | node;
	stat_res &= mis23x_SetParameter(axis, 0x1801, 1, &tmp_uint, 4);
	
	// mapping PDO3
	// disabilita TPDO3
	tmp_uint = 0x80000380 | node;
	stat_res &= mis23x_SetParameter(axis, 0x1802, 1, &tmp_uint, 4);
	// set trasmissione ad ogni sync ricevuto
	tmp_uint = 0x01;
	stat_res &= mis23x_SetParameter(axis, 0x1802, 2, &tmp_uint, 1);
	// reset numero di entry
	tmp_uint = 0x00000;
	stat_res &= mis23x_SetParameter(axis, 0x1A02, 0, &tmp_uint, 1);
	
	// mappa entry 1 come 0x2012 sub 0x0C lunghezza 4 bytes (velocit�)
	tmp_uint = 0x20120C20;
	stat_res &= mis23x_SetParameter(axis, 0x1A02, 1, &tmp_uint, 4);
	
	// set di totale 1 entry
	tmp_uint = 0x00001;
	stat_res &= mis23x_SetParameter(axis, 0x1A02, 0, &tmp_uint, 1);
	// attivazione TPDO3
	tmp_uint = 0x00000380 | node;
	stat_res &= mis23x_SetParameter(axis, 0x1802, 1, &tmp_uint, 4);

	if(stat_res==RES_FAIL){
		DbgPrintf(DBG_MIS23, "Motor init error: SET PDO failed\r\n");
				ERROR_INIT = 4;

		return RES_FAIL;
	}

	//set default motor position limits and motion parameters
	switch(axis)
	{
		case AXIS_GANTRY:  // gantry
			mis23x_object[axis].home_position = MIS23_GANTRY_HOME_POS;
			mis23x_object[axis].min_position_limit = MIS23_GANTRY_MIN_POS_LIMIT;
			mis23x_object[axis].max_position_limit = MIS23_GANTRY_MAX_POS_LIMIT;
			mis23x_object[axis].home_speed = MIS23_GANTRY_HOME_SPEED;  //convert step/s to rpm *100
			mis23x_object[axis].pos_acc = MIS23_GANTRY_ACC;
			mis23x_object[axis].pos_dec = MIS23_GANTRY_ACC;	//Set as the acceleration
			mis23x_object[axis].pos_speed = MIS23_GANTRY_SPEED;
			mis23x_object[axis].jog_speed = MIS23_GANTRY_JOG_SPEED;
			mis23x_object[axis].motion_stat = MOT_MOTION_COMPLETED;
		break;
		
		case AXIS_BED:  // carrello
			mis23x_object[axis].home_position = MIS23_BED_HOME_POS;
			mis23x_object[axis].min_position_limit = MIS23_BED_MIN_POS_LIMIT;
			mis23x_object[axis].max_position_limit = MIS23_BED_MAX_POS_LIMIT;
			mis23x_object[axis].home_speed = MIS23_BED_HOME_SPEED;  //convert step/s to rpm *100
			mis23x_object[axis].pos_acc = MIS23_BED_ACC;
			mis23x_object[axis].pos_dec = MIS23_BED_ACC;
			mis23x_object[axis].pos_speed = MIS23_BED_SPEED;
			mis23x_object[axis].jog_speed = MIS23_BED_JOG_SPEED;
			mis23x_object[axis].motion_stat = MOT_MOTION_COMPLETED;
		break;
		
		case AXIS_BED_LIFT:  //bed lifter:
			mis23x_object[axis].home_position = MIS23_LIFT_HOME_POS;
			mis23x_object[axis].min_position_limit = MIS23_LIFT_MIN_POS_LIMIT;
			mis23x_object[axis].max_position_limit = MIS23_LIFT_MAX_POS_LIMIT;
			mis23x_object[axis].home_speed = MIS23_LIFT_HOME_SPEED;
			mis23x_object[axis].pos_acc = MIS23_LIFT_ACC;
			mis23x_object[axis].pos_dec = MIS23_LIFT_ACC;
			mis23x_object[axis].pos_speed = MIS23_LIFT_SPEED;
			mis23x_object[axis].jog_speed = MIS23_LIFT_JOG_SPEED;
			mis23x_object[axis].motion_stat = MOT_MOTION_COMPLETED;
		break;
		
		case AXIS_PANEL:  //flat panel
			mis23x_object[axis].home_position = MIS23_FLATP_HOME_POS;
			mis23x_object[axis].min_position_limit = MIS23_FLATP_MIN_POS_LIMIT;
			mis23x_object[axis].max_position_limit = MIS23_FLATP_MAX_POS_LIMIT;
			mis23x_object[axis].home_speed = MIS23_FLATP_HOME_SPEED;
			mis23x_object[axis].pos_acc = MIS23_FLATP_ACC;
			mis23x_object[axis].pos_dec = MIS23_FLATP_ACC;
			mis23x_object[axis].pos_speed = MIS23_FLATP_SPEED;
			mis23x_object[axis].jog_speed = MIS23_FLATP_JOG_SPEED;
			mis23x_object[axis].motion_stat = MOT_MOTION_COMPLETED;
		break;
		
		default:
			return RES_FAIL;
			break;
	}
		
	if(mis23x_SetMaxPosition(axis, mis23x_object[axis].max_position_limit) == RES_FAIL)
	{
		mis23x_object[axis].motor_stat = MOT_SET_PARAM_ERROR;
		DbgPrintf(DBG_MIS23, "Motor init error: SetMaxPosition failed\r\n");
		ERROR_INIT = 5;
		return 0;
	}

	if(mis23x_SetMinPosition(axis, mis23x_object[axis].min_position_limit) == RES_FAIL)
	{
		mis23x_object[axis].motor_stat = MOT_SET_PARAM_ERROR;
		DbgPrintf(DBG_MIS23, "Motor init error: SetMinPosition failed\r\n");
				ERROR_INIT = 6;
		return 0;
	}

	// set default acceleration
	if(mis23x_SetAcc(axis, mis23x_object[axis].pos_acc) == RES_FAIL)
	{
		mis23x_object[axis].motor_stat = MOT_SET_PARAM_ERROR;
		ERROR_INIT = 7;
		return RES_FAIL;
	}
	
	// set default acceleration
	if(mis23x_SetDec(axis, mis23x_object[axis].pos_dec) == RES_FAIL)
	{
		mis23x_object[axis].motor_stat = MOT_SET_PARAM_ERROR;
		ERROR_INIT = 8;
		return RES_FAIL;
	}

	 if(mis23x_GetMotorActPos(axis, &mis23x_object[axis].actual_position) == RES_FAIL)
	{
		mis23x_object[axis].motor_stat = MOT_GET_PARAM_ERROR;
		ERROR_INIT = 9;
		return RES_FAIL;
	}

	
	// set parametri di errore posizione
	// following error
	val = 120000;  // 25000 steps
//	val = 0;  // disabled
	if(mis23x_SetParameter(axis, MIS23_REG_OBJ, MIS23_FLWERRMAX, &val, 4) == RES_FAIL)
	{
		mis23x_object[axis].motor_stat = MOT_SET_PARAM_ERROR;
		ERROR_INIT = 10;
		return RES_FAIL;
	}

	// following error time out
	//val = 2;	// 1ms
	//motor_SetParameter(axis, 0x6066, 0, &val, 2);
	//
	//motor_SetHeartbeat(axis, 100);
	
	// set timeout sulla trasmissione sincrona
	mis23x_object[axis].heardbeat_tout = MOT_MIS23x_HTB_TOUT;
	mis23x_object[axis].heartbeat_timer = timer_TimerSet(10*MOT_MIS23x_HTB_TOUT); 

	// send NMT message to go operational
	co_nmt_Send(mis23x_object[axis].node_id, NMT_START);

#ifdef	OLD_STYLE_COPROCESS
	// Initialize the callback structure for CAN interrupt
	icb.node_id = node;
	icb.intr_cback = mis23x_CbackManage;
	// and add it to the callback list.
	micro_AddCback(&icb);
#else
	//Add canopen filter on motor
	uint32_t filetermask_id[]={PDO1TX_FCODE, PDO2TX_FCODE, PDO3TX_FCODE, EMCY_FCODE};
	for(int index_mask=0;index_mask<sizeof(filetermask_id)/sizeof(uint32_t);index_mask++)
	{
		//Add canopen filter on motor
		int filterid=get_freefilterId();
		FDCAN_FilterTypeDef sFilterConfig;
		sFilterConfig.IdType = FDCAN_STANDARD_ID;
		sFilterConfig.FilterIndex = filterid;
		sFilterConfig.FilterType = FDCAN_FILTER_MASK;
		sFilterConfig.FilterConfig = FDCAN_FILTER_TO_RXFIFO0;
	//	sFilterConfig.FilterID1 = (uint32_t)node + (uint32_t)(ID_MASK & (PDO1TX_FCODE | PDO2TX_FCODE | PDO3TX_FCODE | EMCY_FCODE));
		sFilterConfig.FilterID1 = (uint32_t)ALL_ACCEPT_ID_MASK &
				((uint32_t)node + (uint32_t)(ID_MASK & filetermask_id[index_mask]));
		sFilterConfig.FilterID2 = ALL_ACCEPT_ID_MASK;
		if(filterid<0 || set_initfilterId(filterid, &sFilterConfig) == RES_FAIL){
			DbgPrintf(DBG_DUET, "Init error: no more available filterid\r\n");
			mis23x_object[axis].motor_stat = MOT_INIT_ERROR;
			return RES_FAIL;
		}
		icb.filter_id[index_mask]=filterid;
	}
	// Initialize the callback structure for CAN interrupt
	icb.node_id = node;
	icb.intr_cback = mis23x_Machine;
	// and add it to the callback list.
	micro_AddCback(&icb);
#endif

	// force status as "in position" until the first PDO is received
	mis23x_object[axis].statusword |= In_position;
	mis23x_object[axis].initialized = 1;
	mis23x_object[axis].motor_stat = MOT_READY;
	mis23x_object[axis].motion_stat = MOT_MOTION_COMPLETED;
	reset_Motor_HeartBeat();

	return RES_SUCCESS;
}

/**
* \fn uint32_t mis23x_SetMaxPosition(int axis, int32_t max_pos)
* \brief Set the max position limit for the axis
*
* This function perform the commands to set max position limit in step 
*
* \param int axis Axis number (index in the motor_object array)
* \param int32_t max_pos max position.
* \return  1 = success, 0 = error.
*
*/
uint32_t mis23x_SetMaxPosition(int axis, int32_t max_pos)
{
	int32_t mis_pos = MTP_TO_MIS23(max_pos);
	return mis23x_SetParameter(axis, MIS23_REG_OBJ, MIS23_MAX_P_IST, (uint32_t *)&mis_pos, sizeof(uint32_t));
}

/**
* \fn uint32_t mis23x_SetMinPosition(int axis, int32_t min_pos)
* \brief Set the min position limit for the axis
*
* This function perform the commands to set min position limit in step
*
* \param int axis Axis number (index in the motor_object array)
* \param int32_t min_pos min position.
* \return  1 = success, 0 = error.
*
*/
uint32_t mis23x_SetMinPosition(int axis, int32_t min_pos)
{
	int32_t mis_pos = MTP_TO_MIS23(min_pos);
	return mis23x_SetParameter(axis, MIS23_REG_OBJ, MIS23_MIN_P_IST, (uint32_t *)&mis_pos, sizeof(uint32_t));
}

#ifdef	OLD_STYLE_COPROCESS

/**
 * \fn void mis23x_CbackManage(void *param)
 * \brief Callback function called from CAN peripheral interrupt.
 *        Add an element to the message queue of the motor object.
 *  
 * \param 'void *param' pointer to can_mb_conf_t variable 
 * 
 */
void mis23x_CbackManage(void *param, uint8_t *data)
{
	FDCAN_RxHeaderTypeDef * rx_header = (FDCAN_RxHeaderTypeDef *)param;

	int node_id = rx_header->Identifier & CO_NODEMASK;


#warning "check it ibn microcallback"
	/*
		int f_code = p_mbx->ul_id >> CAN_MID_MIDvA_Pos;
		if(f_code == 0)
			f_code = p_mbx->ul_fid & 0x00000780;
	*/
	int f_code = rx_header->Identifier & CO_CODEMASK;

	// search axis number
	int i;
	for(i=0;i<MOTOR_MAXOBJ;i++)
	{
		if(mis23x_object[i].node_id == node_id)
			break;
	}
	if(i >= MOTOR_MAXOBJ) // error, axis number not found
	{
		DbgPrintf(DBG_MIS23, "received msg from unknown node.\r\n");
		return;
	}
	
	uint32_t datal = (uint32_t)*((uint32_t *)&data[0]);
	uint32_t datah = (uint32_t)*((uint32_t *)&data[4]);

//	mis23x_object[i].rx_queue[mis23x_object[i].rx_queue_write_p].datah = p_mbx->ul_datah;
	mis23x_object[i].rx_queue[mis23x_object[i].rx_queue_write_p].datah = datah;
//	mis23x_object[i].rx_queue[mis23x_object[i].rx_queue_write_p].datal = p_mbx->ul_datal;
	mis23x_object[i].rx_queue[mis23x_object[i].rx_queue_write_p].datal = datal;
	mis23x_object[i].rx_queue[mis23x_object[i].rx_queue_write_p].f_code = f_code;
	mis23x_object[i].rx_queue[mis23x_object[i].rx_queue_write_p].idx =  rx_header->RxTimestamp;
	mis23x_object[i].rx_queue[mis23x_object[i].rx_queue_write_p].timestamp = timer_TimerSet(0);

	mis23x_object[i].rx_queue_write_p++;
	if(mis23x_object[i].rx_queue_write_p >= MOTOR_MSG_QUEUE_SIZE)
		mis23x_object[i].rx_queue_write_p = 0;

}
#endif

/**
 * \fn uint32_t mis23x_FaultReset(int axis)
 * \brief Recover a fault resetting the fault bit in the controlword
 *
 * Read and rewrite controlword with fault reset bit modified.
 *  
 * \param int axis Axis number (index in the motor_object array)
 * \return 1 if success, 0 if error.
 * 
 */
uint32_t mis23x_FaultReset(int axis)
{
	uint32_t result = RES_SUCCESS;
	uint32_t value = 0;
	
	if(mis23x_SetParameter(axis, MIS23_REG_OBJ, MIS23_ERR_BITS, &value, sizeof(uint32_t)) == RES_FAIL)
	result = RES_FAIL;
	
	if(mis23x_SetParameter(axis, MIS23_REG_OBJ, MIS23_WARN_BITS, &value, sizeof(uint32_t)) == RES_FAIL)
	result = RES_FAIL;

	return result;
}

/**
* \fn uint32_t mis23x_StopHere(int axis)
* \brief Command the motor stop setting the stop bit in the controlword
*
* Read and rewrite controlword with stop bit modified.
*
* \param int axis Axis number (index in the motor_object array)
* \return 1 if success, 0 if error.
*
*/
uint32_t mis23x_StopHere(int axis)
{
	uint32_t val = 0;
	
	// metto a 0 la velocit�
	mis23x_SetParameter(axis, MIS23_REG_OBJ, MIS23_V_SOLL, &val, sizeof(uint32_t));
	mis23x_object[axis].motion_stat = MOT_STOPPING;

	return 1;	
}


/**
* \fn uint32_t motor_SetModeOperation(int axis, uint32_t mode)
* \brief Set the operational mode of the motor.
*
* Set the operational mode of the motor:
* At power on the motor drive set the operational mode as "passive mode".
*
* \param int8_t mode New mode of operation.
* \param int axis Axis number (index in the motor_object array)
* \return  1 = mode set successfully, 0 = mode set error.
*
*/
uint32_t mis23x_SetModeOperation(int axis, uint32_t mode)
{
	return mis23x_SetParameter(axis, MIS23_REG_OBJ, MIS23_MODE_REG, &mode, sizeof(uint32_t));
}

/**
* \fn uint32_t motor_SetHome(int axis)
* \brief Set home position for the motor as the current position
*
* Before any operation, the motor drive must perform the homing, 
* not to be confused with the zero position
*
* \param int axis Axis number (index in the motor_object array)
* \return  1 = home set success, 0 = home set error.
*
*/
uint32_t mis23x_SetHome(int axis)
{
	//uint32_t tmpuint;
	uint32_t result = RES_SUCCESS;
	
	mis23x_object[axis].home_position =  MTP_TO_MIS23(mis23x_object[axis].actual_position);
	
	return result;
}


/**
* \fn uint32_t mis23x_SetPosition(int axis, uint32_t new_position)
* \brief Set a new position for the motor and start it
*
* This function perform the commands to set a new absolute position,
* start the motor and wait for position reached, monitoring the 
* statusword.
*
* \param int axis Axis number (index in the motor_object array)
* \param uint32_t new_position New absolute position
* \return  1 = success, 0 = error.
*
*/
uint32_t mis23x_SetPosition(int axis, uint32_t new_position)
{
	//macchina non in potenza PIN_POWERSENSE_IN
	if(!digio_GetPowerStatus())
		return RES_FAIL;

	if(mis23x_object[axis].motion_stat != MOT_MOTION_COMPLETED)
		return RES_FAIL;
	
	uint32_t vel = MIS23_VEL_CONV(mis23x_object[axis].pos_speed);
	if(mis23x_SetParameter(axis, MIS23_REG_OBJ, MIS23_V_SOLL, (uint32_t *)&vel, sizeof(uint32_t)) == RES_FAIL)
		return RES_FAIL;

	uint32_t mis23_pos = MTP_TO_MIS23(new_position);		
	return mis23x_GoAbsolute(axis, mis23_pos);
	
}

/**
* \fn static uint32_t mis23x_GoAbsolute(int axis, uint32_t new_position)
* \brief Set a new position for the motor and commands to start moving
*
* This function perform the commands to set a new absolute position,
* and start the motor.
*
* \param int axis Axis number (index in the motor_object array)
* \param uint32_t new_position New absolute position
* \return  1 = success, 0 = error.
*
*/
static uint32_t mis23x_GoAbsolute(int axis, uint32_t new_position)
{
	uint32_t res = RES_SUCCESS;
	
	if(!digio_GetPowerStatus())
		return RES_FAIL;

	BrakeOff(axis);

	if(!mis23x_SetModeOperation(axis, MIS23_POSITION_MODE))
	{
		mis23x_object[axis].motor_stat = MOT_SET_PARAM_ERROR;
		return RES_FAIL;
	}

	if(!mis23x_SetParameter(axis, MIS23_REG_OBJ, MIS23_P_SOLL, &new_position, sizeof(uint32_t)))
	{
		mis23x_object[axis].motor_stat = MOT_SET_PARAM_ERROR;
		return RES_FAIL;
	}
	
	mis23x_object[axis].motion_stat = MOT_MOVING;
	return res;	
	
}

/**
* \fn static uint32_t mis23x_SetAcc(int axis, uint32_t acc)
* \brief Set the acceleration and deceleration rate for the motor.
*
* This function perform the commands to set motor acceleration and 
* deceleration rate, in step/s^2
*
* \param int axis Axis number (index in the motor_object array)
* \param uint32_t acc Acceleration rate.
* \return  1 = success, 0 = error.
*
*/
uint32_t mis23x_SetAcc(int axis, uint32_t mis_acc)
{
	mis_acc=MIS23_ACC_CONV(mis_acc);
	return mis23x_SetParameter(axis, MIS23_REG_OBJ, MIS23_A_SOLL, &mis_acc, sizeof(uint32_t));
}

/**
* \fn uint32_t mis23x_SetDec(int axis, uint32_t dec)
* \brief Set the deceleration rate for the motor
*
* This function perform the commands to set motor deceleration rate, in step/s^2
*
* \param int axis Axis number (index in the motor_object array)
* \param uint32_t dec Deceleration rate.
* \return  1 = success, 0 = error.
*
*/
uint32_t mis23x_SetDec(int axis, uint32_t mis_dec)
{
	mis_dec=MIS23_ACC_CONV(mis_dec);
	return mis23x_SetParameter(axis, MIS23_REG_OBJ, MIS23_D_SOLL, &mis_dec, sizeof(uint32_t));
}


/**
* \fn uint32_t mis23x_GetMotionStatus(int axis)
* \brief Return the motion status: moving-complete/moving
*
* \param int axis Axis number (index in the motor_object array)
* \return  motion status.
*
*/
uint32_t mis23x_GetMotionStatus(int axis)
{
	return mis23x_object[axis].motion_stat;
}

/**
* \fn uint32_t mis23x_GetMotorStatus(int axis)
* \brief Return the motor status.
*
* \param int axis Axis number (index in the motor_object array)
* \return  motor status.
*
*/
uint32_t mis23x_GetMotorStatus(int axis)
{
	return mis23x_object[axis].motor_stat;
}

/**
* \fn uint32_t mis23x_SetJogSpeed(int axis, uint32_t j_speed)
* \brief Set the motor speed for jog mode in the motor object structure
*
* \param int axis Axis number (index in the motor_object array)
* \param uint32_t j_speed Jog mode speed
* \return  1 = success, 0 = error.
*
*/
uint32_t mis23x_SetJogSpeed(int axis, uint32_t mis_speed)
{
//	uint32_t mis_speed = (j_speed*6000)/16384; //convert step/s to rpm *100
	mis23x_object[axis].jog_speed = mis_speed;
	
	return RES_SUCCESS;
}

/**
* \fn uint32_t motor_GoJogFwd(int axis)
* \brief Set the motor in jog mode and start moving in forward direction. 
*
* \param int axis Axis number (index in the motor_object array)
* \return  1 = success, 0 = error.
*
*/
uint32_t mis23x_GoJogFwd(int axis)
{
	uint32_t res;
	
	//Libera il freno corrispondente all'asse
	BrakeOff(axis);

	uint32_t vel = MIS23_VEL_CONV(mis23x_object[axis].pos_speed);
	mis23x_SetParameter(axis, MIS23_REG_OBJ, MIS23_V_SOLL, (uint32_t *)&vel, sizeof(uint32_t));
	
	int32_t mis23_pos = mis23x_object[axis].max_position_limit - 100;
	mis23_pos = MTP_TO_MIS23(mis23_pos);
	res = mis23x_GoAbsolute(axis, (uint32_t)mis23_pos);

	return res;
}

/**
* \fn uint32_t motor_GoJogRev(int axis)
* \brief Set the motor in jog mode and start moving in reverse direction.
*
* \param int axis Axis number (index in the motor_object array)
* \return  1 = success, 0 = error.
*
*/
uint32_t mis23x_GoJogRev(int axis)
{
	uint32_t res;
	
	//Libera il freno corrispondente all'asse
	BrakeOff(axis);

	uint32_t vel = MIS23_VEL_CONV(mis23x_object[axis].pos_speed);
	mis23x_SetParameter(axis, MIS23_REG_OBJ, MIS23_V_SOLL, (uint32_t *)&vel, sizeof(uint32_t));
	
	int32_t mis23_pos = mis23x_object[axis].min_position_limit + 100;
	mis23_pos = MTP_TO_MIS23(mis23_pos);
	res = mis23x_GoAbsolute(axis, (uint32_t)mis23_pos);

	return res;
}

uint32_t mis23x_ReadObj(int axis, uint32_t index, uint32_t subindex, uint32_t *value)
{
	if(index != MIS23_REG_OBJ)
		return RES_FAIL;
	else
		return mis23x_GetParameter(axis, index, subindex, value, 4);	
	
}

uint32_t mis23x_WriteObj(int axis, uint32_t index, uint32_t subindex, uint32_t value)
{
	if(index != MIS23_REG_OBJ)
		return RES_FAIL;
	else
		return mis23x_SetParameter(axis, index, subindex, &value, 4);
}

/**
* \fn static uint32_t motor_SetParameter(int axis, uint32_t idx, uint8_t subidx, uint32_t * val, uint32_t size)
* \brief Upload an object parameter (register) value to the server node
*  writing to the object 0x2012 and subindex equal the register number
*
* Write (upload) a new value in the server object parameter and verify the operation
* reading back the value.
*
* \param uint32_t idx Object dictionary index
* \param uint8_t subidx Object subindex (register number)
* \param uint32_t * val Pointer to the variable containing the new value
* \param uint32_t size Size in byte of the parameter.
* \return  1 = parameter set successfully, 0 = parameter set error.
*
*/
static uint32_t mis23x_SetParameter(int axis, uint32_t idx, uint8_t subidx, uint32_t * val, uint32_t size)
{
	co_msg_t msg;
	//uint32_t res = 0;
	uint8_t dt_out[8] = {0,0,0,0,0,0,0,0};
	uint8_t dt_in[8] = {0,0,0,0,0,0,0,0};
	
	// set parameter
	memset(&msg, 0, sizeof(co_msg_t));
	msg.node = mis23x_object[axis].node_id;
	msg.index = idx;
	msg.subindex = subidx;
	msg.r_w = 'w';
	memcpy(&dt_out[4], val, size);

	msg.data_in = dt_in;
	msg.data_out = dt_out;
	msg.datalen = size;
	if(!co_sdo_Send(&msg))
		return RES_FAIL;
		
	return RES_SUCCESS;
}

/**
* \fn static uint32_t mis23_GetParameter(int axis, uint32_t idx, uint8_t subidx, uint32_t * val, uint32_t size)
* \brief Download an object parameter value from the server node
*
* read (download) an object dictionary entry value from the server node.
*
* \param uint32_t idx Object dictionary index (fixed at 2012)
* \param uint8_t subidx Object subindex (the reg number to read)
* \param uint32_t * val Pointer to the variable containing the new value
* \param uint32_t size Size in byte of the parameter.
* \return  1 = parameter set successfully, 0 = parameter set error.
*
*/
static uint32_t mis23x_GetParameter(int axis, uint32_t idx, uint8_t subidx, uint32_t * val, uint32_t size)
{
	co_msg_t msg;
	//uint32_t res = 0;
	uint8_t dt_out[8] = {0,0,0,0,0,0,0,0};
	uint8_t dt_in[8] = {0,0,0,0,0,0,0,0};
	
	// read parameter
	memset(&msg, 0, sizeof(co_msg_t));
	msg.node = mis23x_object[axis].node_id;
	msg.index = idx;
	msg.subindex = subidx;
	msg.r_w = 'r';

	msg.data_in = dt_in;
	msg.data_out = dt_out;
	if(!co_sdo_Send(&msg))
		return RES_FAIL;
	
	memcpy(val, &dt_in[4], sizeof(uint32_t));

	return RES_SUCCESS;
}

#ifdef	OLD_STYLE_COPROCESS
/**
* \fn void mis23x_Machine(void)
* \brief Main function for motors management.
*
* This function is added to the main CAN management list of functions and is called from the main CAN 
* routine, temporized by interrupt timer.
* Scans the motor_object list for queued messages to manage.
*
*
*/
void mis23x_Machine(int m_idx)
{
	// checks for sync-heartbeat timeout
	if( (mis23x_object[m_idx].heardbeat_tout != 0) && (timer_TimerTest(mis23x_object[m_idx].heartbeat_timer)) ) //controllo heartbeat timeout
	{ //heartbeat scaduto
		// se un motore non invia heartbeat, non posso sapere neanche se prende i comandi, 
		// quindi levo la corrente a tutti
		digio_ResetRelay(REL_POWER_CHAIN);
		mis23x_object[m_idx].motor_stat = MOT_HBT_ERROR;
		mis23x_object[m_idx].motion_stat = MOT_MOTION_COMPLETED;
		mis23x_object[m_idx].heardbeat_tout = 0;  // disable next check
		//DbgPrintf(DBG_MIS23, "Heartbeat timeout node %d\r\n", mis23x_object[m_idx].node_id);
	}
		
	// looks in the received message queue for new messages
	if(mis23x_object[m_idx].rx_queue_write_p != mis23x_object[m_idx].rx_queue_read_p) 
	{
		// there is new message to elaborate
		motor_in_mesg_t msg;
		memcpy(&msg, &mis23x_object[m_idx].rx_queue[mis23x_object[m_idx].rx_queue_read_p], sizeof(motor_in_mesg_t));
		mis23x_object[m_idx].rx_queue_read_p++;
		if(mis23x_object[m_idx].rx_queue_read_p >= MOTOR_MSG_QUEUE_SIZE)
			mis23x_object[m_idx].rx_queue_read_p = 0;

		// perfoms action depending the type of received message.
		switch (msg.f_code)
		{
		case PDO1TX_FCODE :
			// reset timeout heartbeat
			if(mis23x_object[m_idx].heardbeat_tout != 0)
				mis23x_object[m_idx].heartbeat_timer = timer_TimerSet(mis23x_object[m_idx].heardbeat_tout); 

			mis23x_object[m_idx].actual_position = MIS23_TO_MTP((int32_t)msg.datal);
			
			mis23x_object[m_idx].mis_err_bits = msg.datah & 0x0000FFFF;
			//DbgPrintf(DBG_MIS23, "Actual position node %d, %d\r\n", mis23x_object[m_idx].node_id, mis23x_object[m_idx].actual_position);
			break;
		case PDO2TX_FCODE :
			// reset timeout heartbeat
			if(mis23x_object[m_idx].heardbeat_tout != 0)
				mis23x_object[m_idx].heartbeat_timer = timer_TimerSet(mis23x_object[m_idx].heardbeat_tout);
			
			// status bits
			mis23x_object[m_idx].statusword = msg.datal;
			// warning bits	
			mis23x_object[m_idx].mis_warn_bits =  msg.datah & 0x0000FFFF;
			// actual torque
			mis23x_object[m_idx].actual_current = ((msg.datah & 0xFFFF0000) >> 16);
			break;
		case PDO3TX_FCODE :
			mis23x_object[m_idx].actual_speed = (int)msg.datal;
			//DbgPrintf(DBG_MIS23, "speed %d\r\n", mis23x_object[m_idx].actual_speed);
			break;
		case SDOTX_FCODE :
			if((msg.datal & 0xFF000000) == 0x80000000)  // sdo abort message
				DbgPrintf(DBG_MIS23, "SDO ABORT message from %d, mbx %d, fcode %x, data: %x %x\r\n", mis23x_object[m_idx].node_id, msg.idx, msg.f_code, msg.datah, msg.datal);
			else
				DbgPrintf(DBG_MIS23, "SDO message from %d, mbx %d, fcode %x, data: %x %x\r\n", mis23x_object[m_idx].node_id, msg.idx, msg.f_code, msg.datah, msg.datal);
			break;
		case EMCY_FCODE :
			DbgPrintf(DBG_MIS23, "Emergency message from %d mbx %d, fcode %x, data: %x %x\r\n", mis23x_object[m_idx].node_id, msg.idx, msg.f_code, msg.datah, msg.datal);
			break;
		case NMT_FCODE :
			break;
		case NMTERR_FCODE :
			// reset heartbeat timeout
			//if(mis23x_object[m_idx].heardbeat_tout != 0)
				//mis23x_object[m_idx].heartbeat_timer = timer_TimerSet(mis23x_object[m_idx].heardbeat_tout*2); //aggiunta temporanea bm
			//DbgPrintf(DBG_DUET, "Heartbeat from %d\r\n", motor_object[i].node_id);
			break;			
		//case SYNC_FCODE :
		case TIME_FCODE :
			break;
		default :
			DbgPrintf(DBG_MIS23, "New message from %d, mbx %d, fcode %x, data: %x %x\r\n", mis23x_object[m_idx].node_id, msg.idx, msg.f_code, msg.datah, msg.datal);
			break;
		}  // switch (msg.f_code)			
	}  // if(motor_object[i].rx_queue_write_p != motor_object[i].rx_queue_read_p) 
}
#else

/**
  * @brief 	Main function for motor management.
  *
  * @param  int m_idx	Index of the motor in the global structure
  * @param  FDCAN_RxHeaderTypeDef * rx_headern: pointer of CAN header structure
  * @param  uint32_t RxFifo0ITs: 			type of activated interrupt
  * @return None
  */
void mis23x_Machine(void * param, uint8_t *data)
{
	FDCAN_RxHeaderTypeDef * rx_header = (FDCAN_RxHeaderTypeDef *)(param);
	// search axis number
	int m_idx;
	int node_id = rx_header->Identifier & CO_NODEMASK;
	for(m_idx=0;m_idx<MOTOR_MAXOBJ;m_idx++)
	{
		if(mis23x_object[m_idx].node_id == node_id)
			break;
	}
	if(m_idx >= MOTOR_MAXOBJ) // error, axis number not found
	{
		DbgPrintf(DBG_DUET, "Msg from unknown node\r\n");
		return;
	}

	// checks for sync-heartbeat timeout
	if( (mis23x_object[m_idx].heardbeat_tout != 0) && (timer_TimerTest(mis23x_object[m_idx].heartbeat_timer)) ) //controllo heartbeat timeout
	{ //heartbeat scaduto
		// se un motore non invia heartbeat, non posso sapere neanche se prende i comandi,
		// quindi levo la corrente a tutti
		digio_ResetRelay(REL_POWER_CHAIN);
		mis23x_object[m_idx].motor_stat = MOT_HBT_ERROR;
		mis23x_object[m_idx].motion_stat = MOT_MOTION_COMPLETED;
		mis23x_object[m_idx].heardbeat_tout = 0;  // disable next check
		//DbgPrintf(DBG_MIS23, "Heartbeat timeout node %d\r\n", mis23x_object[m_idx].node_id);
	}

	// reset timeout heartbeat
	if(mis23x_object[m_idx].heardbeat_tout != 0)
		mis23x_object[m_idx].heartbeat_timer = timer_TimerSet(mis23x_object[m_idx].heardbeat_tout);


	uint32_t datal, datah;

	// perfoms action depending the type of received message.
	switch (rx_header->Identifier & CO_CODEMASK)
	{
		case PDO1TX_FCODE :
			datal = (uint32_t)*((uint32_t *)&data[0]);
			datah = (uint32_t)*((uint32_t *)&data[4]);
			/*
			// reset timeout heartbeat
			if(mis23x_object[m_idx].heardbeat_tout != 0)
				mis23x_object[m_idx].heartbeat_timer = timer_TimerSet(mis23x_object[m_idx].heardbeat_tout);
			*/
			mis23x_object[m_idx].actual_position = MIS23_TO_MTP((int32_t)datal);

			mis23x_object[m_idx].mis_err_bits = datah & 0x0000FFFF;
			//DbgPrintf(DBG_MIS23, "Actual position node %d, %d\r\n", mis23x_object[m_idx].node_id, mis23x_object[m_idx].actual_position);
			break;
		case PDO2TX_FCODE :
			datal = (uint32_t)*((uint32_t *)&data[0]);
			datah = (uint32_t)*((uint32_t *)&data[4]);
			/*
			// reset timeout heartbeat
			if(mis23x_object[m_idx].heardbeat_tout != 0)
				mis23x_object[m_idx].heartbeat_timer = timer_TimerSet(mis23x_object[m_idx].heardbeat_tout);
			*/
			// status bits
			mis23x_object[m_idx].statusword = datal;
			// warning bits
			mis23x_object[m_idx].mis_warn_bits =  datah & 0x0000FFFF;
			// actual torque
			mis23x_object[m_idx].actual_current = ((datah & 0xFFFF0000) >> 16);
			break;
		case PDO3TX_FCODE :
			datal = (uint32_t)*((uint32_t *)&data[0]);
			datah = (uint32_t)*((uint32_t *)&data[4]);
			mis23x_object[m_idx].actual_speed = (int)datal;
			//DbgPrintf(DBG_MIS23, "speed %d\r\n", mis23x_object[m_idx].actual_speed);
			break;
		case SDOTX_FCODE :
			datal = (uint32_t)*((uint32_t *)&data[0]);
			datah = (uint32_t)*((uint32_t *)&data[4]);
			if((datal & 0xFF000000) == 0x80000000)  // sdo abort message
				DbgPrintf(DBG_MIS23, "SDO ABORT message from %d, filter %d, fcode %x, data: %x %x\r\n", mis23x_object[m_idx].node_id, rx_header->FilterIndex,
																										rx_header->Identifier & CO_CODEMASK, datah, datal);
			else
				DbgPrintf(DBG_MIS23, "SDO message from %d, filter %d, fcode %x, data: %x %x\r\n", mis23x_object[m_idx].node_id, rx_header->FilterIndex,
																										rx_header->Identifier & CO_CODEMASK, datah, datal);
			break;
		case EMCY_FCODE :
			datal = (uint32_t)*((uint32_t *)&data[0]);
			datah = (uint32_t)*((uint32_t *)&data[4]);
			DbgPrintf(DBG_MIS23, "Emergency message from %d filter %d, fcode %x, data: %x %x\r\n", mis23x_object[m_idx].node_id, rx_header->FilterIndex,
																										rx_header->Identifier & CO_CODEMASK, datah, datal);
			break;
		case NMT_FCODE :
			break;
		case NMTERR_FCODE :
			// reset heartbeat timeout
			//if(mis23x_object[m_idx].heardbeat_tout != 0)
				//mis23x_object[m_idx].heartbeat_timer = timer_TimerSet(mis23x_object[m_idx].heardbeat_tout*2); //aggiunta temporanea bm
			//DbgPrintf(DBG_DUET, "Heartbeat from %d\r\n", motor_object[i].node_id);
			break;
		//case SYNC_FCODE :
		case TIME_FCODE :
			break;
		default :
			datal = (uint32_t)*((uint32_t *)&data[0]);
			datah = (uint32_t)*((uint32_t *)&data[4]);
			DbgPrintf(DBG_MIS23, "New message from %d, filter %d, fcode %x, data: %x %x\r\n", mis23x_object[m_idx].node_id, rx_header->FilterIndex,
																								rx_header->Identifier & CO_CODEMASK, datah, datal);
			break;
	}  // switch (msg.f_code)
}
#endif

/**
* \fn uint32_t mis23x_Home(int axis)
* \brief Performs homing of the motor
*
* Set the home speed and start moving towards the position 0
*
* \param int axis Axis number (index in the motor_object array)
* \return  1 = success, 0 = error.
*
*/
uint32_t mis23x_Home(int axis)
{
	uint32_t vel = MIS23_VEL_CONV(mis23x_object[axis].pos_speed);
	if(mis23x_SetParameter(axis, MIS23_REG_OBJ, MIS23_V_SOLL, (uint32_t *)&vel, sizeof(uint32_t)) == RES_FAIL)
		return RES_FAIL;
	
	return mis23x_GoAbsolute(axis, (uint32_t)mis23x_object[axis].home_position);
}

/**
* \fn uint32_t mis23x_SetSpeed(int axis, uint32_t speed)
* \brief Set the motor speed in the motor object structure.
*
* \param int axis Axis number (index in the motor_object array)
* \speed uint32_t speed Motor speed value in step/s
* \return  1 = success, 0 = error.
*
*/	
uint32_t mis23x_SetSpeed(int axis, uint32_t mis_speed)
{
	mis23x_object[axis].pos_speed = mis_speed;
	
	return RES_SUCCESS;
}

/**
* \fn uint32_t mis23x_SetHomeSpeed(int axis, uint32_t h_speed)
* \brief Set the motor speed for homing
*
* \param int axis Axis number (index in the motor_object array)
* \speed uint32_t h_speed Motor homing speed value in step/s
* \return  1 = success, 0 = error.
*
*/
uint32_t mis23x_SetHomeSpeed(int axis, uint32_t h_speed)
{
	mis23x_object[axis].home_speed = h_speed;
	
	return RES_SUCCESS;
}

/**
* \fn uint32_t mis23x_QuickStop(int axis)
* \brief Commands the 'quick stop' of the motor.
*
* Command the 'quick stop' of the motor setting the bit in the control word.
* After this command the motor must be reinitialized.
*
* \param int axis Axis number (index in the motor_object array)
* \return  1 = success, 0 = error.
*
*/
uint32_t mis23x_QuickStop(int axis)
{
	uint32_t val = R24_EmergencyStopWithDeceleration;
	
	// emergency stop senza decelerazione
	mis23x_SetParameter(axis, MIS23_REG_OBJ, 24, &val, 4);
	// mette il motore in modalit� passiva
	//mis23x_SetModeOperation(axis, MIS23_OP_PASSIVE_MODE);
	
	mis23x_object[axis].motion_stat = MOT_STOPPING;		
	
	return RES_SUCCESS;
}

/**
* \fn uint32_t mis23x_GetMotorActPos(int axis, uint32_t* pos)
* \brief Read the actual position from the motor
*
* \param int axis Axis number (index in the motor_object array)
* \return  the actual absolute position of the motor in step.
*
*/
uint32_t mis23x_GetMotorActPos(int axis, int32_t* pos)
{
	//uint32_t act_pos;
	uint32_t result;
	
	if(mis23x_object[axis].node_id != 0)  // se � inizializzato
	{
		*pos = MIS23_TO_MTP(mis23x_object[axis].actual_position);
		result = RES_SUCCESS;
	}
	else
	{
		*pos = 0;
		result = RES_FAIL;
	}
	
	return result;
}

/**
* \fn uint32_t mis23x_GetMotorActualCurrent(int axis)
* \brief Read the actual current from the motor
*
* \param int axis Axis number (index in the motor_object array)
* \return  the actual current in the motor in mA.
*
*/
uint32_t mis23x_GetMotorActualCurrent(int axis)
{
	uint32_t tmp = (uint32_t) ((float)mis23x_object[axis].rated_current*((float)mis23x_object[axis].actual_current/2047.0f));
	return tmp;
}

/**
* \fn uint32_t mis23x_GetMotorRatedCurrent(int axis)
* \brief Read the nominal rated current the motor
*
* \param int axis Axis number (index in the motor_object array)
* \return  the rated motor current in mA.
*
*/
uint32_t mis23x_GetMotorRatedCurrent(int axis)
{
	return mis23x_object[axis].rated_current;
}

/**
* \fn uint32_t mis23x_SetCurrentLimit(int axis)
* \brief Sets the max current limit beyond which the motor must stop 
*
* \param int axis Axis number (index in the motor_object array)
* \param int max_current limit in mA.
*
*/
uint32_t mis23x_SetCurrentLimit(int axis, int max_current)
{	
	mis23x_object[axis].rated_current = max_current;
	uint32_t tmp_uint = AMP_TO_BIT(mis23x_object[axis].rated_current);
	if(mis23x_SetParameter(axis, MIS23_REG_OBJ, MIS23_RUN_CURRENT, &tmp_uint, 4) == RES_FAIL)
	{
		mis23x_object[axis].motor_stat = MOT_SET_PARAM_ERROR;
		return RES_FAIL;
	}
	return RES_SUCCESS;
}

/**
* \fn uint32_t mis23x_SetCurrentLimitTime(int axis)
* \brief Sets the maximum time during which the current can exceed the limit
*
*	----- FEATURE NOT PRESENT ON THIS MODEL OF MOTOR DRIVE -----
*
* \param int axis Axis number (index in the motor_object array)
* \param uint32_t limit_time in ms
*
*/
uint32_t mis23x_SetCurrentLimitTime(int axis, uint32_t limit_time)
{
//	mis23x_object[axis].limit_current_time_ms = limit_time;		
	return RES_SUCCESS;
}

/**
* \fn uint32_t mis23x_SetZero(int axis)
* \brief Set the actual position as position 0
*
* This function write 0 in the registers P_NEW, P_IST and P_SOLL
* using fast command register 24
*
* \param int axis Axis number (index in the motor_object array)
* \return  1 = success, 0 = error.
*
*/
uint32_t mis23x_SetZero(int axis)
{
	uint32_t result = RES_SUCCESS;
	uint32_t value = 0;
	
	result = mis23x_SetParameter(axis, MIS23_REG_OBJ, MIS23_P_NEW, &value, sizeof(uint32_t));
	value = 316;
	result = mis23x_SetParameter(axis, MIS23_REG_OBJ, MIS23_COMMAND, &value, sizeof(uint32_t));
	value = 119;
	result = mis23x_SetParameter(axis, MIS23_REG_OBJ, MIS23_COMMAND, &value, sizeof(uint32_t));
	
	return result;
}

void mis23x_Manage(int axis)
{

	if(mis23x_object[axis].mis_err_bits & Follow_error) //nel caso che un motore perda passi
	{
		mis23x_StopHere(axis);
		mis23x_object[axis].motor_stat = MOT_FOLLOWING_ERROR;
		mis23x_object[axis].motion_stat = MOT_MOTION_COMPLETED;
		DbgPrintf(DBG_MIS23, "Following error node%d\r\n", mis23x_object[axis].node_id);
	}

	if(mis23x_object[axis].motion_stat != MOT_MOTION_COMPLETED)
	{
		if(!digio_GetPowerStatus())
		{
			DbgPrintf(DBG_MOTOR, "Power down node%d\r\n", mis23x_object[axis].node_id);
			motor_StopHere(axis);  // stop and put in passive mode
			digio_ResetRelay(REL_POWER_CHAIN);
			mis23x_object[axis].initialized = 0;
		}
	}

	switch( mis23x_object[axis].motion_stat)
	{
		case MOT_MOTION_COMPLETED:  // fermo
			if(mis23x_object[axis].mis_err_bits & General_error) //in caso di errore
			{
				mis23x_object[axis].motion_stat = MOT_FAULT_STATE;
				mis23x_object[axis].motor_stat = MOT_ERROR;
			}else{
				mis23x_object[axis].motion_stat = MOT_MOTION_COMPLETED;
				mis23x_object[axis].motor_stat = MOT_READY;
			}
		break;

		case MOT_MOVING:
			// qui leggere il valore dell'encoder per la partenza
			DbgPrintf(DBG_MIS23, "MIS23 MOT_MOVING\r\n");		
			if(mis23x_object[axis].mis_err_bits &  General_error) //nel caso che un motore sia in errore
			{
				// fermo il motore
				mis23x_StopHere(axis);
				mis23x_object[axis].motion_stat = MOT_STOPPING;
			}
			else if(mis23x_object[axis].statusword & In_position)
				mis23x_object[axis].motion_stat = MOT_STOPPING;
		break;
		
		case MOT_STOPPING:
			DbgPrintf(DBG_MIS23, "MIS23 MOT_STOPPING 1\r\n");
			mis23x_object[axis].motion_stat = MOT_DELAY_DISABLE;  // delay per far passare l'inerzia prima di mettere il motore in modo passivo
			mis23x_object[axis].disable_timer = timer_TimerSet(MIS23_DISABLE_TIME);
		break;

		case MOT_DELAY_DISABLE:
		if( timer_TimerTest(mis23x_object[axis].disable_timer) )
		{
			DbgPrintf(DBG_MIS23, "MIS23 MOT_DELAY_DISABLE 1\r\n");
			// aziono il freno
			BrakeOn(axis);
			mis23x_object[axis].motion_stat = MOT_MOTION_COMPLETED;
		}
		break;
		
		case MOT_FAULT_STATE:
		DbgPrintf(DBG_MIS23, "MIS23 MOT_FAULT_STATE mis23x_object[%d].mis_err_bits=%d\r\n",axis,mis23x_object[axis].mis_err_bits);
		// se cessano le condizioni di errore torno in MOT_MOTION_COMPLETED
		if( (mis23x_object[axis].mis_err_bits & General_error) == 0)
			mis23x_object[axis].motion_stat = MOT_MOTION_COMPLETED;
		break;
	}

	/*
	if( ((mis23x_object[axis].statusword & In_position) != 0) && (mis23x_object[axis].motion_stat != MOT_MOTION_COMPLETED) )
	{
		mis23x_object[axis].motion_stat = MOT_MOTION_COMPLETED;
		//if(i == AXIS_GANTRY)
		mis23x_SetModeOperation(axis, MIS23_OP_PASSIVE_MODE);
		BrakeOn(axis);
	}
	else if( (mis23x_object[axis].statusword & In_position) == 0)// se sto muovendo
	mis23x_object[axis].motion_stat = MOT_MOVING;
	
	if(mis23x_object[axis].mis_err_bits & General_error) //nel caso che un motore vada in errore
	{
		mis23x_object[axis].motor_stat = MOT_FAULT_ERROR;
		//mis23x_DecodeErrors(i);
	}
	else
	mis23x_object[axis].motor_stat = MOT_READY;

	if((mis23x_object[axis].mis_warn_bits &  0x000001DF) != 0) //nel caso ci siano warnings
	{
		//mis23x_DecodeWarnings(i);
		if(mis23x_object[axis].motor_stat == MOT_READY)  // segnalo warnings solo se non ci sono gi� errori
		mis23x_object[axis].motor_stat = MOT_WARNINGS;
	}
	*/
}

void mis23x_DecodeErrors(int axis)
{
	DbgPrintf(DBG_MIS23, "*** Axis %d Errors: ***\r\n", axis);
	
	if(mis23x_object[axis].mis_err_bits & Follow_error)
		DbgPrintf(DBG_MIS23, "	Follow Error\r\n");
	if(mis23x_object[axis].mis_err_bits & Output_driver)
		DbgPrintf(DBG_MIS23, "	User Output shorted\r\n");
	if(mis23x_object[axis].mis_err_bits & Position_Limit)
		DbgPrintf(DBG_MIS23, "	Position Limit exceeded\r\n");
	if(mis23x_object[axis].mis_err_bits & Low_bus_voltage)
		DbgPrintf(DBG_MIS23, "	Low Bus Voltage\r\n");
	if(mis23x_object[axis].mis_err_bits & Over_voltage)
		DbgPrintf(DBG_MIS23, "	Over Voltage\r\n");
	if(mis23x_object[axis].mis_err_bits & Temperature_90)
		DbgPrintf(DBG_MIS23, "	Temperature over 90 degrees\r\n");
	if(mis23x_object[axis].mis_err_bits & Internal)
		DbgPrintf(DBG_MIS23, "	Self Diagnostic Failed\r\n");
	if(mis23x_object[axis].mis_err_bits & Absolute_multiturn_encoder_lost_position)
		DbgPrintf(DBG_MIS23, "	Encoder Lost Position\r\n");
	if(mis23x_object[axis].mis_err_bits & Absolute_multiturn_encoder_sensor_counting)
		DbgPrintf(DBG_MIS23, "	Encoder Reed Error\r\n");
	if(mis23x_object[axis].mis_err_bits & No_comm_to_absolute_multiturn_encoder)
		DbgPrintf(DBG_MIS23, "	Encoder Communication Error\r\n");
	if(mis23x_object[axis].mis_err_bits & SSI_encoder_counting)
		DbgPrintf(DBG_MIS23, "	SSI Encoder Error\r\n");
	if(mis23x_object[axis].mis_err_bits & Closed_loop)
		DbgPrintf(DBG_MIS23, "	Closed Loop Error\r\n");
	if(mis23x_object[axis].mis_err_bits & External_memory)
		DbgPrintf(DBG_MIS23, "	External Memory Error\r\n");
	if(mis23x_object[axis].mis_err_bits & Absolute_single_turn_encoder)
		DbgPrintf(DBG_MIS23, "	Single Turn Encoder Error\r\n");
	if(mis23x_object[axis].mis_err_bits & Safe_Torque_Off)
		DbgPrintf(DBG_MIS23, "	Safe Torque Off Error\r\n");
	
}

void mis23x_DecodeWarnings(int axis)
{
	DbgPrintf(DBG_MIS23, "*** Axis %d Warnings: ***\r\n", axis);
	
	if(mis23x_object[axis].mis_warn_bits & Positive_limit_active)
		DbgPrintf(DBG_MIS23, "	Positive Limit Position exceeded\r\n");
	if(mis23x_object[axis].mis_warn_bits & Negative_limit_active)
		DbgPrintf(DBG_MIS23, "	Negative Limit Position exceeded\r\n");
	if(mis23x_object[axis].mis_warn_bits & Positive_limit_has_been_active)
		DbgPrintf(DBG_MIS23, "	Positive Limit Position Has Been Active\r\n");
	if(mis23x_object[axis].mis_warn_bits & Negative_limit_has_been_active)
		DbgPrintf(DBG_MIS23, "	Negative Limit Position Has Been Active\r\n");
	if(mis23x_object[axis].mis_warn_bits & Low_bus_voltage)
		DbgPrintf(DBG_MIS23, "	Low Bus Voltage Voltage\r\n");
	if(mis23x_object[axis].mis_warn_bits & Temperature_80)
		DbgPrintf(DBG_MIS23, "	Temperature over 80 degrees\r\n");
	if(mis23x_object[axis].mis_warn_bits & SSI_encoder)
		DbgPrintf(DBG_MIS23, "	SSI Encoder Error\r\n");
	if(mis23x_object[axis].mis_warn_bits & Driver_overload)
		DbgPrintf(DBG_MIS23, "	Driver Overload\r\n");
}
