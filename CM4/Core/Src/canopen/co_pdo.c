/*! \file co_pdo.c \brief CANopen PDO management functions. */
//*****************************************************************************
//
// File Name	: 'co_pdo.c'
// Title		: CANopen PDO management functions.
// Author		: Alessandro
// Company		: Qprel s.r.l.
// Created		: 06/09/2014
// Revised		: -
// Version		: 1.0
//
///	\ingroup CanOpen
/// \defgroup pdo CANopen PDO management functions.
/// \code #include "co_pdo.h" \endcode
/// \par Overview CANopen PDO management functions.
///
//*****************************************************************************
#include "co_main.h"
#include "co_pdo.h"
#include "co_microdep.h"

//#define DBG_CO_PDO 0	//!< enable debug printf for this module

/// Timer variable for SYNC messages timing
const uint32_t rpdo_cobid[4] = { PDO1RX_FCODE, PDO2RX_FCODE, PDO3RX_FCODE, PDO4RX_FCODE };
	
/**
 * \fn void co_pdo_Send(uint32_t n, uint8_t node, uint8_t * data, int dt_len)
 * \brief Send PDOn to specified node.
 *
 * \param 'uint32_t n' PDO number (1 - 4) 
 * \param 'uint8_t node' server node number identifier
 * \param 'uint8_t * data' pointer to data to transmit with PDO (array of 8 byte max)
 * \param 'int dt_len' number of data byte to transmit in the CANopen message.
 *
 */
void co_pdo_Send(uint32_t n, uint8_t node, uint8_t * data, int dt_len)
{
	volatile co_msg_t msg;
	uint8_t dtout[8];
	
	memset((uint8_t*)&msg,0,sizeof(co_msg_t));
	memset(dtout,0,8);
	
	// compose message
	msg.tx_fid = rpdo_cobid[n];
	msg.rx_fid = 0;
	msg.node = node;

	memcpy(dtout, data, 8);
	
	msg.data_out = dtout;
	msg.datalen = dt_len;
	
	micro_SendPdo(&msg);
}


/**
 * \fn void co_pdo_Send1(uint32_t n, uint8_t node, uint8_t * data, int dt_len)
 * \brief Send PDOn to specified node.
 *
 * \param 'uint32_t n' PDO number (1 - 4) 
 * \param 'uint8_t node' server node number identifier
 * \param 'uint8_t * data' pointer to data to transmit with PDO (array of 8 byte max)
 * \param 'int dt_len' number of data byte to transmit in the CANopen message.
 *
 */
void co_pdo_Send1(uint32_t n, uint8_t node, uint8_t * data, int dt_len)
{
	volatile co_msg_t msg;
	uint8_t dtout[8];
	
	memset((uint8_t*)&msg,0,sizeof(co_msg_t));
	memset(dtout,0,8);
	
	// compose message
	msg.tx_fid = rpdo_cobid[n];
	msg.rx_fid = 0;
	msg.node = node;

	memcpy(dtout, data, 8);
	
	msg.data_out = dtout;
	msg.datalen = dt_len;
	
	micro_SendPdo1(&msg);
}

