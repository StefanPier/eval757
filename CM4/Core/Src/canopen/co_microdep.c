/*! \file co_microdep.c \brief Microcontroller dependent functions. */
//*****************************************************************************
//
// File Name	: 'co_microdep.c'
// Title		: Microcontroller dependent functions for can peripheral management.
// Author		: Stefano
// Company		: Qprel s.r.l.
// Created		: 01/04/22
// Revised		: 
// Version		: 1.0
//
///	\ingroup CanOpen
/// \defgroup microdep Microcontroller dependent functions for can peripheral management.
/// \code #include "co_microdep.h" \endcode
/// \par Overview
///		Low level functions to send and receive can message.
///
//*****************************************************************************
#include "main.h"
#include <co_main.h>
#include <co_microdep.h>
//#include "comm/comm_dbg.h"
#include <string.h>
#include "manage_debug.h"

//!< Mask for can reception interrupt
#define RX_INT_MSK	0xE0 //0xF0	//for 4 mailbox in tx	//0xF8 //for 3 mailbox in tx //0xFC	//for 2 mailbox in tx
#define MAX_CAN_FRAME_DATA_LEN      8	//!< CAN frame max data length

#define MAX_INT_CBACK_ELEMENT	10						//!< Max number of element in callback array
static intr_cback_t int_cback[MAX_INT_CBACK_ELEMENT];	//!< Interrupt callback array, one element each node
static int array_filterid[FILTERID_ARRAY_SIZE];						//!< Filter list to be used on CAN (CAN BE FILTER_FREE OR FILTER_INIT)
static int int_cback_elements = 0;						//!< Actual number of elements in callbackarray
static uint8_t MessageMarker = 0;

static FDCAN_HandleTypeDef *p_can_in_use;	//!< Pointer to CAN peripheral in use, assigned in  micro_InitCan function.

//Flag to notify SDO RTR reply
__IO uint32_t NotificationFIFO1_flag = 0;

//Function to manage new message
static void FDCAN0_NewMessageHandler(FDCAN_HandleTypeDef *hfdcan);
static void FDCAN1_NewMessageHandler(FDCAN_HandleTypeDef *hfdcan, co_msg_t* co_msg);

int_time_t int_time;
/*
static void disable_TCint(void);
static void enable_TCint(void);

static void disable_TCint(void){
	NVIC_DisableIRQ((IRQn_Type)ID_TC0);
	NVIC_DisableIRQ((IRQn_Type)ID_TC1);
	int_time.start=timer_TimerSet(0);
}
static void enable_TCint(void){
	int_time.stop=timer_TimerSet(0);
	NVIC_EnableIRQ((IRQn_Type)ID_TC0);
	NVIC_EnableIRQ((IRQn_Type)ID_TC1);
}
*/

/**
 * \fn Can * micro_GetCanInUse(void)
 * \brief Interface to get the CAN peripheral used for communication.
 *
 * \return the pointer to the CAN peripheral used for communication.
 *
 */
FDCAN_HandleTypeDef * micro_GetCanInUse(void)
{
	return p_can_in_use;
}

/**
 * \fn uint32_t micro_InitCan(Can *p_can, uint32_t b_rate)
 * \brief Initialize CAN peripheral
 *
 * \param 'Can *p_can' pointer to CAN peripheral register structure
 * \return 1 if success, 0 if fails.
 *
 */
uint32_t micro_InitCan(FDCAN_HandleTypeDef *p_can)
{
	uint32_t ul_sysclk;
	
	MessageMarker =	0;	//Reset MessageMarker

	/* assign the peripheral pointer to the global variable */
	p_can_in_use = p_can;
	
	/* initialize the callback structures */
	for(int i=0;i<MAX_INT_CBACK_ELEMENT;i++){
		reset_co_icb(&int_cback[i]);
	}

	//Initialization array_filteridc
	for(int i=0;i<FILTERID_ARRAY_SIZE;i++){
		array_filterid[i] = FILTER_FREE;
	}

	/* Enable CAN peripheral clock and set signal for the transceiver. */
	if(p_can_in_use->Instance == (FDCAN_GlobalTypeDef *)FDCAN1)
	{
		//Out from StandBy low level transceiver
		HAL_GPIO_WritePin(FDCAN1_STBY_GPIO_Port, FDCAN1_STBY_Pin, GPIO_PIN_RESET);
	}
	else	//Same for FDCAN2
	{
	}
		

	if (true)
	{
		/* Configure Rx FIFO 0 watermark to 2 */
		HAL_StatusTypeDef status = HAL_FDCAN_ConfigFifoWatermark(p_can_in_use, FDCAN_CFG_RX_FIFO0, 2);

		/* Activate Rx FIFO 0 watermark notification */
		status = HAL_FDCAN_ActivateNotification(p_can_in_use, FDCAN_IT_RX_FIFO0_WATERMARK, 0);
		
		/* Configure global filter to reject all non-matching frames */
		status = HAL_FDCAN_ConfigGlobalFilter(p_can_in_use, FDCAN_REJECT, FDCAN_REJECT, FDCAN_REJECT_REMOTE, FDCAN_REJECT_REMOTE);

		/* Configure Rx FIFO 0 watermark to 2 */
		status = HAL_FDCAN_ConfigFifoWatermark(p_can_in_use, FDCAN_CFG_RX_FIFO0, 2);

		/* Start the FDCAN module */
		status = HAL_FDCAN_Start(p_can_in_use);

		/********************************************************************************************************************/

		/* Configure Rx filter for example */
/*
		FDCAN_FilterTypeDef sFilterConfig;
		sFilterConfig.IdType = FDCAN_STANDARD_ID;
		sFilterConfig.FilterIndex = 0;
		sFilterConfig.FilterType = FDCAN_FILTER_MASK;
		sFilterConfig.FilterConfig = FDCAN_FILTER_TO_RXFIFO0;
		sFilterConfig.FilterID1 = 0x2;
		sFilterConfig.FilterID2 = 0x7FF; // For acceptance, MessageID and FilterID1 must match exactly
		status = HAL_FDCAN_ConfigFilter(p_can_in_use, &sFilterConfig);
*/
		/* Fifo 0 can overwrite its own*/
		HAL_FDCAN_ConfigRxFifoOverwrite(p_can_in_use, FDCAN_RX_FIFO0, FDCAN_RX_FIFO_OVERWRITE);

		/* Configure Rx FIFO 0 watermark to 2 */
		HAL_FDCAN_ConfigFifoWatermark(p_can_in_use, FDCAN_CFG_RX_FIFO0, 2);

		/* Activate Rx FIFO 0 watermark notification */
		HAL_FDCAN_ActivateNotification(p_can_in_use, FDCAN_IT_RX_FIFO0_WATERMARK, 0);
		/* Activate Rx FIFO 0 overwrite/full notification */
		HAL_FDCAN_ActivateNotification(p_can_in_use, FDCAN_IT_RX_FIFO0_FULL, 0);
		/* Activate Rx FIFO 0 message lost notification */
		HAL_FDCAN_ActivateNotification(p_can_in_use, FDCAN_IT_RX_FIFO0_MESSAGE_LOST, 0);
		/* Activate Rx FIFO 0 message lost notification */
		HAL_FDCAN_ActivateNotification(p_can_in_use, FDCAN_IT_RX_FIFO0_NEW_MESSAGE, 0);
		/* Activate Rx FIFO 1 message lost notification */
		HAL_FDCAN_ActivateNotification(p_can_in_use, FDCAN_IT_RX_FIFO1_NEW_MESSAGE, 0);

		/********************************************************************************************************************/

		return 1;
	}
	else 
		return 0;

}


/**
 * \fn void micro_SendNmt(co_msg_t * co_msg)
 * \brief Send an NMT message to node
 * Initialize the can mailbox with the parameter in the co_msg struct
 * and send the message. Returns the number of the mailbox in which is
 * expected the server response. The caller can check this mailbox
 * with the micro_GetRcvready(int mbx) function, passing the mailbox number
 * returned by micro_Send(co_msg_t * co_msg)
 *
 * \param 'co_msg_t * co_msg' pointer to message structure containing data
 * \return the mailbox number were the answer is expected
 */
void micro_SendNmt(co_msg_t * co_msg)
{
	FDCAN_TxHeaderTypeDef TxHeader;

	/* Prepare Tx Header */
	TxHeader.FDFormat = FDCAN_CLASSIC_CAN;
	TxHeader.BitRateSwitch = FDCAN_BRS_OFF;
	TxHeader.ErrorStateIndicator = FDCAN_ESI_ACTIVE;
	TxHeader.MessageMarker = (uint32_t)(++MessageMarker);

	TxHeader.Identifier = co_msg->tx_fid + co_msg->node;
	TxHeader.IdType = FDCAN_STANDARD_ID;
	TxHeader.TxFrameType = FDCAN_DATA_FRAME;
	TxHeader.DataLength = ((uint32_t)(co_msg->datalen)) << 16;
	TxHeader.TxEventFifoControl = FDCAN_NO_TX_EVENTS;

	HAL_StatusTypeDef status = HAL_FDCAN_AddMessageToTxFifoQ(p_can_in_use, &TxHeader, &co_msg->data_out[0]);
	if(status != HAL_OK){
		debug_printf (DEBUG_CO_MICRODEP, "micro_SendNmt send fail!\r\n");
	}

}

/**
  * @brief 	TCallBack on fifo 0 watermark or fifo full or new message
  *
  * @param  FDCAN_HandleTypeDef *hfdcan: 	pointer or can structure
  * @param  uint32_t RxFifo0ITs: 			type of activated interrupt
  * @return None
  */
void HAL_FDCAN_RxFifo0Callback(FDCAN_HandleTypeDef *hfdcan, uint32_t RxFifo0ITs)
{
	if((RxFifo0ITs & FDCAN_IT_RX_FIFO0_NEW_MESSAGE) != RESET){
//		debug_printf (DEBUG_CO_MICRODEP, "New M\r\n");
#ifdef	OLD_STYLE_COPROCESS
		FDCAN0_NewMessageHandler(hfdcan);
#endif
	}else if((RxFifo0ITs & FDCAN_IT_RX_FIFO0_WATERMARK) != RESET){
		debug_printf (DEBUG_CO_MICRODEP, "FIFO0 watermark\r\n");
	}else if((RxFifo0ITs & FDCAN_IT_RX_FIFO0_FULL) != RESET){
		FDCAN0_NewMessageHandler(hfdcan);
		debug_printf (DEBUG_CO_MICRODEP, "FIFO0 full\r\n");
	}else if((RxFifo0ITs & FDCAN_IT_RX_FIFO0_MESSAGE_LOST) != RESET){
		debug_printf (DEBUG_CO_MICRODEP, "FIFO0 lost\r\n");
	}
}


/**
  * @brief 	TCallBack on fifo 1 watermark or fifo full or new message (used for SFO and RTR message)
  *
  * @param  FDCAN_HandleTypeDef *hfdcan: 	pointer or can structure
  * @param  uint32_t RxFifo0ITs: 			type of activated interrupt
  * @return None
  */
void HAL_FDCAN_RxFifo1Callback(FDCAN_HandleTypeDef *hfdcan, uint32_t RxFifo0ITs)
{
	if((RxFifo0ITs & FDCAN_IT_RX_FIFO1_NEW_MESSAGE) != RESET){
		NotificationFIFO1_flag = 1;
	}else if((RxFifo0ITs & FDCAN_IT_RX_FIFO1_WATERMARK) != RESET){
		debug_printf (DEBUG_CO_MICRODEP, "FIFO1 watermark\r\n");
	}else if((RxFifo0ITs & FDCAN_IT_RX_FIFO1_FULL) != RESET){
		debug_printf (DEBUG_CO_MICRODEP, "FIFO1 full\r\n");
	}else if((RxFifo0ITs & FDCAN_IT_RX_FIFO1_MESSAGE_LOST) != RESET){
		debug_printf (DEBUG_CO_MICRODEP, "FIFO1 lost\r\n");
	}
}

/**
  * @brief 	Reset calback structure
  *
  * @param  intr_cback_t * icb callback pointer to be reset
  *
  * @return filter id 0-127 or -1 if there is not free filter id
  */
void reset_co_icb(intr_cback_t* icb){
	memset((void *)icb, 0, sizeof(intr_cback_t));
	for (int i = 0; i<sizeof(icb->filter_id)/sizeof(int) ;i++)
		icb->filter_id[i] = FILTER_FREE;
}

/**
  * @brief 	get_freefilterId Function to retrieve first filter id to be used
  *
  * @return filter id 0-127 or -1 if there is not free filter id
  */
int get_freefilterId(){
	int res = -1;
	int index=FIRST_FREE_FDCAN_FILTER;
	for (index=FIRST_FREE_FDCAN_FILTER; index < FILTERID_ARRAY_SIZE; index++){
		if(array_filterid[index] != FILTER_INIT){
			res=index;
			break;
		}
	}
	return res;
}

/**
  * @brief 	Set CAN filter as used
  *
  * @param	Filter Id to be set
  * @param	FDCAN_FilterTypeDef *sFilterConfig: Filter to bu set on CAN
  *
  * @return RES_FAIL o RES_SUCCESS
  */
int set_initfilterId(int filterid, FDCAN_FilterTypeDef *sFilterConfig){
	int res = RES_FAIL;
	HAL_StatusTypeDef status;
	if(filterid>=FIRST_FREE_FDCAN_FILTER &&  filterid<FILTERID_ARRAY_SIZE){
		sFilterConfig->FilterIndex=filterid;
		status = HAL_FDCAN_ConfigFilter(p_can_in_use, sFilterConfig);
		if(status == HAL_OK){
			array_filterid[filterid]=FILTER_INIT;
			res = RES_SUCCESS;
		}
	}
	return res;
}

/**
* @brief 	Set CAN filter as used
*
* @param 	Filter ID to be reset
*
* @return RES_FAIL o RES_SUCCESS
*/
int reset_initfilterId(int filterid){
	int res = RES_FAIL;
	HAL_StatusTypeDef status;
	if(filterid>=FIRST_FREE_FDCAN_FILTER &&  filterid<FILTERID_ARRAY_SIZE){
		if(array_filterid[filterid]==FILTER_INIT){
			/* Configure Rx filter for example */
			FDCAN_FilterTypeDef sFilterConfig;
			sFilterConfig.IdType = FDCAN_STANDARD_ID;
			sFilterConfig.FilterIndex = filterid;
			sFilterConfig.FilterType = FDCAN_FILTER_MASK;
			sFilterConfig.FilterConfig = FDCAN_FILTER_DISABLE;
			sFilterConfig.FilterID1 = 0x0;
			sFilterConfig.FilterID2 = 0x0; /* For acceptance, MessageID and FilterID1 must match exactly */
			status = HAL_FDCAN_ConfigFilter(p_can_in_use, &sFilterConfig);
			if(status == HAL_OK){
				array_filterid[filterid]=FILTER_FREE;
				res = RES_SUCCESS;
			}
		}
	}
	return res;
}

/**
  * @brief 	micro_AddCback Add Callback on CAN Bus
  *
  * @param  intr_cback_t * cb_str 	callback structure to add to the callback array
  * @param  uint32_t RxFifo0ITs: 			type of activated interrupt
  * @return >=1 if success , 0 if fails (too much elements in callback array, or the first call).
  */
int micro_AddCback(intr_cback_t * cb_str)
{
	int result = 0;		
			
	// cerco se il nodo passato nella struttura cb_str � gi� presente nella lista
	int list_idx[MAX_INT_CBACK_ELEMENT];
	for (int i=0; i<MAX_INT_CBACK_ELEMENT;i++)
		list_idx[i]=-1;
	int how_many = 0;
	
	for(int i=0; i<MAX_INT_CBACK_ELEMENT; i++)
	{
		if(int_cback[i].node_id == cb_str->node_id)
		{
			list_idx[how_many++] = i;
		}
	}
	
	// If already presents, the previous one has been reused
	//if(list_idx >= 0)
	if(how_many >= 1)
	{
		//////////////////////////////////////////////////////////////////////////////
		//Reset CAN Filter to be exchange
		for(int fi=0; fi<(sizeof(int_cback[list_idx[0]].filter_id)/sizeof(int)); fi++){
			int filter_id=int_cback[list_idx[0]].filter_id[fi];
			if ( filter_id != FILTER_FREE )
				if(reset_initfilterId(filter_id)!=RES_SUCCESS)
					return result;	//Reset Failed
		}
		//////////////////////////////////////////////////////////////////////////////
		//Delete multiple associate initialization
		for (int i=1; i<how_many;i++){
			//Reset CAN Filter
			for(int fi=0; fi<(sizeof(int_cback[list_idx[i]].filter_id)/sizeof(int)); fi++){
				int filter_id=int_cback[list_idx[i]].filter_id[fi];
				if ( filter_id != FILTER_FREE )
					if(reset_initfilterId(filter_id)!=RES_SUCCESS)
						return result;	//Reset Failed
			}
			reset_co_icb(&int_cback[list_idx[i]]);
		}

		//New callback and filter association
		memcpy(&int_cback[list_idx[0]], cb_str, sizeof(intr_cback_t));
		result = int_cback_elements;
		//////////////////////////////////////////////////////////////////////////////

	}
	else
	{
		if(int_cback_elements >= MAX_INT_CBACK_ELEMENT)
			return result;

		memcpy(&int_cback[int_cback_elements], cb_str, sizeof(intr_cback_t));
		int_cback_elements++;
		result = int_cback_elements;
	}
	
	return result;		
}



/**
  * @brief 	Default function to manage NewMessage on FDCAN FIFO0
  *
  * @param  FDCAN_HandleTypeDef *hfdcan 	CAN Module structure to be managed
  * @return None
  */
static void FDCAN0_NewMessageHandler(FDCAN_HandleTypeDef *hfdcan)
{
	HAL_StatusTypeDef status;
	FDCAN_RxHeaderTypeDef RxHeader;
	uint8_t RxData[8];
	uint32_t items = HAL_FDCAN_GetRxFifoFillLevel(p_can_in_use, FDCAN_RX_FIFO0);
	if(items!=0){
	//while(HAL_FDCAN_GetRxFifoFillLevel(p_can_in_use, FDCAN_RX_FIFO0)!=0)
		/* Retrieve Rx messages from RX FIFO0 */
		status = HAL_FDCAN_GetRxMessage(hfdcan, FDCAN_RX_FIFO0, &RxHeader, RxData);
		if(status==HAL_OK){
			int node_id = (int)RxHeader.Identifier;
			int cb_idx = 0;
			while(cb_idx < MAX_INT_CBACK_ELEMENT)
			{
				if(int_cback[cb_idx].node_id == node_id)
				{
					int_cback[cb_idx].intr_cback(&RxHeader,RxData);
					break;
				}
				cb_idx++;
			}
		}else {
//			break;
		}
	}
}

/**
  * @brief 	Default function to manage NewMessage on FDCAN FIFO0
  *
  * @param  FDCAN_HandleTypeDef *hfdcan 	CAN Module structure to be managed
  * @return None
  */
void FDCAN0_AllMessageHandler()
{
	HAL_StatusTypeDef status;
	FDCAN_RxHeaderTypeDef RxHeader;
	uint8_t RxData[8];
	uint32_t items = HAL_FDCAN_GetRxFifoFillLevel(p_can_in_use, FDCAN_RX_FIFO0);
	while(HAL_FDCAN_GetRxFifoFillLevel(p_can_in_use, FDCAN_RX_FIFO0)!=0){
		/* Retrieve Rx messages from RX FIFO0 */
		status = HAL_FDCAN_GetRxMessage(p_can_in_use, FDCAN_RX_FIFO0, &RxHeader, RxData);
		if(status==HAL_OK){
			int node_id = (int)RxHeader.Identifier & CO_NODEMASK;
			int cb_idx = 0;
			while(cb_idx < MAX_INT_CBACK_ELEMENT)
			{
				if(int_cback[cb_idx].node_id == node_id)
				{
					int_cback[cb_idx].intr_cback(&RxHeader,RxData);
					break;
				}
				cb_idx++;
			}
		}else {
			break;
		}
	}
}


/**
  * @brief 	Default function to manage NewMessage on CAN
  *
  * @param  FDCAN_HandleTypeDef *hfdcan 	CAN Module structure to be managed
  * @return None
  */
static void FDCAN1_NewMessageHandler(FDCAN_HandleTypeDef *hfdcan, co_msg_t* co_msg)
{
	HAL_StatusTypeDef status;
	FDCAN_RxHeaderTypeDef RxHeader;
	uint8_t RxData[8];
	uint32_t items = HAL_FDCAN_GetRxFifoFillLevel(p_can_in_use, FDCAN_RX_FIFO1);
//	if(items!=0){
	while(HAL_FDCAN_GetRxFifoFillLevel(p_can_in_use, FDCAN_RX_FIFO1)!=0){
		/* Retrieve Rx messages from RX FIFO0 */
		status = HAL_FDCAN_GetRxMessage(hfdcan, FDCAN_RX_FIFO1, &RxHeader, RxData);
		if(status==HAL_OK){
			co_msg->node = RxHeader.Identifier;
			co_msg->datalen = RxHeader.DataLength;
			memcpy(co_msg->data_in,RxData,8);
		}else {
		}
	}
}


//////////////////////////////////////////////////////////////////////////////////////

uint32_t error_send_count=0;
/**
  * @brief 	Default function to Send PDO on CAN
  *
  * @param  co_msg_t * co_msg pointer to message structure containing data
  * @return None
  */
void micro_SendPdo(volatile co_msg_t * co_msg)
{
	
	FDCAN_TxHeaderTypeDef TxHeader;

	/* Prepare Tx Header */
	TxHeader.FDFormat = FDCAN_CLASSIC_CAN;
	TxHeader.BitRateSwitch = FDCAN_BRS_OFF;
	TxHeader.ErrorStateIndicator = FDCAN_ESI_ACTIVE;
	TxHeader.MessageMarker = (uint32_t)(++MessageMarker);

	TxHeader.Identifier = co_msg->tx_fid + co_msg->node;
	TxHeader.IdType = FDCAN_STANDARD_ID;
	TxHeader.TxFrameType = FDCAN_DATA_FRAME;
	TxHeader.DataLength = ((uint32_t)(co_msg->datalen)) << 16;
	TxHeader.TxEventFifoControl = FDCAN_NO_TX_EVENTS;

	HAL_StatusTypeDef status = HAL_FDCAN_AddMessageToTxFifoQ(p_can_in_use, &TxHeader, &co_msg->data_out[0]);
	if(status != HAL_OK){
		debug_printf (DEBUG_CO_MICRODEP, "micro_SendPdo send fail!\r\n");
	}

}


/**
  * @brief 	Send a PDO message to node, for syncro mode. Used on Atmel
  *
  * @param  co_msg_t * co_msg pointer to message structure containing data
  * @return None
  */
#warning "TODO: delete on stm32"
void micro_SendPdo1(volatile co_msg_t * co_msg)
{
	micro_SendPdo(co_msg);
}



/**
 * @brief 	Send a SDO message to node
 * Initialize the can mailbox with the parameter in the co_msg struct
 * and send the message. Returns the number of the mailbox in which is
 * expected the server response. The caller can check this mailbox
 * with the micro_GetRcvready(int mbx) function, passing the mailbox number
 * returned by micro_Send(co_msg_t * co_msg)
 *
 * @param  co_msg_t * co_msg pointer to message structure containing dataù
 * @return FDCAN_FilterTypeDef filter to be disabled after SDO reply
 */
FDCAN_FilterTypeDef micro_SendSdo(co_msg_t * co_msg)
{

	FDCAN_TxHeaderTypeDef TxHeader;

	/* Prepare Tx Header */
	TxHeader.FDFormat = FDCAN_CLASSIC_CAN;
	TxHeader.BitRateSwitch = FDCAN_BRS_OFF;
	TxHeader.ErrorStateIndicator = FDCAN_ESI_ACTIVE;
	TxHeader.MessageMarker = (uint32_t)(++MessageMarker);

	TxHeader.Identifier = co_msg->tx_fid + co_msg->node;
	TxHeader.IdType = FDCAN_STANDARD_ID;
	TxHeader.TxFrameType = FDCAN_DATA_FRAME;
	TxHeader.DataLength = ((uint32_t)(co_msg->datalen)) << 16;
	TxHeader.TxEventFifoControl = FDCAN_NO_TX_EVENTS;

	/* Set filter to match SDO response */
	FDCAN_FilterTypeDef sFilterConfig;
	sFilterConfig.IdType = FDCAN_STANDARD_ID;
	sFilterConfig.FilterIndex = SDO_FDCAN_FILTER;
	sFilterConfig.FilterType = FDCAN_FILTER_MASK;
	sFilterConfig.FilterConfig = FDCAN_FILTER_TO_RXFIFO1;
	sFilterConfig.FilterID1 = SDOTX_FCODE;
	sFilterConfig.FilterID2 = ID_MASK;
#warning "ToDO use SDOTX_FCODE + co_msg->node as below"
//	sFilterConfig.FilterID1 = SDOTX_FCODE + co_msg->node;
//	sFilterConfig.FilterID2 = 0x7FF; /* For acceptance, MessageID and FilterID1 must match exactly */
	HAL_StatusTypeDef status = HAL_FDCAN_ConfigFilter(p_can_in_use, &sFilterConfig);


	debug_printf(DEBUG_CO_MICRODEP, "Send message to %d, mbx %d, ", co_msg->node, sFilterConfig.FilterIndex);
	debug_printf(DEBUG_CO_MICRODEP, "fcode %x, ", co_msg->tx_fid);
	debug_printf(DEBUG_CO_MICRODEP, "data: %x %x\r\n", *(uint32_t *)(&co_msg->data_out[4]), *(uint32_t *)(&co_msg->data_out[0]));
	
	NotificationFIFO1_flag = 0;
	status = HAL_FDCAN_AddMessageToTxFifoQ(p_can_in_use, &TxHeader, &co_msg->data_out[0]);
	if(status != HAL_OK){
		debug_printf (DEBUG_CO_MICRODEP, "micro_SendPdo send fail!\r\n");
	}
		
	/* return rx mailbox reference */
	return sFilterConfig;
}


/**
 * @brief 	Send a RTR message to node
 * Initialize the can mailbox with the parameter in the co_msg struct
 * and send the message. Returns the number of the mailbox in which is
 * expected the server response. The caller can check this mailbox
 * with the micro_GetRcvready(int mbx) function, passing the mailbox number
 * returned by micro_Send(co_msg_t * co_msg)
 *
 * @param  co_msg_t * co_msg pointer to message structure containing data
 * @return FDCAN_FilterTypeDef filter to be disabled after RTR reply
 */
FDCAN_FilterTypeDef micro_SendRtr(co_msg_t * co_msg)
{

	FDCAN_TxHeaderTypeDef TxHeader;

	/* Prepare Tx Header */
	TxHeader.FDFormat = FDCAN_CLASSIC_CAN;
	TxHeader.BitRateSwitch = FDCAN_BRS_OFF;
	TxHeader.ErrorStateIndicator = FDCAN_ESI_ACTIVE;
	TxHeader.MessageMarker = (uint32_t)(++MessageMarker);

	TxHeader.Identifier = co_msg->tx_fid + co_msg->node;
	TxHeader.IdType = FDCAN_STANDARD_ID;
	TxHeader.TxFrameType = FDCAN_REMOTE_FRAME;
	TxHeader.DataLength = ((uint32_t)(co_msg->datalen)) << 16;
	TxHeader.TxEventFifoControl = FDCAN_NO_TX_EVENTS;

	/* Set filter to match RTR response */
	FDCAN_FilterTypeDef sFilterConfig;
	sFilterConfig.IdType = FDCAN_STANDARD_ID;
	sFilterConfig.FilterIndex = RTR_FDCAN_FILTER;
	sFilterConfig.FilterType = FDCAN_FILTER_MASK;
	sFilterConfig.FilterConfig = FDCAN_FILTER_TO_RXFIFO1;
	sFilterConfig.FilterID1 = NMTERR_FCODE;
	sFilterConfig.FilterID2 = ID_MASK;
#warning "ToDO use SDOTX_FCODE + co_msg->node as below"
//	sFilterConfig.FilterID1 = NMTERR_FCODE + co_msg->node;
//	sFilterConfig.FilterID2 = 0x7FF; /* For acceptance, MessageID and FilterID1 must match exactly */
	HAL_StatusTypeDef status = HAL_FDCAN_ConfigFilter(p_can_in_use, &sFilterConfig);


	debug_printf(DEBUG_CO_MICRODEP, "Send message to %d, mbx %d, ", co_msg->node, sFilterConfig.FilterIndex );
	debug_printf(DEBUG_CO_MICRODEP, "fcode %x, ", co_msg->tx_fid);
	debug_printf(DEBUG_CO_MICRODEP, "data: %x %x\r\n", *(uint32_t *)(&co_msg->data_out[4]), *(uint32_t *)(&co_msg->data_out[0]));

	NotificationFIFO1_flag = 0;
	status = HAL_FDCAN_AddMessageToTxFifoQ(p_can_in_use, &TxHeader, &co_msg->data_out[0]);
	if(status != HAL_OK){
		debug_printf (DEBUG_CO_MICRODEP, "micro_SendRtr send fail!\r\n");
	}

	/* return rx mailbox reference */
	return sFilterConfig;
}


/**
 * @brief 	Send a SYNC message on the CAN bus
 *
 * @return None
 */
void micro_SendSync(void)
{
	FDCAN_TxHeaderTypeDef TxHeader;

	/* Prepare Tx Header */
	TxHeader.FDFormat = FDCAN_CLASSIC_CAN;
	TxHeader.BitRateSwitch = FDCAN_BRS_OFF;
	TxHeader.ErrorStateIndicator = FDCAN_ESI_ACTIVE;
	TxHeader.MessageMarker = (uint32_t)(++MessageMarker);

	TxHeader.Identifier = 0x080;
	TxHeader.IdType = FDCAN_STANDARD_ID;
	TxHeader.TxFrameType = FDCAN_DATA_FRAME;
	TxHeader.DataLength = 0;
	TxHeader.TxEventFifoControl = FDCAN_NO_TX_EVENTS;

	HAL_StatusTypeDef status = HAL_FDCAN_AddMessageToTxFifoQ(p_can_in_use, &TxHeader, NULL);
	if(status != HAL_OK){
		debug_printf (DEBUG_CO_MICRODEP, "micro_SendSync send fail!\r\n");
	}
}

typedef struct  __attribute__ ((packed)){
	uint32_t ms:28,
	unused:4;
	uint16_t day;
	uint16_t to_64bit;
}time_of_day;

#define MS_ON_A_DAY		0x5265BFFL

/**
 * @brief 	Send a TIME STAMP message on the CAN bus
 *
 * @return None
 */
void micro_SendTimeStamp(void)
{
#warning "TO DO !!! Manage work > 49 days hold on"

	FDCAN_TxHeaderTypeDef TxHeader;

	co_msg_t co_msg;
	uint32_t timestamp=timer_TimerSet(0);
	time_of_day time_day ={0,0,0,0};
	time_day.ms = timestamp%MS_ON_A_DAY;
	time_day.day = timestamp/MS_ON_A_DAY;
	co_msg.data_out=(uint8_t*)&time_day;
	co_msg.datalen = 6;
	co_msg_t* co_msg_pt=&co_msg;

	/* Prepare Tx Header */
	TxHeader.FDFormat = FDCAN_CLASSIC_CAN;
	TxHeader.BitRateSwitch = FDCAN_BRS_OFF;
	TxHeader.ErrorStateIndicator = FDCAN_ESI_ACTIVE;
	TxHeader.MessageMarker = (uint32_t)(++MessageMarker);

	TxHeader.Identifier = TIME_FCODE;
	TxHeader.IdType = FDCAN_STANDARD_ID;
	TxHeader.TxFrameType = FDCAN_DATA_FRAME;
	TxHeader.DataLength = ((uint32_t)(co_msg_pt->datalen)) << 16;
	TxHeader.TxEventFifoControl = FDCAN_NO_TX_EVENTS;

	HAL_StatusTypeDef status = HAL_FDCAN_AddMessageToTxFifoQ(p_can_in_use, &TxHeader, &co_msg_pt->data_out[0]);
	if(status != HAL_OK){
		debug_printf (DEBUG_CO_MICRODEP, "micro_SendTimeStamp send fail!\r\n");
	}
}

/**
 * @brief 	Send a SYNC message on the CAN bus
 *
 * @return None
 */
void micro_SendHbeat(void)
{

	FDCAN_TxHeaderTypeDef TxHeader;

	/* Prepare Tx Header */
	TxHeader.FDFormat = FDCAN_CLASSIC_CAN;
	TxHeader.BitRateSwitch = FDCAN_BRS_OFF;
	TxHeader.ErrorStateIndicator = FDCAN_ESI_ACTIVE;
	TxHeader.MessageMarker = (uint32_t)(++MessageMarker);

	TxHeader.Identifier = NMTERR_FCODE;
	TxHeader.IdType = FDCAN_STANDARD_ID;
	TxHeader.TxFrameType = FDCAN_DATA_FRAME;
	TxHeader.DataLength = ((uint32_t)(1)) << 16;
	TxHeader.TxEventFifoControl = FDCAN_NO_TX_EVENTS;

	uint8_t data = 0;
	HAL_StatusTypeDef status = HAL_FDCAN_AddMessageToTxFifoQ(p_can_in_use, &TxHeader, &data);
	if(status != HAL_OK){
		debug_printf (DEBUG_CO_MICRODEP, "micro_SendHbeat send fail!\r\n");
	}
}

/**
 * @brief 	Check the specified mailbox for incoming message on SDO or RTR dedicates one
 *
 * @return 1 if present data, 0 otherwise
 */
uint32_t micro_GetRcvReady()
{
	return NotificationFIFO1_flag;
}


/**
 * @brief 	Retrieve data received from the FDCAN on SDO or RTR dedicates one
 *			Copy the data contained in the data field of the FDCAN
 * 			in the data_in field of the co_msg struct
 *
 * @param  FDCAN_HandleTypeDef *hfdcan 	CAN Module structure to be managed
 * @param  co_msg_t * co_msg Pointer to the co_msg struct where to copy the data
 *
 * @return None
 */
void micro_GetRcvData(co_msg_t * co_msg)
{
	FDCAN1_NewMessageHandler(p_can_in_use, co_msg);
}
