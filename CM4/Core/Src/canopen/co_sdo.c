/*! \file co_sdo.c \brief SDO management module */
//*****************************************************************************
//
// File Name	: 'co_sdo.c'
// Title		: Module containing the Service Data Object management routines.
// Author		: Alessandro Qprel s.r.l.
// Created		: 17/09/2014
// Revised		: -
// Version		: 1.0
//
///	\ingroup CanOpen
/// \defgroup This module contains the routine to send and receive service data object message (SDO).
///           
/// \code #include "co_sdo.h" \endcode
/// \par Overview
///
//*****************************************************************************
#include "co_main.h"
#include "co_sdo.h"
//#include "objdict.h"
#include "co_microdep.h"
//#include "conf_debug.h"
//#include "joinedWdt.h"
#include "manage_debug.h"
#include <string.h>

//#define DBG_CO_SDO 0  //!< Enable debug printf for this module

#define TXSDO_DEFAULT_ID		SDORX_FCODE	//!< defaukt COB-ID for CanOpen RSDO
#define RXSDO_DEFAULT_ID		SDOTX_FCODE	//!< defaukt COB-ID for CanOpen TSDO

/** @name client server command specifiers 
 *  client -> server command specifiers definitions
 */
///@{
#define CSS_INIT_SDO_DWLOAD	0x20
#define CCS_SDO_DWLOAD_SEG	0x00
#define CCS_INIT_SDO_UPLOAD	0x40
#define CCS_SDO_UPLOAD_SEG	0x60
#define CCS_SDO_ABORT		0x80
///@}

/** @name server client command specifiers 
 *  server client command specifiers definitions
 */
///@{
#define SCS_INIT_SDO_DWLOAD	0x60
#define SCS_SDO_DWLOAD_SEG	0x20
#define SCS_INIT_SDO_UPLOAD	0x40
#define SCS_SDO_UPLOAD_SEG	0x00
///@}

/** @name command specifier bits
 *  command specifier bits definitions
 */
///@{
#define CS_EXPEDITED_TRANSFER	0x02
#define CS_DATA_SIZE_INDICATED	0x01
#define CS_MORE_SEGMENT			0x01
#define CS_TOGGLE_BIT			0x10
///@}

/// Private function to start upload
static int co_sdo_InitiateUpload(co_msg_t * msg);
/// Private function to upload segment
static int co_sdo_UploadSegments(uint32_t n, co_msg_t * msg);
/// Private function to start upload
static int co_sdo_InitiateDownload(co_msg_t * msg);

/**
 * \fn int co_sdo_Send(co_msg_t * msg)
 * \brief Public function to send an SDO message to node server
 *
 * Calls the appropriate function to read
 * (upload) data from the server, or write (download) data to server,
 * and wait for results.
 *  
 * \param co_msg_t * msg Struct containing the message parameters
 * \return 1 if success, 0 if error.
 * 
 */
int co_sdo_Send(co_msg_t * msg)
{
	int res;
	
	if(msg->r_w == 'r')
	{
		res = co_sdo_InitiateUpload(msg);
	}
	else
	{
		res = co_sdo_InitiateDownload(msg);
	}
	
	return res;
}

/**
 * \fn static int co_sdo_InitiateUpload(co_msg_t * msg)
 * \brief Start upload (read) data from server
 *
 * Takes parameters from msg struct and compose the data field
 * for the CanOpen frame, calls micro_Send(msg) function and wait
 * for server reply. If data to upload needs a segmented transfer, calls 
 * co_sdo_UploadSegments(byte_to_upload, msg) function.
 *  
 * \param co_msg_t * msg Struct containing the message parameters
 * 
 */
static int co_sdo_InitiateUpload(co_msg_t * msg)
{
	FDCAN_FilterTypeDef rx_mbx_ref;
	
	msg->tx_fid = TXSDO_DEFAULT_ID;
	msg->rx_fid = RXSDO_DEFAULT_ID;
	
	msg->data_out[3] = msg->subindex;
	msg->data_out[2] = msg->index >> 8;
	msg->data_out[1] = msg->index & 0xFF;
	msg->data_out[0] = CCS_INIT_SDO_UPLOAD;
	for(int i=4;i<8;i++)
		msg->data_out[i] = 0;
	msg->datalen = 8;
	
	// send message on the bus
	rx_mbx_ref = micro_SendSdo(msg);
	
	// start timer and wait for answer
	uint32_t timeout = timer_TimerSet(200);
	int tout = 1;
	do
	{
		// check for answer ..
		if(micro_GetRcvReady())
		{
			micro_GetRcvData(msg);
			tout = 0;
			break;
		}
	} while (!timer_TimerTest(timeout));
	if(tout)  // timeout error
	{
		DbgPrintf(DBG_CO_SDO, "SDO send timeout.\r\n");
		return 0;
	}
		
	// decode 	
	uint8_t specifier = msg->data_in[0];
	if(specifier & CCS_SDO_ABORT)  // sdo abort
	{
		return 0;
	}
	else
	{
		switch(specifier & 0x03)
		{
			case 1:  // e=0, s=1
			{
				uint32_t byte_to_upload = (uint32_t)msg->data_in[4] | (uint32_t)msg->data_in[6] << 8;
				if(!co_sdo_UploadSegments(byte_to_upload, msg))
					return 0;
			}
			break;
			case 2:
			break;
			case 3:
			break;
		}
	}
	
	return 1;
}

/**
 * \fn static int co_sdo_UploadSegments(uint32_t n, co_msg_t * msg)
 * \brief perform segmented data upload from the server. 
 *
 * this function is called by co_sdo_InitiateUpload(co_msg_t * msg)
 * to upload segmented data.
 *  
 * \param co_msg_t * msg Struct containing the message parameters
 * \param uint32_t n Number of data byte to be uploaded 
 * 
 */
static int co_sdo_UploadSegments(uint32_t n, co_msg_t * msg)
{
	uint8_t toggle = 0;
	uint8_t c_bit;
	FDCAN_FilterTypeDef rx_mbx_ref;

	do 
	{
		msg->tx_fid = TXSDO_DEFAULT_ID;
		msg->rx_fid = RXSDO_DEFAULT_ID;
			
		msg->data_out[3] = msg->subindex;
		msg->data_out[2] = msg->index >> 8;
		msg->data_out[1] = msg->index & 0xFF;
		msg->data_out[0] = CCS_SDO_UPLOAD_SEG;
		if(toggle == 1)
		{
			msg->data_out[0] |= CS_TOGGLE_BIT;
			toggle = 0;
		}
		else
			toggle = 1;
		for(int i=4;i<8;i++)
			msg->data_out[i] = 0;
		msg->datalen = 8;
			
		rx_mbx_ref = micro_SendSdo(msg);
			
		// start timer and wait for answer
		uint32_t timeout = timer_TimerSet(1000);
		do
		{
			// check for answer ..
			if(micro_GetRcvReady())
			{
				micro_GetRcvData(msg);
				//co_sdo_PrintSrvMsg(msg);
				break;
			}
			joinedWdt_restart();
		} while (!timer_TimerTest(timeout));

		// decode
		uint8_t specifier = msg->data_in[0];
		if(specifier & CCS_SDO_ABORT)  // sdo abort
		{
			return 0;
		}

		// get c bit
		c_bit = msg->data_in[0] & CS_MORE_SEGMENT;
	} while (c_bit == 0);
	
	return 1;
}

/**
 * \fn static int co_sdo_InitiateDownload(co_msg_t * msg)
 * \brief Perform expedited download transfer of data to the node server
 *
 * Takes parameters from msg struct and compose the data field
 * for the CanOpen frame, calls micro_Send(msg) function and wait
 * for server reply.
 *  
 * \param co_msg_t * msg Struct containing the message parameters
 * \return 1 if success, 0 if fails.
 * 
 */
static int co_sdo_InitiateDownload(co_msg_t * msg)
{
	FDCAN_FilterTypeDef rx_mbx_ref;
	
	msg->tx_fid = TXSDO_DEFAULT_ID;
	msg->rx_fid = RXSDO_DEFAULT_ID;
	
	msg->data_out[3] = msg->subindex;
	msg->data_out[2] = msg->index >> 8;
	msg->data_out[1] = msg->index & 0xFF;
	if(msg->datalen == 0)
	{
		msg->data_out[0] = CSS_INIT_SDO_DWLOAD | CS_EXPEDITED_TRANSFER;
		//msg->data_out[0] = CSS_INIT_SDO_DWLOAD;
		//msg->datalen = 8;
	}
	else
	{
		msg->data_out[0] = CSS_INIT_SDO_DWLOAD | CS_EXPEDITED_TRANSFER | CS_DATA_SIZE_INDICATED;
		//msg->data_out[0] = CSS_INIT_SDO_DWLOAD | CS_DATA_SIZE_INDICATED;
		msg->data_out[0] |= (4-msg->datalen) << 2;
	}
	
	msg->datalen = 8;
	rx_mbx_ref = micro_SendSdo(msg);
	
	// start timer and wait for answer
	uint32_t timeout = timer_TimerSet(200);
	int tout = 1;
	do
	{
		// check for answer ..
		if(micro_GetRcvReady())
		{
			micro_GetRcvData(msg);
			//co_sdo_PrintSrvMsg(msg);
			tout = 0;
			break;
		}
	} while (!timer_TimerTest(timeout));
	if(tout)  // timeout error
	{
		DbgPrintf(DBG_CO_SDO, "SDO send timeout.\r\n");
		return 0;
	}
		
	// decode 	
	uint8_t specifier = msg->data_in[0];
	if(specifier & CCS_SDO_ABORT)  // sdo abort
	{
		return 0;
	}
		
	return 1;
}


	
