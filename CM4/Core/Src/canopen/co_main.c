/*
 * co_main.c
 *
 *  Created on: Apr 4, 2022
 *
 *      Author: stefano
 *
 *	\ingroup CanOpen
 *	\defgroup main CanOpen master machine processor
 *	\code #include "co_main.h" \endcode
 *	\par Overview Process messages coming from nodes, calling node management
 *	              callback functions registered from node initialization routines.
 *
 *****************************************************************************/

#include "co_main.h"
#include "co_microdep.h"
#include "co_nmt.h"
#include "motors.h"

#define COMAIN_HBEAT_INTERVAL 500	//Deve essere multiplo del clock del TC0 che per adesso è a 20ms (50Hz)

/// Array of node management callback functions
static p_cback_t cb_array[CO_MAIN_CBACK_ARRAY_SIZE];		//CallBack CAN

/// Number of nodes registered in the management callback array
//static int cb_array_idx = 0;
/// SYNC interval in ms
static uint32_t sync_interval = 0;
/// TIME STAMP interval in ms
static uint32_t timer_interval = 0;
/// Timer variable for TIME STAMP message timing
static uint32_t timestamp_timer = 0;
/// Timer variable for SYNC messages timing
static uint32_t sync_timer = 0;
/// HEARTBEAT interval in ms
static uint32_t hbeat_interval = 0;

/// Private function for master machine processor timing.
static void co_main_InitTiming(TIM_HandleTypeDef *htim);

/**
 * @brief 	Initialize the CanOpen management machine
 * @param  	FDCAN_HandleTypeDef *p_can pointer to FDCAN to use
 * @param	TIM_HandleTypeDef *htim pointer to timer to use to send
 * 			sync and timestam message over can
 * @return RES_SUCCESS if success, RES_FAIL otherwise
 */
uint32_t co_main_Init(FDCAN_HandleTypeDef *p_can, TIM_HandleTypeDef *htim)
{
	// Initalize CAN peripheral
	if(!micro_InitCan(p_can))
		return RES_FAIL;

	// Reset the callback array.
	for(int i=0; i<CO_MAIN_CBACK_ARRAY_SIZE; i++){
		cb_array[i].p_cback = NULL;
		cb_array[i].tipo = NOT_TYPED;
	}

	// Initialize timer for SYNC message and callback functions.
	co_main_InitTiming(htim);

	// Send message on CAN bus to reset all nodes
	co_nmt_Send(0, NMT_RESET);

	// Activate sync from main board
	co_main_StartSync(MOT_SYNC_RX_TOUT);

	return RES_SUCCESS;
}


/**
 * @brief 	Can be used to process FDCAN0 FIFO0 message
 * @param  	None
 * @return 	None
 */
/**
 * \fn void co_main_Process(void)
 * \brief Can be used to call in polling motors and collimator management functions.
 *        Alternative to colling callback functions in cb_array[].
 *
 */
// questa funzioine non è più usata: vengono chiamate le funzioni di
// callback in cb_array su interrupt di del timer
void co_main_Process(void)
{
	FDCAN0_AllMessageHandler();
}

/**
 * \fn void co_main_InitTiming(void)
 * \brief Initialize timer (100Hz) used to send SYNC message and call the
 *        callback functions in cb_array[], to read Canopen Message.
 *
 */
static void co_main_InitTiming(TIM_HandleTypeDef *htim)
{

	HAL_StatusTypeDef status = HAL_TIM_Base_Start_IT(htim);
}

/**
 * \fn void TC_Canopen_Handler(void)
 * \brief co_main timer interrupt handling function.
 *        Send SYNC message and call callback functions
 *        of macroblocks (motors, dosimeter, collimator).
 *
 */
void TC_Canopen_Handler(TIM_HandleTypeDef *htim)
{

	if(sync_interval != 0)
	{
		if(timer_TimerTest(sync_timer))
		{
			sync_timer = timer_TimerSet(sync_interval);
			micro_SendSync();
		}else if (timer_TimerTest(timestamp_timer)){
			timestamp_timer = timer_TimerSet(timer_interval);
			micro_SendTimeStamp();
		}
	}

#ifdef OLD_STYLE_COPROCESS
	for(int i=0; i<CO_MAIN_CBACK_ARRAY_SIZE; i++){
		if(cb_array[i].tipo!=NOT_TYPED){
			cb_array[i].p_cback();
		}
	}
#else
	//Previous for statement substitution:
	co_main_Process();
#endif

}

/**
 * \fn int co_main_AddCback(p_cback_t pcb)
 * \brief Add callback function to the callback array.
 *        Must be called from motors, dosimeter and collimator initialization functions.
 *
 * \param 'p_cback_t pcb' pointer to callback function
 *
 *	\return 1 if succeed, 0 otherwise
 */
int co_main_AddCback(p_cback_t pcb)
{
	int i=0;
	for(i=0;i<CO_MAIN_CBACK_ARRAY_SIZE;i++)
		if(cb_array[i].tipo==pcb.tipo)
			break;
	//Not already present or not more memory for callback
	if(i>=CO_MAIN_CBACK_ARRAY_SIZE){
		for(i=0;i<CO_MAIN_CBACK_ARRAY_SIZE;i++)
			if(cb_array[i].tipo==NOT_TYPED)
				break;
		if(i>=CO_MAIN_CBACK_ARRAY_SIZE)
			return 0;
	}

	cb_array[i] = pcb;
	return 1;
}

/**
 * \fn void co_main_SetSync(uint32_t ms)
 * \brief Set the SYNC interval in ms
 *
 * \param 'uint32_t ms' SYNC message interval in ms
 *         if 0 disable sending SYNC messages.
 *
 */
void co_main_StartSync(uint32_t ms)
{
	sync_interval = ms;
	sync_timer = timer_TimerSet(sync_interval);
	timer_interval = 4*sync_interval;
	timestamp_timer = timer_TimerSet(timer_interval);
	// restart di tutti i timers dei motori
	co_main_SetHbeat(COMAIN_HBEAT_INTERVAL);
}

void co_main_StopSync(void)
{
	sync_interval = 0;
	timer_interval = 0;
	// fermo anche l'hb altrimenti ho una condizione di errore
	co_main_SetHbeat(0);
}

/**
 * \fn void co_main_SetHbeat(uint32_t ms)
 * \brief Set the HEARTBEAT interval in ms
 *
 * \param 'uint32_t ms' HEARTBEAT message interval in ms
 *         if 0 disable sending HEARTBEAT messages.
 *
 */
void co_main_SetHbeat(uint32_t ms)
{
	hbeat_interval = ms;
}

uint32_t co_main_GetHbeatInterval(void)
{
	return hbeat_interval;
}
