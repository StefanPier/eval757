/*! \file co_nmt.c \brief CanOpen network management module */
//*****************************************************************************
//
// File Name	: 'co_nmt.c'
// Title		: Module containing network management function.
// Author		: Alessandro Qprel s.r.l.
// Company		: Qprel s.r.l.
// Created		: 17/09/2014
// Revised		: -
// Version		: 1.0
//
///	\ingroup CanOpen
/// \defgroup This module contains the routine to send CanOpen network management messages
///
/// \code #include "co_nmt.h" \endcode
/// \par Overview
///
//*****************************************************************************
#include "manage_debug.h"
#include <co_nmt.h>
#include "co_main.h"
#include "co_microdep.h"
//#include "comm/comm_dbg.h"

//#define DBG_CO_NMT 0	//!< Enable debug printf for this module

#define TXNMT_DEFAULT_ID		0x00	//!< defaukt COB-ID for CanOpen NMT messages
#define RXNMT_DEFAULT_ID		0x00	//!< defaukt COB-ID for CanOpen NMT messages

/**
 * \fn void co_nmt_Send(uint8_t node, uint8_t cmd)
 * \brief Sends an NMT message to node server
 *
 * \param uint8_t node Server node identifier
 * \param uint8_t cmd Command to send
 * 
 */
void co_nmt_Send(uint8_t node, uint8_t cmd)
{
	co_msg_t co_msg;
	uint8_t dt_out[8] = {0,0,0,0,0,0,0,0};

	// create message
	co_msg.data_in = NULL;
	co_msg.datalen = 2;
	co_msg.tx_fid = TXNMT_DEFAULT_ID;
	co_msg.node = TXNMT_DEFAULT_ID;  // i messaggi di NMT hanno ID 0x00
	co_msg.r_w = 'w';
	dt_out[0] = cmd;
	dt_out[1] = node;
	co_msg.data_out = dt_out;

	// send message
	micro_SendNmt(&co_msg);
	
}

/**
 * \fn void co_nmt_Decode(uint8_t * data)
 * \brief Decode network management messages coming from nodes.
 *
 * \param 'uint8_t * data' message payload with operating code
 * 
 */
void co_nmt_Decode(uint8_t * data)
{
	if(data[0] == 0)
		DbgPrintf(DEBUG_CO_NMT, "NMT message : Boot-up\r\n");
	else if(data[0] == 4)
		DbgPrintf(DEBUG_CO_NMT, "NMT message : Stopped\r\n");
	else if(data[0] == 5)
		DbgPrintf(DEBUG_CO_NMT, "NMT message : Operational\r\n");
	else if(data[0] == 127)
		DbgPrintf(DEBUG_CO_NMT, "NMT message : Pre-operational\r\n");

}

