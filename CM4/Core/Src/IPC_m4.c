#include "main.h"
#include "settings.h"
#include "openamp.h"
#include "CM4_State.h"
#include "IPC_m4.h"
#include "buffer.h"
#include "protocol.h"
#include "ipc_protocol.h"
#include "ipc_command.h"

/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
#define RPMSG_SERVICE_NAME              "ipc_imaginalis_service"

/* Private variables ---------------------------------------------------------*/
static volatile uint32_t tout_ipc_update=0;
static volatile IPC_msg_t received_data __attribute__ ((__section__ ("._ipc_buffer"))) ={0};
static volatile int  message_received  __attribute__ ((__section__ ("._ipc_buffer"))) = 0;
//static volatile IPC_msg_t received_data[MAX_IPCC_QUEUE_SIZE] __attribute__ ((__section__ ("._ipc_buffer"))) =  = {0};
//static volatile _queue_IPC_msg_t received_data __attribute__ ((__section__ ("._ipc_buffer"))) = {0};
static struct rpmsg_endpoint rp_endpoint;
extern update_cm4_state_t cm4_state;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

HSEM_TypeDef * HSEM_DEBUG = HSEM;

/** @brief Callback for IPC rx queue investigation on CM4
 *
 *  @param	none
 *  @return IPC_msg_t message from CM7
 */
static int rpmsg_recv_callback(struct rpmsg_endpoint *ept, void *data,
               size_t len, uint32_t src, void *priv)
{
	received_data = *((IPC_msg_t *) data);
	message_received=1;

	//Manage Message from CM7
	int data_check = ipc_protocol_Process((cmd_msg_t *)&received_data);
	//Try to recognize other IPC_msg_t
	if(data_check==RES_FAIL){
		steute_msg_t * steute_msg = (steute_msg_t *)(&received_data);
		if(steute_msg->channel == IPC_STEUTE_CHANNEL){
			//Change Steute State object
			CM4_UpdateSteuteState(&steute_msg->steute_state);
		}
	}else{
		exe_ipc_cmd();
	}

	return 0;
}

/** @brief Blocking Receive IPC Message from CM7
 *
 *  @param	none
 *  @return IPC_msg_t message from CM7
 */
IPC_msg_t blocking_receive_message(void)
{
	while (message_received == 0)
	{
		OPENAMP_check_for_message();
	}
	message_received = 0;

	return received_data;
}

/** @brief Not Blocking Receive IPC Message from CM7
 *
 *  @param	IPC_msg_t *message from CM7
 *  @return
 *  	RES_SUCCESS if there is a message from CM7
 *  	RES_FAIL if there is any message from CM7
 */
int notblocking_receive_message(IPC_msg_t *rcv)
{
	if (message_received == 0)
	{
		OPENAMP_check_for_message();
		*rcv = received_data;
	}
	if(message_received){
		message_received--;
		if(message_received<0)
			message_received=0;
		return RES_SUCCESS;
	}else{
		return RES_FAIL;
	}
}


/** @brief Drop a message from CM7
 *
 *  @param	none
 *  @return none
 */
void drop_message(void)
{

	MAILBOX_Clear();

}


/** @brief IPC channel same Protocol Init
 *
 *  @param	none
 *  @return none
 */
void comm_IPC_Init()
{
	memset((void *)&received_data,0,sizeof(_queue_IPC_msg_t));
	binaryproto_function_init();
	ipc_command_system_init();
}

/** @brief IPC channel Initialization
 *
 *  @param	none
 *  @return none
 */
void IPC_init(void){
	IPC_msg_t message={0};

	int32_t status = 0;
	/* Inilitize the mailbox use notify the other core on new message */
	MAILBOX_Init();

	/* Inilitize OpenAmp and libmetal libraries,
	 * blocked from the MASTER *wait until the resource_table is correctly initialized */
	if (MX_OPENAMP_Init(RPMSG_REMOTE, NULL)!= HAL_OK)
		Error_Handler();

	/* create a endpoint for rmpsg communication*/
	status = OPENAMP_create_endpoint(&rp_endpoint, RPMSG_SERVICE_NAME, RPMSG_ADDR_ANY, rpmsg_recv_callback, NULL);
	if (status < 0)
	{
		Error_Handler();
	}



	/* Pingpong application*/
	/* Reveice an interger from the master, incremennt it and send back the result to the master*/
	memset(&message,0,sizeof(message));

	//Wait first command to set destination address
	message = blocking_receive_message();

	tout_ipc_update = HAL_GetTick();
}

/** @brief Send Update CM4 state on CM7 core
 *
 *  @param	none
 *  @return RPMSG_SUCCESS if send succefully
 */
int IPC_SendUpdate(void){
	IPC_msg_t message= (IPC_msg_t)cm4_state;
	int status = OPENAMP_send(&rp_endpoint, &message, sizeof(message));
	return status;
}

/** @brief Send Update CM4 state on CM7 core
 *
 *  @param	none
 *  @return RPMSG_SUCCESS if send succefully
 */
int IPC_SendDebug(cBuffer * buff){
	IPC_msg_t message;
	message.cm4_msg.channel = IPC_DEBUG_CHANNEL;
	int status = 0;
	int lenbuff = bufferGetDatalength(buff);
	int max_char_msg = sizeof(message.cm4_msg.msg_data);
	int nmess = lenbuff/max_char_msg;
	int msg_idx =0;
	int data_idx =0;
	int buff_data_idx =0;
	if(nmess==0)
	{
		//Only one message
		for (data_idx=0;data_idx<lenbuff;data_idx++){
			message.cm4_msg.msg_data[data_idx]=bufferGetAtIndex(buff,data_idx);
		}
		//Zero terminated string
		message.cm4_msg.msg_data[data_idx]=0;
		status = OPENAMP_send(&rp_endpoint, &message, sizeof(message));
	}else{
		if (lenbuff%max_char_msg==0)
			nmess--;	//To avoid to not send one all zeros message
		//More then one message (and even the last truncate one if rest ccurs: lenbuff%max_char_msg!=0)
		for (msg_idx=0;msg_idx<=nmess;msg_idx++){
			memset(message.cm4_msg.msg_data,0,max_char_msg);
			//Only one message
			for (data_idx = 0;data_idx<(max_char_msg-1) && buff_data_idx < lenbuff; data_idx++){
				message.cm4_msg.msg_data[data_idx]=bufferGetAtIndex(buff,data_idx);
				buff_data_idx++;
			}
			//Zero terminated string (for partial message, avoidig bad debug printf on CM7)
			message.cm4_msg.msg_data[data_idx]=0;
			status = OPENAMP_send(&rp_endpoint, &message, sizeof(message));
			if(status<0)
				break;
		}
	}
	if(status<0)
		status++;
	return status;
}

/** @brief Send Protocol Message reply from CM4 on CM7 core
 *
 *  @param	none
 *  @return RPMSG_SUCCESS if send succefully
 */
int IPC_SendReply(IPC_msg_t *message){
	int status = OPENAMP_send(&rp_endpoint, message, sizeof(IPC_msg_t));
	return status;
}

/** @brief Manage IPC channel to/from CM7 core
 *
 *  @param	none
 *  @return none
 */
void IPC_Manage(){
	IPC_msg_t message={0};
	//Update CM4 state
	/*
	if((HAL_GetTick() - tout_ipc_update) > UPDATE_TIME_CM4_STATE)
	{
		IPC_SendUpdate();
		tout_ipc_update=HAL_GetTick();
	}
	*/
	//Manage IPC all messages from CM7
	notblocking_receive_message(&message);
}
