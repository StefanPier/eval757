/*! \file dig_io.c \brief digital input/output management. */
//*****************************************************************************
//
// File Name	: 'dig_io.c'
// Title		: input/output management.
// Author		: Author: Alessandro
// Company		: Qprel s.r.l.
// Created		: 01/04/2015
// Revised		:
// Version		: 1.0
// Target MCU	: SAM4E
//
/// \par Overview

///		Functions, data structures and macros for digital input/output management.
//
//*****************************************************************************
#include "main.h"
#include <string.h>
#include "dig_io.h"
//#include "adconv.h"
#include "motors.h"
#include "timer.h"
#include "manage_debug.h"
#include "linact.h"

//#define DEBUG_DIGIO true

extern uint8_t exti_counter[LAST_EXTI_IRQn_idx];
dr_times_t dr_trigger_times;

trigger_status_t trigger_status;
volatile uint32_t oldTrig=0;
uint32_t weelCounterVar=0;

bool trigMachineFlag = false;

static volatile int trig_machine;			//!< status of machine for RX and image transfer synchronization
static volatile int trig_machine_back;		//!< status of machine to return from waiting Panel XRay Enable signal
static volatile uint32_t trig_count;		//!< counter for pulse measurement
static volatile uint32_t total_trig_count;	//!< counter for pulse measurement

static volatile uint8_t dummy_trig_machine=0;


//! Photocell Interrupt service
static void digio_FCellIntr(void);
static void PotenzaInt (uint32_t a, uint32_t b);
//! HandSwitch Interrupt service
static void HandSwitchInt (void);
//static void LinakInt (uint32_t a, uint32_t b);
/**
 * \fn bool digio_SetRelay(int relay_n)
 * \brief Activate relay
 *
 * \param 'int relay_n' (0 - 5)
 * \return true if relay_n is in the range 0 - 5,
 *         false if out of range.
 *
 */
bool digio_SetRelay(int relay_n)
{
	bool res = true;
	
	if((relay_n < 0)||(relay_n > 5))
		res = false;
	else
	{
		switch(relay_n)
		{
			case 0:
				HAL_GPIO_WritePin(REL1_PORT, REL1_GPIO, RELS_ACTIVE_LEVEL);
				break;
			case 1:
				HAL_GPIO_WritePin(REL2_PORT, REL2_GPIO, RELS_ACTIVE_LEVEL);
				break;
			case 2:
				HAL_GPIO_WritePin(REL3_PORT, REL3_GPIO, RELS_ACTIVE_LEVEL);
				break;
			case 3:
				HAL_GPIO_WritePin(REL4_PORT, REL4_GPIO, RELS_ACTIVE_LEVEL);
				break;
			case 4:
				HAL_GPIO_WritePin(REL5_PORT, REL5_GPIO, RELS_ACTIVE_LEVEL);
				break;
			case 5:
				HAL_GPIO_WritePin(REL6_PORT, REL6_GPIO, RELS_ACTIVE_LEVEL);
				break;
		}
	}
	return res;		
}

/**
 * \fn bool digio_ResetRelay(int relay_n)
 * \brief deactivate relay
 *
 * \param 'int relay_n' (0 - 5)
 * \return true if relay_n is in the range 0 - 5,
 *         false if out of range.
 *
 */
bool digio_ResetRelay(int relay_n)
{
	bool res = true;
	
	if((relay_n < 0)||(relay_n > 5))
		res = false;
	else
	{
		switch(relay_n)
		{
			case 0:
				HAL_GPIO_WritePin(REL1_PORT, REL1_GPIO, RELS_INACTIVE_LEVEL);
				break;
			case 1:
				HAL_GPIO_WritePin(REL2_PORT, REL2_GPIO, RELS_INACTIVE_LEVEL);
				break;
			case 2:
				HAL_GPIO_WritePin(REL3_PORT, REL3_GPIO, RELS_INACTIVE_LEVEL);
				break;
			case 3:
				HAL_GPIO_WritePin(REL4_PORT, REL4_GPIO, RELS_INACTIVE_LEVEL);
				break;
			case 4:
				HAL_GPIO_WritePin(REL5_PORT, REL5_GPIO, RELS_INACTIVE_LEVEL);
				break;
			case 5:
				HAL_GPIO_WritePin(REL6_PORT, REL6_GPIO, RELS_INACTIVE_LEVEL);
				break;
		}
	}
	return res;
}

/**
 * \fn bool digio_GetRelStat(int relay_n)
 * \brief return the relay status
 *
 * \param 'int relay_n' (0 - 5)
 * \return true if relay_n is activated,
 *         false if deactivated.
 *
 */
bool digio_GetRelStat(int relay_n)
{
	bool res = false;

	if((relay_n < 0)||(relay_n > 5))
		res = false;
	else
	{
		switch(relay_n)
		{
			case 0:
//				res = ioport_get_pin_level(REL0_GPIO);
				res = (bool)HAL_GPIO_ReadPin(REL1_PORT, REL1_GPIO);
				break;
			case 1:
				res = (bool)HAL_GPIO_ReadPin(REL2_PORT, REL2_GPIO);
				break;
			case 2:
				res = (bool)HAL_GPIO_ReadPin(REL3_PORT, REL3_GPIO);
				break;
			case 3:
				res = (bool)HAL_GPIO_ReadPin(REL4_PORT, REL4_GPIO);
				break;
			case 4:
				res = (bool)HAL_GPIO_ReadPin(REL5_PORT, REL5_GPIO);
				break;
			case 5:
				res = (bool)HAL_GPIO_ReadPin(REL6_PORT, REL6_GPIO);
				break;
		}
	}
	return res;	
}

//#define VDIGIO	IN_0_10_0

/**
 * \fn uint32_t digio_ReadDInput(void)
 * \brief read all the digital inputs
 *
 * \return the state of the inputs
 *
 */
uint32_t digio_ReadDInput(void)
{
	uint32_t io = 0;
	
	// read true digital input
	//io = ioport_get_port_level(GPIO_DINS_PORT, GPIO_DINS_MASK);
	//io = GPIO_DINS_POS(io);
	io |= (bool)HAL_GPIO_ReadPin(DIGITIN1_PORT, DIGITIN1_GPIO) << 0;
	io |= (bool)HAL_GPIO_ReadPin(DIGITIN2_PORT, DIGITIN2_GPIO) << 1;
	io |= (bool)HAL_GPIO_ReadPin(DIGITIN3_PORT, DIGITIN3_GPIO) << 2;
	io |= (bool)HAL_GPIO_ReadPin(DIGITIN4_PORT, DIGITIN4_GPIO) << 3;
	io |= (bool)HAL_GPIO_ReadPin(DIGITIN5_PORT, DIGITIN5_GPIO) << 4;
	io |= (bool)HAL_GPIO_ReadPin(DIGITIN6_PORT, DIGITIN6_GPIO) << 5;
	io |= (bool)HAL_GPIO_ReadPin(DIGITIN7_PORT, DIGITIN7_GPIO) << 6;
	io |= (bool)HAL_GPIO_ReadPin(DIGITIN8_PORT, DIGITIN8_GPIO) << 7;
	
	// read virtual digital input (from analog input)
#warning "integrare anche la parte dell'adc!!!!!!!!!!"
	/*
	uint32_t v_io = adconv_GetChannel(VDIGIO);
	if(v_io > 1024)
		io |= (1 << 8);
	*/
	return io;
}

/**
 * \fn bool digio_SetOutput(int out_n)
 * \brief Activate digital out
 *
 * \param 'int out_n' (0 - 7)
 * \return true if out_n is in the range 0 - 7,
 *         false if out of range.
 *
 */
bool digio_SetOutput(int out_n)
{
	bool res = true;
	
	if((out_n < 0)||(out_n > 7))
		res = false;
	else
	{
		switch(out_n)
		{
			case 0:
//				ioport_set_pin_level(OUT0_GPIO, OUTS_ACTIVE_LEVEL);
				HAL_GPIO_WritePin(DIGITOUT1_PORT, DIGITOUT1_GPIO, OUTS_ACTIVE_LEVEL);
				break;
			case 1:
				HAL_GPIO_WritePin(DIGITOUT2_PORT, DIGITOUT2_GPIO, OUTS_ACTIVE_LEVEL);
				break;
			case 2:
				HAL_GPIO_WritePin(DIGITOUT3_PORT, DIGITOUT3_GPIO, OUTS_ACTIVE_LEVEL);
				break;
			case 3:
				HAL_GPIO_WritePin(DIGITOUT4_PORT, DIGITOUT4_GPIO, OUTS_ACTIVE_LEVEL);
				break;
			case 4:
				HAL_GPIO_WritePin(DIGITOUT5_PORT, DIGITOUT5_GPIO, OUTS_ACTIVE_LEVEL);
				break;
			case 5:
				HAL_GPIO_WritePin(DIGITOUT6_PORT, DIGITOUT6_GPIO, OUTS_ACTIVE_LEVEL);
				break;
			case 6:
				HAL_GPIO_WritePin(DIGITOUT7_PORT, DIGITOUT7_GPIO, OUTS_ACTIVE_LEVEL);
				break;
			case 7:
				HAL_GPIO_WritePin(DIGITOUT8_PORT, DIGITOUT8_GPIO, OUTS_ACTIVE_LEVEL);
				break;
		}
	}
	return res;
}

/**
 * \fn bool digio_ResetOutput(int out_n)
 * \brief deactivate digital out
 *
 * \param 'int out_n' (0 - 7)
 * \return true if out_n is in the range 0 - 7,
 *         false if out of range.
 *
 */
bool digio_ResetOutput(int out_n)
{
	bool res = true;
	
	if((out_n < 0)||(out_n > 7))
		res = false;
	else
	{
		switch(out_n)
		{
			case 0:
//				ioport_set_pin_level(OUT0_GPIO, OUTS_INACTIVE_LEVEL);
				HAL_GPIO_WritePin(DIGITOUT1_PORT, DIGITOUT1_GPIO, OUTS_INACTIVE_LEVEL);
				break;
			case 1:
				HAL_GPIO_WritePin(DIGITOUT2_PORT, DIGITOUT2_GPIO, OUTS_INACTIVE_LEVEL);
				break;
			case 2:
				HAL_GPIO_WritePin(DIGITOUT3_PORT, DIGITOUT3_GPIO, OUTS_INACTIVE_LEVEL);
				break;
			case 3:
				HAL_GPIO_WritePin(DIGITOUT4_PORT, DIGITOUT4_GPIO, OUTS_INACTIVE_LEVEL);
				break;
			case 4:
				HAL_GPIO_WritePin(DIGITOUT5_PORT, DIGITOUT5_GPIO, OUTS_INACTIVE_LEVEL);
				break;
			case 5:
				HAL_GPIO_WritePin(DIGITOUT6_PORT, DIGITOUT6_GPIO, OUTS_INACTIVE_LEVEL);
				break;
			case 6:
				HAL_GPIO_WritePin(DIGITOUT7_PORT, DIGITOUT7_GPIO, OUTS_INACTIVE_LEVEL);
				break;
			case 7:
				HAL_GPIO_WritePin(DIGITOUT8_PORT, DIGITOUT8_GPIO, OUTS_INACTIVE_LEVEL);
				break;
		}
	}
	return res;
}

/**
 * \fn void io_Init(void)
 * \brief Initialize timer and interrupts for RX synchronization and
 *		  image acquisition.	
 */
uint8_t digio_Init(void)
{
	uint8_t res1=RES_FAIL, res2=RES_FAIL;
	
	memset (exti_counter, 0, sizeof(exti_counter));
	memset(&trigger_status,0, sizeof(trigger_status_t));
	weelCounterGest(WEEL_RESET);
	
	trigger_status.triggerMode=NULL_MODE;
	trigger_status.panelReadySignalEnable= 0;
	trigger_status.triggerEnable= TRIGGER_RX_DISABLE;
	//trigger_status.triggerSource= PHONIC_WEEL_SOURCE,
	
	trigger_status.t_delay= DEFAULT_T_DELAY;
	trigger_status.t_interframe= DEFAULT_T_INTERFRAME;
	trigger_status.t_panel= DEFAULT_T_PANELPULSE;
	trigger_status.t_rxPulse= DEFAULT_T_RXPULSE;
	
	trig_machine= 0;
	res1= digio_InitTrigTimer();
	res2= PinInInterruptEnable();
	
	PotenzaInt(0, 0);

//	ioport_set_pin_level(PANEL_PULSE_OUT, OUTS_INACTIVE_LEVEL);
	HAL_GPIO_WritePin(PANEL_PULSE_OUT_PORT, PANEL_PULSE_OUT_GPIO, OUTS_INACTIVE_LEVEL);
	
	if( (res1==RES_FAIL) || (res2==RES_FAIL) ) 
		return RES_FAIL; //errore
	else
		return RES_SUCCESS; //nessun errore
}

/**
 * \fn void digio_FCellIntr(void)
 * \brief phonic wheel input interrupt handler
 *
 */
static void digio_FCellIntr(void) //chiamato da interrupt pin
{
	// per DR e FL ignoro trigger fotocellula
	if ( (trigger_status.triggerMode == FL_MODE) || (trigger_status.triggerMode == DR_MODE))
		return;
	
	weelCounterGest(WEEL_INC);
	
	if (trigger_status.triggerEnable == TRIGGER_RX_ENABLE) //se i raggi sono abilitati
	{
		//if (trigger_status.triggerSource== PHONIC_WEEL_SOURCE ) digio_TrigStart();
		digio_TrigStart();
	}
}

/**
 * \fn 		void digio_TrigStart(void)
 * \brief 	Pulses generation start for phonic wheel synchronization.
 *			Called from interrupt generated by phonic wheel sensor.
 *
 */
void digio_TrigStart(void)
{
	if(trigger_status.triggerMode == CT_SPEED_MODE) //se sono in modalit� speed mode
	{
		// se c'� gi� stato un trig precedente questo lo salto
		// e rimetto a 0 per fare il prossimo
		if(oldTrig)
		{
			oldTrig = 0; //rimetto a 0 la variabile ed esco
			return;
		}
		else //nessun trig precedente
		{
			oldTrig = 1; //preparo ad 1 la variabile per saltare il prossimo
		}
	}

	// se il ciclo precedente non � finito sono in errore
	if(trig_machine != 0 )
	{
		trigMachineStopCondition();
		DbgPrintf(DBG_DIGIO, "ERR Pulse velocity\r\n");
		trigger_status.fastErrorCounter++;
		DbgPrintf(DBG_DIGIO, "fastErrorCounter %d\r\n",trigger_status.fastErrorCounter);
		return;
	}
	
	if(trigger_status.triggerCounter > trigger_status.maxSessionTrigger) //controllo raggiungimento max scatti
	{
		trigMachineStopCondition();
		trigger_status.tooManyTriggerCounter++;
		DbgPrintf(DBG_DIGIO, "trigger_status.tooManyTriggerCounter %d\r\n",trigger_status.tooManyTriggerCounter);
		DbgPrintf(DBG_DIGIO, "ERR threshold pulse counter overcome\r\n");
		return;
	}
	
	trig_machine = 0;
	trig_count = 0;
//	ioport_set_pin_level(PANEL_PULSE_OUT, OUTS_ACTIVE_LEVEL); //pannello on
	HAL_GPIO_WritePin(PANEL_PULSE_OUT_PORT, PANEL_PULSE_OUT_GPIO, OUTS_ACTIVE_LEVEL);
//	tc_start(TC1, 0); //Start timer
	HAL_TIM_Base_Start_IT(&htim_trigger);
}

uint32_t weelCounterGest (uint32_t data)
{
	uint32_t result = 0;

	switch(data)
	{
		case WEEL_RESET:
		weelCounterVar=0;
		break;
		
		case WEEL_INC:
		weelCounterVar++;
		break;
		
		case WEEL_DEC:
		if(weelCounterVar >0) weelCounterVar--;
		break;
		
		case WEEL_RICH:
		result = weelCounterVar;
		break;
	}

	return result;
}

/**
 * \fn void digio_InitTrigTimer(void)
 * \brief Initialize timer for trigger signal synchronization (100us)
 *
 */
uint8_t digio_InitTrigTimer(void)
{
	uint32_t ul_tcclks;
	uint32_t ul_div;
	uint8_t res=RES_SUCCESS;

	DbgPrintf(DBG_DIGIO, "STAT Initialize interrupt pin I and timer\r\n");
	/*
	// Configure PMC.
	pmc_enable_periph_clk(ID_TC3);
	tc_find_mck_divisor(TRIG_FREQ, sysclk_get_main_hz(), &ul_div, &ul_tcclks, sysclk_get_main_hz() );
	tc_init(TC1, 0, ul_tcclks | TC_CMR_CPCTRG | TC_CMR_WAVSEL_UP_RC);
	tc_write_rc(TC1, 0,((sysclk_get_main_hz() / ul_div) / TRIG_FREQ) );
	// Configure and enable interrupt on RC compare.
	NVIC_EnableIRQ((IRQn_Type)ID_TC3);
	NVIC_SetPriority((IRQn_Type)ID_TC3, 2);
	tc_enable_interrupt(TC1, 0, TC_IER_CPCS);
	*/
	TIM_MasterConfigTypeDef sMasterConfig = {0};
	//100us -> 10kHz
	htim_trigger.Instance = TIM7;
	htim_trigger.Init.Prescaler = 100;
	htim_trigger.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim_trigger.Init.Period = 120;
	htim_trigger.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim_trigger) != HAL_OK)
	{
		res = RES_FAIL;
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim_trigger, &sMasterConfig) != HAL_OK)
	{
		res = RES_FAIL;
		Error_Handler();
	}

	__HAL_RCC_TIM7_CLK_ENABLE();
	/* TIM7 interrupt Init */
	HAL_NVIC_SetPriority(TIM7_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(TIM7_IRQn);

/****************  DEBUG ***********/
//	tc_start(TC1, 0);	/* Start timer. */
/****************  DEBUG ***********/

	//// abilitazione interrupt sul pin ingresso fotocellula
	//pmc_enable_periph_clk(ID_PIOD);
	//pio_set_input(PIOD, PIO_PD26, PIO_DEGLITCH | PIO_DEBOUNCE);
	//res= pio_handler_set(PIOD, ID_PIOD, PIO_PD26, PIO_IT_RISE_EDGE, digio_FCellIntr)?RES_FAIL:RES_SUCCESS;
	//pio_enable_interrupt(PIOD, PIO_PD26); //era il 20
	//NVIC_EnableIRQ(PIOD_IRQn);

	return res;
}	

// abilitazione dell'interrupt ruota fonica: attivato solo all'inizio della CT
uint32_t digio_FCellEnable(void)
{
	uint32_t res=RES_SUCCESS;
	// Enabling photocell interrupt
	/*
	pmc_enable_periph_clk(ID_PIOD);
	pio_set_input(PIOD, PIO_PD26, PIO_DEGLITCH | PIO_DEBOUNCE);
	res = pio_handler_set(PIOD, ID_PIOD, PIO_PD26, PIO_IT_RISE_EDGE, digio_FCellIntr)?RES_FAIL:RES_SUCCESS;
	pio_enable_interrupt(PIOD, PIO_PD26); //era il 20
	NVIC_EnableIRQ(PIOD_IRQn);
	*/
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	PHOTOCELL_CLK;
    GPIO_InitStruct.Pin = PHOTOCELL_GPIO;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(PHOTOCELL_PORT, &GPIO_InitStruct);

    HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(EXTI4_IRQn);

	return res;
}

void digio_FCellDisable(void)
{
	//	pio_disable_interrupt(PIOD, PIO_PD26);
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	PHOTOCELL_CLK;
    GPIO_InitStruct.Pin = PHOTOCELL_GPIO;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(PHOTOCELL_PORT, &GPIO_InitStruct);
}

uint8_t PinInInterruptEnable (void)
{
//	volatile uint8_t res2=RES_FAIL, res3=RES_FAIL; //se 1 errore, se 0 ok.
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	DbgPrintf(DBG_DIGIO, "STAT initialize interrupt pin II\r\n");
	
	//pmc_enable_periph_clk(ID_PIOD); //inserire qui le impostazioni per la gestione dell'interrupt del panel ready
	//pio_set_input(PIOD, PIN_FLATPANELREADY_IN, PIO_DEGLITCH | PIO_DEBOUNCE);
	//res1= pio_handler_set(PIOD, ID_PIOD, PIN_FLATPANELREADY_IN, INTERRUPT_EDGE_SENSE_T, PanelReadyInt)?RES_FAIL:RES_SUCCESS;
	//pio_enable_interrupt(PIOD, PIN_FLATPANELREADY_IN);
	/*
	PIN_FLATPANELREADY_IN_CLK;
    GPIO_InitStruct.Pin = PIN_FLATPANELREADY_IN_GPIO;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(PIN_FLATPANELREADY_IN_PORT, &GPIO_InitStruct);
	*/
	
	//pmc_enable_periph_clk(ID_PIOD); //inserire qui le impostazioni per la gestione dell'interrupt della "potenza"
	/*
	pio_set_input(PIOD, PIN_POWERSENSE_IN, PIO_DEGLITCH | PIO_DEBOUNCE);
	res2= pio_handler_set(PIOD, ID_PIOD, PIN_POWERSENSE_IN, INTERRUPT_EDGE_SENSE_T, PotenzaInt)?RES_FAIL:RES_SUCCESS;
	pio_enable_interrupt(PIOD, PIN_POWERSENSE_IN);
	*/
#warning	"PIN_POWERSENSE_IN"
	//PE15
	PIN_POWERSENSE_IN_CLK;
    GPIO_InitStruct.Pin = PIN_POWERSENSE_IN_GPIO;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(PIN_POWERSENSE_IN_PORT, &GPIO_InitStruct);
#warning	"PIN_HANDSWITCH_IN"
	/*
	pio_set_input(PIOD, PIN_HANDSWITCH_IN, PIO_DEGLITCH | PIO_DEBOUNCE);
	res3= pio_handler_set(PIOD, ID_PIOD, PIN_HANDSWITCH_IN, INTERRUPT_EDGE_SENSE_T, HandSwitchInt)?RES_FAIL:RES_SUCCESS;
	pio_enable_interrupt(PIOD, PIN_HANDSWITCH_IN);
	*/
    //PH3
	PIN_HANDSWITCH_IN_CLK;
    GPIO_InitStruct.Pin = PIN_HANDSWITCH_IN_GPIO;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(PIN_HANDSWITCH_IN_PORT, &GPIO_InitStruct);


	// interrup ingresso feedback motore Linak
	//pio_set_input(PIOD, PIO_PD27, PIO_DEGLITCH | PIO_DEBOUNCE);
	//res1= pio_handler_set(PIOD, ID_PIOD, PIO_PD27, INTERRUPT_EDGE_SENSE_T, LinakInt)?RES_FAIL:RES_SUCCESS;
	//pio_enable_interrupt(PIOD, PIO_PD27);
	
//	NVIC_EnableIRQ(PIOD_IRQn);
    /* EXTI interrupt init*/
    //PH3, POWERSENSE
    HAL_NVIC_SetPriority(EXTI3_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(EXTI3_IRQn);
    //PE15, HANDSWITCH
    HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

//	if( (res1==1) || (res2==1) || (res3==1) )
    /*
	if( (res2==RES_FAIL) || (res3==RES_FAIL) )
		return RES_FAIL; //errore
	else
	*/
		return RES_SUCCESS; //nessun errore
}

/**
  * @brief EXTI line detection callbacks
  * @param GPIO_Pin: Specifies the pins connected EXTI line
  * @retval None
  */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{

	if (GPIO_Pin == PIN_HANDSWITCH_IN_GPIO)
	{
		//Handswitch control
		HandSwitchInt();
	}

	if (GPIO_Pin == PIN_POWERSENSE_IN_GPIO)
	{
		//Handswitch control
		digio_FCellIntr();
	}

#ifdef	LINACT_HALL
	if (GPIO_Pin == LINACT_HALL_IN_GPIO)
	{
		//Linact control on LINACT HALL
		linact_Count();
	}
#endif

}


static void HandSwitchInt (void)
{	
//	if( ioport_get_pin_level(GPIO_DIN4)==0 ) //PIN_HANDSWITCH_IN
	if( HAL_GPIO_ReadPin(PIN_HANDSWITCH_IN_PORT, PIN_HANDSWITCH_IN_GPIO) == GPIO_PIN_RESET ) //PIN_HANDSWITCH_IN
	{
		trigger_status.handswitchStatus=1;
		DbgPrintf(DBG_DIGIO, "STAT pressed handswitch\r\n");
	}
	else
		{
			trigger_status.handswitchStatus=0;
			DbgPrintf(DBG_DIGIO, "STAT not pressed handswitch\r\n");
		}
}

//static void LinakInt (uint32_t a, uint32_t b)
//{
	//linact_Count();
//}

static void PotenzaInt (uint32_t a, uint32_t b)
{
//	if( ioport_get_pin_level(GPIO_DIN0)==1 ) //macchina in potenza PIN_POWERSENSE_IN
	if( HAL_GPIO_ReadPin(PIN_POWERSENSE_IN_PORT, PIN_POWERSENSE_IN_GPIO) == GPIO_PIN_SET ) //PIN_HANDSWITCH_IN
	{
		trigger_status.powerStatus=1;
		DbgPrintf(DBG_DIGIO, "STAT on power\r\n");
	}
	else //macchina NON in potenza
		{
			trigger_status.powerStatus=0;
			DbgPrintf(DBG_DIGIO, "STAT not on power\r\n");
		}
}

bool digio_GetPowerStatus(void)
{
#warning "!!!!!!!!!!!!!!!!!digio_GetPowerStatus = true!!!!!!!!!!!!!!!!!!!!!!!"
	/*
	bool result = false;
	
//	if(ioport_get_pin_level(GPIO_DIN0) == 1)
	if( HAL_GPIO_ReadPin(PIN_POWERSENSE_IN_PORT, PIN_POWERSENSE_IN_GPIO) == GPIO_PIN_SET ) //PIN_HANDSWITCH_IN
		result = true;
	return result;
	*/
	return true;
}

/**
 *
 * @brief	Function to manage trigger output state under timer control
 * @param	TIM_HandleTypeDef *htim, timer handler to serve
 *
 * @return	None
 *
 */
void TC7_Trigger_Handler(TIM_HandleTypeDef *htim)
{

	if(trigger_status.triggerEnable ==TRIGGER_RX_DISABLE) 
	{
		DbgPrintf(DBG_DIGIO, "ERR out from TC1 Hand to disabled rays\r\n");
		trigMachineStopCondition();
		return; //se i raggi non sono abilitati, non eseguo nulla
	}
//**************************************************************************************************

	if(trigger_status.triggerCounter > trigger_status.maxSessionTrigger) //controllo raggiungimento max scatti
	{
		trigMachineStopCondition();
		trigger_status.tooManyTriggerCounter++;
		DbgPrintf(DBG_DIGIO, "trigger_status.tooManyTriggerCounter %d\r\n",trigger_status.tooManyTriggerCounter);
		DbgPrintf(DBG_DIGIO, "ERR threshold pulse counter overcome\r\n");
		return;
	}
		
	//controllo handswitch/footswitch e macchina in potenza
	if(trigger_status.handswitchStatus==0 || trigger_status.powerStatus==0) //se handswitch NON premuto o macchina NON pi� in potenza
	{
		trigMachineStopCondition();
		if(trigger_status.powerStatus==0) //operazioni per macchina non pi� in potenza
		{ //stop e reset motori
			DbgPrintf(DBG_DIGIO, "ERR machine out from power during RX\r\n");
			
			motorStopNoPower();
				
			//ATTENZIONE!!!!!!
			//Genera Watch Dog trigger perch� si alluppa sulla richiesta ai motori e non se ne esce perch� siamo in un interrupt
			//e non si aggiorna il sys_tick di sistema
			/*
			if( motorStopDuringRX() == 0)
				DbgPrintf(DBG_DIGIO, "STATUS/ERR motori fermati con errore\r\n");
			else
				DbgPrintf(DBG_DIGIO, "STATUS/ERR motori fermati con successo\r\n");
			*/
		}
			
		if(trigger_status.handswitchStatus== 0) //operazioni per handswitch/footswitch non premuto
		{
			DbgPrintf(DBG_DIGIO, "STATUS not pressed handswitch\r\n");
		}
			
		return;
	}


	switch (trigger_status.triggerMode)
	{
		case CT_SPEED_MODE: //modalit� TAC

	#warning "check led!!!"
		HAL_GPIO_TogglePin(LED_DIGIO_PORT, LED_DIGIO_GPIO);
		trig_count++;
		switch (trig_machine)
		{
			case 0:
				if(trig_count >= trigger_status.t_panel) //alla scadenza: flat panel trigger off (acceso precedentemente)
				{
	//				ioport_set_pin_level(PANEL_PULSE_OUT, OUTS_INACTIVE_LEVEL);
					HAL_GPIO_WritePin(PANEL_PULSE_OUT_PORT, PANEL_PULSE_OUT_GPIO, OUTS_INACTIVE_LEVEL);
										//                                                                       ___
					// se � abilitato il segnale panel ready vado ad attendere l'impulso ___|   |____
					if(trigger_status.panelReadySignalEnable)
					{
						trig_machine = 3;
						trig_count = 0;
					}
					else  // altrimenti attendo il tempo di delay
					{
						trig_machine++;
						trig_count = 0;
					}
				}
			break;

			case 1:
			if(trig_count >= trigger_status.t_delay) //alla scadenza: rx trigger ON
			{
	//			ioport_set_pin_level(RX_PULSE_OUT, OUTS_ACTIVE_LEVEL);
				HAL_GPIO_WritePin(RX_PULSE_OUT_PORT, RX_PULSE_OUT_GPIO, OUTS_ACTIVE_LEVEL);
				trigger_status.triggerCounter++;
				trig_count = 0;
				trig_machine++;
			}
			break;

			case 2:
			if(trig_count >= trigger_status.t_rxPulse) //alla scadenza: rx trigger OFF
			{
	//			ioport_set_pin_level(RX_PULSE_OUT, OUTS_INACTIVE_LEVEL);
				HAL_GPIO_WritePin(RX_PULSE_OUT_PORT, RX_PULSE_OUT_GPIO, OUTS_INACTIVE_LEVEL);
	//			tc_stop(TC1, 0);
				HAL_TIM_Base_Stop_IT(&htim_trigger);
				DbgPrintf(DBG_DIGIO, "TC1DISABLE\r\n");
				trig_count = 0;
				trig_machine = 0;
			}
			break;

			case 3:  // attesa del segnale panel ready alto
	//			if( ioport_get_pin_level(PIN_FLATPANELREADY_IN) == 1 )
				if(HAL_GPIO_ReadPin(PIN_FLATPANELREADY_IN_PORT, PIN_FLATPANELREADY_IN_GPIO)==GPIO_PIN_SET)
				{
	//				ioport_set_pin_level(RX_PULSE_OUT, OUTS_ACTIVE_LEVEL);
					HAL_GPIO_WritePin(RX_PULSE_OUT_PORT, RX_PULSE_OUT_GPIO, OUTS_ACTIVE_LEVEL);
					trigger_status.triggerCounter++;
					trig_count = 0;
					trig_machine = 2;
				}
			break;

		} //switch (trig_machine)
	
		break; //break del CT_SPEED_MODE
	
	case CT_MODE: //modalit� TAC
//		ioport_toggle_pin_level(LED2_GPIO); //lampeggio led
		HAL_GPIO_TogglePin(LED_DIGIO_PORT, LED_DIGIO_GPIO);
		trig_count++;
		switch (trig_machine)
		{
			case 0:
				if(trig_count >= trigger_status.t_panel) //alla scadenza: flat panel trigger off (acceso precedentemente)
				{
//					ioport_set_pin_level(PANEL_PULSE_OUT, OUTS_INACTIVE_LEVEL);
					HAL_GPIO_WritePin(PANEL_PULSE_OUT_PORT, PANEL_PULSE_OUT_GPIO, OUTS_INACTIVE_LEVEL);
					// se � abilitato il segnale panel ready vado ad attendere l'impulso ___|   |____ dal pannello
					if(trigger_status.panelReadySignalEnable)
					{
						trig_machine = 3;
						trig_count = 0;
					}
					else  // altrimenti attendo il tempo di delay
					{
						trig_machine++;
						trig_count = 0;
					}
				}
			break;
			
			case 1:
				if(trig_count >= trigger_status.t_delay) //alla scadenza: rx trigger ON
				{
					//ioport_set_pin_level(RX_PULSE_OUT, OUTS_ACTIVE_LEVEL);
					HAL_GPIO_WritePin(RX_PULSE_OUT_PORT, RX_PULSE_OUT_GPIO, OUTS_ACTIVE_LEVEL);
					trigger_status.triggerCounter++;
					trig_count = 0;
					trig_machine++;
				}
			break;
			
			case 2:
				if(trig_count >= trigger_status.t_rxPulse) //alla scadenza: rx trigger OFF
				{
					//ioport_set_pin_level(RX_PULSE_OUT, OUTS_INACTIVE_LEVEL);
					HAL_GPIO_WritePin(RX_PULSE_OUT_PORT, RX_PULSE_OUT_GPIO, OUTS_INACTIVE_LEVEL);
//					tc_stop(TC1, 0);
					HAL_TIM_Base_Stop_IT(htim);
					DbgPrintf(DBG_DIGIO, "CT_TC1DISABLE\r\n");
					trig_count = 0;
					trig_machine = 0;
				}
			break;
			
			case 3:  // attesa del segnale panel ready alto
				if( HAL_GPIO_ReadPin(PIN_FLATPANELREADY_IN_PORT, PIN_FLATPANELREADY_IN_GPIO)==GPIO_PIN_SET )
				{
//					ioport_set_pin_level(RX_PULSE_OUT, OUTS_ACTIVE_LEVEL);
					HAL_GPIO_WritePin(RX_PULSE_OUT_PORT, RX_PULSE_OUT_GPIO, OUTS_ACTIVE_LEVEL);
					trig_count = 0;
					trig_machine = 2;
				}
			break;

		} //switch (trig_machine)
		
		break; //break del CT_MODE
	
	case FL_MODE: //modalit� fluoroscopia
	
		//ioport_toggle_pin_level(LED2_GPIO); //lampeggio led
		HAL_GPIO_TogglePin(LED_DIGIO_PORT, LED_DIGIO_GPIO);
		trig_count++;
		total_trig_count++;
		switch (trig_machine)
		{
			case 0:  // panel trigger  attivo
//				ioport_set_pin_level(PANEL_PULSE_OUT, OUTS_INACTIVE_LEVEL);
				HAL_GPIO_WritePin(PANEL_PULSE_OUT_PORT, PANEL_PULSE_OUT_GPIO, OUTS_INACTIVE_LEVEL);
				DbgPrintf(DBG_DIGIO, "Pulse_Low %d\r\n",timer_TimerSet(0));
				trig_machine = 5;
				trig_count = 0;
				
			break;
			
			case 5:
				if(trig_count >= trigger_status.t_panel) //alla scadenza: flat panel trigger off
				{
//					ioport_set_pin_level(PANEL_PULSE_OUT, OUTS_ACTIVE_LEVEL);
					HAL_GPIO_WritePin(PANEL_PULSE_OUT_PORT, PANEL_PULSE_OUT_GPIO, OUTS_ACTIVE_LEVEL);
					DbgPrintf(DBG_DIGIO, "Pulse_High %d\r\n",timer_TimerSet(0));
					//                                                                       ___
					// se � abilitato il segnale panel ready vado ad attendere l'impulso ___|   |____ dal pannello
					if(trigger_status.panelReadySignalEnable)
					{
						trig_machine = 9;
						trig_count = 0;
					}
					else  // altrimenti attendo il tempo di delay
					{
						trig_machine++;
						trig_count = 0;
					}
				}
			break;
			
			case 6:
				if(trig_count >= trigger_status.t_delay) //alla scadenza: rx trigger ON
				{
//					ioport_set_pin_level(RX_PULSE_OUT, OUTS_ACTIVE_LEVEL);
					HAL_GPIO_WritePin(RX_PULSE_OUT_PORT, RX_PULSE_OUT_GPIO, OUTS_ACTIVE_LEVEL);
					DbgPrintf(DBG_DIGIO, "RX_High %d\r\n",timer_TimerSet(0));
					trigger_status.triggerCounter++;
					trig_count = 0;
					trig_machine++;
				}
			break;
			
			case 7:
				if(trig_count >= trigger_status.t_rxPulse) //alla scadenza: rx trigger OFF
				{
//					ioport_set_pin_level(RX_PULSE_OUT, OUTS_INACTIVE_LEVEL);
					HAL_GPIO_WritePin(RX_PULSE_OUT_PORT, RX_PULSE_OUT_GPIO, OUTS_INACTIVE_LEVEL);
					DbgPrintf(DBG_DIGIO, "RX_Low %d\r\n",timer_TimerSet(0));
					trig_count = 0;
					trig_machine++;
				}
			break;
			
			case 8: //questo � il tempo di attesa tra un impulso di rx e un altro
				if(total_trig_count >= trigger_status.t_interframe) //alla scadenza: riparto per un altro scatto
				{
//					ioport_set_pin_level(PANEL_PULSE_OUT, OUTS_INACTIVE_LEVEL);
					HAL_GPIO_WritePin(PANEL_PULSE_OUT_PORT, PANEL_PULSE_OUT_GPIO, OUTS_INACTIVE_LEVEL);
					DbgPrintf(DBG_DIGIO, "Panel_Low %d\r\n",timer_TimerSet(0));
					trig_machine= 5;
						
					trig_count = 0;
					total_trig_count = 0;
				}
			break;
			
			case 9:  // attesa del segnale panel ready alto
//				if( ioport_get_pin_level(PIN_FLATPANELREADY_IN) == 1 )
				if( HAL_GPIO_ReadPin(PIN_FLATPANELREADY_IN_PORT, PIN_FLATPANELREADY_IN_GPIO)==GPIO_PIN_SET )
				{
//					ioport_set_pin_level(RX_PULSE_OUT, OUTS_ACTIVE_LEVEL);
					HAL_GPIO_WritePin(RX_PULSE_OUT_PORT, RX_PULSE_OUT_GPIO, OUTS_ACTIVE_LEVEL);
					DbgPrintf(DBG_DIGIO, "Flat Panel Ready RX_Low %d\r\n",timer_TimerSet(0));
					trigger_status.triggerCounter++;

					trig_count = 0;
					trig_machine = 7;
				}
				// sicurezza: se il segnale PIN_FLATPANELREADY_IN non arriva comunque concludo con il tempo di interframe
				else if(total_trig_count >= trigger_status.t_interframe) //e fermo tutto
				{
					trigMachineStopCondition();
					trigger_status.fastErrorCounter++;
					DbgPrintf(DBG_DIGIO, "fastErrorCounter %d\r\n",trigger_status.fastErrorCounter);
					}
			break;
		}//switch (trig_machine)
		
		break; //break del FL_MODE
	
	case DR_MODE: //modalit� "singolo" scatto
		//ioport_toggle_pin_level(LED2_GPIO); //lampeggio led
		trig_count++;
		switch (trig_machine)
		{
			case 0:
//			ioport_set_pin_level(PANEL_PULSE_OUT, OUTS_INACTIVE_LEVEL);
			HAL_GPIO_WritePin(PANEL_PULSE_OUT_PORT, PANEL_PULSE_OUT_GPIO, OUTS_INACTIVE_LEVEL);
			trig_machine++;
			trig_count = 0;
			break;
			
			case 1:
			if(trig_count >= dr_trigger_times.pulse_panel_lenght)
			{
//				ioport_set_pin_level(PANEL_PULSE_OUT, OUTS_ACTIVE_LEVEL);
				HAL_GPIO_WritePin(PANEL_PULSE_OUT_PORT, PANEL_PULSE_OUT_GPIO, OUTS_ACTIVE_LEVEL);
				trig_machine++;
				trig_count = 0;
			}
			break;

			case 2:
			if(trig_count >= (dr_trigger_times.t_frame - dr_trigger_times.pulse_panel_lenght) )
			{
//				ioport_set_pin_level(PANEL_PULSE_OUT, OUTS_INACTIVE_LEVEL);
				HAL_GPIO_WritePin(PANEL_PULSE_OUT_PORT, PANEL_PULSE_OUT_GPIO, OUTS_INACTIVE_LEVEL);
				trig_machine++;
				trig_count = 0;
			}
			break;

			case 3:
			if(trig_count >= dr_trigger_times.pulse_panel_lenght)
			{
//				ioport_set_pin_level(PANEL_PULSE_OUT, OUTS_ACTIVE_LEVEL);
				HAL_GPIO_WritePin(PANEL_PULSE_OUT_PORT, PANEL_PULSE_OUT_GPIO, OUTS_ACTIVE_LEVEL);
				trig_machine++;
				trig_count = 0;
			}
			break;


			case 4:  
			if(trig_count >= (dr_trigger_times.t_frame - dr_trigger_times.rx_pulse_lenght - dr_trigger_times.t_rx_start) )   
			{
//				ioport_set_pin_level(RX_PULSE_OUT, OUTS_ACTIVE_LEVEL);
				HAL_GPIO_WritePin(RX_PULSE_OUT_PORT, RX_PULSE_OUT_GPIO, OUTS_ACTIVE_LEVEL);
				trig_machine++;
				trig_count = 0;
			}
			break;

			case 5:  
			if(trig_count >= dr_trigger_times.rx_pulse_lenght )   
			{
				/*
				ioport_set_pin_level(RX_PULSE_OUT, OUTS_INACTIVE_LEVEL);
				ioport_set_pin_level(PANEL_PULSE_OUT, OUTS_INACTIVE_LEVEL);
				*/
				HAL_GPIO_WritePin(RX_PULSE_OUT_PORT, RX_PULSE_OUT_GPIO, OUTS_INACTIVE_LEVEL);
				HAL_GPIO_WritePin(PANEL_PULSE_OUT_PORT, PANEL_PULSE_OUT_GPIO, OUTS_INACTIVE_LEVEL);
				trig_machine++;
				trig_count = 0;
			}
			break;

			case 6:  
			if(trig_count >= dr_trigger_times.pulse_panel_lenght )   
			{
				/*
				ioport_set_pin_level(PANEL_PULSE_OUT, OUTS_ACTIVE_LEVEL);
				ioport_set_pin_level(RX_PULSE_OUT, OUTS_INACTIVE_LEVEL);
				ioport_set_pin_level(LED2_GPIO, LEDS_INACTIVE_LEVEL);  //led off
				*/
				HAL_GPIO_WritePin(PANEL_PULSE_OUT_PORT, PANEL_PULSE_OUT_GPIO, OUTS_ACTIVE_LEVEL);
				HAL_GPIO_WritePin(RX_PULSE_OUT_PORT, RX_PULSE_OUT_GPIO, OUTS_INACTIVE_LEVEL);
				HAL_GPIO_WritePin(LED_DIGIO_PORT, LED_DIGIO_GPIO, LEDS_INACTIVE_LEVEL);
				trigMachineStopCondition();
			}
			break;

		} //switch (trig_machine)
	
		break; //break del DR_MODE
	
	case DARKFRAMES_MODE:
		trig_count++;
		if (trigger_status.triggerEnable == TRIGGER_RX_DUMMY)
		{
			switch (trig_machine)
			{
				case 0:
				if(trig_count >= trigger_status.t_panel) //alla scadenza: flat panel trigger ON
				{
//					ioport_set_pin_level(PANEL_PULSE_OUT, OUTS_INACTIVE_LEVEL);
					HAL_GPIO_WritePin(PANEL_PULSE_OUT_PORT, PANEL_PULSE_OUT_GPIO, OUTS_INACTIVE_LEVEL);
					trig_machine++;
					trig_count= 0;
				}
				break;
				
				case 1:
				if(trig_count >= trigger_status.t_panel) //alla scadenza: flat panel trigger off (acceso precedentemente)
				{
//					ioport_set_pin_level(PANEL_PULSE_OUT, OUTS_ACTIVE_LEVEL);
					HAL_GPIO_WritePin(PANEL_PULSE_OUT_PORT, PANEL_PULSE_OUT_GPIO, OUTS_ACTIVE_LEVEL);
					trigMachineStopCondition();
					trigger_status.triggerEnable= TRIGGER_RX_DISABLE;
					trig_machine= 0;
					trig_count= 0;
				}
				break;
			}
		} //if
		break;	//break del DARKFRAMES_MODE
	
	default:
		break;
	} //switch (trigger_status.triggerMode) FL_MODE DR_MODE FL_MODE

} //funzione void TC7_Photocell_Handler(void)

// questa funzione triggera il pannello forzatamente
void panelTriggerNow (void)
{
	/*
	ioport_set_pin_level(PANEL_PULSE_OUT, OUTS_INACTIVE_LEVEL);
	delay_ms(10);
	ioport_set_pin_level(PANEL_PULSE_OUT, OUTS_ACTIVE_LEVEL);
	*/
	HAL_GPIO_WritePin(PANEL_PULSE_OUT_PORT, PANEL_PULSE_OUT_GPIO, OUTS_INACTIVE_LEVEL);
	HAL_Delay(10);
	HAL_GPIO_WritePin(PANEL_PULSE_OUT_PORT, PANEL_PULSE_OUT_GPIO, OUTS_ACTIVE_LEVEL);
}

//vengono eseguite operazioni di stop del tc e disattivazione uscite
void trigMachineStopCondition (void)
{
	digio_FCellDisable();
	stopMachineState();
}

//Stop Machine state
void stopMachineState(void){
//	tc_stop(TC1, 0);
	HAL_TIM_Base_Stop_IT(&htim_trigger);

	DbgPrintf(DBG_DIGIO, "stopMachineState %d\r\n",timer_TimerSet(0));

//	ioport_set_pin_level(RX_PULSE_OUT, OUTS_INACTIVE_LEVEL);
	HAL_GPIO_WritePin(RX_PULSE_OUT_PORT, RX_PULSE_OUT_GPIO, OUTS_INACTIVE_LEVEL);

//	ioport_set_pin_level(PANEL_PULSE_OUT, OUTS_INACTIVE_LEVEL);
	HAL_GPIO_WritePin(PANEL_PULSE_OUT_PORT, PANEL_PULSE_OUT_GPIO, OUTS_INACTIVE_LEVEL);

//	ioport_set_pin_level(LED2_GPIO, LEDS_INACTIVE_LEVEL);  //led off
	HAL_GPIO_WritePin(LED_DIGIO_PORT, LED_DIGIO_GPIO, LEDS_INACTIVE_LEVEL);
	trig_machine = 0;
	trig_count = 0;

	trigMachineFlag = false;
}

/**
  * @brief 	Start X-Ray and acquisition
  * 		Execute if machine is not in CT Mode
  *
  * @return None
  */
uint8_t TrigManualStart (void)
{
	uint8_t err = 0;

	if(trig_machine > 0)
	{
		err++; //in alternativa al controllo sopra, evito di fare un restart se la macchina � gi� partita
		DbgPrintf(DBG_DIGIO, "ERR machin already on\r\n");
	}
	
	if(trigger_status.triggerCounter > trigger_status.maxSessionTrigger) 
	{
		err++; //se non � stato resettato il limite del contascatti, non parto
		DbgPrintf(DBG_DIGIO, "ERR contascatti not reset\r\n");
	}
	
	if(trigger_status.triggerMode== CT_MODE || trigger_status.triggerMode== CT_SPEED_MODE) 
	{
		err++; //se sono in modalit� tac non � accettato lo start manuale
		DbgPrintf(DBG_DIGIO, "ERR error modality (CT)\r\n");
	}
	
	
	
	if(err== 0) //nessun errore, posso eseguire lo start
	{
		DbgPrintf(DBG_DIGIO, "TrigManualStart %d\r\n",timer_TimerSet(0));
		if(trigger_status.triggerMode == FL_MODE){
//				ioport_set_pin_level(PANEL_PULSE_OUT, OUTS_ACTIVE_LEVEL);
			HAL_GPIO_WritePin(PANEL_PULSE_OUT_PORT, PANEL_PULSE_OUT_GPIO, RELS_ACTIVE_LEVEL);
		}
		trig_machine= 0;
		trig_count= 0;
		total_trig_count = 0;
		trigMachineFlag = true;
//		tc_start(TC1, 0);
		HAL_TIM_Base_Start_IT(&htim_trigger);
		return 1; //esito positivo
	}
	else
		return 0; //errori presenti, quindi restituisco 0
}

//al ricevimento di un comando di stop da parte del computer, qui vengono eseguite le operazioni necessarie
void trigStopFromCommand (void)
{
	trigMachineStopCondition();
}

static uint32_t aux1_switch_state = 0;
static uint32_t aux1_key_state = 0;
static uint32_t aux1_debounce_timer = 0;
void digio_AuxKeyManage(void)
{
	// AUX1
	if(timer_TimerTest(aux1_debounce_timer))
	{
//		if(ioport_get_pin_level(KEY_AUX1_GPIO) == false)  // se premuto ...
		if(HAL_GPIO_ReadPin(KEY_AUX1_PORT, KEY_AUX1_GPIO)==GPIO_PIN_RESET)
		{
			if(aux1_key_state == 0) // e se era in stato di rilascio
			{
				aux1_key_state = 1;  // metto in stato premuto
				switch(aux1_switch_state)
				{
					case 0:
//						ioport_set_pin_level(REL0_GPIO, RELS_ACTIVE_LEVEL);
						HAL_GPIO_WritePin(REL1_PORT, REL1_GPIO, RELS_ACTIVE_LEVEL);
						aux1_switch_state = 1;
						DbgPrintf(DBG_DIGIO, "AUX1 ON\r\n");
					break;
			
					default:
					case 1:
//						ioport_set_pin_level(REL0_GPIO, RELS_INACTIVE_LEVEL);
						HAL_GPIO_WritePin(REL1_PORT, REL1_GPIO, RELS_INACTIVE_LEVEL);
						aux1_switch_state = 0;
						DbgPrintf(DBG_DIGIO, "AUX1 OFF\r\n");
					break;
				}
			}
		}
		else  // tasto rilasciato
			aux1_key_state = 0;
		
		// timer antirimbalzo 200 ms
		aux1_debounce_timer = timer_TimerSet(200);
	}
	
}
