/**
\file command.c
\brief Libreria per la gestione di comandi, con associazione a funzione di esecuzione
\author Lorenzo Giardina <lorenzo@lorenzogiardina.com>
\version 1.0
\date Last revision: 1/1/2016
\note  This library is created for Qprel srl
This code is distributed under the GNU Public License which can be found at http://www.gnu.org/licenses/gpl.txt
*/

#include "stxetx.h"
#include "buffer.h"
#include <string.h>
#include "main.h"
#include "command.h"
#include "ipc_command.h"
#include "binaryprotocol.h"

//#define DBG_COMMAND 0  //!< Enable debug printf for this module

static command_t command[N_COMMAND];
static fifo_ipc_command_t rx_ipc_cmd[N_TRUNK];

static unsigned long packetNum=0;


/** @brief 	Function to elaborate command from CM7 core to populate the rx_ipc_cmd fifo
 *  @param	cmd_msg_t *cmd to analyze
 *  @return RES_FAIL if any message found
 *  		!=RES_FAIL Command Number present in fifo rx
 */
int ipc_process_packet (cmd_msg_t *cmd_ipc)
{
	if (rx_ipc_cmd[COM_IPC].n_cmd_present >= N_MAX_BUFFER_COMANDI_RX)
	{
		DbgPrintf (DBG_COMMAND, "Full Cmd Buffer!\r\n");

		return RES_FAIL;
	}

	if(cmd_ipc->channel==IPC_CMD_CHANNEL &&
			cmd_ipc->cmd_type==IPC_CMD_REQUEST &&
				cmd_ipc->cmd_id<LAST_BIN_CMD)
	{
		//Copy packet in fifo
		rx_ipc_cmd[COM_IPC].comando[rx_ipc_cmd[COM_IPC].n_cmd_present]=*cmd_ipc;

		rx_ipc_cmd[COM_IPC].n_cmd_present++;
		packetNum++;
		return rx_ipc_cmd[COM_IPC].n_cmd_present;
	}
	return RES_FAIL;
}

//! Funzione esecuzione funzione associata al comando con controllo presenza nel buffer comandi di un comando valido
void exe_ipc_cmd (void)
{
	unsigned char i=0;

	while (rx_ipc_cmd[COM_IPC].n_cmd_present)
	{
		DbgPrintf (DBG_COMMAND, "\r\nWaiting to process command...\r\n");
		//Safety reason
		if(strlen((char *)rx_ipc_cmd[COM_IPC].comando[i].cmd_data)>=N_MAX_DATA_LENGHT)
		{
			rx_ipc_cmd[COM_IPC].comando[i].cmd_data[N_MAX_DATA_LENGHT-1]=0;
		}
       	exe_cmd_func(rx_ipc_cmd[COM_IPC].comando[i].cmd_id, rx_ipc_cmd[COM_IPC].comando[i].id, rx_ipc_cmd[COM_IPC].comando[i].cmd_data);
        rx_ipc_cmd[COM_IPC].n_cmd_present--;
        i++;
#ifdef CORE_CM7
        HAL_GPIO_TogglePin(LD1_GPIO_Port, LD1_Pin);
#endif
        //		ioport_toggle_pin_level(LED3_GPIO);
	}
}

//! Inizializzazione sistema di pacchettizzazione STXETX e associazione funzioni base
void ipc_command_system_init()
{
	rx_ipc_cmd[COM_IPC].n_cmd_present=0;		//Reset del numero di comandi presenti nella fifo
}
