/*
 * binaryprotocol.c
 *
 * Created: 27/02/2015 11:01:14
 *  Author: Alessandro
 */ 

#include <string.h>
#include "binaryprotocol.h"
#include "IPC_m4.h"
#include "motors.h"


static char tmpstr[N_MAX_DATA_LENGHT];


/**
 * \brief		Retrieve parameter from a list of them, 
 *				flush out the retrieved one
 * 
 * \param src		List of params separated by a separator symbol
 * \param separator Parameters separator
 * \param param		Retrieved parameter
 * 
 * \return int, 1 = success, 0 = fail
 */
static int list_GetNextParam(unsigned char * src, char separator, char * param)
{
	char ctmp;
	uint32_t i = 0;
	int len_src=strlen((const char *)src);
	
	while(i < len_src)
	{
		ctmp = src[i];
		if((ctmp == separator)||(ctmp == 0))
		break;
		param[i] = ctmp;
		i++;
	}
	
	param[i] = 0;
	strcpy((char *)src, (char *)(src+i+1)); // cut out the extracted param
	return i;
}

/**
 * \brief		Retrieve parameter from a list of them, 
 *				flush out the retrieved one
 * 
 * \param src	List of comma separated params
 * \param param	Retrieved parameter
 * 
 * \return int, 1 = success, 0 = fail
 */
static int list_command_GetNextParam(unsigned char * src, char * param)
{
	return list_GetNextParam(src, ',' ,param);
}

/**
 * \brief		Retrieve parameter from a list of them, 
 *				flush out the retrieved one
 * 
 * \param src	List of point separated ip params (es. 169.254.1.77)
 * \param param	Retrieved parameter
 * 
 * \return int, 1 = success, 0 = fail
 */
static int list_GetNextIpParam(unsigned char * src, char * param)
{
	return list_GetNextParam(src, '.' ,param);
}


/**
 * \brief				Callback to communicate FW version
 * 
 * \param trunk			Communication bus number
 * \param dataptrvoid	Unused Parameter
 * 
 * \return void
 */
static void verfwcmd (unsigned char trunk, unsigned char* dataptrvoid)
{
	
	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = FWVERSION_CMD;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	//Populate REPLY
	strcpy((char *)command.cmd_msg.cmd_data, "1.55");
	DbgPrintf(DBG_BINARY_PROTO, "CMD FW version: %s\r\n",tmpstr);
	IPC_SendReply(&command);
}

/**
  * @brief 	Command to operate motor home on a axis
  * @param  unsigned char trunk: Communication bus number
  * @param  unsigned char* dataptrvoid: pointer generic_cmd_t with
  * 		follows (1) parameters:
  * 			- (uint32_t) axis
  * @return None
  *
  **/
static void motor_home (unsigned char trunk, unsigned char* dataptrvoid){
	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = MOTOR_HOME_CMD;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	if(rx_cmd->num_params==1 && motor_Home(axis))
	{
		debug_printf(DBG_BINARY_PROTO, "motor_Home axis: %d, OK.\r\n", axis);
		strcpy((char *)command.cmd_msg.cmd_data, "ACK");
	}
	else
	{
		debug_printf(DBG_BINARY_PROTO, "motor_Home axis: %d, FAILED.\r\n", axis);
		strcpy((char *)command.cmd_msg.cmd_data, "NAK");
	}
	DbgPrintf(DBG_BINARY_PROTO, "CMD motor_home\r\n");
	IPC_SendReply(&command);
}

/**
* @brief				Callback function to SetHomeSpeed
*
* @param trunk			Communication bus number
* @param  unsigned char* dataptrvoid
* 		String of separated params:
* 			- axis
* 			- speed param
* @return None
*/
static void M_SetHomeSpeed(unsigned char trunk, unsigned char* dataptrvoid)
{
	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = SET_HOME_SPEED_CMD;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	uint32_t speed_param = rx_cmd->parameters[1];
	if(rx_cmd->num_params==2 && motor_SetHomeSpeed(axis, speed_param))
	{
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetHomeSpeed axis: %d, OK.\r\n", axis);
		strcpy((char *)command.cmd_msg.cmd_data, "ACK");
	}
	else
	{
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetHomeSpeed axis: %d, FAILED.\r\n", axis);
		strcpy((char *)command.cmd_msg.cmd_data, "NAK");
	}

	DbgPrintf(DBG_BINARY_PROTO, "CMD M_SetHomeSpeed\r\n");
	IPC_SendReply(&command);
}

/**
* @brief				Callback function to set position
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis and position params in a string
* 		String of separated params:
* 			- axis
* 			- speed param
*
* @return None
*/
static void M_SetPosition(unsigned char trunk, unsigned char* dataptrvoid)
{
	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = SET_POSITION_CMD;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	int32_t pos_param = (int32_t)rx_cmd->parameters[1];
	if(rx_cmd->num_params==2 && motor_SetPosition(axis, pos_param))
	{
		strcpy((char *)command.cmd_msg.cmd_data, "ACK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetPosition axis: %d, pos: %d, OK.\r\n", axis, pos_param);
	}
	else
	{
		strcpy((char *)command.cmd_msg.cmd_data, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetPosition axis: %d, pos: %d, FAILED.\r\n", axis,pos_param);
	}

	DbgPrintf(DBG_BINARY_PROTO, "CMD M_SetPosition\r\n");
	IPC_SendReply(&command);
}

/**
* @brief				Callback function to set speed
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis and speed params in a string
* 		String of separated params:
* 			- axis
* 			- speed param
*
* @return None
*/
static void M_SetSpeed(unsigned char trunk, unsigned char* dataptrvoid)
{

	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = SET_SPEED_CMD;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	int32_t speed_param = (int32_t)rx_cmd->parameters[1];
	if(rx_cmd->num_params==2 && motor_SetSpeed(axis, speed_param))
	{
		strcpy((char *)command.cmd_msg.cmd_data, "ACK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetSpeed axis: %d OK, speed: %u rx\r\n", axis, speed_param);
	}
	else
	{
		strcpy((char *)command.cmd_msg.cmd_data, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetSpeed axis: %d FAILED.\r\n", axis);
	}

	DbgPrintf(DBG_BINARY_PROTO, "CMD M_SetSpeed\r\n");
	IPC_SendReply(&command);
}

/**
* @brief				Callback function to set acceleration
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis and acceleration params in a string4
* 		String of separated params:
* 			- axis
* 			- acceleration param
*
* @return None
*/
static void M_SetAcc(unsigned char trunk, unsigned char* dataptrvoid)
{

	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = SET_ACC_CMD;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	int32_t acc_param = (int32_t)rx_cmd->parameters[1];
	if(rx_cmd->num_params==2 && motor_SetAcc(axis, acc_param))
	{
		strcpy((char *)command.cmd_msg.cmd_data, "ACK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetAcc axis: %d, OK\r\n", axis);
	}
	else
	{
		strcpy((char *)command.cmd_msg.cmd_data, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetAcc axis: %d, FAILED\r\n", axis);
	}

	DbgPrintf(DBG_BINARY_PROTO, "CMD M_SetAcc\r\n");
	IPC_SendReply(&command);
}

/**
* @brief				Set the speed in the motor jog mode
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis and jog speed params in a string4
* 		String of separated params:
* 			- axis
* 			- jog speed param
*
* @return None
*/
static void M_SetJogSpeed(unsigned char trunk, unsigned char* dataptrvoid)
{
	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = SET_JOG_SPEED_CMD;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	int32_t jog_param = (int32_t)rx_cmd->parameters[1];
	if(rx_cmd->num_params==2 && motor_SetJogSpeed(axis, jog_param))
	{
		strcpy((char *)command.cmd_msg.cmd_data, "ACK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetJogSpeed axis: %d, OK\r\n", axis);
	}
	else
	{
		strcpy((char *)command.cmd_msg.cmd_data, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetJogSpeed axis: %d, FAILED\r\n", axis);
	}

	DbgPrintf(DBG_BINARY_PROTO, "CMD M_SetJogSpeed\r\n");
	IPC_SendReply(&command);
}

/**
* @brief				Callback to perform forward steps in motor jog mode
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis in a string
* 		String of separated params:
* 			- axis
*
* @return None
*/
static void M_GoJogFwd(unsigned char trunk, unsigned char* dataptrvoid)
{

	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = GO_JOG_FWD_CMD;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	if(rx_cmd->num_params==1 && motor_GoJogFwd(axis))
	{
		strcpy((char *)command.cmd_msg.cmd_data, "ACK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_GoJogFwd axis: %d, OK\r\n", axis);
	}
	else
	{
		strcpy((char *)command.cmd_msg.cmd_data, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_GoJogFwd axis: %d, FAILED\r\n", axis);
	}

	DbgPrintf(DBG_BINARY_PROTO, "CMD M_GoJogFwd\r\n");
	IPC_SendReply(&command);
}

/**
* @brief				Callback to perform reverse steps in motor jog mode
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis in a string
* 		String of separated params:
* 			- axis
*
* @return None
*/
static void M_GoJogRev(unsigned char trunk, unsigned char* dataptrvoid)
{

	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = GO_JOG_REV_CMD;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	if(rx_cmd->num_params==1 && motor_GoJogRev(axis))
	{
		strcpy((char *)command.cmd_msg.cmd_data, "ACK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_GoJogRev axis: %d, OK\r\n", axis);
	}
	else
	{
		strcpy((char *)command.cmd_msg.cmd_data, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_GoJogRev axis: %d, FAILED\r\n", axis);
	}

	DbgPrintf(DBG_BINARY_PROTO, "CMD M_GoJogRev\r\n");
	IPC_SendReply(&command);
}

/**
* @brief				Callback to communicate the motion status
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis params for the relative motion status
* 		String of comma separated params:
* 			- axis
*
* @return None
*/
static void M_GetMotionStatus(unsigned char trunk, unsigned char* dataptrvoid)
{

	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = GET_MOTION_STATUS_CMD;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	if(rx_cmd->num_params==1)
	{
		uint32_t u32tmp = motor_GetMotionStatus(axis);
		//Avoid to send different states from COMPLETE and MOVING (not managed by SW)
		if(u32tmp > 1)
			u32tmp = 1;
		sprintf((char *)command.cmd_msg.cmd_data, "%d", (unsigned int)u32tmp);
		DbgPrintf(DBG_BINARY_PROTO, "motor_GetMotionStatus axis: %d, OK\r\n", axis);
	}
	else
	{
		strcpy((char *)command.cmd_msg.cmd_data, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_GetMotionStatus axis: %d, FAILED\r\n", axis);
	}

	DbgPrintf(DBG_BINARY_PROTO, "CMD M_GetMotionStatus\r\n");
	IPC_SendReply(&command);
}

/**
* @brief				Callback to communicate the motor status
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis params for the motor status
* 		String of separated params:
* 			- axis
*
* @return None
*/
static void M_GetMotorStatus(unsigned char trunk, unsigned char* dataptrvoid)
{

	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = GET_MOTOR_STATUS_CMD;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	if(rx_cmd->num_params==1)
	{
		uint32_t u32tmp = motor_GetMotorStatus(axis);
		sprintf((char *)command.cmd_msg.cmd_data, "%d", (unsigned int)u32tmp);
		DbgPrintf(DBG_BINARY_PROTO, "motor_GetMotorStatus axis: %d, OK\r\n", axis);
	}
	else
	{
		strcpy((char *)command.cmd_msg.cmd_data, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "M_GetMotorStatus axis: %d, FAILED\r\n", axis);
	}

	DbgPrintf(DBG_BINARY_PROTO, "CMD M_GetMotorStatus\r\n");
	IPC_SendReply(&command);
}

/**
* @brief				Callback to send quick stop the motor command
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis parameter
* 		String of comma separated params:
* 			- axis
*
* @return None
*/
static void M_QuickStop(unsigned char trunk, unsigned char* dataptrvoid)
{
	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = QUICK_STOP_CMD;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	if(rx_cmd->num_params==1  && motor_QuickStop(axis))
	{
		strcpy((char *)command.cmd_msg.cmd_data, "ACK");
		DbgPrintf(DBG_BINARY_PROTO, "M_QuickStop axis: %d, OK\r\n", axis);
	}
	else
	{
		strcpy((char *)command.cmd_msg.cmd_data, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "M_QuickStop axis: %d, FAILED\r\n", axis);
	}

	DbgPrintf(DBG_BINARY_PROTO, "CMD M_QuickStop\r\n");
	IPC_SendReply(&command);

}

/**
* @brief				Callback to send quick stop the motor command
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis parameter
* 		String of separated params:
* 			- axis
*
* @return None
*/
static void M_StopHere(unsigned char trunk, unsigned char* dataptrvoid)
{
	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = M_STOP_CMD;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	if(rx_cmd->num_params==1  && motor_StopHere(axis))
	{
		strcpy((char *)command.cmd_msg.cmd_data, "ACK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_StopHere axis: %d, OK\r\n", axis);
	}
	else
	{
		strcpy((char *)command.cmd_msg.cmd_data, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_StopHere axis: %d, FAILED\r\n", axis);
	}

	DbgPrintf(DBG_BINARY_PROTO, "CMD M_StopHere\r\n");
	IPC_SendReply(&command);

}

/**
* @brief				Callback to retrieve the actual position of the motor
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis parameter
* 		String of separated params:
* 			- axis
*
* @return None
*/
static void M_GetMotorActPos(unsigned char trunk, unsigned char* dataptrvoid)
{
	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = GET_ACTUAL_POS_CMD;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	int32_t position = 0;
	if(rx_cmd->num_params==1  && motor_GetMotorActPos(axis, &position))
	{
		sprintf((char *)command.cmd_msg.cmd_data, "%d", (unsigned int)position);
		DbgPrintf(DBG_BINARY_PROTO, "motor_GetMotorActPos axis: %d, pos: %d.\r\n", axis, position);
	}
	else
	{
		strcpy((char *)command.cmd_msg.cmd_data, "-1");
		DbgPrintf(DBG_BINARY_PROTO, "motor_GetMotorActPos: FAIL\r\n");
	}

	DbgPrintf(DBG_BINARY_PROTO, "CMD M_GetMotorActPos\r\n");
	IPC_SendReply(&command);
}

/**
* @brief				Callback for motor drive initialization
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis parameter
* 		String of separated params:
* 			- axis
* 			- node
*
* @return None
*/
static void M_Init(unsigned char trunk, unsigned char* dataptrvoid)
{

	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = MOTOR_INIT_CMD;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	uint32_t node = rx_cmd->parameters[1];
	if(rx_cmd->num_params==2  && motor_Init(axis, node))
	{
		sprintf((char *)command.cmd_msg.cmd_data, "ACK");
		DbgPrintf(DBG_BINARY_PROTO, "M_Init axis: %d, node: %d, OK.\r\n", axis, node);
	}
	else
	{
		strcpy((char *)command.cmd_msg.cmd_data, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "M_Init axis: %d, node: %d, FAILED.\r\n", axis, node);
	}

	DbgPrintf(DBG_BINARY_PROTO, "CMD M_Init\r\n");
	IPC_SendReply(&command);

}

/**
* @brief				Callback function to set min position limit
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis and min position params in a string
* 		String of separated params:
* 			- axis
* 			- min position
*
* @return None
*/
static void M_SetMinPos(unsigned char trunk, unsigned char* dataptrvoid)
{

	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = MOTOR_SET_MIN_POS;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	int32_t min_pos = (int32_t)rx_cmd->parameters[1];
	if(rx_cmd->num_params==2  && motor_SetMinPosition(axis, min_pos))
	{
		sprintf((char *)command.cmd_msg.cmd_data, "ACK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetMinPosition axis: %d, OK\r\n", axis);
	}
	else
	{
		strcpy((char *)command.cmd_msg.cmd_data, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetMinPosition axis: %d, FAILED\r\n", axis);
	}

	DbgPrintf(DBG_BINARY_PROTO, "CMD M_SetMinPos\r\n");
	IPC_SendReply(&command);

}

/**
* @brief				Callback function to set max position limit
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis and max position params in a string
* 		String of separated params:
* 			- axis
* 			- max position
*
* @return None
*/
static void M_SetMaxPos(unsigned char trunk, unsigned char* dataptrvoid)
{

	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = MOTOR_SET_MAX_POS;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	int32_t max_pos = (int32_t)rx_cmd->parameters[1];
	if(rx_cmd->num_params==2  && motor_SetMaxPosition(axis, max_pos))
	{
		sprintf((char *)command.cmd_msg.cmd_data, "ACK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetMaxPosition axis: %d, OK\r\n", axis);
	}
	else
	{
		strcpy((char *)command.cmd_msg.cmd_data, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetMaxPosition axis: %d, FAILED\r\n", axis);
	}

	DbgPrintf(DBG_BINARY_PROTO, "CMD M_SetMaxPos\r\n");
	IPC_SendReply(&command);

}

/**
* @brief				Callback to motor home at current position
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis params in a string
* 		String of separated params:
* 			- axis
*
* @return None
*/
static void M_SetHome(unsigned char trunk, unsigned char* dataptrvoid)
{

	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = MOTOR_SET_HOME;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	if(rx_cmd->num_params==1  && motor_SetHome(axis))
	{
		sprintf((char *)command.cmd_msg.cmd_data, "ACK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetHome axis: %d, OK\r\n", axis);
	}
	else
	{
		strcpy((char *)command.cmd_msg.cmd_data, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetHome axis: %d, FAILED\r\n", axis);
	}

	DbgPrintf(DBG_BINARY_PROTO, "CMD M_SetHome\r\n");
	IPC_SendReply(&command);

}

/**
* @brief				Callback to set motor Deceleraztion
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis and deceleration params in a string
* 		String of separated params:
* 			- axis
* 			- deceleration
*
* @return None
*/
static void M_SetDec(unsigned char trunk, unsigned char* dataptrvoid)
{
	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = SET_DEC_CMD;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	uint32_t deceleration = (int32_t)rx_cmd->parameters[1];
	if(rx_cmd->num_params==2  && motor_SetDec(axis, deceleration))
	{
		sprintf((char *)command.cmd_msg.cmd_data, "ACK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetDec axis: %d, dec %d OK\r\n", axis, deceleration);
	}
	else
	{
		strcpy((char *)command.cmd_msg.cmd_data, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetDec axis: %d, dec %d  FAILED\r\n", axis, deceleration);
	}

	DbgPrintf(DBG_BINARY_PROTO, "CMD M_SetDec\r\n");
	IPC_SendReply(&command);
}

/**
* @brief				Callback to get actual current on motor
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis params in a string
* 		String of separated params:
* 			- axis
*
* @return None
*/
static void M_GetActualCurrent(unsigned char trunk, unsigned char* dataptrvoid)
{

	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = GET_ACTUAL_CURRENT;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	uint32_t val = motor_GetMotorActualCurrent(axis);
	sprintf((char *)command.cmd_msg.cmd_data, "%d",val);
	DbgPrintf(DBG_BINARY_PROTO, "CMD M_GetActualCurrent\r\n");
	IPC_SendReply(&command);

}

/**
* @brief				Callback to set current limit on a motor
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis and deceleration params in a string
* 		String of separated params:
* 			- axis
* 			- deceleration
*
* @return None
*/
static void M_SetCurrentLimit(unsigned char trunk, unsigned char* dataptrvoid)
{
	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = MOTOR_SET_CURRENT_LIMIT;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	uint32_t curr_limit = (int32_t)rx_cmd->parameters[1];
	if(rx_cmd->num_params==2  && motor_SetCurrentLimit(axis, curr_limit))
	{
		sprintf((char *)command.cmd_msg.cmd_data, "ACK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetCurrentLimit axis: %d, limit %d OK\r\n", axis, curr_limit);
	}
	else
	{
		strcpy((char *)command.cmd_msg.cmd_data, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetCurrentLimit axis: %d, limit %d  FAILED\r\n", axis, curr_limit);
	}

	DbgPrintf(DBG_BINARY_PROTO, "CMD M_SetCurrentLimit\r\n");
	IPC_SendReply(&command);
}

/**
* @brief				Callback to set time on current limit on a motor
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis and timme on current limit params in a string
* 		String of separated params:
* 			- axis
* 			- time
*
* @return None
*/
static void M_SetCurrentLimitTime(unsigned char trunk, unsigned char* dataptrvoid)
{
	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = MOTOR_SET_CURRENT_LIMIT_TIME;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	uint32_t time = (int32_t)rx_cmd->parameters[1];
	if(rx_cmd->num_params==2  && motor_SetCurrentLimitTime(axis, time))
	{
		sprintf((char *)command.cmd_msg.cmd_data, "ACK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetCurrentLimitTime axis: %d, time limit %d OK\r\n", axis, time);
	}
	else
	{
		strcpy((char *)command.cmd_msg.cmd_data, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "motor_SetCurrentLimitTime axis: %d, time limit %d  FAILED\r\n", axis, time);
	}

	DbgPrintf(DBG_BINARY_PROTO, "CMD motor_SetCurrentLimitTime\r\n");
	IPC_SendReply(&command);
}

/**
* @brief				Callback to get maximum current on a motor drive
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis params in a string
* 		String of separated params:
* 			- axis
*
* @return None
*/
static void M_GetRatedCurrent(unsigned char trunk, unsigned char* dataptrvoid)
{
	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = MOTOR_GET_RATED_CURRENT;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	int axis = (int)rx_cmd->parameters[0];
	uint32_t maximum_current = motor_GetMotorRatedCurrent(axis);
	sprintf((char *)command.cmd_msg.cmd_data, "%d",maximum_current);
	DbgPrintf(DBG_BINARY_PROTO, "M_GetRatedCurrent axis: %d, max current on drive %d OK\r\n", axis, maximum_current);

	DbgPrintf(DBG_BINARY_PROTO, "CMD M_GetRatedCurrent\r\n");
	IPC_SendReply(&command);


}

/**
* @brief				Service mode management on motor power
*
* @param trunk			Communication bus number
* @param dataptrvoid	Enable params in a string
* 		String of separated params:
* 			- Enable
*
* @return None
*/
static void M_Sync_ServiceMode(unsigned char trunk, unsigned char* dataptrvoid){
	generic_cmd_t * rx_cmd= ((generic_cmd_t *)(dataptrvoid));
	IPC_msg_t command;
	command.cmd_msg.id = trunk;
	command.cmd_msg.channel = IPC_CMD_CHANNEL;
	command.cmd_msg.cmd_id = M_SYNC_SERVICEMODE_CMD;
	command.cmd_msg.cmd_type = IPC_CMD_REPLY;
	uint32_t enable_service = (int)rx_cmd->parameters[0];
	motor_EnableServiceState(enable_service?true:false);
	sprintf((char *)command.cmd_msg.cmd_data, "ACK");

	DbgPrintf(DBG_BINARY_PROTO, "M_SYNC_SERVICEMODE_CMD=%d terminated\r\n",enable_service);
	IPC_SendReply(&command);
}

//******************************************************************//
//					END	 SECTION									//
//******************************************************************//



/**
 * \brief			Binary function initialization
 * 
 * \param			void
 * 
 * \return void
 */
void binaryproto_function_init (void)
{
	command_reset();
	//Generic
	command_init (FWVERSION_CMD, verfwcmd);
	//Motor Specific
	command_init (MOTOR_HOME_CMD, motor_home);
	command_init (SET_HOME_SPEED_CMD, M_SetHomeSpeed);
	command_init (SET_POSITION_CMD, M_SetPosition);
	command_init (SET_SPEED_CMD, M_SetSpeed);
	command_init (SET_ACC_CMD, M_SetAcc);
	command_init (SET_JOG_SPEED_CMD, M_SetJogSpeed);
	command_init (GO_JOG_FWD_CMD, M_GoJogFwd);
	command_init (GO_JOG_REV_CMD, M_GoJogRev);
	command_init (GET_MOTION_STATUS_CMD, M_GetMotionStatus);
	command_init (GET_MOTOR_STATUS_CMD, M_GetMotorStatus);
	command_init (QUICK_STOP_CMD, M_QuickStop);
	command_init (M_STOP_CMD, M_StopHere);
	command_init (GET_ACTUAL_POS_CMD, M_GetMotorActPos);
	command_init (MOTOR_INIT_CMD, M_Init);
	command_init (MOTOR_SET_MIN_POS, M_SetMinPos);
	command_init (MOTOR_SET_MAX_POS, M_SetMaxPos);
	command_init (MOTOR_SET_HOME, M_SetHome);
	command_init (SET_DEC_CMD, M_SetDec);
	command_init (GET_ACTUAL_CURRENT, M_GetActualCurrent);
	command_init(MOTOR_SET_CURRENT_LIMIT, M_SetCurrentLimit);		// soglia limite in mA per andare in fault
	command_init(MOTOR_SET_CURRENT_LIMIT_TIME, M_SetCurrentLimitTime);	// limite di tempo per il quale può essere superata la soglia in ms
	command_init(MOTOR_GET_RATED_CURRENT, M_GetRatedCurrent);
	command_init( M_SYNC_SERVICEMODE_CMD, M_Sync_ServiceMode);
}
