/*
 * Protocol.c
 *
 * Created: 06/12/2021 10:30:11
 *  Author: Stefano
 */ 
#include "main.h"
//#include "dbgprotocol.h"
#include "binaryprotocol.h"
#include "IPC_common.h"
#include "ipc_command.h"
#include "protocol.h"
#include "ipc_protocol.h"

/**
 * 	Function to check IPC_CMD_CHANNEL from CM7
 *
/** @brief 	Function to check IPC_CMD_CHANNEL from CM7
 *  @param	cmd_msg_t *cmd to analyze
 *  @return ERROR if any message found
 *  		!=ERROR if at least 1 message present
 */
int ipc_protocol_Process(cmd_msg_t *cmd)
{
	//Only IPC command
	int data_check = ipc_process_packet(cmd);
	return data_check;
}

void ipc_protocol_Init(void)
{
	binaryproto_function_init();
}

