/*! \file mis23.h \brief Control module for JVL MIS23x motors*/
//*****************************************************************************
//
// File Name	: 'mis23x.h'
// Title		: Control module for JVL MIS23x stepper motors.
// Author		: Alessandro
// Company		: Qprel s.r.l.	 
// Created		: 14/10/2017
// Revised		: -
// Version		: 1.0
//
//*****************************************************************************


#ifndef MIS23X_H_
#define MIS23X_H_

#include "main.h"

#include "motors.h"

//Disable phase time to avoid inertial movement
#define MIS23_DISABLE_TIME 500

/** @name operation mode 
 *  motor operation mode enumeration
 */
///@{
enum {
	MIS23_OP_PASSIVE_MODE = 0,
	MIS23_OP_VELOCITY_MODE = 1,
	MIS23_POSITION_MODE = 2,
	MIS23_STOP_MODE = 11,
	MIS23_ZERO_SEARCH_1 = 13,
	MIS23_ZERO_SEARCH_2 = 14,
	MIS23_CYCLIC_POS_MODE = 32
	};
///@}	

/** @name register object address definition
 */
///@{
#define MIS23_REG_OBJ 0x2012
///@}

/** @name MIS23x register
 *  motor register definitions
 */
///@{
#define	MIS23_PROG_VERSION			1
#define	MIS23_MODE_REG				2
#define	MIS23_P_SOLL				3
#define	MIS23_V_SOLL				5
#define	MIS23_A_SOLL				6
#define	MIS23_RUN_CURRENT			7
#define	MIS23_STANDBY_TIME			8
#define	MIS23_STANDBY_CURRENT		9
#define	MIS23_P_IST					10
#define	MIS23_V_IST					12
#define	MIS23_V_START				13
#define	MIS23_ENCODER_POSITION		16
#define	MIS23_INPUTS				18
#define	MIS23_OUTPUTS				19
#define	MIS23_FLWERR				20
#define MIS23_FLWERRMAX				22	// 32bit R/W (?2^31)?(2^31?1) 0 Steps The maximum allowed value in FLWERR before an error is triggered. If FLWERRMAX = 0, the error is disabled.
#define MIS23_COMMAND				24	// 32bit R/W FastMac
#define MIS23_STATUSBITS			25	// 32bit R Special Status bits:
#define MIS23_TEMP					26	// 32bit R ? ?2.27 � uses offset Temperature measured inside the motor.
#define MIS23_MIN_P_IST				28	// 32bit R/W (?231)?(231?1) 0 Steps Negative software position limit Position limit min
#define MIS23_RESERVED29			29	// (intended for 64?bit MIN_P_IST hi?word)
#define MIS23_MAX_P_IST				30	// 32bit R/W (?231)?(231?1) 0 Steps Positive software position limit Position limit max
#define MIS23_RESERVED31			31	// (intended for 64?bit MAX_P_IST hi?word)
#define MIS23_ACC_EMERG				32	// 32bit R/W 1?500,000 10,000 RPM/s Acceleration to use when performing an emergency stop when an error has occurred.
#define MIS23_IN_POSITION_WINDOW	33	// 32bit R/W 0?(232?1) 20000 Steps Selects how close the internal encoder position must be to P_SOLL to set the InPhysical?Position status bit and prevent further AutoCorrection.
#define MIS23_IN_POSITION_COUNT		34	// 32bit R/W 0?100 2 Counts The number of times to attempt AutoCorrection. A value of zero disables AutoCorrection.
#define MIS23_ERR_BITS				35	// 32bit R/W 0 Special Error bits
#define MIS23_WARN_BITS				36	// 32bit R/W 0 Special Warning bits
#define MIS23_STARTMODE				37	// 32bit R/W 0, 1, 2, 3 0 ? The motor will change to this mode after power up. This is also the mode that is used after a zero search is completed.
#define MIS23_P_HOME				38	// 32bit R/W (?231)?(231?1) 0 Steps The found zero point is offset with this value. Zero search position
#define MIS23_RESERVED39			39	// (intended for 64?bit P_HOME hi?word)
#define MIS23_V_HOME				40	// 32bit R/W ?300,000?300,000 ?5000 0.01 RPM The velocity to use during zero search. Set a negative velocity to search in the negative direction.
#define MIS23_HOMEMODE				42	// 32bit R/W 0,13,14 0 ? Select the zero search that should start on power up. Zero search mode
#define MIS23_RESERVED43			43	//Reserved 32bit R/W 1?8 0 Planned ? Not supported yet!
#define MIS23_ABS_ENC_POS			46	// 32bit R 0?409,500 0 Steps The position last read from the internal magnetic encoder. This is the absolute single?turn position.
#define MIS23_EXTENCODER			47	// 32bit R (?231)?(231?1) 0 Counts The value from an external encoder, eg. SSI. SSI Encoder value
#define MIS23_FLEXREG				48	// 32bit R ? 0 ? A mix of 16 bits from different registers. The user can set this up.
#define MIS23_SETUP_BITS			124	// 32bit R/W ? 0 
#define MIS23_P_NEW					144 //
#define MIS23_MAX_VOLTAGE_CURRENT	157	// 32bit R 0?100 [VDC] 0?9000 [mARMS] Volt Bit 0?15: Max voltage on bus If the bus voltage exceeds this value, the motor will go in error. Bit 16?31: Full scale motor current in mARMS
#define MIS23_D_SOLL				174 //
///@}	

/** @name MIS23x commands
 *  motor commands for reg. 24
 */
///@{
#define R24_ResyncP_ISTandP_ENCODER		257
#define R24_ResetCPU					267
#define R24_SaveToFlashAndResetCPU		268
#define R24_SaveToFlashAndContinue		269
#define R24_PresetH3withP_NEW			316
#define R24_SetRS422toSSI				320
#define R24_ReadSSi						321
#define R24_ReadSSIandConvertToBinary	322
#define R24_ClearFlashSectorsInRXP		342
#define R24_PresetEncoderP_ISTandP_SOLLwithP_NEW	354
#define R24_EmergencyStopWithDeceleration			394
#define R24_EmergencyStopNoDeceleration				395
///@}

/** @name MIS23x setup
 *  motor setups bit for reg. 124
 */
///@{
#define Invert_motor_direction							0x00000001
#define Dont_start_program_after_pow_up					0x00000002
//Bit 2?3: External encoder input type
#define Synchronize_to_encoder_after_passive			0x00000010
#define In_phys_Position_update_continuously			0x00000020
#define Startup_Transfer_single_turn_position_to_P_IST	0x00000400 
#define Startup_Transfer_multi_turn_position_to_P_IST	0x00000800
#define Startup_Keep_External_Encoder					0x00001000
#define Startup_Keep_SSI_Value							0x00002000
#define CANopen_Beckhoff_mode							0x00004000
#define External_Encoder_counting_direction				0x00010000
#define Disable_position_limit_error					0x00020000
#define Disable_brake_temporarily						0x00080000
#define Disable_SSI_encoder_error						0x00100000
#define Low_bus_voltage_to_Error						0x00200000
#define Low_bus_voltage_to_Passive						0x00400000
#define Low_bus_voltage_to_0RPM							0x00800000
#define Enable_closed_loop								0x01000000
#define Enable_closed_loop_current_control				0x02000000
#define Position_limits_without_memory					0x10000000
///@}

/** @name MIS23x status
 *  motor status bits definitions
 */
///@{
//Bit 0: Reserved
#define AutoCorrection_active	0x00000002
#define In_Physical_Position	0x00000004
#define At_velocity				0x00000008
#define In_position				0x00000010
#define Accelerating			0x00000020
#define Decelerating			0x00000040
#define Zero_search_done		0x00000080
#define PassWord_lock			0x00000100
#define Magnetic_encoder_error	0x00000200
//Bits 10?13: Reserved
#define Electrom_brake_active			0x00004000
#define Closed_loop_lead_lag_detected	0x00008000
#define Closed_loop_activated			0x00010000
#define Internal_encoder_calibrated		0x00020000
#define Standby_current_is_used			0x00040000
#define External_memory_ok				0x00080000
#define Internal_encoder_ok				0x00100000
#define Ethernet_sync_activated			0x00200000
#define In_target_position				0x00400000
#define STO_channel_A_ok				0x00800000
#define STO_channel_B_ok				0x01000000
//Bit 25?31: Reserved
///@}

/** @name MIS23x errors
 *  motor error bits definitions
 */
///@{
#define General_error								0x00000001	//(always set together with another error bit)
#define Follow_error								0x00000002
#define Output_driver								0x00000004
#define Position_Limit								0x00000008
#define Low_bus_voltage								0x00000010
#define Over_voltage								0x00000020
#define Temperature_90								0x00000040
#define Internal									0x00000080	//(Self diagnostics failed)
#define Absolute_multiturn_encoder_lost_position	0x00000100
#define Absolute_multiturn_encoder_sensor_counting	0x00000200
#define No_comm_to_absolute_multiturn_encoder		0x00000400
#define SSI_encoder_counting						0x00000800
#define Closed_loop									0x00001000
#define External_memory								0x00002000
#define Absolute_single_turn_encoder				0x00004000
#define Safe_Torque_Off								0x08000000	// (STO)
///@}

/** @name MIS23x warnings
 *  motor warning bits definitions
 */
///@{
#define Positive_limit_active			0x00000001
#define Negative_limit_active			0x00000002
#define Positive_limit_has_been_active	0x00000004
#define Negative_limit_has_been_active	0x00000008
#define Low_bus_voltage					0x00000010
//Bit 5: Reserved					
#define Temperature_80					0x00000040
#define SSI_encoder						0x00000080
#define Driver_overload					0x00000100
///@}

/** @name default position limits
 *	Motorpower => 16384 step/revolution
 *	JVL => 409600 step/revolution
 *  Position conversion from Motorpower to JVL encoder:
 *	JVL = (409600/16384)*MTP => JVL = 25*MTP
 */

#define	MIS23_RPM_STEP	409600.0f
#define MIS23_ACC_CONV(x)		(((float)(x))/MIS23_RPM_STEP)*100.0
#define MIS23_VEL_CONV(x)		(((float)(x))/MIS23_RPM_STEP)*6000.0

///@{
	#define MIS23_GANTRY_MIN_POS_LIMIT	-100
	#define MIS23_GANTRY_HOME_POS		21430
	#define MIS23_GANTRY_MAX_POS_LIMIT	3900260
	#define MIS23_GANTRY_SPEED			(30000)	//300rpm <-> 30000 Step/s,
	#define MIS23_GANTRY_HOME_SPEED		MIS23_GANTRY_SPEED
	#define MIS23_GANTRY_JOG_SPEED	    MIS23_GANTRY_SPEED
	#define MIS23_GANTRY_ACC			1000

	#define MIS23_BED_MIN_POS_LIMIT		-100
	#define MIS23_BED_HOME_POS			163840
	#define MIS23_BED_MAX_POS_LIMIT		3112960
	#define MIS23_BED_SPEED				50000
	#define MIS23_BED_HOME_SPEED		MIS23_BED_SPEED
	#define MIS23_BED_JOG_SPEED			MIS23_BED_SPEED
	#define MIS23_BED_ACC				1000

	#define MIS23_FLATP_MIN_POS_LIMIT	436906
	#define MIS23_FLATP_HOME_POS		1075882
	#define MIS23_FLATP_MAX_POS_LIMIT	1780394
	#define MIS23_FLATP_SPEED			50000	
	#define MIS23_FLATP_HOME_SPEED		MIS23_FLATP_SPEED
	#define MIS23_FLATP_JOG_SPEED		MIS23_FLATP_SPEED
	#define MIS23_FLATP_ACC				1000
	
	#define MIS23_LIFT_MIN_POS_LIMIT	-50000
	#define MIS23_LIFT_HOME_POS			0
	#define MIS23_LIFT_MAX_POS_LIMIT	90050000
	#define MIS23_LIFT_SPEED			30000
	#define MIS23_LIFT_HOME_SPEED		MIS23_LIFT_SPEED
	#define MIS23_LIFT_JOG_SPEED		MIS23_LIFT_SPEED
	#define MIS23_LIFT_ACC				1000
//	#define MIS23_LIFT_RANGE_MM			4900	//37CM

	#define MTP_TO_MIS23(x) (x)		//(x*25)
	#define MIS23_TO_MTP(x) (x)		//(((x*10)/25 +5)/10)
///@}

/** @name default max current for each motor in mA
 *	and current to binary conversion macro
 *  convert current value in mA to binary value to write in reg 7
 *	100 bit = 5.87 mA
 */
///@{
	#define MIS23_GANTRY_MAX_CURRENT	3500	
	#define MIS23_BED_MAX_CURRENT		3500
	#define MIS23_FLATP_MAX_CURRENT		3500
	#define MIS23_LIFT_MAX_CURRENT		3500

	#define AMP_TO_BIT(x)	((x*100)/587)
	#define BIT_TO_AMP(x)	(((float)x*587.0f)/100.0f)
///@}

///* motor general command functions */
uint32_t mis23x_Init(int axis, uint8_t node);
//uint32_t motor_StateCommand(int axis, uint32_t cmd);
uint32_t mis23x_FaultReset(int axis);
//uint32_t motor_Stop(int axis);
uint32_t mis23x_GetMotionStatus(int axis);
uint32_t mis23x_GetMotorStatus(int axis);
//uint32_t motor_SetHeartbeat(int axis, uint32_t value);
uint32_t mis23x_SetModeOperation(int axis, uint32_t mode);

///*** POSITION MODE ***/
///* motor set position command */
uint32_t mis23x_SetPosition(int axis, uint32_t new_position);
uint32_t mis23x_SetSpeed(int axis, uint32_t speed);
uint32_t mis23x_SetAcc(int axis, uint32_t acc);
uint32_t mis23x_SetDec(int axis, uint32_t dec);
///***  HOMING ***/
uint32_t mis23x_SetHome(int axis);
uint32_t mis23x_Home(int axis);
///* motor set homing method */
//uint32_t motor_SetHomingMethod(int axis, uint32_t h_method);
//uint32_t motor_SetHomingOffset(int axis, int32_t h_offset);
//uint32_t motor_SetHomingSpeed(int axis, uint32_t h_speed);
//uint32_t motor_SetHomingAcc(int axis, uint32_t h_acc);
///***  JOG MODE ***/
uint32_t mis23x_SetJogSpeed(int axis, uint32_t j_speed);
uint32_t mis23x_GoJogFwd(int axis);
uint32_t mis23x_GoJogRev(int axis);

#ifdef	OLD_STYLE_COPROCESS
	void mis23x_Machine(int m_idx);
#else
	void mis23x_Machine(void * param, uint8_t *data);
#endif
void mis23x_Manage(int axis);
uint32_t mis23x_SetHomeSpeed(int axis, uint32_t h_speed);

//void motor_InitTiming(void);
uint32_t mis23x_QuickStop(int axis);
uint32_t mis23x_StopHere(int axis);
uint32_t mis23x_GetMotorActPos(int axis, int32_t* pos);
void mis23x_InitData(motor_obj_t * m_obj);
uint32_t mis23x_SetMinPosition(int axis, int32_t min_pos);
uint32_t mis23x_SetMaxPosition(int axis, int32_t max_pos);

uint32_t mis23x_GetMotorActualCurrent(int axis);
uint32_t mis23x_GetMotorRatedCurrent(int axis);
////uint32_t motor_ShutDown(void);
////uint32_t motor_SwitchOn(void);
////uint32_t motor_DisableVoltage(void);
////uint32_t motor_DisableOperation(void);
////uint32_t motor_EnableOperation(void);
//
uint32_t mis23x_SetCurrentLimit(int axis, int max_current);
uint32_t mis23x_SetCurrentLimitTime(int axis, uint32_t limit_time);
uint32_t mis23x_SetZero(int axis);
//uint32_t mis23x_ResetErrors(int axis);
void mis23x_DecodeErrors(int axis);
void mis23x_DecodeWarnings(int axis);

uint32_t mis23x_ReadObj(int axis, uint32_t index, uint32_t subindex, uint32_t *value);
uint32_t mis23x_WriteObj(int axis, uint32_t index, uint32_t subindex, uint32_t value);
#endif /* DUET_H_ */
