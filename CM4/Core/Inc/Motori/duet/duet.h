/*! \file duet.h \brief Motor control module */
//*****************************************************************************
//
// File Name	: 'duet.h'
// Title		: Motor control module.
// Author		: Alessandro
// Company		: Qprel s.r.l.	 
// Created		: 17/09/2014
// Revised		: -
// Version		: 1.0
//
//*****************************************************************************


#ifndef DUET_H_
#define DUET_H_

#include <stdbool.h>
#include "main.h"
#include "motors.h"

/** @name motor states definitions
 */
///@{
#define	DUET_NOT_READY_TO_SWITCH_ON	0x00
#define MOT_SWITCH_ON_DISABLE		0x40
#define MOT_READY_TO_SWITCH_ON		0x21
#define MOT_SWITCHED_ON				0x23
#define MOT_OPERATION_ENABLED		0x27
#define MOT_QUICK_STOP_ACTIVE		0x07
#define MOT_FAULT_REACTION_ACTIVE	0x0F
#define MOT_FAULT					0x08
#define MOT_STATUS_MASK				0x6F
///@}

/**
 * \brief motor change state commands
*/
enum {
	 MOT_CMD_SHUTDOWN = 0,
	 MOT_CMD_SWITCH_ON,
	 MOT_CMD_VOLT_DISABLE,
	 MOT_CMD_QUICK_STOP,
	 MOT_CMD_DISABLE_OPERATION,
	 MOT_CMD_ENABLE_OPERATION,
	 MOT_CMD_FAULT_RESET,
	 MOT_CMD_STOP
};

/** @name controlword bits 
 *  controlword bits definitions
 */
///@{
#define MOTOR_SWON_BIT		0x0001
#define MOTOR_ENV_BIT		0x0002
#define MOTOR_QSTOP_BIT		0x0004
#define MOTOR_ENOP_BIT		0x0008 
#define MOTOR_INTERP_BIT	0x0010 
#define MOTOR_FRES_BIT		0x0080
#define MOTOR_HALT_BIT		0x0100
///@}

/** @name general statusword bits 
 *  general statusword bits definitions
 */
///@{
#define MOTOR_READY_TO_SWITCH_ON_BIT	0x0001
#define MOTOR_SWITCHED_ON_BIT			0x0002
#define MOTOR_OPERATION_ENABLE_BIT		0x0004
#define MOTOR_FAULT_BIT					0x0008
#define MOTOR_VOLTAGE_ENABLED_BIT		0x0010
#define MOTOR_QUICK_STOP_BIT			0x0020
#define MOTOR_SWITCH_ON_DISABLED_BIT	0x0040
#define MOTOR_WARNING_BIT				0x0080
#define MOTOR_REMOTE_BIT				0x0200
#define MOTOR_TARGET_REACHED_BIT		0x0400
#define MOTOR_INTERNAL_LIMIT_ACTIVE_BIT	0x0800
#define MOTOR_IP_MODE_ACTIVE			0x1000
#define MOTOR_FOLLOWING_ERROR_BIT		0x2000
///@}

/** @name op mode statusword bits 
 *  operation mode dependent statusword bits definitions
 */
///@{
// PP mode
#define MOTOR_PP_SET_POINTACKNOWLEDGED	0x1000
#define MOTOR_PP_FOLLOWING_ERROR		0x2000
// PV mode
// TQ mode
// HM mode
// IP mode
///@}

/** @name default position limits
 */
///@{
#define DUET_GANTRY_MIN_POS_LIMIT	0
#define DUET_GANTRY_HOME_POS		21430
#define DUET_GANTRY_MAX_POS_LIMIT	3900260

#define DUET_BED_MIN_POS_LIMIT		0
#define DUET_BED_HOME_POS			163840
#define DUET_BED_MAX_POS_LIMIT		3112960

#define DUET_FLATP_MIN_POS_LIMIT	436906
#define DUET_FLATP_HOME_POS			1075882
#define DUET_FLATP_MAX_POS_LIMIT	1780394

#define DUET_TILT_MIN_POS_LIMIT		-143360
#define DUET_TILT_HOME_POS			0
#define DUET_TILT_MAX_POS_LIMIT		552960
#define DUET_TILT_VEL				122880
#define DUET_TILT_ACC				1228800
#define DUET_TILT_DEC				1228800

#define DUET_BACKLIFT_MIN_POS_LIMIT		-1228800
#define DUET_BACKLIFT_HOME_POS			0
#define DUET_BACKLIFT_MAX_POS_LIMIT		39321600
#define DUET_BACKLIFT_VEL				319488
#define DUET_BACKLIFT_ACC				1228800
#define DUET_BACKLIFT_DEC				1228800

#define DUET_FRONTLIFTDX_MIN_POS_LIMIT		-614400
#define DUET_FRONTLIFTDX_HOME_POS			0
#define DUET_FRONTLIFTDX_MAX_POS_LIMIT		19660800
#define DUET_FRONTLIFTDX_VEL				184320
#define DUET_FRONTLIFTDX_ACC				614400
#define DUET_FRONTLIFTDX_DEC				614400

#define DUET_FRONTLIFTSX_MIN_POS_LIMIT		-614400
#define DUET_FRONTLIFTSX_HOME_POS			0
#define DUET_FRONTLIFTSX_MAX_POS_LIMIT		19660800
#define DUET_FRONTLIFTSX_VEL				184320
#define DUET_FRONTLIFTSX_ACC				614400
#define DUET_FRONTLIFTSX_DEC				614400

#define DUET_LIFTER_MIN_POS_LIMIT		-5000000
#define DUET_LIFTER_HOME_POS			0
#define DUET_LIFTER_MAX_POS_LIMIT		5000000
#define DUET_LIFTER_VEL					(200000/2)	//200000	//step/s		//55705
#define DUET_LIFTER_ACC					(122880/2)	//122880	//step/s/s		//55705
#define DUET_LIFTER_DEC					(122880/2)	//122880	//step/s/s		//55705

#define	STEP_TO_MM						3277		//3276.8 step/mm
#define OFFSET_2DEGREE					21435		//Offset 2 degrees on vimago for keyboard step

///@}

uint32_t duet_WriteControlWord(int axis, uint32_t cw);
uint32_t duet_ReadStatusWord(int axis, volatile uint32_t * sw);
uint32_t duet_ReadControlWord(int axis, uint32_t * cw);
/* motor general command functions */
uint32_t duet_Init(int axis, uint8_t node);
uint32_t duet_StateCommand(int axis, uint32_t cmd);
uint32_t duet_FaultReset(int axis);
uint32_t duet_Stop(int axis);
uint32_t duet_GetMotionStatus(int axis);
uint32_t duet_GetMotorStatus(int axis);
bool duet_SetMotorStatus(int axis, int new_stat);
uint32_t duet_SetHeartbeat(int axis, uint32_t value);

/** @name operation mode 
 *  motor operation mode enumeration
 */
///@{
enum {
	OP_NO_MODE = -1,
	OP_RESERVED_0,
	OP_PROFILED_POSITION,
	OP_VELOCITY,  // not supported by elmo drive
	OP_PROFILED_VELOCITY,
	OP_TORQUE_PROFILED,
	OP_RESERVED_5,
	OP_HOMING,
	OP_INTERPOLATE_POSITION,
	OP_LAST_MODE
	};
///@}


typedef struct{
	bool enable;
	int ratio;
	int count;
}syncro_stop_t;	

typedef struct {
	bool enable;
	int axis;		
}syncro_motor_t;

#ifdef AGNOSTIC_AXIS
	extern syncro_motor_t motor_sync1;
	extern syncro_motor_t motor_sync2;
#endif

extern syncro_stop_t syncro_stop;

//Using to enabke a similar management as in Pegaso machine
//#define		SAME_AS_SYNCRO
	
uint32_t duet_SetModeOperation(int axis, uint32_t mode);
uint32_t duet_GetModeOperation(int axis, uint32_t *mode);

/*** PROFILED POSITION ***/
/* motor set position command */
uint32_t duet_SetPosition(int axis, int32_t new_position);
uint32_t duet_SetSpeed(int axis, uint32_t speed);
uint32_t duet_SetAcc(int axis, uint32_t acc);
uint32_t duet_SetDec(int axis, uint32_t dec);
/***  HOMING ***/
uint32_t duet_SetHome(int axis);
uint32_t duet_Home(int axis);
/* motor set homing method */
uint32_t duet_SetHomingMethod(int axis, uint32_t h_method);
uint32_t duet_SetHomingOffset(int axis, int32_t h_offset);
uint32_t duet_SetHomingSpeed(int axis, uint32_t h_speed);
uint32_t duet_SetHomingAcc(int axis, uint32_t h_acc);
/***  JOG MODE ***/
uint32_t duet_SetJogSpeed(int axis, uint32_t j_speed);
uint32_t duet_GoJogFwd(int axis);
uint32_t duet_GoJogRev(int axis);
#ifdef	OLD_STYLE_COPROCESS
void duet_Machine(int m_idx);
#else
void duet_Machine(void *, uint8_t *);
#endif
void duet_Manage(int axis);
void duet_ManageSlider(void);
void duet_ManageSlider_same_as_syncro(void);
uint32_t duet_SetHomeSpeed(int axis, uint32_t h_speed);

void duet_InitTiming(void);
uint32_t duet_QuickStop(int axis);
uint32_t duet_StopHere(int axis);
uint32_t duet_GetMotorActPos(int axis, int32_t* pos);
void duet_InitData(motor_obj_t * m_obj);
uint32_t duet_SetMinPosition(int axis, int32_t min_pos);
uint32_t duet_SetMaxPosition(int axis, int32_t max_pos);

uint32_t duet_GetMotorRatedCurrent(int axis);
uint32_t duet_SetMotorRatedCurrent(int axis, uint32_t val);
uint32_t duet_GetMotorActualCurrent(int axis);
//uint32_t motor_ShutDown(void);
//uint32_t motor_SwitchOn(void);
//uint32_t motor_DisableVoltage(void);
//uint32_t motor_DisableOperation(void);
//uint32_t motor_EnableOperation(void);

uint32_t duet_SetCurrentLimit(int axis, int max_current);
uint32_t duet_SetCurrentLimitTime(int axis, uint32_t limit_time);

/// Function to command motor in standby mode without power applied on the shaft
uint32_t duet_OperationDisable(int axis);
uint32_t duet_SetZero(int axis);

uint32_t duet_ReadObj(int32_t axis, uint32_t index, uint32_t subindex, uint32_t *value);
uint32_t duet_WriteObj(int32_t axis, uint32_t index, uint32_t subindex, uint32_t value);

/// Function to set parameter in the motor node object dictionary
uint32_t duet_SetParameter(int axis, uint32_t idx, uint8_t subidx, uint32_t * val, uint32_t size);

/// Function to command motor in operative mode
uint32_t duet_OperationEnable(int axis);


#endif /* DUET_H_ */
