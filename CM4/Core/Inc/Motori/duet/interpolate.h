/*
 * interpolate.h
 *
 * Created: 01/06/2020 09:07:44
 *  Author: alessandro.rossi
 */ 


#ifndef INTERPOLATE_H_
#define INTERPOLATE_H_

#include "main.h"
#include "settings.h"

extern TIM_HandleTypeDef htim8;
#define		htim_ip		htim8

//#define LED_IP 	LED1_GPIO
#define	LED_DIGIO_CLK		LED1_CLK
#define	LED_DIGIO_PORT		LED1_PORT
#define	LED_DIGIO_GPIO		LED1_GPIO

uint32_t duet_SetInterpolatePositionLinear(int axis1, int axis2, int32_t new_position);
void setMaxutOfSyncErr(int par);
void IP_Handler(void);

extern int  interp_end;
extern uint32_t last_syncro_Sendtimer;

#endif /* INTERPOLATE_H_ */
