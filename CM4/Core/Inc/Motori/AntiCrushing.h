/*
 * AntiCrushing.h
 *
 * Created: 25/01/2021 10:37:51
 *  Author: stefano
 */ 


#ifndef ANTICRUSHING_H_
#define ANTICRUSHING_H_

#include <stdbool.h>
#include "main.h"

typedef struct{
	bool presence;
	bool enable;
	bool alarm;
}anticrush_t;

extern anticrush_t anticrush_state; 

void AntiCrushingInit(void);
void AntiCrushingCheck(void);
uint8_t AntiCrushingInterruptEnable (void);
void AntiCrushingInterruptDisable (void);
void CrushingEnable(uint8_t en);

#endif /* ANTICRUSHING_H_ */
