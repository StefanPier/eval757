/*
 * linact.h
 *
 * Created: 04/02/2018 15:54:02
 *  Author: Alessandro
 */ 


#ifndef LINACT_H_
#define LINACT_H_

	#include "main.h"
	#include "settings.h"
	#include "motors.h"

	/* Linear actuator digital commands defines  */
	#define ACT1_FWD_CLK	DIGITOUT4_CLK
	#define ACT1_FWD_PORT	DIGITOUT4_PORT
	#define ACT1_FWD_GPIO	DIGITOUT4_GPIO

	#define ACT1_REV_CLK 	DIGITOUT5_CLK
	#define ACT1_REV_PORT 	DIGITOUT5_PORT
	#define ACT1_REV_GPIO 	DIGITOUT5_GPIO

	#define ACT2_FWD_CLK 	DIGITOUT6_CLK
	#define ACT2_FWD_PORT	DIGITOUT6_PORT
	#define ACT2_FWD_GPIO 	DIGITOUT6_GPIO

	#define ACT2_REV_CLK	DIGITOUT7_CLK
	#define ACT2_REV_PORT	DIGITOUT7_PORT
	#define ACT2_REV_GPIO	DIGITOUT7_GPIO

	#define	LINACT_HALL_IN_CLK		DIGITIN8_CLK
	#define	LINACT_HALL_IN_PORT		DIGITIN8_PORT
	#define	LINACT_HALL_IN_GPIO		DIGITIN8_GPIO

	//#define LINACT_0_10
	//#define LINACT_4_20
	#define LINACT_HALL

	#define LINACT_MAX_POS 4000
	#define LINACT_MIN_POS 0

	void linact_Count(void);
	void linact_InitData(motor_obj_t * m_obj);
	uint32_t linact_Init(int axis, uint8_t node);
	void linact_InMachine(int m_idx);
	uint32_t linact_SetPosition(int axis, uint32_t new_position);
	uint32_t linact_GetMotorActPos(int axis, int32_t* pos);
	uint32_t linact_StopHere(int axis);
	void linact_GetPosition(void);
	uint32_t linact_Home(int axis);
	uint32_t linact_ResetPosition(int axis, int position);
	uint32_t linact_GoJogFwd(int axis);
	uint32_t linact_GoJogRev(int axis);
	uint32_t linact_GetMotionStatus(int axis);
	uint32_t linact_GetMotorStatus(int axis);

#endif /* LINACT_H_ */
