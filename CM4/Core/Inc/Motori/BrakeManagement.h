/*
 * BrakeManagement.h
 *
 * Created: 24/11/2020 14:46:35
 *  Author: stefano
 */ 


#ifndef BRAKEMANAGEMENT_H_
#define BRAKEMANAGEMENT_H_

#include  "main.h"

#define	MAX_AXIS_NUMBER	10

#define TIME_TO_START_MOVEMENT	4000

enum{
	InvertedLogicBrake=0,
	NormalLogicBrake,
};

typedef struct{
	uint8_t	enable;
//	uint8_t	axis;
	uint8_t	relay;
	uint8_t	brakeLogic;
	uint32_t timeOff;
}brake_t;



void BrakeInit(void);
void BrakeOn(uint8_t axis);
void BrakeOff(uint8_t axis);
void CheckBrakeOn(uint8_t axis);
void ReloadBrakeOn(uint8_t axis);

#define BRAKE_WAIT_ON	50

extern brake_t brake_manage_v[MAX_AXIS_NUMBER];

#endif /* BRAKEMANAGEMENT_H_ */
