/*
 * Lifter.h
 *
 * Created: 10/08/2020 14:52:44
 *  Author: Stefano
 *  Company: Qprel srl 
 */ 


#ifndef STILLE_H_
#define STILLE_H_

#include "main.h"
#include "buffer.h"
#include "motors.h"


#define		STILLE_CRC_LEN				2	//CRC len
#define		STILLE_CMD_LEN				2	//Lifter header len
#define		STILLE_LENGHT_DIM			2	//Lifter lenght of RT packet in byte
#define		STILLE_PARAM_INDEX_DIM		2	//Lifter parameter index dimension in byte
#define		STILLE_RT_TX_HEADER			(STILLE_LENGHT_DIM+STILLE_PARAM_INDEX_DIM)	//Lifter header on tx for RT cmd: lenght+ index(+ data)
#define		STILLE_RG_REPLY_HEADER		(STILLE_LENGHT_DIM+1)		//Lifter header reply for RG cmd: lenght+ack if all is good (+data)
#define		MAX_STILLE_COMMAND_LEN		100						//Buffer to send lifter message
#define		STILLE_WDT_TIME				200						// [ms]
#define		STILLE_GUARD_TIME			100						// [ms]
#define		ROPEN_CMD_CMD				5			//Standard RO command len
#define		MAX_STILLE_SPEED			100			//
//#define		MM_TO_STEP					0.125		// [mm/step] for Max10-A100295AC510F-000
//#define		MAX_STILLE_MM_POSITION		100.0f

//#define		STILLE_MM_TO_STEP(x)		((uint32_t)(((x)/MM_TO_STEP)+0.5f))
//#define		MAX_STILLE_STEP_POSITION	STILLE_MM_TO_STEP(MAX_STILLE_MM_POSITION)
//#define		STILLE_STEP_TO_MM(x)		((uint32_t)((float)(x)*MM_TO_STEP)+0.5f)

//Maximum limit on parameter request
#define MAX_PAR_RG	32		//Max params number for RG command	TODO CHECK
#define MAX_PAR_RT	32		//Max params number for RT command	TODO CHECK
#define MAX_PAR_RE	3		//Max params number for RE command	TODO CHECK
#define MAX_PAR_RS	3		//Max params number for RS command	TODO CHECK
#define MAX_PAR_RC	3		//Max params number for RC command	
#define MAX_PAR_RO	1		//Max params number for RO command
#define MAX_PAR_RA	0		//Max params number for RA command



//Command Enum
typedef enum{
	RG=0,	//Remote data get
	RT,		//Remote data Transfer
	RC,		//Remote Cycling
	RE,		//Remote execute function
	RS,		//Remote stop function
	RO,		//Remote mode open
	RA		//Remote mode abort
} stille_enum;

//Error code
typedef enum{
	STILLE_TOUT=0x0,
	STILLE_ACK=0x6,
	STILLE_CSE=0x80,	//Checksum Error
	STILLE_PDE,			//Parameter data error
	STILLE_PCE,			//Parameter count error
	STILLE_ICE,			//Invalid command error
	STILLE_PE			//Permission error
}stille_error_enum;

//Function first parameter code
typedef enum{
	STILLE_NOMOVE_FUN=0,				//No Move
	STILLE_MOVEIN_FUN=1,				//Move In
	STILLE_MOVEOUT_FUN,					//Move Out
	STILLE_MOVEPOSMEM1_FUN,				//Move to position Mem1
	STILLE_MOVEPOSMEM2_FUN,				//Move to position Mem2
	STILLE_MOVEPOSMEM3_FUN,				//Move to position Mem3
	STILLE_MOVEPOSMEM4_FUN,				//Move to position Mem4
	STILLE_MOVEIN_INTERMEDIATE_FUN,		//Move to position Intermediate In
	STILLE_MOVEOUT_INTERMEDIATE_FUN,	//Move to position Intermediate Out
	STILLE_MOVE_TO_REMOTE_FUN,			//Move to Remote position
	STILLE_UNUSED_PAR_FUN=0xFF,			//Unused parameter initialized
}stille_funpar_enum;

//Optional soft and rugged start
typedef enum{
	STILLE_RUGGED_STARTSTOP=0,
	STILLE_SOFT_STARTSTOP=0x01,
}stille_start_enum;

//Available actuator and rugged start
typedef enum{
	STILLE_ACTUATOR0=0x00,
	STILLE_ACTUATOR1,
	STILLE_ACTUATOR2,
	STILLE_ACTUATOR3,
	STILLE_ACTUATOR4,
	STILLE_ACTUATOR5,
}stille_actuator_enum;

//Fake node arguments to assign lifter actuator command
#define		STILLE_NODE_MOTOR1		200	//Movimento Assiale su asse longitudinale al lettino (Piano orizzontale)
										//	step(161-1438)	corsa 0-400mm
#define		STILLE_NODE_MOTOR2		201 //Movimento Rotazionale su asse perpendicolare al lettino (Piano orizzontale)
										//	step -345 0 450 gradi -15degree 0 +15degree(counter clock)
#define		STILLE_NODE_MOTOR3		202	//Movimento su Piano verticale
										//	step 597 3908	corsa 655mm - 1010mm
#define		STILLE_NODE_MOTOR4		203	//Movimento Rotazionale su asse longitudinale al lettino (Piano orizzontale)
										//	2 255 560		-15 0degree +15degree (counter clock)
#define		STILLE_NODE_MOTOR5		204	//Movimento Assiale su asse perpendicolare al lettino (Piano orizzontale)
										//	352 881  1548		-75mm 0 +75mm on the right
#define		STILLE_NODE_MOTOR6		205

#define		STILLE_MOTOR_STATUS_BASEADDR	0x00D1
#define		STILLE_GETPOS_BASEADDR			0x0011
#define		STILLE_MOTION_STATUS_BASEADDR	0x0171
#define		STILLE_MOTION_ERROR_BASEADDR	0x0081
#define		STILLE_SETSPEED_BASEADDR		0x3011
#define		STILLE_SETPOS_BASEADDR			0x3021


//Available actuator and rugged start
typedef enum{
	STILLE_STOP=0x00,
	STILLE_MOVING,
}stille_state_enum;



typedef struct{
	stille_enum	cmd;
	uint8_t datal;		//First data to be send
	uint8_t datah;		//Second data to be send
}stille_cmd;

typedef union{
	uint8_t val;
	struct __attribute__ ((packed)){
		uint8_t drive_availability:1,
		signal_limit_in_out:1,
		signal_switch_1:1,
		signal_switch_2:1,
		motion_active:1,
		position_reached:1,
		not_used0:1,
		not_used1:1;
	};
}stille_status1_t;

extern stille_cmd available_stille_cmd[];

void process_stille_protocol(void);
void process_lettino_protocol(void);
void renew_stille_wdt(void);
bool stille_checkresponse(int cmd_enum,cBuffer* buff, uint8_t *ptr_rx_parameter, uint16_t size_par_rx);
uint16_t CalculateChecksum(const unsigned char* pAdr, int len);
int isLifterCmd(cBuffer* cmd_buff, uint16_t bad_start_byte);
bool stille_Command(int cmd_enum, uint8_t *ptr_tx_parameter, uint16_t size_par_tx, uint8_t *ptr_rx_parameter, uint16_t size_par_rx);
void test_stille_movement(void);
void test_stille_readwrite(void);
bool check_stille_error(uint8_t);
bool stille_Read_Parameter(uint16_t parameter, uint8_t *rx_reply,int expeted_rx);
bool stille_Read_Parameter_OnlyValue(uint16_t parameter, void *rx_reply,int expeted_rx);
bool stille_Read_2Parameter_OnlyValues(uint16_t parameter1_idx,int expeted1_rx, void *rx1_reply,uint16_t parameter2_idx,int expeted2_rx, void *rx2_reply);
bool stille_Write_Parameter(uint8_t * parameter, uint16_t num_tx_par,uint8_t *rx_reply,int expected_rx);
bool stille_start_function(uint8_t fun_index,uint8_t fun_par0, uint8_t fun_par1, uint8_t speed_par);
bool stille_stop_function(uint8_t fun_index,uint8_t fun_par0, uint8_t fun_par1);
bool stille_stop_function_small(uint8_t fun_index,uint8_t fun_par0, uint8_t fun_par1);
bool stille_move_to_position(uint32_t Step_Pos,  uint8_t stille_axes);
bool stille_set_target_position(uint32_t Step_Pos, uint8_t stille_axes);
bool stille_set_speed(uint32_t Speed_step_to_s, uint8_t stille_axes);
bool check_connection(uint8_t stille_axes);

//To generalize driver:
void stille_InitData(motor_obj_t * m_obj);
uint32_t stille_Init(int axis, uint8_t node);
uint32_t stille_deInit(void);
uint32_t stille_Home(int axis);
uint32_t stille_SetHome(int axis);
uint32_t stille_SetMaxPosition(int axis, int32_t max_pos);
uint32_t stille_SetMinPosition(int axis, int32_t max_pos);
uint32_t stille_SetPosition(int axis, uint32_t new_position_mm);
uint32_t stille_SetSpeed(int axis, uint32_t speed);
uint32_t stille_GetMotionStatus(int axis);
uint32_t stille_GetMotionStatus_real(int axis);
uint32_t stille_GetMotorStatus(int axis);
uint32_t stille_GetMotorStatus_real(int axis);
uint32_t stille_GetMotorAndMotionStatus(int axis);
uint32_t stille_StopHere(int axis);
uint32_t stille_GetMotorActPos(int axis, int32_t* pos);
uint32_t stille_GetMotorActPos_real(int axis, int32_t* pos);
uint32_t stille_GetMotorActPos_all_axis(void);
bool set_ActualPositionBurst(void);
bool set_MotorStatusBurst(void);
uint32_t stille_GetMotorStatus_all(void);
bool set_MotionStatusBurst(void);
uint32_t stille_GetMotionStatus_all(int32_t* mot_stat);


#endif /* STILLE_H_ */
