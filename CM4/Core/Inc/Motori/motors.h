/*! \file duet.h \brief General motor control module */
//*****************************************************************************
//
// File Name	: 'motors.h'
// Title		: Motor control module.
// Author		: Alessandro
// Company		: Qprel s.r.l.
// Created		: 14/10/2017
// Revised		: -
// Version		: 1.0
//
//*****************************************************************************

#include "main.h"
#include "co_microdep.h"

#ifndef MOTORS_H_
#define MOTORS_H_

/** @name function result definitions 
 */
///@{
#define RES_FAIL	0
#define RES_SUCCESS	1
///@}

/** @name motor model definitions 
 */
///@{
#define MOT_NOMODEL 0
#define MOT_DUET	1
#define	MOT_JVL		2
#define MOT_LINAK	3
#define MOT_STILLE	4
///@}

/** @name motor sync reception timeout ms
 */
///@{
#define MOT_SYNC_RX_TOUT		100		//Per averlo preciso Deve essere multiplo del clock del TC0 che per adesso � a 10ms (100Hz)
#define MOT_MIS23x_HTB_TOUT		(6*MOT_SYNC_RX_TOUT)

///@}

/** @name high level 
 *  motor status definitions
 */
///@{
#define MOT_NOT_INIT			0	//!< motor not initialized
#define	MOT_READY				1	//!< motor initialized
#define MOT_ERROR				2
#define MOT_INIT_ERROR			3   //!< error during initilization
#define MOT_SET_PARAM_ERROR		4	//!< error settig parameter with SDO
#define MOT_GET_PARAM_ERROR		5	//!< error reading parameter with SDO
#define MOT_OVERCURRENT_ERROR	6	//!< current over imposed limit error
#define MOT_HBT_ERROR			7	//!< motor not responding to sync command
#define MOT_FAULT_ERROR			8	//!< fault in the motor status word
#define MOT_FOLLOWING_ERROR		9	//!< motor exceded maximum position steps error
#define MOT_EMERGENCY_ERROR		10	//!< motor sent emergency message
#define MOT_WARNINGS			11	//!< motor warning bit active
#define MOT_SLIDER_NOT_ALLIGN	12	//!< motors not allignet to be guieded syncronously
#define MOT_CRUSHING_ERROR		13	//!< Anti crushing sensor pressed !! Safety problem!!! 

//#define Output_driver									0x00000004
//#define Position_Limit								0x00000008
//#define Low_bus_voltage								0x00000010
//#define Over_voltage									0x00000020
//#define Temperature_90								0x00000040
//#define Internal										0x00000080	//(Self diagnostics failed)
//#define Absolute_multiturn_encoder_lost_position		0x00000100
//#define Absolute_multiturn_encoder_sensor_counting	0x00000200
//#define No_comm_to_absolute_multiturn_encoder			0x00000400
//#define SSI_encoder_counting							0x00000800
//#define Closed_loop									0x00001000
//#define External_memory								0x00002000
//#define Absolute_single_turn_encoder					0x00004000
//#define Safe_Torque_Off								0x08000000	// (STO)
///@}

/** @name high level 
 *  motor moving status
 */
///@{
	/*
#define	MOT_MOTION_COMPLETED	0
#define MOT_MOVING				1
#define MOT_STOPPING			2
#define MOT_DELAY_DISABLE		3
#define MOT_DELAY_MOVING		4
MOT_FAULT_STATE
*/
	enum {
		MOT_MOTION_COMPLETED = 0,
		MOT_MOVING,
		MOT_STOPPING,
		MOT_DELAY_DISABLE,
		MOT_DELAY_MOVING,
		MOT_FAULT_STATE,
	};
///@}

/** @name default node to function binding
 */
///@{
#define MOT_GANTRY	1
#define MOT_BED		2
#define MOT_PANEL	3
///@}

#define AGNOSTIC_AXIS		//Make agnostic choice on axis for Syncro node

/** @name default axis to function binding
 */
///@{
#define AXIS_GANTRY			0	// Motore per Gantry
#define AXIS_BED			1	// Motore per lettino
#define AXIS_ACT1			1	// motore alternativo per lettino - LinAct
#ifndef AGNOSTIC_AXIS
	#define AXIS_SLIDER1		1	// Slider contemporaneo se gestito MotorPower: Motore 1 // Alternativamente Lettino Stille
#else
	#define AXIS_SLIDER1		(motor_sync1.axis)
#endif
#define NODE_SLIDER1		8	// Slider contemporaneo se gestito MotorPower: Motore 1
#define AXIS_PANEL			2	// Motore per pannello
#define AXIS_BED_LIFT		3
#define AXIS_TILT			3	// motore per tilt - Motorpower
#define AXIS_ACT2			3	// motore alternativo per tilt - LinAct
#define AXIS_BACKLIFT		4	// lifter posteriore pegaso - Motorpower
#define AXIS_FRONTLIFT_DX	5	// lifter anteriore destro pegaso - Motorpower
#define AXIS_FRONTLIFT_SX	6	// lifter anteriore sinistro pegaso - Motorpower
//	#define AXIS_LETTINO1		7	// Lettino Stille
#ifndef AGNOSTIC_AXIS
	#define AXIS_SLIDER2		8	// Slider contemporaneo se gestito MotorPower: Motore 2 // Alternativamente Lettino Stille
#else	
	#define AXIS_SLIDER2		(motor_sync2.axis)
#endif
#define NODE_SLIDER2		9////1////9	// Slider contemporaneo se gestito MotorPower: Motore 1
//	#define AXIS_LETTINO2		8	// Lettino Stille
//	#define AXIS_LETTINO3		9	// Lettino Stille
//	#define AXIS_LETTINO4		10	// Lettino Stille
///@}

///** @name default position limits
 //*/
/////@{
//#define GANTRY_MIN_POS_LIMIT	0
//#define GANTRY_HOME_POS			21430
//#define GANTRY_MAX_POS_LIMIT	3900260
//#define BED_MIN_POS_LIMIT		0
//#define BED_HOME_POS			163840
//#define BED_MAX_POS_LIMIT		3112960
//#define FLATP_MIN_POS_LIMIT		436906
//#define FLATP_HOME_POS			1075882
//#define FLATP_MAX_POS_LIMIT		1780394
/////@}

#define MOTOR_HEARTB_INTERVAL	1000

/**
 * \struct motor_in_mesg_t
 * \brief Structure to hold CANopen received message data.
 *
 */
typedef struct motor_in_mesg_str {
	int f_code;			//!< Type of message
	int idx;			//!< Mailbox index (unused)
	uint32_t datal;		//!< Low 4 byte of data
	uint32_t datah;		//!< High 4 bytes of data
	uint32_t timestamp;		//!< High 4 bytes of data	
	}motor_in_mesg_t;

#define MOTOR_MSG_QUEUE_SIZE	40//20	//!< Dimension of the received message queue foe each motor

#define MOTOR_MAXOBJ	11	//!< Max number of motor nodes

/**
 * \struct motor_obj_t
 * \brief Structure of motor object.
 *  Holds data and parameters for motor management.
 *
 */
typedef struct motor_obj_str {
	uint8_t node_id;								//!< CANopen node ID of the motor
	volatile uint32_t statusword;					//!< Status word of the motor (received by TPDO)
	volatile uint32_t controlword;					//!< Control word for the motor (transmitted by RPDO)
	int home_speed;									//!< Speed for homing in step/s
	int home_position;								//!< Position were motor go when the command MotorHome is called
	int pos_speed;									//!< Speed for normal positionimg in step/s
	int pos_acc;									//!< Acceleration and deceleration in step/s^2
	int pos_dec;									//!< Acceleration and deceleration in step/s^2
	int jog_speed;									//!< Speed for motor jogging in forward or reverse direction
	int motion_stat;								//!< Motion status (complete/moving)
	int motor_stat;									//!< MKotor status (ready, homed, error)
	int mac_state;									//!< not used
	int32_t actual_position;							//!< Motor actual position. Periodically upgraded by TPDO
	int32_t requested_position;
	int min_position_limit;
	int max_position_limit;
	int actual_current;								//!< Motor actual power consumption in rated-current/1000
	uint32_t rated_current;							//!< Rated current of the motor in milli Amperes
	int limit_current_threshold;					//!< Max current limit for permitted the motor
	uint32_t max_current_timer;	
	uint32_t limit_current_time_ms;					//!< Time window in ms for the current over the max permitted value
#ifdef	OLD_STYLE_COPROCESS
	motor_in_mesg_t rx_queue[MOTOR_MSG_QUEUE_SIZE];	//!< Received message queue (circular buffer of motor_in_mesg_t structs)
	int rx_queue_write_p;							//!< Write position in message queue
	int rx_queue_read_p;							//!< Read position in message queue
#endif
	uint32_t heartbeat_timer;		
//	uint32_t flag_heartbeat_timer;
	uint32_t heardbeat_tout;		
	int32_t actual_speed;
	uint32_t motor_model;		
	uint32_t mis_err_bits;
	uint32_t mis_warn_bits;
	uint32_t initialized;
	int32_t pulse_counter;
	uint32_t emergency_data_l;
	uint32_t emergency_data_h;
	uint8_t emergency_flag;
	uint32_t disable_timer;
}motor_obj_t;

void motors_InitData(void);
uint32_t motor_Init(int axis, uint8_t node);
uint32_t motor_SetPosition(int axis, int32_t new_position);
#ifdef OLD_STYLE_COPROCESS
	void motor_Machine(void);
#endif
uint32_t motor_SetMaxPosition(int axis, int32_t max_pos);
uint32_t motor_SetMinPosition(int axis, int32_t min_pos);
uint32_t motor_SetSpeed(int axis, uint32_t speed);
uint32_t motor_SetAcc(int axis, uint32_t acc);
uint32_t motor_Home(int axis);
uint32_t motor_FaultReset(int axis);
uint32_t motor_SetHome(int axis);
uint32_t motor_GetMotionStatus(int axis);
uint32_t motor_GetMotorStatus(int axis);
uint32_t motor_SetJogSpeed(int axis, uint32_t j_speed);
uint32_t motor_GetMotorActPos(int axis, int32_t* pos);
uint32_t motor_GoJogFwd(int axis);
uint32_t motor_GoJogRev(int axis);
uint32_t motor_SetModeOperation(int axis, uint32_t mode);
uint32_t motor_SetDec(int axis, uint32_t dec);
uint32_t motor_SetCurrentLimit(int axis, int max_current);
uint32_t motor_SetCurrentLimitTime(int axis, uint32_t limit_time);
uint32_t motor_GetMotorActualCurrent(int axis);
uint32_t motor_QuickStop(int axis);
void motorStopNoPower (void);
uint32_t motor_SetHomeSpeed(int axis, uint32_t h_speed);
uint32_t motor_StopHere(int axis);

void motor_JogFunction(void);
void motor_ManageEmergency(void);
void motor_EnableServiceState(bool enabled);
void motor_ResetSync(void);
uint32_t motor_SetZero(int axis);
//uint32_t motor_ResetError(int axis);
uint32_t motor_GetErrorsBits(uint32_t axis, uint32_t* errors);
uint32_t motor_GetWarnsBits(uint32_t axis, uint32_t* warnings);
// lettura/scrittura registro generico del motore
uint32_t motor_ReadObject(uint32_t axis, uint32_t index, uint32_t subindex, uint32_t *value);
uint32_t motor_WriteObject(uint32_t axis, uint32_t index, uint32_t subindex, uint32_t value);
uint32_t motor_ResetPosition(int axis, uint32_t new_position);
uint32_t motor_GetMotorRatedCurrent(int axis);
void reset_Motor_HeartBeat(void);

//	#define TEST_ONLY_SYNC_MOTOR
#ifdef	TEST_ONLY_SYNC_MOTOR
void test_only_sync();
#endif
#endif /* MOTORS_H_ */
