/*
 * IPC_m4.h
 *
 *  Created on: Feb 14, 2022
 *      Author: stefano
 */

#ifndef INC_IPC_M4_H_
#define INC_IPC_M4_H_

#include "IPC_common.h"
#include "buffer.h"

void IPC_init(void);
IPC_msg_t blocking_receive_message(void);
int notblocking_receive_message(IPC_msg_t *);
int IPC_SendUpdate();
int IPC_SendDebug(cBuffer * buff);
void IPC_Manage();
void comm_IPC_Init();
int IPC_SendReply(IPC_msg_t *message);


typedef struct{
	int rx_queue_write_p;								//!< Write position in message queue
	int rx_queue_read_p;								//!< Read position in message queue
	IPC_msg_t received_data[MAX_IPCC_QUEUE_SIZE];
}_queue_IPC_msg_t;

#endif /* INC_IPC_M4_H_ */
