/*! \file co_microdep.h \brief Microcontroller dependent function */
//*****************************************************************************
//
// File Name	: 'co_microdep.h'
// Title		: Microcontroller dependent function for can periferal management
// Author		: Alessandro Qprel s.r.l.
// Created		: 17/09/2014
// Revised		: -
// Version		: 1.0
//
//*****************************************************************************
#ifndef CO_MICRODEP_H_
#define CO_MICRODEP_H_

#include "main.h"
#include "timer.h"
#include "co_main.h"

//! Filter list labal defines
#define	FILTERID_ARRAY_SIZE		128		//!< Size of global filters to be used on a single CAN module
										//!< (so even for SDO and RTR service)
#define FILTER_FREE				-1		//!< Filter free not used, not ssociated
#define FILTER_INIT				1		//!< Filter associated


//!< 	Symbol used to store each message on a queue for each co item
//!<	As in the prevoius concept of FW
//!<	If not defined, new FDCAN FIFO facility is used
//#define OLD_STYLE_COPROCESS


enum {
	SDO_FDCAN_FILTER=0,
	RTR_FDCAN_FILTER,
	FIRST_FREE_FDCAN_FILTER,
};

/**
 * \struct intr_cback_t
 * \brief Structure for CAN interrupt callback functions.
 *
 */
typedef struct {
	int node_id;		/*Node Id*/
	int filter_id[8];	/* List of can filter used/initialized on this motor */
	void (*intr_cback)(void *param, uint8_t *data);	/* Call Back function to manage can message */
}intr_cback_t;

extern uint32_t error_send_count;
typedef struct{
	uint32_t start;
	uint32_t stop;
}int_time_t;
extern int_time_t int_time;

//!< Add callback function to manage message from node device.
int micro_AddCback(intr_cback_t * cb_str);
//!< Initialize CAN peripheral.
uint32_t micro_InitCan(FDCAN_HandleTypeDef *);
//!< Send an SDO message to node and return the mailbox index used to receive 
FDCAN_FilterTypeDef micro_SendSdo(co_msg_t * co_msg);

//!< Send a PDO message to node. **USED ONLY ON duet_StateCommand TO AVOID CONFLICT ON SYNCRO MOTORS**
//void micro_SendPdo_srv(volatile co_msg_t * co_msg);
//!< Send a PDO message FOR SYNCROMODE message to node.
void micro_SendPdo(volatile co_msg_t * co_msg);
//!< Send a PDO FOR SYNCROMODE message to node.
void micro_SendPdo1(volatile co_msg_t * co_msg);
//!< Send an NMT message to node.
void micro_SendNmt(co_msg_t * co_msg);
//!< Retrieve data received from the can mailbox.
void micro_GetRcvData(co_msg_t * co_msg);
//!< Reset calback structure
void reset_co_icb(intr_cback_t*);
//!< Return first CAN filter to be used
int get_freefilterId();
//!< Set CAN filter as used
int set_initfilterId(int filterid, FDCAN_FilterTypeDef *sFilterConfig);
//!< Reset CAN filter as used
int reset_initfilterId(int filterid);
//!< Check the specified mailbox for incoming message.
uint32_t micro_GetRcvReady();
//!< Send a SYNC message on the CAN bus.
void micro_SendSync(void);
//!< Send a TIME STAMP message on the CAN bus.
void micro_SendTimeStamp(void);
//!< Interface to get the CAN peripheral used for communication.
FDCAN_HandleTypeDef * micro_GetCanInUse(void);
//!< Send an HEARTBEAT message on the CAN bus.
void micro_SendHbeat(void);
//!< Manage All message in FIFO0 of CAN0 to process CO
void FDCAN0_AllMessageHandler();

FDCAN_FilterTypeDef micro_SendRtr(co_msg_t * co_msg);
#endif /* CO_MICRODEP_H_ */
