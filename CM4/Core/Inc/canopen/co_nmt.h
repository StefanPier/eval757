/*! \file co_nmt.h \brief CanOpen network management module */
//*****************************************************************************
//
// File Name	: 'co_sdo.h'
// Title		: Module containing network management function.
// Author		: Alessandro
// Company		: Qprel s.r.l.
// Created		: 17/09/2014
// Revised		: -
// Version		: 1.0
//
//*****************************************************************************

#ifndef CO_NMT_H_
#define CO_NMT_H_

#include "main.h"
/** @name nmt commands 
 *  network management command definitions
 */
///@{
#define NMT_START				1		//!< start remote node (go operational)
#define NMT_STOP				2		//!< stop remote node (go prepared)
#define NMT_PRE_OPERATIONAL		128		//!< enter pre-operational state
#define NMT_RESET				129		//!< reset node (full software reset)
#define NMT_RESET_COMM			130		//!< reset communication
///@}

//!< Sends an NMT message to node server
void co_nmt_Send(uint8_t node, uint8_t cmd);
//!<  Decode network management messages coming from nodes.
void co_nmt_Decode(uint8_t * data);

#endif /* CO_NMT_H_ */
