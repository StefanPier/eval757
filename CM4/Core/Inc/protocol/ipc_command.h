/**
\file command.h
\brief Header libreria per la gestione di comandi, con associazione a funzione di esecuzione
\author Lorenzo Giardina <lorenzo@lorenzogiardina.com>
\version 1.0
\date Last revision: 1/1/2016
\note
This code is distributed under the GNU Public License which can be found at http://www.gnu.org/licenses/gpl.txt
*/

#ifndef IPC_COMMAND_H
#define IPC_COMMAND_H

#include "buffer.h"
#include "protocol.h"
#include "IPC_common.h"

//! Fifo for ipc command
//! \param n_cmd_present Active command request from ipc
//! \param comando List of cmd_msg_t command
typedef struct
{
	unsigned char n_cmd_present;
	cmd_msg_t comando[N_MAX_BUFFER_COMANDI_RX];
}fifo_ipc_command_t;

int ipc_process_packet (cmd_msg_t *cmd_ipc);
void exe_ipc_cmd (void);
void ipc_command_system_init ();

#endif
