/*
 * comm_lifter.h
 *
 * Created: 10/08/2020 15:00:01
 *  Author: user
 */ 


#ifndef COMM_LIFTER_H_
#define COMM_LIFTER_H_

#include "buffer.h"

#define STILLE_BED_UART	huart3
extern UART_HandleTypeDef STILLE_BED_UART;

#define  STILLE_BUFFER_LEN 200

void comm_StilleInit(uint32_t brate);
void comm_stille_write_byte (unsigned char *dato, int size);
cBuffer* comm_stille_get_buffercmd(void);
void Stille_Handler(void);

#endif /* COMM_LIFTER_H_ */
