/*
 * CM4_State.h
 *
 *  Created on: 25 feb 2022
 *      Author: stefano
 */

#ifndef INC_CM4_STATE_H_
#define INC_CM4_STATE_H_

#include "steute.h"
#include "IPC_common.h"

extern update_cm4_state_t cm4_state;

void CM4_InitState();
void CM4_UpdateSteuteState(steute_state_t *psteute_state);

#endif /* INC_CM4_STATE_H_ */
