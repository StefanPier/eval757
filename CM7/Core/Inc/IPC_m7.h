/*
 * IPC.h
 *
 *  Created on: Feb 10, 2022
 *      Author: stefano
 */

#ifndef INC_IPC_M7_H_
#define INC_IPC_M7_H_
#include <settings.h>
#include "openamp.h"
#include "IPC_common.h"

//Storage of a queue
typedef struct{
	QueueHandle_t QueueHandle;
	uint8_t QueueBuffer[ MAX_IPCC_QUEUE_SIZE * sizeof( IPC_msg_t ) ];
	StaticQueue_t QueueControlBlock;
}ipc_static_buffer_t;

struct rpmsg_endpoint * IPC_init(void);
void MakeIPCQueue(void);
void IPC_RX_Task(void const *argument);
void IPC_TX_Task(void const *argument);
void process_IPC(IPC_msg_t *message);
void new_service_cb(struct rpmsg_device *rdev, const char *name, uint32_t dest);
void service_destroy_cb(struct rpmsg_endpoint *ept);

extern ipc_static_buffer_t ipc_queue;

#endif /* INC_IPC_M7_H_ */
