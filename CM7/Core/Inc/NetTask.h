/*
 * NetTask.h
 *
 *  Created on: Apr 27, 2021
 *      Author: stefano
 */

#ifndef INC_NETTASK_H_
#define INC_NETTASK_H_
#include <settings.h>
#include "cmsis_os.h"
#include "buffer.h"

//Definisco:
//	WITH_NETCONN==1 se lwip gestito con netconn class
//	WITH_NETCONN==0 se invece lo gestisco a più alto livello
#define 	WITH_NETCONN	1

//Se definito uso la pipe in Tx da gestire con un altro task per inviare su ethernet i messaggi in uscita dal protocollo
#define 	PROTOCOL_TX_ON_PIPE

//Maximum waiting time to send tcp event in internal queue (ms)
#define WAITING_SEND_TCPEVENT_TIME	100U
#define MAX_SOCKET_CONNECTION		5
#define MAX_BYTE_FOR_SOCKET			128

typedef struct{
#if WITH_NETCONN
	//Socket id
	struct netconn * conn;
#else
	//Socket id
	int conn;
#endif
	//Index on socket pool
	int idx;
}accepted_socket_t;


//Redundant Rx Storage For Qprel protocol usage
typedef struct{
	cBuffer circRxBuffer;
	unsigned char dataRx[MAX_BYTE_FOR_SOCKET];
}protocol_socket_Rxbuff_t;

//Storage of a queue
typedef struct{
	uint8_t QueueBuffer[ MAX_BYTE_FOR_SOCKET * sizeof( uint8_t ) ];
	StaticQueue_t QueueControlBlock;
}socket_static_buffer_t;

typedef struct{
	int occuped;
	accepted_socket_t associated_socket;
}socket_state_t;

typedef struct{
	//General State Socket
	int available_socket;
	//Socket statet: busy/not busy state e di netcon/conn handle
	socket_state_t socket_state[MAX_SOCKET_CONNECTION];
	//Rx Section:
	//Message queue for each socket of MAX_BYTE_FOR_SOCKET bytes
	QueueHandle_t NetRxQueueHandle_Pool[MAX_SOCKET_CONNECTION];
	//Parameter and allocate space for the socket queue
	socket_static_buffer_t NetRxQueueBuffer_Pool[MAX_SOCKET_CONNECTION];
	//Buffer to allow protocol control (cpied from NetRxQueueBuffer_Pool-queue section)
	protocol_socket_Rxbuff_t protocol_socket_Rxbuffer[MAX_SOCKET_CONNECTION];
#ifdef PROTOCOL_TX_ON_PIPE
	//Tx Section
	QueueHandle_t NetTxQueueHandle_Pool[MAX_SOCKET_CONNECTION];
	socket_static_buffer_t NetTxQueueBuffer_Pool[MAX_SOCKET_CONNECTION];
	SemaphoreHandle_t NetTxQueueSem[MAX_SOCKET_CONNECTION];
#endif
}state_socket_pool_t;

#ifdef PROTOCOL_TX_ON_PIPE
	void write_onsocket (unsigned char dato, int trunk_idx);
#endif
void ManageNetTask(void const *argument);
void MakeSocketQueues(void);
int getAvailableSocket(void);

#endif /* INC_NETTASK_H_ */
