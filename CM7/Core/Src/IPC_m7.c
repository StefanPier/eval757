/*
 * IPC.c
 *
 *  Created on: Feb 10, 2022
 *      Author: stefano
 */


#include <manage_debug.h>
#include "main.h"
#include "openamp.h"
#include "FreeRTOS.h"
#include "cmsis_os.h"
#include "IPC_m7.h"
#include "binaryprotocol.h"
#include "NetTask.h"

/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
#define RPMSG_CHAN_NAME              "openamp_pingpong_demo"

/* Private variables ---------------------------------------------------------*/
static volatile IPC_msg_t received_data;
static struct rpmsg_endpoint rp_endpoint;

static osThreadId IPC_TX_TaskHandle;
ipc_static_buffer_t ipc_queue;


/* Extern variables ---------------------------------------------------------*/

/**
  * @brief 	Callback called by IPC openamp rmsg, for each messages present in queue
  *
  * @param  struct rpmsg_endpoint *ept: structure of the rmsg endpoint
  * @param  void *data pointer to a IPC_msg_t structure
  * @param  size_t len length of data in byte
  * @param  uint32_t src: local rpmsg address
  * @param  void *priv:	private pointer for sriver use
  * @return RPMSG_SUCCESS
  */
static int rpmsg_recv_callback(struct rpmsg_endpoint *ept, void *data,
                size_t len, uint32_t src, void *priv)
{
  received_data = *((IPC_msg_t *) data);
  process_IPC(&received_data);
//	osSemaphoreRelease (osSemaphore_MessageReception);
	return RPMSG_SUCCESS;
}

/**
  * @brief 	Callback called by IPC rmsg, for endpoint destruction
  *
  * @param  struct rpmsg_endpoint *ept: structure of the rmsg endpoint to be destroyed
  * @return None
  */
 void service_destroy_cb(struct rpmsg_endpoint *ept)
{
  /* this function is called while remote endpoint as been destroyed, the
   * service is no more available
   */
}

 /**
   * @brief 	Callback called by IPC openamp on new service added
   * 			Adding callback for each message on channel
   *
   * @param  struct rpmsg_endpoint *ept: structure of the remote endpoint
   * @param  const char *name: service name
   * @param  uint32_t dest: remote rpmsg address
   * @return None
   */
void new_service_cb(struct rpmsg_device *rdev, const char *name, uint32_t dest)
{
  /* create a endpoint for rmpsg communication */
  OPENAMP_create_endpoint(&rp_endpoint, name, dest, rpmsg_recv_callback,
                          service_destroy_cb);
}


/**
  * @brief 	Make IPC queue ti IPC Task
  *
  * @param  None
  * @return None
  */

void MakeIPCQueue(void){
    /* Create a queue capable of containing 10 uint64_t values. */
	ipc_queue.QueueHandle = xQueueCreateStatic( MAX_IPCC_QUEUE_SIZE ,
													sizeof( IPC_msg_t ),
													ipc_queue.QueueBuffer,
													&ipc_queue.QueueControlBlock );
	if(ipc_queue.QueueHandle==NULL){
		Error_Handler();
	}
}

/**
  * @brief 	Openamp IPC Initialization
  * 		Used in IPC_RX_Task
  * @param  None
  * @return struct rpmsg_endpoint *ept: structure of the remote endpoint
  * 		of the new IPC Service
  */
struct rpmsg_endpoint * IPC_Init(void){
      UBaseType_t uxHighWaterMark;

      int32_t status;
      int32_t timeout;

	  /* Define used semaphore */
	  osSemaphoreDef(CHN_CREAT);
	  osSemaphoreDef(MSG_RECPT);
	  /* Create the semaphore */
//	  osSemaphore_ChannelCreation  = osSemaphoreCreate(osSemaphore(CHN_CREAT) , 1);
//	  osSemaphore_MessageReception = osSemaphoreCreate(osSemaphore(MSG_RECPT) , 1);


      /* After starting the FreeRTOS kernel, Cortex-M7 will release Cortex-M4  by means of
	     HSEM notification */

      HAL_NVIC_ClearPendingIRQ(HSEM1_IRQn);
	  __HAL_HSEM_CLEAR_FLAG(__HAL_HSEM_SEMID_TO_MASK(HSEM_ID_1));

	  /*HW semaphore Clock enable*/
	  __HAL_RCC_HSEM_CLK_ENABLE();

	  /*Take HSEM */
	  HAL_HSEM_FastTake(HSEM_ID_0);
	  /*Release HSEM in order to notify the CPU2(CM4)*/
	  HAL_HSEM_Release(HSEM_ID_0,0);

	  /* wait until CPU2 wakes up from stop mode */
	  timeout = 0xFFFF;
	  while((__HAL_RCC_GET_FLAG(RCC_FLAG_D2CKRDY) == RESET) && (timeout-- > 0));
	  if ( timeout < 0 )
	  {
	    Error_Handler();
	  }

	  uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
	  /* Initialize the mailbox use notify the other core on new message */
	  MAILBOX_Init();

	  /* Initialize the rpmsg endpoint to set default addresses to RPMSG_ADDR_ANY */
	  rpmsg_init_ept(&rp_endpoint, RPMSG_CHAN_NAME, RPMSG_ADDR_ANY, RPMSG_ADDR_ANY,
	                 NULL, NULL);

	  uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );

	  /* Initialize OpenAmp and libmetal libraries SLAVE wait to have correct shared memory*/
	  if (MX_OPENAMP_Init(RPMSG_MASTER, new_service_cb)!= HAL_OK)
		  Error_Handler();

	  uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );

	  //Here syncro withslave to simulate a message and enabling endpoint creation
	  //MAILBOX_Notify(NULL,0);

	  /*
	   * The rpmsg service is initiate by the remote processor, on A7 new_service_cb
	   * callback is received on service creation. Wait for the callback
	   */
	  OPENAMP_Wait_EndPointready(&rp_endpoint);

	  /* Send the massage to the remote CPU */
	  int message = 22;
	  status = OPENAMP_send(&rp_endpoint, &message, sizeof(message));
	  osDelay(1);

	  if (status < 0)
	  {
	    Error_Handler();
	  }
	  debug_printf(DEBUG_IPC,"IPC Tx %d\n\r",message);

	  return &rp_endpoint;
}

/**
  * @brief 	IPC to manage message on IPC channel:
  * 		Depending of the message:
  * 			-	give reply on Protocol queue (IPC_CMD_CHANNE, IPC_CMD_REPLY)
  * 			-   give debug information (IPC_DEBUG_CHANNEL)
  * 			-	Updatte local state on CM7 (IPC_UPDATE_CHANNEL)
  * @param  IPC_msg_t *message: pointer of a message data on IPC channel
  * @return None
  */
void process_IPC(IPC_msg_t *message){
	HAL_GPIO_TogglePin(LD1_GPIO_Port, LD1_Pin);
	//Choos IPC direction, anmd send to the corrisponding task
	if(message->cmd_msg.channel==IPC_UPDATE_CHANNEL){
		debug_printf(DEBUG_IPC,"IPC Rx %d\n\r",message->update_cm4_state.adc_value[0]);
	}else if(message->cm4_msg.channel==IPC_DEBUG_CHANNEL){
		debug_printf(DEBUG_CM4,message->cm4_msg.msg_data);
	}else {
		//For Good Reply
		if(message->cmd_msg.channel==IPC_CMD_CHANNEL &&
				message->cmd_msg.cmd_type==IPC_CMD_REPLY &&
					message->cmd_msg.cmd_id<LAST_BIN_CMD){
			//If on socket protocol reply, send it over ethernet
			if(message->cmd_msg.id>=0 && message->cmd_msg.id<MAX_SOCKET_CONNECTION){
				//Send Reply
				command_send(message->cmd_msg.id, 1, message->cmd_msg.cmd_id, strlen((char *)message->cmd_msg.cmd_data)+1, message->cmd_msg.cmd_data);
			}
		}else{
			Error_Handler();
		}
	}

}

/**
  * @brief 	Task to receive message on IPC Opanp channel
  * @param  void const *argument: argument of new thread (not used)
  * @return None
  */
void IPC_RX_Task(void const *argument)
{
	UBaseType_t uxHighWaterMark;
	/* Inspect our own high water mark on entering the task. */
	uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
	struct rpmsg_endpoint *rp_endpoint=IPC_Init();
	osThreadDef(IPC_TX_Task, IPC_TX_Task, osPriorityNormal, 0, 512);
	IPC_TX_TaskHandle = osThreadCreate(osThread(IPC_TX_Task), rp_endpoint);
	if(IPC_TX_TaskHandle==NULL)
		Reset_Handler();
	int status=1;
	/* Infinite loop */
	for(;;)
	{

	   /* Receive the massage from the remote CPU */
		OPENAMP_check_for_message();
		uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
		osDelay(100);
	}
}

/**
  * @brief 	Task to receive message on IPC Opanp channel
  * @param  void const *argument: argument of new thread (not used)
  * @return None
  */
void IPC_TX_Task(void const *argument)
{
	IPC_msg_t events;
    UBaseType_t uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
    debug_printf(DEBUG_IPC,"IPC_TX_Task\n");
    uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
    while(1) {
    	uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
    	memset((void *)&events,0,sizeof(events));
        BaseType_t res = xQueueReceive(ipc_queue.QueueHandle, &events, portMAX_DELAY); // Wait here an event on netconn

        if ( (res == pdPASS) &&
        		( (events.cmd_msg.channel==IPC_CMD_CHANNEL &&
        		events.cmd_msg.cmd_type==IPC_CMD_REQUEST &&
					events.cmd_msg.cmd_id<LAST_BIN_CMD) ||
					(events.steute_msg.channel==IPC_STEUTE_CHANNEL)) )// If netconn is a server and receive incoming event on it
        {
        	//Send request over IPC
        	int32_t status = OPENAMP_send(&rp_endpoint, &events, sizeof(events));
//        	int32_t status=1;
        	if(status<RPMSG_SUCCESS)
        		debug_printf(DEBUG_IPC,"IPC Tx Error %d\n\r",status);
        }else{
        	res++;
        }

		uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
        osDelay(1);
	}
}


/**
  * @brief 	Callback on stack overflow in FreeRTOS
  * @param  xTaskHandle xTask: handle of the task on which stackoverflow occurs
  * @param  signed char *pcTaskName: task name on which stackoverflow occurs
  * @return None
  */
void vApplicationStackOverflowHook( xTaskHandle xTask, signed char *pcTaskName ){
	while(1);
}
