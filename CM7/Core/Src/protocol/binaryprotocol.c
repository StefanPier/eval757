/*
 * binaryprotocol.c
 *
 * Created: 27/02/2015 11:01:14
 *  Author: Alessandro
 */ 

#include <manage_debug.h>
#include <string.h>
#include "IPC_m7.h"
#include "binaryprotocol.h"
#include "main.h"


static char tmpstr[N_MAX_DATA_LENGHT];

/**
  * @brief 	Retrieve separated parameter from a string of IP addr,
  *			flushing out the retrieved one
  * @param  unsigned char * src: String of separated params
  * @param	char separator: char separator between parameters
  * @param  unsigned char* dataptrvoid:	Retrieved parameter
  * @return >=1 = success, 0 = fail
  */
static int list_GetNextParam(unsigned char * src, char separator, char * param)
{
	char ctmp;
	uint32_t i = 0;
	int len_src=strlen((const char *)src);
	
	while(i < len_src)
	{
		ctmp = src[i];
		if((ctmp == separator)||(ctmp == 0))
		break;
		param[i] = ctmp;
		i++;
	}
	
	param[i] = 0;
	strcpy((char *)src, (char *)(src+i+1)); // cut out the extracted param
	return i;
}

/**
  * @brief 	Retrieve comma separated parameter from a string of IP addr,
  *			flushing out the retrieved one
  * @param  unsigned char * src: String of comma separated params
  * @param  unsigned char* dataptrvoid:	Retrieved parameter
  * @return >=1 = success, 0 = fail
  */
static int list_command_GetNextParam(unsigned char * src, char * param)
{
	return list_GetNextParam(src, ',' ,param);
}

/**
  * @brief 	Retrieve parameter from a string of IP addr,
  *			flushing out the retrieved one
  * @param  unsigned char * src: String of point separated ip params (es. 169.254.1.77)
  * @param  unsigned char* dataptrvoid:	Retrieved parameter
  * @return ( >=1 = success, 0 = fail)
  */
static int list_GetNextIpParam(unsigned char * src, char * param)
{
	return list_GetNextParam(src, '.' ,param);
}

/**
  * @brief 	Command to communicate FW version
  * @param  unsigned char trunk: Communication bus number
  * @param  unsigned char* dataptrvoid:	Unused Parameter
  * @return None
  */
static void verfwcmd (unsigned char trunk, unsigned char* dataptrvoid)
{
	
	strcpy(tmpstr, "1.55");
	
	debug_printf(DEBUG_BYNARY_PROTOCOL,"CMD FW version: %s\r\n",tmpstr);
	command_send(trunk, 1, FWVERSION_CMD, strlen(tmpstr)+1, (unsigned char *)tmpstr);

}


/**
  * @brief 	Command to operate motor home on a axis
  * @param  unsigned char trunk: Communication bus number
  * @param  unsigned char* dataptrvoid
  * 		String of comma separated params:
  * 			- axis
  * @return None
  */static void motor_home (unsigned char trunk, unsigned char* dataptrvoid)
{
	int err=0;
	BaseType_t reply_send;
	// Get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr))
	{
		strcpy(tmpstr, "NAK");
		debug_printf(DEBUG_BYNARY_PROTOCOL,"M_Home: fail to get axis number\r\n");
		err = ERROR;
	}
	else
	{
		uint32_t axis = (uint32_t)atoi(tmpstr);
		//Header command on IPC
		IPC_msg_t command;
		command.cmd_msg.id = trunk;
		command.cmd_msg.channel = IPC_CMD_CHANNEL;
		command.cmd_msg.cmd_id = MOTOR_HOME_CMD;
		command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
		//Populate command
		generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
		cmd->num_params=1;
		cmd->parameters[0]=axis;
		//Tx Event on IPC command queue
		reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
		if(reply_send!=pdPASS)
			err = ERROR;
	}

	if(err != SUCCESS)
		command_send(trunk, 1, MOTOR_HOME_CMD, strlen(tmpstr)+1, (unsigned char *)tmpstr);
}

/**
* @brief				Callback function to SetHomeSpeed
*
* @param trunk			Communication bus number
* @param  unsigned char* dataptrvoid
* 		String of comma separated params:
* 			- axis
* 			- speed param
* @return None
*/
static void M_SetHomeSpeed(unsigned char trunk, unsigned char* dataptrvoid)
{
	int err=0;
	uint32_t axis = 0;
	uint32_t speed_param = 0;
	BaseType_t reply_send;

	// get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr))
	{
		strcpy(tmpstr, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "M_SetHomeSpeed: fail to get axis number !\r\n");
		err = ERROR;
	}
	else
	{
		axis = (uint32_t)atoi(tmpstr);
		// get speed
		if(!list_command_GetNextParam(dataptrvoid, tmpstr))
		{
			strcpy(tmpstr, "NAK");
			DbgPrintf(DBG_BINARY_PROTO, "M_SetHomeSpeed: fail to get speed value !\r\n");
			err = ERROR;
		}
		else
		{
			speed_param = (uint32_t)atoi(tmpstr);

			//Header command on IPC
			IPC_msg_t command;
			command.cmd_msg.id = trunk;
			command.cmd_msg.channel = IPC_CMD_CHANNEL;
			command.cmd_msg.cmd_id = MOTOR_HOME_CMD;
			command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
			//Populate command
			generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
			cmd->num_params=2;
			cmd->parameters[0]=axis;
			cmd->parameters[1]=speed_param;
			//Tx Event on IPC command queue
			reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
			if(reply_send!=pdPASS)
				err = ERROR;
		}
	}
	if(err != SUCCESS)
		command_send(trunk, 1, SET_HOME_SPEED_CMD, strlen(tmpstr)+1, (unsigned char *)tmpstr);
}

/**
* @brief				Callback function to set position
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis and position params in a string
* 		String of comma separated params:
* 			- axis
* 			- speed param
*
* @return None
*/
static void M_SetPosition(unsigned char trunk, unsigned char* dataptrvoid)
{
	int err=0;
	uint32_t axis = 0;
	int32_t pos_param = 0;
	BaseType_t reply_send;

	// get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr))
	{
		strcpy(tmpstr, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "M_SetPosition: fail to get axis number\r\n");
		err = ERROR;
	}
	else
	{
		axis = (uint32_t)atoi(tmpstr);
		// get position
		if(!list_command_GetNextParam(dataptrvoid, tmpstr))
		{
			strcpy(tmpstr, "NAK");
			DbgPrintf(DBG_BINARY_PROTO, "M_SetPosition: fail to get position value\r\n");
			err = ERROR;
		}
		else
		{

			 pos_param = (int32_t)atoi(tmpstr);

			//Header command on IPC
			IPC_msg_t command;
			command.cmd_msg.id = trunk;
			command.cmd_msg.channel = IPC_CMD_CHANNEL;
			command.cmd_msg.cmd_id = SET_POSITION_CMD;
			command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
			//Populate command
			generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
			cmd->num_params=2;
			cmd->parameters[0]=axis;
			cmd->parameters[1]=(uint32_t)pos_param;
			//Tx Event on IPC command queue
			reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
			if(reply_send!=pdPASS)
				err = ERROR;
		}
	}
	if(err != SUCCESS)
		command_send(trunk, 1, SET_POSITION_CMD, strlen(tmpstr)+1, (unsigned char *)tmpstr);
}

/**
* @brief				Callback function to set speed
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis and speed params in a string
* 		String of comma separated params:
* 			- axis
* 			- speed param
*
* @return None
*/
static void M_SetSpeed(unsigned char trunk, unsigned char* dataptrvoid)
{
	int err=0;
	uint32_t axis = 0;
	int32_t speed_param = 0;
	BaseType_t reply_send;

	// get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr))
	{
		strcpy(tmpstr, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "M_SetSpeed: fail to get speed value !\r\n");
		err = ERROR;
	}
	else
	{
		axis = (uint32_t)atoi(tmpstr);
		// get position
		if(!list_command_GetNextParam(dataptrvoid, tmpstr))
		{
			strcpy(tmpstr, "NAK");
			DbgPrintf(DBG_BINARY_PROTO, "M_SetSpeed: fail to get speed value !\r\n");
			err = ERROR;
		}
		else
		{

			 speed_param = (int32_t)atoi(tmpstr);

			//Header command on IPC
			IPC_msg_t command;
			command.cmd_msg.id = trunk;
			command.cmd_msg.channel = IPC_CMD_CHANNEL;
			command.cmd_msg.cmd_id = SET_SPEED_CMD;
			command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
			//Populate command
			generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
			cmd->num_params=2;
			cmd->parameters[0]=axis;
			cmd->parameters[1]=(uint32_t)speed_param;
			//Tx Event on IPC command queue
			reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
			if(reply_send!=pdPASS)
				err = ERROR;
		}
	}
	if(err != SUCCESS)
		command_send(trunk, 1, SET_SPEED_CMD, strlen(tmpstr)+1, (unsigned char *)tmpstr);
}

/**
* @brief				Callback function to set acceleration
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis and acceleration params in a string4
* 		String of separated params:
* 			- axis
* 			- acceleration param
*
* @return None
*/
static void M_SetAcc(unsigned char trunk, unsigned char* dataptrvoid)
{
	int err=0;
	uint32_t acc_param;
	uint32_t axis = 0;
	BaseType_t reply_send;

	// get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr))
	{
		strcpy(tmpstr, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "M_SetAcc: fail to get axis number\r\n");
		err = ERROR;
	}
	else
	{
		axis = (uint32_t)atoi(tmpstr);
		// get acceleration
		if(!list_command_GetNextParam(dataptrvoid, tmpstr))
		{
			strcpy(tmpstr, "NAK");
			DbgPrintf(DBG_BINARY_PROTO, "M_SetAcc: fail to get acceleration value\r\n");
			err = ERROR;
		}
		else
		{
			acc_param = atoi(tmpstr);
			//Header command on IPC
			IPC_msg_t command;
			command.cmd_msg.id = trunk;
			command.cmd_msg.channel = IPC_CMD_CHANNEL;
			command.cmd_msg.cmd_id = SET_ACC_CMD;
			command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
			//Populate command
			generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
			cmd->num_params=2;
			cmd->parameters[0]=axis;
			cmd->parameters[1]=(uint32_t)acc_param;
			//Tx Event on IPC command queue
			reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
			if(reply_send!=pdPASS)
				err = ERROR;
		}
	}
	if(err != SUCCESS)
		command_send(trunk, 1, SET_ACC_CMD, strlen(tmpstr)+1, (unsigned char *)tmpstr);
}

/**
* @brief				Set the speed in the motor jog mode
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis and jog speed params in a string4
* 		String of comma separated params:
* 			- axis
* 			- jog speed param
*
* @return None
*/
static void M_SetJogSpeed(unsigned char trunk, unsigned char* dataptrvoid)
{
	int err=0;
	uint32_t jog_speed_param;
	uint32_t axis = 0;
	BaseType_t reply_send;

	// get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
		strcpy(tmpstr, "NAK");
		err = ERROR;
	}else
	{
		axis = (uint32_t)atoi(tmpstr);
		// get speed
		if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
			strcpy(tmpstr, "NAK");
			err = ERROR;
		}else
		{
			jog_speed_param = atoi(tmpstr);
			//Header command on IPC
			IPC_msg_t command;
			command.cmd_msg.id = trunk;
			command.cmd_msg.channel = IPC_CMD_CHANNEL;
			command.cmd_msg.cmd_id = SET_JOG_SPEED_CMD;
			command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
			//Populate command
			generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
			cmd->num_params=2;
			cmd->parameters[0]=axis;
			cmd->parameters[1]=(uint32_t)jog_speed_param;
			//Tx Event on IPC command queue
			reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
			if(reply_send!=pdPASS)
				err = ERROR;
		}
	}

	if(err != SUCCESS)
		command_send(trunk, 1, SET_JOG_SPEED_CMD, strlen(tmpstr)+1, (unsigned char *)tmpstr);
}

/**
* @brief				Callback to perform forward steps in motor jog mode
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis and step params in a string
* 		String of comma separated params:
* 			- axis
*
* @return None
*/
static void M_GoJogFwd(unsigned char trunk, unsigned char* dataptrvoid)
{
	int err=0;
	uint32_t axis = 0;
	BaseType_t reply_send;

	// get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
		strcpy(tmpstr, "NAK");
		err = ERROR;
	}else
		{
		axis = (uint32_t)atoi(tmpstr);
		//Header command on IPC
		IPC_msg_t command;
		command.cmd_msg.id = trunk;
		command.cmd_msg.channel = IPC_CMD_CHANNEL;
		command.cmd_msg.cmd_id = GO_JOG_FWD_CMD;
		command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
		//Populate command
		generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
		cmd->num_params=1;
		cmd->parameters[0]=axis;
		//Tx Event on IPC command queue
		reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
		if(reply_send!=pdPASS)
			err = ERROR;
	}

	if(err != SUCCESS)
		command_send(trunk, 1, GO_JOG_FWD_CMD, strlen(tmpstr)+1, (unsigned char *)tmpstr);

}

/**
* @brief				Callback to perform reverse steps in motor jog mode
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis in a string
* 		String of comma separated params:
* 			- axis
*
* @return None
*/
static void M_GoJogRev(unsigned char trunk, unsigned char* dataptrvoid)
{

	int err=0;
	uint32_t axis = 0;
	BaseType_t reply_send;

	// get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
		strcpy(tmpstr, "NAK");
		err = ERROR;
	}else
		{
		axis = (uint32_t)atoi(tmpstr);
		//Header command on IPC
		IPC_msg_t command;
		command.cmd_msg.id = trunk;
		command.cmd_msg.channel = IPC_CMD_CHANNEL;
		command.cmd_msg.cmd_id = GO_JOG_REV_CMD;
		command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
		//Populate command
		generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
		cmd->num_params=1;
		cmd->parameters[0]=axis;
		//Tx Event on IPC command queue
		reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
		if(reply_send!=pdPASS)
			err = ERROR;
	}

	if(err != SUCCESS)
		command_send(trunk, 1, GO_JOG_REV_CMD, strlen(tmpstr)+1, (unsigned char *)tmpstr);
}

/**
* @brief				Callback to communicate the motion status
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis params for the relative motion status
* 		String of comma separated params:
* 			- axis
*
* @return None
*/
static void M_GetMotionStatus(unsigned char trunk, unsigned char* dataptrvoid)
{
	int err=0;
	uint32_t axis = 0;
	BaseType_t reply_send;

	// get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
		strcpy(tmpstr, "NAK");
		err = ERROR;
	}else {
		axis = (uint32_t)atoi(tmpstr);
		//Header command on IPC
		IPC_msg_t command;
		command.cmd_msg.id = trunk;
		command.cmd_msg.channel = IPC_CMD_CHANNEL;
		command.cmd_msg.cmd_id = GET_MOTION_STATUS_CMD;
		command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
		//Populate command
		generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
		cmd->num_params=1;
		cmd->parameters[0]=axis;
		//Tx Event on IPC command queue
		reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
		if(reply_send!=pdPASS)
			err = ERROR;
	}

	if(err != SUCCESS)
		command_send(trunk, 1, GET_MOTION_STATUS_CMD, strlen(tmpstr)+1, (unsigned char *)tmpstr);
}

/**
* @brief				Callback to communicate the motor status
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis params for the motor status
* 		String of comma separated params:
* 			- axis
*
* @return None
*/
static void M_GetMotorStatus(unsigned char trunk, unsigned char* dataptrvoid)
{
	int err=0;
	uint32_t axis = 0;
	BaseType_t reply_send;

	// get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
		strcpy(tmpstr, "NAK");
		err = ERROR;
	}else {
		axis = (uint32_t)atoi(tmpstr);
		//Header command on IPC
		IPC_msg_t command;
		command.cmd_msg.id = trunk;
		command.cmd_msg.channel = IPC_CMD_CHANNEL;
		command.cmd_msg.cmd_id = GET_MOTOR_STATUS_CMD;
		command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
		//Populate command
		generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
		cmd->num_params=1;
		cmd->parameters[0]=axis;
		//Tx Event on IPC command queue
		reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
		if(reply_send!=pdPASS)
			err = ERROR;
	}

	if(err != SUCCESS)
		command_send(trunk, 1, GET_MOTOR_STATUS_CMD, strlen(tmpstr)+1, (unsigned char *)tmpstr);

}

/**
* @brief				Callback to send quick stop the motor command
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis parameter
* 		String of comma separated params:
* 			- axis
*
* @return None
*/
static void M_QuickStop(unsigned char trunk, unsigned char* dataptrvoid)
{


	int err=0;
	uint32_t axis = 0;
	BaseType_t reply_send;

	// get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
		strcpy(tmpstr, "NAK");
		err = ERROR;
	}else {
		axis = (uint32_t)atoi(tmpstr);
		//Header command on IPC
		IPC_msg_t command;
		command.cmd_msg.id = trunk;
		command.cmd_msg.channel = IPC_CMD_CHANNEL;
		command.cmd_msg.cmd_id = QUICK_STOP_CMD;
		command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
		//Populate command
		generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
		cmd->num_params=1;
		cmd->parameters[0]=axis;
		//Tx Event on IPC command queue
		reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
		if(reply_send!=pdPASS)
			err = ERROR;
	}

	if(err != SUCCESS)
		command_send(trunk, 1, QUICK_STOP_CMD, strlen(tmpstr)+1, (unsigned char *)tmpstr);
}

/**
* @brief				Callback to send stop motor command
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis parameter
* 		String of comma separated params:
* 			- axis
*
* @return None
*/
static void M_StopHere(unsigned char trunk, unsigned char* dataptrvoid)
{


	int err=0;
	uint32_t axis = 0;
	BaseType_t reply_send;

	// get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
		strcpy(tmpstr, "NAK");
		err = ERROR;
	}else {
		axis = (uint32_t)atoi(tmpstr);
		//Header command on IPC
		IPC_msg_t command;
		command.cmd_msg.id = trunk;
		command.cmd_msg.channel = IPC_CMD_CHANNEL;
		command.cmd_msg.cmd_id = M_STOP_CMD;
		command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
		//Populate command
		generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
		cmd->num_params=1;
		cmd->parameters[0]=axis;
		//Tx Event on IPC command queue
		reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
		if(reply_send!=pdPASS)
			err = ERROR;
	}

	if(err != SUCCESS)
		command_send(trunk, 1, M_STOP_CMD, strlen(tmpstr)+1, (unsigned char *)tmpstr);
}

/**
* @brief				Callback to retrieve the actual position of the motor
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis parameter
* 		String of comma separated params:
* 			- axis
*
* @return None
*/
static void M_GetMotorActPos(unsigned char trunk, unsigned char* dataptrvoid)
{
	int err=0;
	uint32_t axis = 0;
	BaseType_t reply_send;

	// get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
		strcpy(tmpstr, "NAK");
		err = ERROR;
	}else {
		axis = (uint32_t)atoi(tmpstr);
		//Header command on IPC
		IPC_msg_t command;
		command.cmd_msg.id = trunk;
		command.cmd_msg.channel = IPC_CMD_CHANNEL;
		command.cmd_msg.cmd_id = GET_ACTUAL_POS_CMD;
		command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
		//Populate command
		generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
		cmd->num_params=1;
		cmd->parameters[0]=axis;
		//Tx Event on IPC command queue
		reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
		if(reply_send!=pdPASS)
			err = ERROR;
	}

	if(err != SUCCESS)
		command_send(trunk, 1, GET_ACTUAL_POS_CMD, strlen(tmpstr)+1, (unsigned char *)tmpstr);

}

/**
* @brief				Callback for motor drive initialization
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis parameter
* 		String of comma separated params:
* 			- axis
* 			- node
*
* @return None
*/
static void M_Init(unsigned char trunk, unsigned char* dataptrvoid)
{
	int err=0;
	uint32_t node;
	uint32_t axis = 0;
	BaseType_t reply_send;

	// get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
		strcpy(tmpstr, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "M_Init: fail to get axis number\r\n");
		err = ERROR;
	}else
	{
		axis = (uint32_t)atoi(tmpstr);
		// get speed
		if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
			strcpy(tmpstr, "NAK");
			DbgPrintf(DBG_BINARY_PROTO, "M_Init: fail to get node number\r\n");
			err = ERROR;
		}else
		{
			node = atoi(tmpstr);
			//Header command on IPC
			IPC_msg_t command;
			command.cmd_msg.id = trunk;
			command.cmd_msg.channel = IPC_CMD_CHANNEL;
			command.cmd_msg.cmd_id = MOTOR_INIT_CMD;
			command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
			//Populate command
			generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
			cmd->num_params=2;
			cmd->parameters[0]=axis;
			cmd->parameters[1]=node;
			//Tx Event on IPC command queue
			reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
			if(reply_send!=pdPASS)
				err = ERROR;
		}
	}
	if(err != SUCCESS)
		command_send(trunk, 1, MOTOR_INIT_CMD, strlen(tmpstr)+1, (unsigned char *)tmpstr);
}

/**
* @brief				Callback function to set min position limit
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis and min position params in a string
* 		String of comma separated params:
* 			- axis
* 			- min position
*
* @return None
*/
static void M_SetMinPos(unsigned char trunk, unsigned char* dataptrvoid)
{
	int err=0;
	int32_t min_pos;
	uint32_t axis = 0;
	BaseType_t reply_send;

	// get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
		strcpy(tmpstr, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "M_SetMinPos: fail to get axis number\r\n");
		err = ERROR;
	}else
	{
		axis = (uint32_t)atoi(tmpstr);
		// get speed
		if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
			strcpy(tmpstr, "NAK");
			DbgPrintf(DBG_BINARY_PROTO, "M_SetMinPos: fail to get position limit value\r\n");
			err = ERROR;
		}else
		{
			min_pos = (int32_t)atoi(tmpstr);
			//Header command on IPC
			IPC_msg_t command;
			command.cmd_msg.id = trunk;
			command.cmd_msg.channel = IPC_CMD_CHANNEL;
			command.cmd_msg.cmd_id = MOTOR_SET_MIN_POS;
			command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
			//Populate command
			generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
			cmd->num_params=2;
			cmd->parameters[0]=axis;
			cmd->parameters[1]=min_pos;
			//Tx Event on IPC command queue
			reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
			if(reply_send!=pdPASS)
				err = ERROR;
		}
	}
	if(err != SUCCESS)
		command_send(trunk, 1, MOTOR_SET_MIN_POS, strlen(tmpstr)+1, (unsigned char *)tmpstr);
}

/**
* @brief				Callback function to set max position limit
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis and max position params in a string
* 		String of comma separated params:
* 			- axis
* 			- max position
*
* @return None
*/
static void M_SetMaxPos(unsigned char trunk, unsigned char* dataptrvoid)
{
	int err=0;
	int32_t max_pos;
	uint32_t axis = 0;
	BaseType_t reply_send;

	// get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
		strcpy(tmpstr, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "M_SetMaxPos: fail to get axis number\r\n");
		err = ERROR;
	}else
	{
		axis = (uint32_t)atoi(tmpstr);
		// get speed
		if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
			strcpy(tmpstr, "NAK");
			DbgPrintf(DBG_BINARY_PROTO, "M_SetMaxPos: fail to get position limit value\r\n");
			err = ERROR;
		}else
		{
			max_pos = (int32_t)atoi(tmpstr);
			//Header command on IPC
			IPC_msg_t command;
			command.cmd_msg.id = trunk;
			command.cmd_msg.channel = IPC_CMD_CHANNEL;
			command.cmd_msg.cmd_id = MOTOR_SET_MAX_POS;
			command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
			//Populate command
			generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
			cmd->num_params=2;
			cmd->parameters[0]=axis;
			cmd->parameters[1]=max_pos;
			//Tx Event on IPC command queue
			reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
			if(reply_send!=pdPASS)
				err = ERROR;
		}
	}
	if(err != SUCCESS)
		command_send(trunk, 1, MOTOR_SET_MAX_POS, strlen(tmpstr)+1, (unsigned char *)tmpstr);
}


/**
* @brief				Callback to motor home at current position
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis and min position params in a string
* 		String of comma separated params:
* 			- axis
*
* @return None
*/
static void M_SetHome(unsigned char trunk, unsigned char* dataptrvoid)
{
	int err=0;
	uint32_t axis = 0;
	BaseType_t reply_send;

	// get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
		strcpy(tmpstr, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "M_SetHome: fail to get axis number !\r\n");
		err = ERROR;
	}else
	{
		axis = (uint32_t)atoi(tmpstr);
		//Header command on IPC
		IPC_msg_t command;
		command.cmd_msg.id = trunk;
		command.cmd_msg.channel = IPC_CMD_CHANNEL;
		command.cmd_msg.cmd_id = MOTOR_SET_HOME;
		command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
		//Populate command
		generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
		cmd->num_params=1;
		cmd->parameters[0]=axis;
		//Tx Event on IPC command queue
		reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
		if(reply_send!=pdPASS)
			err = ERROR;
	}
	if(err != SUCCESS)
		command_send(trunk, 1, MOTOR_SET_HOME, strlen(tmpstr)+1, (unsigned char *)tmpstr);

}

/**
* @brief				Callback to set motor Deceleraztion
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis and deceleration params in a string
* 		String of comma separated params:
* 			- axis
* 			- deceleration
*
* @return None
*/
static void M_SetDec(unsigned char trunk, unsigned char* dataptrvoid)
{
	int err=0;
	uint32_t deceleration;
	uint32_t axis = 0;
	BaseType_t reply_send;

	// get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
		strcpy(tmpstr, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "M_SetDec: fail to get axis number\r\n");
		err = ERROR;
	}else
	{
		axis = (uint32_t)atoi(tmpstr);
		// get speed
		if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
			strcpy(tmpstr, "NAK");
			DbgPrintf(DBG_BINARY_PROTO, "M_SetDec: fail to get dec value\r\n");
			err = ERROR;
		}else
		{
			deceleration = (uint32_t)atoi(tmpstr);
			//Header command on IPC
			IPC_msg_t command;
			command.cmd_msg.id = trunk;
			command.cmd_msg.channel = IPC_CMD_CHANNEL;
			command.cmd_msg.cmd_id = SET_DEC_CMD;
			command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
			//Populate command
			generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
			cmd->num_params=2;
			cmd->parameters[0]=axis;
			cmd->parameters[1]=deceleration;
			//Tx Event on IPC command queue
			reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
			if(reply_send!=pdPASS)
				err = ERROR;
		}
	}
	if(err != SUCCESS)
		command_send(trunk, 1, SET_DEC_CMD, strlen(tmpstr)+1, (unsigned char *)tmpstr);

}

/**
* @brief				Callback to get actual current on motor
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis params in a string
* 		String of comma separated params:
* 			- axis
*
* @return None
*/
static void M_GetActualCurrent(unsigned char trunk, unsigned char* dataptrvoid)
{
	int err=0;
	uint32_t axis = 0;
	BaseType_t reply_send;

	// get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
		strcpy(tmpstr, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "M_GetActualCurrent: fail to get axis number !\r\n");
		err = ERROR;
	}else
	{
		axis = (uint32_t)atoi(tmpstr);
		//Header command on IPC
		IPC_msg_t command;
		command.cmd_msg.id = trunk;
		command.cmd_msg.channel = IPC_CMD_CHANNEL;
		command.cmd_msg.cmd_id = GET_ACTUAL_CURRENT;
		command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
		//Populate command
		generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
		cmd->num_params=1;
		cmd->parameters[0]=axis;
		//Tx Event on IPC command queue
		reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
		if(reply_send!=pdPASS)
			err = ERROR;
	}
	if(err != SUCCESS)
		command_send(trunk, 1, GET_ACTUAL_CURRENT, strlen(tmpstr)+1, (unsigned char *)tmpstr);

}

/**
* @brief				Callback to set current limit on a motor
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis and current limit params in a string
* 		String of comma separated params:
* 			- axis
* 			- deceleration
*
* @return None
*/
static void M_SetCurrentLimit(unsigned char trunk, unsigned char* dataptrvoid)
{

	int err=0;
	uint32_t curr_limit;
	uint32_t axis = 0;
	BaseType_t reply_send;

	// get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
		strcpy(tmpstr, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "M_SetCurrentLimit: fail to get axis number\r\n");
		err = ERROR;
	}else
	{
		axis = (uint32_t)atoi(tmpstr);
		// get speed
		if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
			strcpy(tmpstr, "NAK");
			DbgPrintf(DBG_BINARY_PROTO, "M_SetCurrentLimit: fail to get limit value\r\n");
			err = ERROR;
		}else
		{
			curr_limit = (uint32_t)atoi(tmpstr);
			//Header command on IPC
			IPC_msg_t command;
			command.cmd_msg.id = trunk;
			command.cmd_msg.channel = IPC_CMD_CHANNEL;
			command.cmd_msg.cmd_id = MOTOR_SET_CURRENT_LIMIT;
			command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
			//Populate command
			generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
			cmd->num_params=2;
			cmd->parameters[0]=axis;
			cmd->parameters[1]=curr_limit;
			//Tx Event on IPC command queue
			reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
			if(reply_send!=pdPASS)
				err = ERROR;
		}
	}
	if(err != SUCCESS)
		command_send(trunk, 1, MOTOR_SET_CURRENT_LIMIT, strlen(tmpstr)+1, (unsigned char *)tmpstr);

}

/**
* @brief				Callback to set time on current limit on a motor
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis and timme on current limit params in a string
* 		String of comma separated params:
* 			- axis
* 			- time
*
* @return None
*/
static void M_SetCurrentLimitTime(unsigned char trunk, unsigned char* dataptrvoid)
{

	int err=0;
	uint32_t time;
	uint32_t axis = 0;
	BaseType_t reply_send;

	// get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
		strcpy(tmpstr, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "M_SetCurrentLimitTime: fail to get axis number\r\n");
		err = ERROR;
	}else
	{
		axis = (uint32_t)atoi(tmpstr);
		// get speed
		if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
			strcpy(tmpstr, "NAK");
			DbgPrintf(DBG_BINARY_PROTO, "M_SetCurrentLimitTime: fail to get limit value\r\n");
			err = ERROR;
		}else
		{
			time = (uint32_t)atoi(tmpstr);
			//Header command on IPC
			IPC_msg_t command;
			command.cmd_msg.id = trunk;
			command.cmd_msg.channel = IPC_CMD_CHANNEL;
			command.cmd_msg.cmd_id = MOTOR_SET_CURRENT_LIMIT_TIME;
			command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
			//Populate command
			generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
			cmd->num_params=2;
			cmd->parameters[0]=axis;
			cmd->parameters[1]=time;
			//Tx Event on IPC command queue
			reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
			if(reply_send!=pdPASS)
				err = ERROR;
		}
	}
	if(err != SUCCESS)
		command_send(trunk, 1, MOTOR_SET_CURRENT_LIMIT_TIME, strlen(tmpstr)+1, (unsigned char *)tmpstr);

}

/**
* @brief				Callback to get maximum current on a motor drive
*
* @param trunk			Communication bus number
* @param dataptrvoid	Axis params in a string
* 		String of comma separated params:
* 			- axis
*
* @return None
*/
static void M_GetRatedCurrent(unsigned char trunk, unsigned char* dataptrvoid)
{
	int err=0;
	uint32_t axis = 0;
	BaseType_t reply_send;

	// get axis
	if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
		strcpy(tmpstr, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "M_GetRatedCurrent: fail to get axis number !\r\n");
		err = ERROR;
	}else
	{
		axis = (uint32_t)atoi(tmpstr);
		//Header command on IPC
		IPC_msg_t command;
		command.cmd_msg.id = trunk;
		command.cmd_msg.channel = IPC_CMD_CHANNEL;
		command.cmd_msg.cmd_id = MOTOR_GET_RATED_CURRENT;
		command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
		//Populate command
		generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
		cmd->num_params=1;
		cmd->parameters[0]=axis;
		//Tx Event on IPC command queue
		reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
		if(reply_send!=pdPASS)
			err = ERROR;
	}
	if(err != SUCCESS)
		command_send(trunk, 1, MOTOR_GET_RATED_CURRENT, strlen(tmpstr)+1, (unsigned char *)tmpstr);

}

/**
* @brief				Service mode management on motor power
*
* @param trunk			Communication bus number
* @param dataptrvoid	Enable params in a string
* 		String of comma separated params:
* 			- Enable
*
* @return None
*/
static void M_Sync_ServiceMode(unsigned char trunk, unsigned char* dataptrvoid){
	int err=0;
	uint32_t enable_service = 0;
	BaseType_t reply_send;

	// get enable
	if(!list_command_GetNextParam(dataptrvoid, tmpstr)){
		strcpy(tmpstr, "NAK");
		DbgPrintf(DBG_BINARY_PROTO, "ERR M_SYNC_SERVICEMODE_CMD NAK: enable par not found\r\n");
		err = ERROR;
	}else
	{
		enable_service = (uint8_t)atoi(tmpstr);
		//Header command on IPC
		IPC_msg_t command;
		command.cmd_msg.id = trunk;
		command.cmd_msg.channel = IPC_CMD_CHANNEL;
		command.cmd_msg.cmd_id = M_SYNC_SERVICEMODE_CMD;
		command.cmd_msg.cmd_type = IPC_CMD_REQUEST;
		//Populate command
		generic_cmd_t * cmd = (generic_cmd_t *) command.cmd_msg.cmd_data;
		cmd->num_params=1;
		cmd->parameters[0]=enable_service;
		//Tx Event on IPC command queue
		reply_send = xQueueSend(ipc_queue.QueueHandle,&command,100U);
		if(reply_send!=pdPASS)
			err = ERROR;
	}
	if(err != SUCCESS)
		command_send(trunk, 1, M_SYNC_SERVICEMODE_CMD, strlen(tmpstr)+1, (unsigned char *)tmpstr);

};



//******************************************************************//
//					END	 SECTION									//
//******************************************************************//



/**
 * \brief			Binary function initialization
 * 
 * \param			void
 * 
 * \return void
 */
void binaryproto_function_init (void)
{
	command_reset();
	command_init (FWVERSION_CMD, verfwcmd);
	//Motor Specific
	command_init (MOTOR_HOME_CMD, motor_home);
	command_init (SET_HOME_SPEED_CMD, M_SetHomeSpeed);
	command_init (SET_POSITION_CMD, M_SetPosition);
	command_init (SET_SPEED_CMD, M_SetSpeed);
	command_init (SET_ACC_CMD, M_SetAcc);
	command_init (SET_JOG_SPEED_CMD, M_SetJogSpeed);
	command_init (GO_JOG_FWD_CMD, M_GoJogFwd);
	command_init (GO_JOG_REV_CMD, M_GoJogRev);
	command_init (GET_MOTION_STATUS_CMD, M_GetMotionStatus);
	command_init (GET_MOTOR_STATUS_CMD, M_GetMotorStatus);
	command_init (QUICK_STOP_CMD, M_QuickStop);
	command_init (M_STOP_CMD, M_StopHere);
	command_init (GET_ACTUAL_POS_CMD, M_GetMotorActPos);
	command_init (MOTOR_INIT_CMD, M_Init);
	command_init (MOTOR_SET_MIN_POS, M_SetMinPos);
	command_init (MOTOR_SET_MAX_POS, M_SetMaxPos);
	command_init (MOTOR_SET_HOME, M_SetHome);
	command_init (SET_DEC_CMD, M_SetDec);
	command_init (GET_ACTUAL_CURRENT, M_GetActualCurrent);
	command_init(MOTOR_SET_CURRENT_LIMIT, M_SetCurrentLimit);		// soglia limite in mA per andare in fault
	command_init(MOTOR_SET_CURRENT_LIMIT_TIME, M_SetCurrentLimitTime);	// limite di tempo per il quale può essere superata la soglia in ms
	command_init(MOTOR_GET_RATED_CURRENT, M_GetRatedCurrent);
	command_init( M_SYNC_SERVICEMODE_CMD, M_Sync_ServiceMode);

}
