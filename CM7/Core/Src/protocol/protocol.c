/*
 * Protocol.c
 *
 * Created: 06/12/2021 10:30:11
 *  Author: Stefano
 */ 
#include "main.h"
#include "cmsis_os.h"
#include "binaryprotocol.h"
#include "command.h"
#include "dbgprotocol.h"
#include "NetTask.h"

//Queue Message from socket task
extern state_socket_pool_t state_socket_pool;

//osThreadId ProtocolTaskHandle;
/*
osThreadId_t ProtocolTaskHandle;
const osThreadAttr_t ProtocolTask_attributes = {
  .name = "ProtocolTask",
  .stack_size = 512 * 4,
  .priority = (osPriority_t) osPriorityLow,
};
*/
/*
static void copy_data_on_socket_protocol_cBuffer(uint8_t *msd_data_ptr,uint32_t msg_count,cBuffer *circRxBuffer){
	if(msd_data_ptr!=NULL && msg_count>0){
		for (int char_index=0; char_index<msg_count; char_index++){
			bufferAddToEnd(circRxBuffer, msd_data_ptr[char_index]);
		}
	}
}
*/

void ProtocolTask(void const * argument){
	uint8_t msd_data_ptr[MAX_BYTE_FOR_SOCKET],dato;
	uint32_t msg_count=0;
////	osStatus_t res;
	BaseType_t res;
	int socket_index=0;

	while(1){

		//Get on occuped and data-ready socket the data
		for(socket_index=0;socket_index<MAX_SOCKET_CONNECTION;socket_index++){
			//RxQueue Section
			msg_count = uxQueueMessagesWaiting(state_socket_pool.NetRxQueueHandle_Pool[socket_index]);
			if(msg_count>0){
				//To avoid buffer overflow
				if(msg_count>MAX_BYTE_FOR_SOCKET)
					msg_count=MAX_BYTE_FOR_SOCKET;
				uint32_t byte_idx=0;
				for(byte_idx=0;byte_idx<msg_count;byte_idx++){
					res=xQueueReceive(state_socket_pool.NetRxQueueHandle_Pool[socket_index],&(msd_data_ptr[byte_idx]),0);
					if(res!=pdTRUE ){
						Error_Handler();
					}
				}
				//Copy data on buffer and process
				copy_data_on_protocol_cBuffer(msd_data_ptr,msg_count,&state_socket_pool.protocol_socket_Rxbuffer[socket_index].circRxBuffer);
				/*
				//If active socket, process data:
				int data_check;
				data_check = process_packet(socket_index);
				if((data_check>= 0) && (data_check != ACK_RECEIVED))
				{
					exe_cmd(socket_index);
				}
				*/
			}
			//If active socket, process data:
			int data_check;
			data_check = process_packet(socket_index);
			if((data_check>= 0) && (data_check != ACK_RECEIVED))
			{
				exe_cmd(socket_index);
			}
#ifdef PROTOCOL_TX_ON_PIPE
			//TxQueueSection
			//RxQueue Section
			msg_count = uxQueueMessagesWaiting(state_socket_pool.NetTxQueueHandle_Pool[socket_index]);
			if(msg_count>0){
				uint32_t byte_idx=0;
				for(byte_idx=0;byte_idx<msg_count;byte_idx++){
					res=xQueueReceive(state_socket_pool.NetTxQueueHandle_Pool[socket_index],&dato,0);
					if(res!=pdTRUE ){
						Error_Handler();
					}
					//Send Data on socket
					write_onsocket(dato,socket_index);
				}
			}
#endif
		}
		osDelay(10);
	};
}

void protocol_Init(void)
{
	binaryproto_function_init();
}

/*
void protocol_Process(void)
{
	int data_check;
	
	comm_usb_receive_byte();

	comm_dbg_receive_byte();

	//check socket channel
	//data_check = process_packet(COM_SOCKET);
	//if((data_check>= 0) && (data_check != ACK_RECEIVED))
	//{
		//exe_cmd(COM_SOCKET);
	//}
	
	// check USART1 channel
	#ifndef USE_LIFTER
	data_check = process_packet(COM_USART1);
	if((data_check>= 0) && (data_check != ACK_RECEIVED))
	{
		exe_cmd(COM_USART1);
	}
	#endif
	
	// check serial USB channel
	data_check = process_packet(COM_USB);
	if((data_check>= 0) && (data_check != ACK_RECEIVED))
	{
		exe_cmd(COM_USB);
	}

	// check serial BT channel
	#ifndef NO_BLUETOOTH
	data_check = process_packet(COM_BLUET);
	if((data_check>= 0) && (data_check != ACK_RECEIVED))
	{
		exe_cmd(COM_BLUET);
	}
	#endif
	// check serial debug channel
	dbgprotocol_Debug(COM_DBG);
	//dbgprotocol_Debug(COM_BLUET);
}
*/
