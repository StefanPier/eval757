/*
 * dbgprotocol.c
 *
 * Created: 27/02/2015 14:57:54
 *  Author: Alessandro
 */ 

#include "dbgprotocol.h"
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#define CR	13
#define LF	10
#define TAB	9
#define BKS	8
#define UPA 65

static void seriali_SendNack(void);
static void seriali_SendAck(void);
static int seriali_str2int(char *str, uint32_t *val);
static int StrHex2Uint32(char *str, uint32_t *vd);
//static int seriali_DecodeSdo(void);
static int seriali_DbgGetNextParam(char * param);
static void seriali_PrintCtrlW(uint32_t ctrlw);
static void seriali_PrintStsW(uint32_t stsw);

void dbgprotocol_Parse(void);

static cBuffer* dbg_rxbuff;

/**
 * \brief			Initialization of the debug protocol
 * 
 * \param buff		Debug Buffer
 * 
 * \return void
 */
void dbgprotocolInit ( cBuffer* buff)
{
	dbg_rxbuff = buff;
}

/**
 * \brief		Decode end perform motor initialization
 * 
 * \param		Initialization parameter: axis and node
 * 
 * \return int	1 if succeful, 0 otherwise
 */
static int seriali_DecodeIni(void)
{
	char tmpstr[10];
	uint32_t axis;
	uint8_t node;
	uint32_t u32tmp;
		
	// get axis
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &axis);	
	// get node number
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);	
	node = (uint8_t)u32tmp;
	
#warning TODO MOTOR INITIALIZATION FUNCTION
	return 1;//motor_Init(axis, node);
}

/**
 * \brief		Decode and Set the operational mode of the motor
 * 
 * \param		Axis and Mode parameters 
 * 
 * \return int	1 = mode set successfully, 0 = mode set error.
 */
static int seriali_DecodeOpm(void)
{
	char tmpstr[10];
	uint32_t u32tmp;
	uint32_t axis;
		
	// get axis
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &axis);
	// get mode
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
#warning TODO motor_SetModeOperation FUNCTION
	return 1;//motor_SetModeOperation(axis, u32tmp);
}

/**
 * \brief		Decode and perform the "Home" operation
 * 
 * \param		Axis parameter
 * 
 * \return int	1 = success, 0 = error.
 */
static int seriali_DecodeGho(void)
{
	char tmpstr[10];
	uint32_t axis;
	
	// get axis
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &axis);
	
#warning TODO motor_Home FUNCTION
	return 1;//motor_Home(axis);
}

/**
 * \brief		Set homing speed
 * 
 * \param		Axis and speed parameters in a string
 * 
 * \return int	1 = parameter set successfully, 0 = parameter set error.
 */
static int seriali_DecodeShs(void)
{
	char tmpstr[10];
	uint32_t u32tmp;
	uint32_t axis;
	
	// get axis
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &axis);
	// get speed
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
#warning TODO duet_SetHomingSpeed FUNCTION
	return 1;//motor_Home(axis);
//	return duet_SetHomingSpeed(axis, u32tmp);
}
		
/**
 * \brief		Decode and Set motor speed
 * 
 * \param		Axis and speed parameters in a string 
 * 
 * \return int	1	
 */
static int seriali_DecodeSps(void)
{
	char tmpstr[10];
	uint32_t u32tmp;
	uint32_t axis;
	
	// get axis
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &axis);
	// get speed
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
#warning TODO motor_SetSpeed FUNCTION
	return 1;//motor_Home(axis);
	//return motor_SetSpeed(axis, u32tmp);
}
		
/**
 * \brief		Decode and set acceleration deceleration rate
 * 
 * \param		Axis and speed parameters in a string 
 * 
 * \return int	\return  1 = success, 0 = error.
 */
static int seriali_DecodeSpa(void)
{
	char tmpstr[10];
	uint32_t u32tmp;
	uint32_t axis;
	
	// get axis
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &axis);	
	// get acceleration
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
	
#warning TODO motor_SetAcc FUNCTION
	return 1;//motor_Home(axis);
	//return motor_SetAcc(axis, u32tmp);
}
		

/**
 * \brief	Decode and move motor to a position
 * 
 * \param	Axis and new position parameters
 * 
 * \return  1 = success, 0 = error.
 */
static int seriali_DecodeMgo(void)
{
	char tmpstr[10];
	uint32_t u32tmp;
	uint32_t axis;
	
	// get axis
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &axis);
	// get new position
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
	
#warning TODO motor_SetPosition FUNCTION
	return 1;//motor_Home(axis);
	//return motor_SetPosition(axis, u32tmp);
}

/**
 * \brief		Decode ad Set the motor speed for jog mode in the motor object structure
 * 
 * \param		Axis and speed parameters
 * 
 * \return		1 = success, 0 = error.
 */
static int seriali_DecodeSjs(void)
{
	char tmpstr[10];
	uint32_t u32tmp;
	uint32_t axis;
	
	// get axis
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &axis);
	// get speed
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
	
#warning TODO motor_SetJogSpeed FUNCTION
	return 1;//motor_Home(axis);
	//return motor_SetJogSpeed(axis, u32tmp);
}

/**
 * \brief	Decode and start motor in jog mode and start moving in forward direction. 
 * 
 * \param	Axis and steps parameter in a string
 * 
 * \return  1 = success, 0 = error.
 */
static int seriali_DecodeGjf(void)
{
	char tmpstr[10];
	uint32_t u32tmp;
	uint32_t axis;
	
	// get axis
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &axis);
	//// get steps
	//if(!seriali_DbgGetNextParam(tmpstr))
		//return 0;
	seriali_str2int(tmpstr, &u32tmp);
	
#warning TODO motor_GoJogFwd FUNCTION
	return 1;//motor_Home(axis);
	//return motor_GoJogFwd(axis);
}

/**
 * \brief	Decode and set the motor in jog mode and start moving in reverse direction
 * 
 * \param	Axis and steps parameter in a string	
 * 
 * \return  1 = success, 0 = error.
 */
static int seriali_DecodeGjr(void)
{
	char tmpstr[10];
	uint32_t u32tmp;
	uint32_t axis;
	
	// get axis
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &axis);
	//// get steps
	//if(!seriali_DbgGetNextParam(tmpstr))
		//return 0;
	seriali_str2int(tmpstr, &u32tmp);
	
#warning TODO motor_GoJogRev FUNCTION
	return 1;//motor_Home(axis);
	//return motor_GoJogRev(axis);
}

/**
 * \brief	Decode and set the motor heartbeat timeout
 * 
 * \param	Axis and timeout in ms parameter in a string	
 * 
 * \return  1 = success, 0 = error.
 */
static int seriali_DecodeShb(void)
{
	char tmpstr[10];
	uint32_t u32tmp;
	uint32_t axis;
	
	// get axis
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &axis);
	// get hb timeout
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
	
#warning TODO duet_SetHeartbeat FUNCTION
	return 1;//motor_Home(axis);
	//return duet_SetHeartbeat(axis, u32tmp);
}


/**
 * \brief	Decode and sent a SDO message to a node
 * 
 * \param	Node,index,subindex,r/w (hexadecimal value) and eventually
 *			data to write as parameters in a comma separated string form
 *
 * \return  1 = success, 0 = error.
 */
static int seriali_DecodeSdo(void)
{
#warning TODO seriali_DecodeSdo FUNCTION
	return 1;//motor_Home(axis);
/*
	co_msg_t msg;
	char tmpstr[10];
	uint32_t u32tmp;
	uint8_t dt_out[8] = {0,0,0,0,0,0,0,0};
	uint8_t dt_in[8] = {0,0,0,0,0,0,0,0};
	
	memset(&msg, 0, sizeof(co_msg_t));

	// get node
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
	msg.node = (uint8_t)u32tmp;
	// get index
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
	msg.index = u32tmp;
	// get subindex
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
	msg.subindex = (uint8_t)u32tmp;
	// get r/w
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	msg.r_w = tmpstr[0];
	// get data and len if w
	if(msg.r_w == 'w')
	{
		if(!seriali_DbgGetNextParam(tmpstr))
			return 0;
		seriali_str2int(tmpstr, &u32tmp);
		memcpy(&dt_out[4], &u32tmp, sizeof(uint32_t));
		if(!seriali_DbgGetNextParam(tmpstr))
			return 0;
		seriali_str2int(tmpstr, &u32tmp);
		msg.datalen = (uint8_t)u32tmp;
	}

	msg.data_out = dt_out;
	msg.data_in = dt_in;
	// Send requested SDO message waiting for the reply
	if(co_sdo_Send(&msg))
		return 1;
	else
		return 0;
	*/
}

static int seriali_DecodeRtr(void)
{
#warning TODO seriali_DecodeRtr FUNCTION
	return 1;//motor_Home(axis);
/*
	co_msg_t msg;
	char tmpstr[10];
	uint32_t u32tmp;
	uint8_t dt_out[8] = {0,0,0,0,0,0,0,0};
	uint8_t dt_in[8] = {0,0,0,0,0,0,0,0};
	
	memset(&msg, 0, sizeof(co_msg_t));

	// get address
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
		msg.tx_fid = u32tmp;
	// get data  lenght
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
		msg.datalen = (uint8_t)u32tmp;
	//// get subindex
	//if(!seriali_DbgGetNextParam(tmpstr))
	//return 0;
	//seriali_str2int(tmpstr, &u32tmp);
	//msg.subindex = (uint8_t)u32tmp;
	// get r/w
	//if(!seriali_DbgGetNextParam(tmpstr))
		//return 0;
	//msg.r_w = tmpstr[0];
	//// get data and len if w
	//if(msg.r_w == 'w')
	//{
		//if(!seriali_DbgGetNextParam(tmpstr))
		//return 0;
		//seriali_str2int(tmpstr, &u32tmp);
		//memcpy(&dt_out[4], &u32tmp, sizeof(uint32_t));
		//if(!seriali_DbgGetNextParam(tmpstr))
		//return 0;
		//seriali_str2int(tmpstr, &u32tmp);
		//msg.datalen = (uint8_t)u32tmp;
	//}

	msg.data_out = dt_out;
	msg.data_in = dt_in;
	// Send requested SDO message waiting for the reply
	//if(co_sdo_Send(&msg))
	micro_SendRtr(&msg);
		return 1;
	//else
	//return 0;

 */
}

static int seriali_DecodeJvh(void)
{
	uint32_t ax;
	char tmpstr[10];
	
	// get axis
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &ax);
	
#warning TODO motor_SetHome FUNCTION
	return 1;//motor_Home(axis);
/*
	if(!motor_SetHome(ax))
		return 0;
	else
		return 1;
		*/
}

/**
 * \brief		Decode and send a PDOn to a node
 * 
 * \param		Node,PDO number,data and lenght as string
 * 
 * \return int	1
 */
static int seriali_DecodePdo(void)
{
	uint8_t node;
	char tmpstr[10];
	uint32_t u32tmp;
	uint32_t pdo_n;
	int len;

	// get node
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
	node = (uint8_t)u32tmp;
	// get pdo number
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &pdo_n);
	// get data
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
	// get lenght
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, (uint32_t*)&len);

	uint8_t dt_out[8] = {0,0,0,0,0,0,0,0};

#warning TODO co_pdo_Send FUNCTION
	return 1;//motor_Home(axis);
	/*
	memcpy(&dt_out[0], &u32tmp, sizeof(uint32_t));		
	//memcpy(&dt_out[6], (uint8_t*)&u32tmp, 2);
	co_pdo_Send(pdo_n-1, node, dt_out, len);
	return 1;
	*/
}

/**
 * \brief		Decode and sSends an NMT message to a server
 * 
 * \param		Node and command parameters in a string
 * 
 * \return int	1
 */
static int seriali_DecodeNmt(void)
{
	uint8_t cmd, node;
	char tmpstr[10];
	uint32_t u32tmp;

	// get node
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
	node = (uint8_t)u32tmp;
	// get command
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
	cmd = (uint8_t)u32tmp;
	
#warning TODO co_nmt_Send FUNCTION
	return 1;//motor_Home(axis);
/*
	// Send the message 
	co_nmt_Send(node, cmd);
	return 1;
*/
}

/**
 * \brief		Decode and Write a Control Word at index 0x6040, subindex 0
 * 
 * \param		Axis and Control word as parameters in a string
 * 
 * \return		1 if success, 0 if error.
 */
static int seriali_DecodeWcw(void)
{
	char tmpstr[10];
	uint32_t u32tmp;
	uint32_t axis;
	
	// get axis
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &axis);	
	// get value
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
	
#warning TODO duet_WriteControlWord FUNCTION
	return 1;//motor_Home(axis);

	//return duet_WriteControlWord(axis, u32tmp);
}

/**
 * \brief		Decode to send a SDO reading message 
 * 
 * \param		Node, index, subindex as parameters in a string format
 * 
 * \return		1 if success, 0 if error.
 */
static int seriali_DecodeRsd(void)
{

#warning TODO seriali_DecodeRsd FUNCTION
	return 1;
/*
	co_msg_t msg;
	char tmpstr[10];
	uint32_t u32tmp;
	uint8_t dt_out[8] = {0,0,0,0,0,0,0,0};
	uint8_t dt_in[8] = {0,0,0,0,0,0,0,0};
	
	memset(&msg, 0, sizeof(co_msg_t));

	// get node
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
	msg.node = (uint8_t)u32tmp;
	// get index
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
	msg.index = u32tmp;
	// get subindex
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
	msg.subindex = (uint8_t)u32tmp;

	msg.r_w = 'r';
	msg.data_out = dt_out;
	msg.data_in = dt_in;

	// Send SDO message waiting for the reply
	if(co_sdo_Send(&msg))
		return 1;
	else
		return 0;
	*/
}

/**
 * \brief		Decode to send a SDO writing message 
 * 
 * \param		Node, index, subindex and data as parameters in a string format
 * 
 * \return		1 if success, 0 if error.
 */
static int seriali_DecodeWsd(void)
{
#warning TODO seriali_DecodeWsd FUNCTION
	return 1;
/*
	co_msg_t msg;
	char tmpstr[10];
	uint32_t u32tmp;
	uint8_t dt_out[8];
	uint8_t dt_in[8];
	
	memset(&msg, 0, sizeof(co_msg_t));

	// get node
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
	msg.node = (uint8_t)u32tmp;
	// get index
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
	msg.index = u32tmp;
	// get subindex
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
	msg.subindex = (uint8_t)u32tmp;
	// r/w
	msg.r_w = 'w';
	// get value
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &u32tmp);
	
	//seriali_str2int(tmpstr, (uint32_t *)&dt_out[4]);
	memcpy(&dt_out[4], &u32tmp, sizeof(uint32_t));
	
	msg.data_out = dt_out;
	msg.data_in = dt_in;
	
	// invio il messaggio SDO richiesto ed attendo la risposta
	if(co_sdo_Send(&msg))
		return 1;
	else
		return 0;	
	*/
}

/**
 * \brief		Decode and Read the statusword at index 0x6041, subindex 0
 * 
 * \param		Axis as parameter in a string format
 * 
 * \return		1 if success, 0 if error.
 */
static int seriali_DecodeRsw(void)
{
	uint32_t stsw;
	uint32_t axis;
	char tmpstr[10];
	
	// get axis
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &axis);
	
#warning TODO duet_ReadStatusWord FUNCTION
	return 1;
/*
	int res = duet_ReadStatusWord(axis, &stsw);
	if(res)
	{
		seriali_PrintStsW(stsw);
		return 1;
	}
	else
		return 0;
		*/
}

/**
 * \brief		Decode and Read the control word at index 0x6040, subindex 0
 * 
 * \param		Axis as parameter in a string format
 * 
 * \return		1 if success, 0 if error.
 */
static int seriali_DecodeRcw(void)
{
	uint32_t ctrlw;
	uint32_t axis;
	char tmpstr[10];
	
	// get axis
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &axis);	

	#warning TODO duet_ReadControlWord FUNCTION
	return 1;
	/*
	int res = duet_ReadControlWord(axis, &ctrlw);
	if(res)
	{
		seriali_PrintCtrlW(ctrlw);
		return 1;
	}
	
	return 0;
	*/
}


/**
 * \brief		Decode to activate relay
 * 
 * \param		Relay number as parameter in a string format
 * 
 * \return		1 if success, 0 if error.
 */
static int seriali_DecodeSrl(void)
{
	uint32_t rel_n;
	char tmpstr[10];
	
	// get relay
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &rel_n);

	#warning TODO digio_SetRelay FUNCTION
	return 1;
	/*
	if(!digio_SetRelay(rel_n))
		return 0;
	
	return 1;
	*/
}

/**
 * \brief		Decode to reset relay state (deactivated)
 * 
 * \param		Relay number as parameter in a string format
 * 
 * \return		1 if success, 0 if error.
 */
static int seriali_DecodeRrl(void)
{
	uint32_t rel_n;
	char tmpstr[10];
	
	// get relay
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &rel_n);
	#warning TODO digio_ResetRelay FUNCTION
	return 1;
	/*
	if(!digio_ResetRelay(rel_n))
		return 0;
	
	return 1;
	*/
}

/**
 * \brief		Decode command to read all the digital inputs
 * 
 * \return		1
 */
static int seriali_DecodeRdi(void)
{
	uint32_t i_o;

	i_o = digio_ReadDInput();
	
	DbgPrintf(true, "%X \r\n", i_o);
	DbgPrintf(true, " I8 I7 I6 I5 I4 I3 I2 I1 I0 \r\n");
	for(int i=8;i >=0;i--)
	{
		if(i_o & (1 << i))
			DbgPrintf(true, "  1");
		else
			DbgPrintf(true, "  0");
	}	
	DbgPrintf(true, "\r\n");
	
	return 1;
}

/**
 * \brief	Decode if output number if out of range (0-7)
 * 
 * \return	0 if out of range, 1 otherwise
 */
static int seriali_DecodeRou(void)
{
	uint32_t out_n;
	char tmpstr[10];
	
	// get relay
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &out_n);
	if(!digio_ResetOutput(out_n))
		return 0;
	
	return 1;
}

/**
 * \brief	Decode what and activate digital out
 * 
 * \return	0 if out of range, 1 otherwise
 */
static int seriali_DecodeSou(void)
{
	uint32_t out_n;
	char tmpstr[10];
	
	// get relay
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &out_n);
	if(!digio_SetOutput(out_n))
		return 0;

	return 1;
}

/**
 * \brief	Decode and get channel
 * 
 * \return 0 is fail, 1 otherwise
 */
static int seriali_DecodeRai(void)
{
	uint32_t ain_n;
	char tmpstr[10];
	
	// get relay
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &ain_n);
	
	uint32_t analog = adconv_GetChannel(ain_n);
	
	DbgPrintf(true, "Channel %d = %d\r\n", ain_n, analog);

	return 1;
}

/**
 * \brief			Function to print debug message
 * 
 * \param stsw		Mask of the message
 * 
 * \return void
 */
static void seriali_PrintStsW(uint32_t stsw)
{
	/*
	if(stsw & MOTOR_READY_TO_SWITCH_ON_BIT)	
		DbgPrintf(true, "MOTOR_READY_TO_SWITCH_ON_BIT :ON\r\n");
	else
		DbgPrintf(true, "MOTOR_READY_TO_SWITCH_ON_BIT :OFF\r\n");

	if(stsw & MOTOR_SWITCHED_ON_BIT)
		DbgPrintf(true, "MOTOR_SWITCHED_ON_BIT :ON\r\n");
	if(stsw & MOTOR_OPERATION_ENABLE_BIT)
		DbgPrintf(true, "MOTOR_OPERATION_ENABLE_BIT :ON\r\n");
	if(stsw & MOTOR_FAULT_BIT)
		DbgPrintf(true, "MOTOR_FAULT_BIT :ON\r\n");
	if(stsw & MOTOR_VOLTAGE_ENABLED_BIT)
		DbgPrintf(true, "MOTOR_VOLTAGE_ENABLED_BIT :ON\r\n");
	if(stsw & MOTOR_QUICK_STOP_BIT)
		DbgPrintf(true, "MOTOR_QUICK_STOP_BIT :OFF\r\n");
	if(stsw & MOTOR_SWITCH_ON_DISABLED_BIT)
		DbgPrintf(true, "MOTOR_SWITCH_ON_DISABLED_BIT :ON\r\n");
	if(stsw & MOTOR_WARNING_BIT)
		DbgPrintf(true, "MOTOR_WARNING_BIT :ON\r\n");
	if(stsw & MOTOR_REMOTE_BIT)
		DbgPrintf(true, "MOTOR_REMOTE_BIT :ON\r\n");
	if(stsw & MOTOR_TARGET_REACHED_BIT)
		DbgPrintf(true, "MOTOR_TARGET_REACHED_BIT :ON\r\n");
	if(stsw & MOTOR_INTERNAL_LIMIT_ACTIVE_BIT)
		DbgPrintf(true, "MOTOR_INTERNAL_LIMIT_ACTIVE_BIT :ON\r\n");
	*/
		
}

static void seriali_PrintCtrlW(uint32_t ctrlw)
{
	
}

static void seriali_SendNack(void)
{
//	udi_cdc_write_buf("NACK \r\n", 7);
	DbgPrintf(true, "\r\nNACK\r\n");
	//bufferFlush(&dbg_rxbuff);
}

static void seriali_SendAck(void)
{
//	udi_cdc_write_buf("OK \r\n", 5);
	DbgPrintf(true, "\r\nOK\r\n");
	//bufferFlush(&dbg_rxbuff);
}

static int seriali_str2int(char *str, uint32_t *val)
{
	int res = 1;
	
	if((strstr(str, "0x") != NULL) || (strstr(str, "0X") != NULL))// allora � esadecimale
	{
		if(StrHex2Uint32(&str[2], val) == 0)
			res = 0;
	}
	else  // � decimale
		*val = atoi(str);
	
	return res;
}

int StrHex2Uint32(char *str, uint32_t *vd)
{
	uint32_t aux=0;

	for(unsigned int i=0;i<strlen(str);i++)
	{
		aux = aux*16;
		if( (str[i]>='0') && (str[i]<='9') )
			aux+=str[i]-'0';
		else if((str[i]>='A') && (str[i]<='F'))
			aux+=str[i]-'A'+10;
		else if((str[i]>='a') && (str[i]<='f'))
			aux+=str[i]-'a'+10;
		else
			return 0;
	}
	
	*vd = aux;

	return 1;
}

/*! \fn int seriali_DbgGetNextParam(uint8_t * param)
    \brief Get next parameter from the rx buffer

    \param *param 
	\return > 0 if success, 0 if fail
*/
static int seriali_DbgGetNextParam(char * param)
{
	uint8_t ctmp;
	int i = 0;
	
	while(bufferGetFromFront(dbg_rxbuff, &ctmp))
	{
		if((ctmp == ',')||(ctmp == CR))
			break;
		param[i] = ctmp;
		i++;
	}
	
	param[i] = 0;

	return i;
}

static int seriali_DecodeSyn(void)
{
	uint32_t interval;
	char tmpstr[10];
	
	// get milliseconds
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &interval);
	
	co_main_StartSync(interval);
	
	return 1;
}

static int seriali_DecodeHbt(void)
{
	uint32_t interval;
	char tmpstr[10];
	
	// get milliseconds
	if(!seriali_DbgGetNextParam(tmpstr))
		return 0;
	seriali_str2int(tmpstr, &interval);
	
	co_main_SetHbeat(interval);
	
	return 1;
}

static int seriali_DecodeUpg(void)
{
	fw_Upgrade();
	
	return 1;
}

static int seriali_DecodeCin(void)
{
	collim_Init(0x30);
	
	return 1;
}

static int seriali_DecodeTrg(void)
{
	
	digio_TrigStart();

	return 1;
}

void dbgprotocol_Parse(void)
{
	uint8_t ctmp;
	char tag[4];
	uint32_t res = 0;
	
	bufferGetFromFront(dbg_rxbuff, (uint8_t*)&tag[0]);
	bufferGetFromFront(dbg_rxbuff, (uint8_t*)&tag[1]);
	bufferGetFromFront(dbg_rxbuff, (uint8_t*)&tag[2]);
	tag[3] = 0;

	bufferGetFromFront(dbg_rxbuff, &ctmp);  // per togliere la ','

	if(strcmp(tag, "sdo") == 0)
		res = seriali_DecodeSdo();
	else if(strcmp(tag, "pdo") == 0)
		res = seriali_DecodePdo();
	else if(strcmp(tag, "nmt") == 0)
		res = seriali_DecodeNmt();
	else if(strcmp(tag, "ini") == 0)
		res = seriali_DecodeIni();
	else if(strcmp(tag, "rsw") == 0)
		res = seriali_DecodeRsw();
	else if(strcmp(tag, "wcw") == 0)
		res = seriali_DecodeWcw();
	else if(strcmp(tag, "rcw") == 0)
		res = seriali_DecodeRcw();
	else if(strcmp(tag, "rsd") == 0)
		res = seriali_DecodeRsd();
	else if(strcmp(tag, "wsd") == 0)
		res = seriali_DecodeWsd();
	else if(strcmp(tag, "sho") == 0)
		res = duet_SetHome(0);
	else if(strcmp(tag, "opm") == 0)
		res = seriali_DecodeOpm();
	/* high level command function  */
	else if(strcmp(tag, "gho") == 0)
		res = seriali_DecodeGho();
	else if(strcmp(tag, "shs") == 0)
		res = seriali_DecodeShs();
	else if(strcmp(tag, "mgo") == 0)
		res = seriali_DecodeMgo();
	else if(strcmp(tag, "sps") == 0)
		res = seriali_DecodeSps();
	else if(strcmp(tag, "spa") == 0)
		res = seriali_DecodeSpa();
	else if(strcmp(tag, "sjs") == 0)
		res = seriali_DecodeSjs();
	else if(strcmp(tag, "gjf") == 0)
		res = seriali_DecodeGjf();
	else if(strcmp(tag, "gjr") == 0)
		res = seriali_DecodeGjr();
	
	else if(strcmp(tag, "shb") == 0)
		res = seriali_DecodeShb();

	else if(strcmp(tag, "syn") == 0)
		res = seriali_DecodeSyn();
	else if(strcmp(tag, "hbt") == 0)	
		res = seriali_DecodeHbt();
		
	else if(strcmp(tag, "upg") == 0)
		res = seriali_DecodeUpg();	
		
	else if(strcmp(tag, "srl") == 0)
		res = seriali_DecodeSrl();
	else if(strcmp(tag, "rrl") == 0)
		res = seriali_DecodeRrl();
	else if(strcmp(tag, "rdi") == 0)
		res = seriali_DecodeRdi();
	else if(strcmp(tag, "rai") == 0)
		res = seriali_DecodeRai();
	else if(strcmp(tag, "rou") == 0)
		res = seriali_DecodeRou();
	else if(strcmp(tag, "sou") == 0)
		res = seriali_DecodeSou();
	else if(strcmp(tag, "trg") == 0)
		res = seriali_DecodeTrg();
	else if(strcmp(tag, "cin") == 0)
		res = seriali_DecodeCin();		
		
	else if(strcmp(tag, "rtr") == 0)
		res = seriali_DecodeRtr();
	else if(strcmp(tag, "jvh") == 0)
		res = seriali_DecodeJvh();
	else
	{
		// faccio l'elenco comandi
		DbgPrintf(true, "\r\n sdo,nodeid,index,subindex,r/w,[data,len] : send SDO command to nodeid\r\n");
		DbgPrintf(true, " pdo,nodeid,n,data,len : transmit 32 bit DWORD data on PDO n to nodeid\r\n");
		DbgPrintf(true, " nmt,nodeid,cmd : NMT command cmd to nodeid\r\n");

		DbgPrintf(true, "\r\n");
		
		DbgPrintf(true, " rsw,axis : read statusword\r\n");
		DbgPrintf(true, " wcw,axis, value : write value to controlword\r\n");
		DbgPrintf(true, " rcw,axis : read controlword\r\n");
		DbgPrintf(true, " rsd,node,idx,subidx : read at idx,subidx with sdo\r\n");
		DbgPrintf(true, " wsd,node,idx,subidx,value : write at idx,subidx with sdo\r\n");
		
		DbgPrintf(true, "\r\n");
		
		DbgPrintf(true, " ini,axis,nodenumber : initialize motor drive\r\n");
		DbgPrintf(true, " sho,axis : set home at current position\r\n");
		DbgPrintf(true, " opm,axis,mode : set mode of operation\r\n");

		DbgPrintf(true, "\r\n");

		DbgPrintf(true, " gho,axisnum : int GO_HOME(int axisNum)\r\n");
		DbgPrintf(true, " shs,axisnum,speed : int SET_HOME_SPEED(int axisNum, int speed)\r\n");
		DbgPrintf(true, " mgo,axisnum,position : int GO_POS(int axisNum, int position)\r\n");
		DbgPrintf(true, " sps,axisnum,speed : int SET_POS_SPEED(int axisNum, int speed)\r\n");
		DbgPrintf(true, " spa,axisnum,acc : int SET_POS_ACC(int axisNum, int acc)\r\n");
		DbgPrintf(true, " sjs,axisnum,speed : int SET_JOG_SPEED(int axisNum, int speed)\r\n");
		DbgPrintf(true, " gjf,axisnum,step : int GO_JOG_FW(int axisNum)\r\n");
		DbgPrintf(true, " gjr,axisnum,step : int GO_JOG_REV(int axisNum)\r\n");
		
		DbgPrintf(true, " shb,axisnum,timeout : set heartbeat timeout for axis\r\n");
		
		DbgPrintf(true, " gst,axisnum : int GET_AXIS_STATUS(int axisNum)\r\n");
		DbgPrintf(true, " gms,axisnum : int GET_MOTION_STATUS(int axisNum)\r\n");
		
		DbgPrintf(true, " syn,ms : sync message every ms milliseconds, 0 to stop\r\n");
		DbgPrintf(true, " hbt,ms : heartbeat message every ms milliseconds, 0 to stop\r\n");

		DbgPrintf(true, "\r\n");
		
		DbgPrintf(true, " srl,n : activate relay n (0-5)\r\n");
		DbgPrintf(true, " rrl,n : deactivate relay n (0-5)\r\n");
		DbgPrintf(true, " rdi : read digital input\r\n");
		DbgPrintf(true, " rai,n : read analog input n (0 - 7)\r\n");

		DbgPrintf(true, " sou,n : activate out n (0-7)\r\n");
		DbgPrintf(true, " rou,n : deactivate out n (0-7)\r\n");

		DbgPrintf(true, " trg : start RX trigger\r\n");

		DbgPrintf(true, "\r\n");
		
		DbgPrintf(true, " rtr,addr,len : send remote request\r\n");
		DbgPrintf(true, " jvh,axisnum : set JVL motor actual position as home 0\r\n");

		DbgPrintf(true, "\r\n");

	}

	if(res)
		seriali_SendAck();
	else
		seriali_SendNack();
	
	// flush del buffer
	bufferFlush(dbg_rxbuff);
}

#pragma GCC diagnostic warning "-Wmissing-format-attribute"
static uint32_t buf_len_prev = 0;
void dbgprotocol_Debug(eCom_t eComm )
{
	uint8_t uc_key;
	
	
	//if(eComm == COM_DBG)
	//{
		uint32_t buf_len = bufferGetDatalength(dbg_rxbuff);
		
		if(buf_len > 0)
		{
			uc_key = bufferGetAtIndex(dbg_rxbuff, buf_len-1);
			//if(uc_key == BKS)  // backspace
			//{
				//usart_serial_putchar((Usart *)CONSOLE_UART, uc_key);
				//bufferDumpFromEnd(dbg_rxbuff, 1);  // rimuovo il backspace
				//bufferDumpFromEnd(dbg_rxbuff, 1);  // rimuovo il carattere
				//buf_len -= 2;
			//}
			//else
			//{
				// eco verso il terminale
				if(buf_len > buf_len_prev){
#warning TODO SEND OVER USART THE CHARACTERS
//					usart_serial_putchar((Usart *)CONSOLE_UART, uc_key);
					//DbgPrintf(true, "%c",uc_key);
				}
				if(uc_key == CR)
				{
#warning TODO SEND OVER USART THE LINE FEED
//					usart_serial_putchar((Usart *)CONSOLE_UART, LF);
					dbgprotocol_Parse();
					buf_len_prev = 0;
					buf_len = 0;
				}
			//}
			buf_len_prev = buf_len;
		}
	//}
}
