/*
 * manage_debug.c
 *
 *  Created on: 26 gen 2022
 *      Author: stefano
 */

#ifndef SRC_MANAGE_DEBUG_C_
#define SRC_MANAGE_DEBUG_C_
#include <settings.h>
#include <stdbool.h>

//For Retrocompatibility
#define 	DbgPrintf	debug_printf

enum{
	/*******************************/
	USED_DEBUG,
	/*******************************/
	DEBUG_STEUTE,
	DEBUG_BYNARY_PROTOCOL,
	DEBUG_CM4,
	DEBUG_CO_MICRODEP,
	DEBUG_CO_NMT,
	DEBUG_IPC,
	//-------------------------------

	/*******************************/
	NOT_USED_DEBUG,
	/*******************************/
	DEBUG_NET,
	//-------------------------------
};

#define	MAX_BYTE_FOR_IPC_DEBUG		240
#define	MAX_BYTE_FOR_DEBUG_QUEUE	1024
#define	MINIMUM_DEBUG_BYTE			sizeof( uint8_t )

/// single module debug printf enable

enum{
	CO_MICROD=0,	//!< Debug printf for co_microdep.c module
	CO_NMT,			//!< Debug printf for co_nmt.c module
	CO_PDO,			//!< Debug printf for co_pdo.c module
	CO_SDO,			//!< Debug printf for co_sdo.c module
	COLLIM,			//!< Debug printf for collimatore.c module
	FW_UPG,			//!< Debug printf for fw_upgrade.c module
	DUET,			//!< Debug printf for duet.c module
	BINARY_PROTO,	//!< Debug printf for binaryprotocol.c module
	COMMAND,		//!< Debug printf for command.c module
	ADCONV,			//!< Debug printf for adconv.c module
	DIGIO,			//!< Debug printf for dig_io.c module
	MIS23,			//!< Debug printf for mis23x.c module
	SOCK,			//!< Debug printf for socked.c module
	MOTOR,			//!< Debug printf for motors.c module
	LINACT,			//!< Debug printf for linact.c module
	TIMESTAMP_ENABLE,	//! Insertion of Timestamp before debug message
	DOSIMETER_DBG,	//! Insertion of dosimetro.c module
	STILLE_DBG,		//! Insertion of Stille.c module
	INTERP_DBG,		//! Insertion of MotorPower Interpolation debug
	BRAKE_DBG,		//! Insertion of Motor Brake managemente
	LAST_DBG
};

#define DBG_CO_MICROD				(1 && cmd_printf_enable[CO_MICROD])	//!< Debug printf for co_microdep.c module
#define DBG_CO_NMT					(1 && cmd_printf_enable[CO_NMT])	//!< Debug printf for co_nmt.c module
#define DBG_CO_PDO					(1 && cmd_printf_enable[CO_PDO])	//!< Debug printf for co_pdo.c module
#define DBG_CO_SDO					(1 && cmd_printf_enable[CO_SDO])	//!< Debug printf for co_sdo.c module
#define DBG_COLLIM					(1 && cmd_printf_enable[COLLIM])	//!< Debug printf for collimatore.c module
#define DBG_FW_UPG					(1 && cmd_printf_enable[FW_UPG])	//!< Debug printf for fw_upgrade.c module
#define DBG_DUET					(1 && cmd_printf_enable[DUET])	//!< Debug printf for duet.c module
#define DBG_BINARY_PROTO			(1 && cmd_printf_enable[BINARY_PROTO])	//!< Debug printf for binaryprotocol.c module
#define DBG_COMMAND					(1 && cmd_printf_enable[COMMAND])	//!< Debug printf for command.c module
#define DBG_ADCONV					(1 && cmd_printf_enable[ADCONV])	//!< Debug printf for adconv.c module
#define DBG_DIGIO					(1 && cmd_printf_enable[DIGIO])	//!< Debug printf for dig_io.c module
#define DBG_MIS23					(1 && cmd_printf_enable[MIS23])	//!< Debug printf for mis23x.c module
#define DBG_SOCK					(1 && cmd_printf_enable[SOCK])	//!< Debug printf for socked.c module
#define DBG_MOTOR					(1 && cmd_printf_enable[MOTOR])	//!< Debug printf for motors.c module
#define DBG_LINACT					(1 && cmd_printf_enable[LINACT])	//!< Debug printf for linact.c module
#define	DBGPRINT_TIMESTAMP_ENABLE	(1 && cmd_printf_enable[TIMESTAMP_ENABLE])	//!< Debug printf for timestamp suffix
#define	DBGPRINT_DOSIMETER			(1 && cmd_printf_enable[DOSIMETER_DBG])	//!< Debug printf for dosimetro.c module
#define	DBGPRINT_STILLE				(1 && cmd_printf_enable[STILLE_DBG])	//!< Debug printf for Stille.c module
#define DBG_INTERP					(1 && cmd_printf_enable[INTERP_DBG])	//!< Debug printf for MotorPower Interpolation
#define DBG_BRAKE					(1 && cmd_printf_enable[BRAKE_DBG])	//!< Debug printf for Motor Brake managemente


#ifdef	CORE_CM7
	//Storage of a queue
	typedef struct{
		QueueHandle_t QueueHandle;
		uint8_t QueueBuffer[ MAX_BYTE_FOR_DEBUG_QUEUE * sizeof( uint8_t ) ];
		StaticQueue_t QueueControlBlock;
	}debug_static_buffer_t;
#endif

void MakeDebugQueue();
void debug_printf(bool debug, const char *s, ...);
void DebugTask(void const * argument);

extern char cmd_printf_enable[LAST_DBG];


#endif /* SRC_MANAGE_DEBUG_C_ */
