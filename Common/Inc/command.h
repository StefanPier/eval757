/**
\file command.h
\brief Header libreria per la gestione di comandi, con associazione a funzione di esecuzione
\author Lorenzo Giardina <lorenzo@lorenzogiardina.com>
\version 1.0
\date Last revision: 1/1/2016
\note
This code is distributed under the GNU Public License which can be found at http://www.gnu.org/licenses/gpl.txt
*/

#ifndef COMMAND_H
#define COMMAND_H

#include "buffer.h"
#include "protocol.h"

//! \param N_COMMAND Definizione numero MAX di comandi disponibili (0-N_COMMAND)
#define N_COMMAND				100		//Numero di comandi totali

#define N_TRUNK					COM_LAST		//Indica il numero di canali su cui gestire il flusso dati (tipicamente numero di canali seriali usati)

#ifdef CORE_M7
#define N_MAX_BUFFER_COMANDI_RX	20		//Numero massimo di comandi nel buffer ricezione per cm7
#else
#define N_MAX_BUFFER_COMANDI_RX	2		//Numero massimo di comandi nel buffer ricezione per cm4
#endif
#ifdef NO_BLUETOOTH
	#define N_MAX_DATA_LENGHT		40  	//Numero massimo dei dati nel campo dati del pacchetto stxetx
#else
	#define N_MAX_DATA_LENGHT		240  	//Numero massimo dei dati nel campo dati del pacchetto stxetx
#endif


//! Descrizione del pacchetto di ACK
//! Nel campo STATUS è presente il numero progressivo del pacchetto che deve finire nella struttura ack_t campo packet_n
#define ACK_ENABLED				0		//abilita l'invio automatico del pacchetto di ACK al ricevimento del comando

#define ACK_PACKET				0x62	//Identificativo nel "TYPE" Pacchetto di ACK

#define ACK_RECEIVED			127		//Valore di ritorno si Process_Packet() alla ricezione di un ACK

#define ACK_TIMEOUT				250		//Timout per ricezione ACK in ms

#define MAX_CMD_RETRY           1      //Numero di tentativi in caso di non ricezione ACK

//! Struttura dati per ACK
//! \param ack_received Se a 1 indica che è stato ricevuto un ACK
//! \param packet_n Indica a che numero di pacchetto si riferice
typedef struct
{
	unsigned char ack_received;
	unsigned int packet_n;
}ack_t;

//! Struttura dati per i comandi
//! \param command comando
//! \param function puntatore a void, usato per puntare la funzione da eseguire
typedef struct command_t
{
	unsigned char command;
	unsigned char occuped;			//flag introduced to allow command != index on command variable (type command_t)
	void (*function) (unsigned char trunk, unsigned char* ptr_data);
}command_t;

//! Struttura dati per i comandi in ricezione o trasmissione
//! \param status Status byte
//! \param type	Tipo dato --> Usato molte volte per identificare il comando
//! \param lenght Lunghezza del campo dati associati al comando
//! \param checksum checksum ad 8 bit equivalente alla somma di tutti i byte escluso STX e ETX
typedef struct command_struct_t
{
	//[STX][status][type][length][user data...][checksum][ETX]
	unsigned char status;
	unsigned char type;
	unsigned char lenght;
	unsigned char data[N_MAX_DATA_LENGHT];
	unsigned char checksum;
}command_struct_t;

//! Struttura dati per i comandi in ricezione o trasmissione
//! \param n_cmd_present Numero di comandi nella lista presenti
//! \param comando Singolo comando secondo la struttura command_struct_t
//! \param dataout_func Puntatore a funzione di uscita dato
//! \param tx_enable Puntatore a funzione di abilitazione TX
//! \param rx_enable Puntatore a funzione di abilitazione RX
//! \param datain_func Puntatore a funzione che restituisce il dato nel buffer
//! \param ms_delay_func Puntatore a funzione che esegue un delay di 1ms
typedef struct fifo_command_t
{
	unsigned char n_cmd_present;
	command_struct_t comando[N_MAX_BUFFER_COMANDI_RX];
	cBuffer* (*datain_func) (void * arg);
	void (*dataout_func)(unsigned char data, void *arg);
	void (*tx_enable)(int);
	void (*rx_enable)(int);
	void (*ms_delay_func) (void);
	void * arg;
}fifo_command_t;

//! Inizializzazione sistema di pacchettizzazione STXETX e associazione funzioni base
unsigned char command_system_init (unsigned char n_trunk,void (*out_func)(unsigned char data, void *arg), cBuffer* (*getbyte) (void *arg),void (*tx_enable_func)(int),void (*rx_enable_func)(int),void (*ms_delay) (void), void *arg);

//! Invio dati generico
void command_send(unsigned char n_trunk, unsigned char status, unsigned char type, unsigned char datalength, unsigned char* dataptr);

//! Invio dati generico con attesa ack
unsigned char command_send_attend_ack(unsigned char n_trunk, unsigned char status, unsigned char type, unsigned char datalength, unsigned char* dataptr);

//! Puntatore a funzione di esecuzione
//! \return void
//! \param ptr_data {Puntatore a campo dati, vettore dove sono contenuti i parametri che servono alla funzione finale}
//void (*puntatore_funzione_exec) (unsigned char* ptr_data);



//! Funzione di memset a 0 di tutte le struttiure comando
void command_reset(void);

//! Funzione di inizializzazione ed associazione COMANDO -> FUNZIONE DA ESEGUIRE
//! \return 0 se ERRORE (campo comando fuori range), 1 se OK
//! \param comando {Numero corrispondente al comando da inizializzare (da 0 a N_COMMAND)
//! \param ptr_data {Puntatore a campo dati, vettore dove sono contenuti i parametri che servono alla funzione finale}
unsigned char command_init (unsigned char comando,void (*function_cmd) (unsigned char trunk, unsigned char* ptr_data)); //*********=====**********====********

//! Funzione di esecuzione FUNZIONE associata al comando
//! \return 0 se ERRORE (campo comando fuori range), 1 se OK
//! \param comando Numero corrispondente al comando da inizializzare (da 0 a N_COMMAND)
//! \param ptr_data Puntatore a campo dati, vettore dove sono contenuti i parametri che servono alla funzione finale
unsigned char exe_cmd_func (unsigned char comando, unsigned char trunk, unsigned char* ptr_data );

//! Funzione esecuzione funzione associata al comando con controllo presenza nel buffer comandi di un comando valido
void exe_cmd (unsigned char n_trunk);

//! Funzione che elabora un pacchetto dati e lo inserisce nel buffer di ricezione
//! \return Numero di pacchetto nella lista comandi inserito se errore restituisce -1 o ACK_PACKET se riceve un pacchetto ACK
int process_packet (unsigned char n_trunk);

#endif
