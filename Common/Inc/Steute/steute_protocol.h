/*
 * steute_protocol.h
 *
 *  Created on: Jan 24, 2022
 *      Author: stefano
 */

#ifndef INC_STEUTE_STEUTE_PROTOCOL_H_
#define INC_STEUTE_STEUTE_PROTOCOL_H_

#ifdef	STEUTE_ON_CONTROL
	#ifdef	STEUTE_CONTROL_ON_CM7
		#include "cmsis_os.h"
	#endif
#endif

#include "buffer.h"

#define		STEUTE_START_BYTE		0xAA
#define		STEUTE_CR_BYTE			0x0D
#define		STEUTE_LF_BYTE			0x0A

#define		STEUTE_START_POS	0
#define		STEUTE_LENGHT_POS	1


////////////////	FIXED COMMANDS	/////////////////////////////
//The following telegram sets the receiver to pairing mode:
#define STEUTE_ACTIVATE_PAIRING	0xAA0400115C0D0D0A
//If the Connection LEDs of transmitter and receiver blink red fast during pairing procedure, the pairing is
//carried out after this telegram has been sent.
#define	STEUTE_CONFIRM_PAIRING		0xAA0400126C6E0D0A
//Hereby, all pairing information is deleted in the receiveR
#define	STEUTE_DELETE_PAIRING		0xAA0400137C4F0D0A
//Ensures that the switch is not set to Sleep Mode again. If the switch is not active when this command is
//sent, it stays active with the next wake-up
#define	STEUTE_STAY_ACTIVE			0xAA040511A3F80D0A
//Makes the footswitch (if there is a connection) disconnect the connection and go to Sleep Mode, but only
//after there is no pedal actuation and KeyTime has expired
#define	STEUTE_GO_SLEEP				0xAA040512939B0D0A

//Activation of configuration mode
#define	STEUTE_ACTIVE_CONF_MODE		0xAA0444018F340D0A
//Terminate configuration mode
#define	STEUTE_TERMINATE_CONF_MODE	0xAA0444009F150D0A

////////////////	FIXED COMMANDS	END//////////////////////////

////////////////	FIXED REPLIES	//////////////////////////////
#define STEUTE_PAIRING_STARTED		0xAA0B001002000000117D6E0D0A
#define	STEUTE_PAIRING_NOT_STARTED	0xAA0B0010C80000001108780D0A

#define	STEUTE_PAIRING_CONFIRMED		0xAA0B001002000000124D0D0D0A
#define	STEUTE_PAIRING_NOT_ALLOWED		0xAA0B0010C800000012381B0D0A

#define	STEUTE_PAIRING_DELETED			0xAA0B001002000000135D2C0D0A
#define	STEUTE_PAIRING_DEL_IMPOSSIBLE	0xAA0B0010C800000013283A0D0A

#define	STEUTE_STAYACTIVE_SET		0xAA0B00100200000511829B0D0A
#define	STEUTE_STAYACTIVE_OT_SET	0xAA0B0010C800000511F78D0D0A

#define	STEUTE_GO_SLEEP_SET			0xAA0B00100200000512B2F80D0A
#define	STEUTE_GO_SLEEP_NOT_SET		0xAA0B00100200000512B2F80D0A

#define	STEUTE_CONFMODE_LOCAL		0xAA064801000166E10D0A	//Configuration mode has been started (switch has no radio connection)
#define	STEUTE_CONFMODE_RADIO		0xAA064801000346A30D0A	//Configuration mode has been started (radio connection to switch is active

#define	STEUTE_CONFMODE_TERMINATED	0xAA064800000041F00D0A
////////////////	FIXED REPLIES END	//////////////////////////

#define STEUTE_MAXRXPACKETSIZE	256
#define	STEUTE_HEADERLENGTH		3	//Start+Lenght+id
#define STEUTE_CRC_LEN			2	// CRC16 CCITT, polynomial 0x1021, Init 0x0000
#define STEUTE_ETX_LEN			2	// Carriage return + line feed
#define	STEUTE_TRAILERLENGTH	(STEUTE_CRC_LEN+STEUTE_ETX_LEN)	//2CRC + Carriage + Line Feed
#define	STEUTE_TOUT		3000		//ms of timeout
#define STEUTE_RSSI_UNDEFINED_VALUE	-127

#define TRUE_STEUTE_PACKET			1		//Define True
#define FALSE_STEUTE_PACKET			0		//Define False

//Define for mask input on gpio input
#define	STEUTE_LEFT_PEDAL_MASK			(0x01<<0)
#define	STEUTE_CENTRAL_PEDAL_MASK		(0x01<<1)
#define	STEUTE_RIGHT_PEDAL_MASK			(0x01<<2)
#define	STEUTE_LEFT_UPPER_PEDAL_MASK	(0x01<<3)
#define	STEUTE_RIGHT_UPPER_PEDAL_MASK	(0x01<<4)

//USED ON OUR PROTOCOL ONLY AS VALUES IN THE STEUTE STATE VARIABLE (cmd_send)
enum{
	STEUTE_ANY_CMD_SEND = 0x00,
	STEUTE_STARTPAIR_CMD_SEND = 0x01,
	STEUTE_CONFIRMPAIR_CMD_SEND = 0x02,
	STEUTE_TERMINATEPAIR_CMD_SEND =0x03,
};

//USED ON OUR PROTOCOL ONLY AS VALUES IN THE STEUTE STATE VARIABLE (_pedal)
enum{
	STEUTE_PEDAL_UNDEF,
	STEUTE_PEDAL_OFF,
	STEUTE_PEDAL_ON,
};

typedef enum {
	STEUTE_STATUS_AND_PAIR_ID = 0x00,
	STEUTE_STEUTEACTIVE_SLEEP_ID = 0x05,
	STEUTE_SWITCHING_ID = 0x18,
	STEUTE_CONFMODE_DATA_ID = 0x30,
	STEUTE_CONFMODE_STATUS_ID = 0x38,
	STEUTE_CONFMODE_ACTIVATION_ID = 0x44,
	STEUTE_CONFMODE_ID = 0x48,
}steute_telegram_id_t;

//Switching data and status data struct
typedef struct{
	uint8_t start_byte;
	uint8_t length;
	uint8_t id;
}__packed steute_rxheader_struct_t;

typedef enum{
	STEUTE_UNPRESSED = 0,
	STEUTE_PRESSED,
}switch_steute_enum_t;

//Status data
typedef enum{
	STEUTE_ANY_REPLY_RX = 0x00,	//USED ON OUR PROTOCOL ONLY AS DEFAULT VALUE IN THE STEUTE STATE VARIABLE (status_code)
	STEUTE_RX_STARTED = 0x01, //= Reiceiver has been started
	STEUTE_CMD_RX = 0x02, //= Command has been received
	STEUTE_CMD_IMP = 0xC8, //= Execution of command impossible
	STEUTE_CMD_UNK = 0xC9, //= Unknown command
	STEUTE_INV_PAIR = 0xCA, //= Invalid pairing ID
	STEUTE_CRC_ERR = 0xF0, // = CRC error
	STEUTE_TOO_LONG = 0xF1, //= Length of message exceeded
	STEUTE_TOO_SMALL = 0xF2, //= Length of message not achieved
	STEUTE_NOT_ENOUGHT = 0xF3, //= Length specification in message incorrect
	STEUTE_SYS_ERR = 0xFF, //= System error
} status_enum_t;

typedef struct{
	uint8_t status_id;			//Pairing-ID
	uint8_t status_code;		//status enum type
	uint8_t status_detail[4];	//	Additional information on status, if required.
	uint16_t crc;
	uint8_t cr;		//carriage return
	uint8_t lf;		//line feed
} __packed steute_status_struct_t;

typedef struct{
	steute_rxheader_struct_t header;
	steute_status_struct_t status;		//line feed
} __packed steute_status_complete_t;

//Switch Data

/////////////////	Switch Data Status Detail	/////////////////////////////////
typedef enum {
	STEUTE_STATUS_RESERVED_FOR_DEBUG = 0x00,
	STEUTE_STATUS_BATTERY = 0x01,
	STEUTE_STATUS_ACCELEROMETERYX = 0x02,
	STEUTE_STATUS_ACCELEROMETERZ = 0x03,
	STEUTE_STATUS_BATTERY_VOLT = 0x04,
	STEUTE_STATUS_POWER = 0x05,
	STEUTE_STATUS_bit015 = 0x06,
}switch_status_id_possibility_t;

//STEUTE_STATUS_BATTERY byte 6 and 7 structure:
typedef struct{
	uint8_t battery_led_state:4;
	uint8_t tilted_position:2;
	uint8_t locked_position:2;
	uint8_t battery_charge;	//0-100
}__packed STEUTE_STATUS_BATTERY_t;

//STEUTE_STATUS_ACCELEROMETERYX byte 6 and 7 structure:
typedef struct{
	uint8_t accelerometer_y;
	uint8_t accelerometer_x;
}__packed STEUTE_STATUS_ACCELEROMETERYX_t;

//STEUTE_STATUS_ACCELEROMETERZ byte 6 and 7 structure:
typedef struct{
	uint8_t reserved;
	uint8_t accelerometer_z;
} __packed STEUTE_STATUS_ACCELEROMETERZ_t;

//STEUTE_STATUS_BATTERY_VOLT byte 6 and 7 structure:
typedef struct{
	uint8_t cable_radio:1;
	uint8_t charging:1;
	uint16_t battery_voltage:14;	//0-5000mV
}__packed STEUTE_STATUS_BATTERY_VOLT_t;

//STEUTE_STATUS_POWER byte 6 and 7 structure:
typedef struct{
	uint8_t power_level_0;	//Power level 0: -23dBm, 1: 0dBm, 2: 1dBm, ... , 8: 7dBm
	int8_t rssi_level;		//RSSI level in dBm: Value range -127 ... 0.
							//-127 = 0x7F = undefined or no measured value available
}__packed STEUTE_STATUS_POWER_t;

//STEUTE_STATUS_POWER byte 6 and 7 structure:
typedef struct{
	uint8_t digital_bit15:1;	//Digital In 1-16. Unvalued states (not influenced by tilt- or lock-status)
	uint8_t digital_bit14:1;	//Bit 0 = Input 1 ... Bit 15 = Input 16
	uint8_t digital_bit13:1;	//For each bit: processor GPIO Port | /(I/O expander bit)
	uint8_t digital_bit12:1;
	uint8_t digital_bit11:1;
	uint8_t digital_bit10:1;
	uint8_t digital_bit9:1;
	uint8_t digital_bit8:1;
	uint8_t digital_bit7:1;
	uint8_t digital_bit6:1;
	uint8_t digital_bit5:1;
	uint8_t digital_bit4:1;
	uint8_t digital_bit3:1;
	uint8_t digital_bit2:1;
	uint8_t digital_bit1:1;
	uint8_t digital_bit0:1;
}__packed STEUTE_STATUS_bit015_bit_t;

typedef union{
	uint16_t value;
	STEUTE_STATUS_bit015_bit_t bit;
}__packed STEUTE_STATUS_bit015_t;

typedef union {
	STEUTE_STATUS_BATTERY_t status_battery;
	STEUTE_STATUS_ACCELEROMETERYX_t status_accelerometer_yx;
	STEUTE_STATUS_ACCELEROMETERZ_t status_accelerometer_z;
	STEUTE_STATUS_BATTERY_VOLT_t status_batter_volt;
	STEUTE_STATUS_POWER_t status_power;
	STEUTE_STATUS_bit015_t status_bit0_15;
}__packed union_switch_status_t;

typedef struct{
	uint8_t status_id;		//switch_status_id_possibility_t
	union_switch_status_t status_data;
}__packed swuitch_data_status_detail_t;

/////////////////	Switch Data Status Detail END	/////////////////////////////////

/////////////////	Switch Data Detail	/////////////////////////////////

typedef struct{
	uint8_t gpio_input;			//Bit 0 = Input 1 ... Bit 7 = Input 8, read by processor GPIO port
	uint8_t gpio_input_inveted;	//Bit 0 = Input 1 ... Bit 7 = Input 8, read by processor GPIO, inverted
	uint8_t i2c_input;			//Bit 0 = Input 8 ... Bit 7 = Input 1, read by I²C I/O expander
	uint8_t i2c_input_inveted;	//Bit 0 = Input 8 ... Bit 7 = Input 1, read by I²C I/O expander, inveted
	uint16_t reserved;
	uint8_t normal_relay_state;		//Rated state of output relays. Each bit represents the state of a standard relay (i.e.
									//non-monitored relay). 0 = off, 1 = on
	uint8_t validate_relay_state;	//State of Validate relays. Each bit represents the state of a standard relay (i.e.
									//monitored relay). 0 = off, 1 = on.
	int8_t rssi_level;		//RSSI level in dBm: Value range -127 ... 0.
							//-127 = 0x7F = undefined or no measured value available
	uint8_t time_gap;		//Time gap to previous telegram (ms)
}__packed swuitch_data_detail_t;
/////////////////	Switch Data Detail END	/////////////////////////////////

typedef struct{
	steute_rxheader_struct_t header;
	uint8_t reserved;
	uint8_t debug_code;
	swuitch_data_status_detail_t status_detail;
	uint8_t packet_counter;
	swuitch_data_detail_t data_detail;
	uint16_t crc;
	uint8_t cr;		//carriage return
	uint8_t lf;		//line feed
}__packed steute_switch_struct_t;

uint16_t CRC16_8(cBuffer * cbuffer_to_crc, uint32_t size);
int process_steute_packet (cBuffer *,uint8_t *);
void Analyze_steute_packet (uint8_t *);

#endif /* INC_STEUTE_STEUTE_PROTOCOL_H_ */
