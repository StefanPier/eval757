/*
 * steute.h
 *
 *  Created on: Jan 24, 2022
 *      Author: stefano
 */

#ifndef INC_STEUTE_STEUTE_H_
#define INC_STEUTE_STEUTE_H_

#include "settings.h"

#include "buffer.h"
#include "steute_protocol.h"

#ifdef	STEUTE_ON_CONTROL
	#ifdef	STEUTE_CONTROL_ON_CM7
		#include "cmsis_os.h"
	#endif
#endif

#define 	MAX_SOCKET_CONNECTION			5
#define 	MAX_BYTE_FOR_STEUTE_QUEUE		1024	//512
#define 	MAX_BYTE_FOR_STEUTE_MSG			128
#define 	MINIMUM_CALLBACK_BYTE			1

//Steute Protocol
#define	MINIMUM_STEUTE_BYTE			sizeof( uint8_t )

#ifdef	STEUTE_ON_CONTROL
	#ifdef	STEUTE_CONTROL_ON_CM7
		//Storage of a queue
		typedef struct{
			QueueHandle_t QueueHandle;
			uint8_t QueueBuffer[ MAX_BYTE_FOR_STEUTE_QUEUE *  MINIMUM_STEUTE_BYTE];
			StaticQueue_t QueueControlBlock;
		}steute_static_buffer_t;
	#endif
#endif

void MakeSteuteQueue();
void SteuteProtocolTask(void const * argument);

enum{
	STEUTE_NOT_STARTED=0x00,
	STEUTE_STARTED=0x01,
};

typedef struct {
	uint8_t started;
	uint8_t cmd_send;
	//Status section
	uint8_t status_code;		//used even for cmd_reply
	uint8_t status_detail[4];	//used even for cmd_reply
	//Switch section
	uint8_t packet_counter;
	int8_t rssi;
	uint8_t left_pedal;
	uint8_t central_pedal;
	uint8_t right_pedal;
	uint8_t left_upper_pedal;
	uint8_t right_upper_pedal;
}__packed steute_state_t;

//extern steute_state_t steute_state;

#endif /* INC_STEUTE_STEUTE_H_ */
