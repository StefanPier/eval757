/*
 * binaryprotocol.h
 *
 *	Header file del
 *
 * Created: 28/02/2022 11:01:35
 *  Author: Stefano
 */ 
#ifndef BINARYPROTOCOL_H_
#define BINARYPROTOCOL_H_

#include "command.h"

/** @name Protocol command enumerator
 */
///@{
enum lista_comandi {
	FWVERSION_CMD = 0,				//0
	// Motors
	MOTOR_HOME_CMD,					//1
	SET_HOME_SPEED_CMD,				//2
	SET_POSITION_CMD,				//3
	SET_SPEED_CMD,					//4
	SET_ACC_CMD,					//5
	SET_JOG_SPEED_CMD,				//6
	GO_JOG_FWD_CMD,					//7
	GO_JOG_REV_CMD,					//8
	GET_MOTION_STATUS_CMD,			//9
	GET_MOTOR_STATUS_CMD,			//10
	QUICK_STOP_CMD,					//11
	M_STOP_CMD,						//12
	GET_ACTUAL_POS_CMD,				//13
	MOTOR_INIT_CMD,					//14
	MOTOR_SET_MIN_POS,				//15
	MOTOR_SET_MAX_POS,				//16
	MOTOR_SET_HOME,					//17
	// Collimator
	COLLIM_INIT_CMD,				//18
	COLLIM_GET_SIDE_POS_CMD,		
	COLLIM_SET_SIDE_POS_CMD,		
	COLLIM_GET_FILTER_POS_CMD,		
	COLLIM_SET_FILTER_POS_CMD,		//22
	// I_O
	REALY_ON,						//23
	RELAY_OFF,		
	RELAY_STATUS,				
	OUT_ON,							//26
	OUT_OFF,							
	READ_DIN,						
	READ_AIN,						//29
	// Collimator Updater
	UPG_START,						//30
	UPG_DBLOCK,						
	UPG_DLAST,						//32
	//sorgente e flatpanel
	TRIGGER_SET, //(33) per abilitare o disabilitare il trigger ai raggi
	TRIGGER_MODEFLATPANEL_SET, //(34) per settare il modo di funzionamento (CT, FL, DR, DARKFRAMES o NULL) e se si deve attendere il flat panel ready
	TRIGGER_TIME_SET, //(35) per settare i tempi di trigger (pannello, pause, raggi)
	TRIGGER_START, //(36) per abilitare o disabilitare l'attesa del ready dal flat panel
	//aggiunti motori
	SET_DEC_CMD, //(37) comando per settare decelerazione motore
	GET_ACTUAL_CURRENT, //(38) comando per chiedere corrente attuale assorbita dal motore
	//sorgente e flatpanel
	TRIGGER_MAXTRIGGERPERSESSION_SET, //(39) per settare il numero massimo di scatti per sessione
	TRIGGER_STOP, //(40)
	GET_TRIGGER_ERROR, //41
	TRIGGER_FP_NOW, //42
	COLLIM_LASER_CONTROL, //43
	COLLIM_CHANGE_VELOCITY, //44
	RW_WEELCOUNTER, //45
	COLLIM_GET_STATUS,	// 46
	// comando per riavviare il fw in modalit� bootloader
	FW_UPGRADE,			// 47
	// comandi per impostare la soglia di corrente massima ed il tempo massimo che pu� essere superata in ms
	MOTOR_SET_CURRENT_LIMIT,			// 48 soglia limite in mA per andare in fault
	MOTOR_SET_CURRENT_LIMIT_TIME,		// 49 limite di tempo per il quale pu� essere superata la soglia in ms
	MOTOR_GET_RATED_CURRENT,			// 50 corrente di targa del motore in mA
	
	DR_TIME_SET,			// 51 da eliminare perch� era solo per prova
	MOTOR_SET_ZERO,			// 52 per fare lo zero ai motori JVL
	MOTOR_RESET_ERROR,		// 53 reset bits di errore per ora solo motori JVL
	MOTOR_GET_ERRORS,		// 54
	MOTOR_GET_WARNS,		// 55
	
	MOTOR_READ_OBJ,			//56
	MOTOR_WRITE_OBJ,		//57

	RESET_POSITION_CMD,		//58
	
	GET_ANALOG_CHANNEL,   // 59

	M_SYNC_SERVICEMODE_CMD=64,			//64 - Service mode per motori sincroni


	DBG_CTRL = 66,			//66	Enable/Disable Debug Printf
	
	DOSIM_INIT_CMD,			//67 	Init Dosimeter
	DOSIM_READPAR_CMD,		//68 	Read a readable parameter from Object dictionary of the dosimeter
	DOSIM_WRITEPAR_CMD,		//69 	Write a writable parameter from Object dictionary of the dosimeter
	DOSIM_STATUS_CMD=96,	//96 	Dosimeter status
	DOSIM_VALUE_CMD=97,		//97	Chiede i valori di interesse al dosimetro: valore e magnitude



	
	//-- IMGLS021 Bluetooth led board --//
	SET_IPADDR=70,					//Not Implemented Here, only on Pegaso
	BT_SET_PROFILE=71,				//62	Can set profile, and relays outputs
	BT_SAVE_PROFILE=72,			//63	Save a profile
	BT_SET_LIMITS=73,				//64	Set control limits of profile			
	BT_CONNECTION = 74,				//Reconect to bluetooth
	BT_INQUIRY = 75,				//Find BT device
	BT_RESET_DEFAULT =76,
	BT_FW_VERSION = 77,
	BT_LIMIT_PROFILE =78,
	BT_GET_STATUS=82,				//82	Get BT Status
	BT_SET_RELAYS=86,				//87	Set BT Relay
	
	//	LIFTER SECTION
	LETTINO_STOPCONN_CMD =79,		//79 - Stop Connectio e deinit Lettino
	REBOOT_CMD,						//80 - Main Reboot command
	BOOT_VERS_CMD,					//81 - Restituisce la versione del bootloader (utile dal Bootloader 2.0.2 in avanti)	

	STARTUS_CMD=83,				//83 - Comanda lo START di echo ultrasonici ad intervalli di minimi di 500ms
	STOPUS_CMD,					//84 - Comanda lo STOP di echo ultrasonici
	STATUSUS_CMD,				//85 - Comando per inviare le misure dai sensori US Attivati

	MANAGEBRAKE_CMD=87,			//87 - Comando per gestire il freno
	
	RESET_ERROR_CMD=88,			//88 - Comando per annullare errore MOT_CRUSHING_ERROR
	AC_ENABLE_CMD=95,			//95 - Comando per disabilitare/abilitare sensore Anti-Crushing

	
	LAST_BIN_CMD=98,			// Primo comando disponibile
};
///@}

//!< Initialize the protocol callback list
void binaryproto_function_init (void);

#define 	MAX_PARAMS_NUM	(N_MAX_DATA_LENGHT/sizeof(uint32_t))-1)

//Structure of data for each command
typedef struct {
	uint32_t num_params;
	uint32_t parameters[(MAX_PARAMS_NUM];
} generic_cmd_t;

#endif /* BINARYPROTOCOL_H_ */
