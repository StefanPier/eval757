/*! \file timer.h \brief System timer management. */
//*****************************************************************************
//
// File Name	: 'timer.h'
// Title		: System and ethernet timer management.
// Author		: Author: Alessandro
// Company		: Qprel s.r.l.
// Created		: 01/04/2015
// Revised		: 
// Version		: 1.0
// Target MCU	: SAM4E
//
/// \par Overview
///		Use the SAM4E system timer to generate the system tick of 1 ms.  
///		Contains the function to initialize the system timer, get the current system
///     tick, create and test general pourpose timer.
//
//*****************************************************************************
#ifndef TIMER_H_
#define TIMER_H_

#include <stdint.h>
#include <stdbool.h>

//! create a timer that elapse after time_ms ms
uint32_t timer_TimerSet(uint32_t time_ms);
//! check if timer has expired
bool timer_TimerTest(uint32_t timer);
//! return the system tick
uint32_t timer_TimerGetSysTick(void);

#endif /* TIMER_H_ */
