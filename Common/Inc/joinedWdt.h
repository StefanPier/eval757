/*
 * joinedWdt.h
 *
 * Created: 27/08/2020 12:15:04
 *  Author: user
 */ 


#ifndef JOINEDWDT_H_
#define JOINEDWDT_H_

#include "main.h"


uint32_t init_joinedWdt(void);

void joinedWdt_restart(void);

#endif /* JOINEDWDT_H_ */
