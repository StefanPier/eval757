/*! \file timer.c \brief System timer management. */
//*****************************************************************************
//
// File Name	: 'timer.c'
// Title		: System and ethernet timer management.
// Author		: Author: Alessandro
// Company		: Qprel s.r.l.
// Created		: 01/04/2015
// Revised		:
// Version		: 1.0
// Target MCU	: SAM4E
//
/// \par Overview
///		Use the SAM4E system timer to generate the system tick of 1 ms.
///		Contains the function to initialize the system timer, get the current system
///     tick, create and test general purpose timer.
//
//*****************************************************************************
#include <timer.h>

#ifdef CORE_CM7
	#include "cmsis_os.h"
	#define 	GetTick()	osKernelSysTick()
#endif
#ifdef CORE_CM4
	#define 	GetTick()	HAL_GetTick()
#endif

/**
 * \fn uint32_t timer_TimerSet(uint32_t time_ms)
 * \brief Initialize a timer variable for time measurement 
 *
 * Set the timer value in system ticks, at which the timer will elapse
 *  
 * \param uint32_t time_ms Timer interval in millisecond
 * \return the system tick value when the timer will elapse
 * 
 */
uint32_t timer_TimerSet(uint32_t time_ms)
{
	return HAL_GetTick() + time_ms;
}

/**
 * \fn bool timer_TimerTest(uint32_t timer)
 * \brief Check for elapsed timer 
 *
 * Check if the timer is elapsed
 *  
 * \param uint32_t time_ms Timer variable obtained from micro_TimerSet(uint32_t time_ms)
 * \return true if the timer elapsed, false if not.
 * 
 */
bool timer_TimerTest(uint32_t timer)
{
	if(timer <= HAL_GetTick())
		return true;
	else
		return false;
	
}

/**
 * \fn uint32_t timer_TimerGetSysTick(void)
 * \brief Return the system tick value 
 *
 * \return The system ticks value
 * 
 */
uint32_t timer_TimerGetSysTick(void)
{
	return HAL_GetTick();
}

