//
//
// Revised by: Lorenzo Giardina <lorenzo.giardina@virgilio.it>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
/*! \file buffer.c \brief Multipurpose byte buffer structure and methods. */
//*****************************************************************************
//
// File Name	: 'buffer.c'
// Title		: Multipurpose byte buffer structure and methods
// Author		: Pascal Stang - Copyright (C) 2001-2002
// Created		: 9/23/2001
// Revised		: 9/23/2001
// Version		: 1.0
// Target MCU	: any
// Editor Tabs	: 4
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//
//*****************************************************************************
#include "buffer.h"

/** 
*	Global Variables
**/

/**
 * \brief			Utility function to copy byte on a citcular buffer
 *
 * \param msd_data_ptr	Buffer to copy to cbuffer
 * \param msg_count		Number of byte to copy
 * \param circRxBuffer	Circular buffer to copy to
 *
 * \return void
 */
void copy_data_on_protocol_cBuffer(uint8_t *msd_data_ptr,uint32_t msg_count,cBuffer *circRxBuffer){
	if(msd_data_ptr!=0 && msg_count>0){
		for (int char_index=0; char_index<msg_count; char_index++){
			bufferAddToEnd(circRxBuffer, msd_data_ptr[char_index]);
		}
	}
}

/**
 * \brief			Initialization
 * 
 * \param buffer	Buffer to initialize
 * \param start		Start pointer
 * \param size		Size of the buffer
 * 
 * \return void
 */
void bufferInit(cBuffer* buffer, uint8_t *start, uint32_t size)
{
	// set start pointer of the buffer
	buffer->dataptr = start;
	buffer->size = size;
	// initialize index and length
	buffer->dataindex = 0;
	buffer->datalength = 0;
}


/************************************************************************/
/*	Access Rouines                                                      */
/************************************************************************/

/**
 * \brief			Get from the first buffer item
 * 
 * \param buffer	Data Buffer
 * \param data		Pointer to the retieved item
 * 
 * \return bool		True if successful
 */
bool  bufferGetFromFront(cBuffer* buffer, uint8_t *data)
{
	bool res = false;
	
	// check to see if there's data in the buffer
	if(buffer->datalength)
	{
		// get the first character from buffer
		*data = buffer->dataptr[buffer->dataindex];
		// move index down and decrement length
		buffer->dataindex++;
		if(buffer->dataindex >= buffer->size)
		{
			buffer->dataindex %= buffer->size;
		}
		buffer->datalength--;
		res = true;
	}
	// return
	return res;
}

/**
 * \brief				Dumping data from the front of the buffer
 * 
 * \param buffer		Buffer to 
 * \param numbytes		Number of byte to dump from the front of buffer
 * 
 * \return void
 */
void bufferDumpFromFront(cBuffer* buffer, uint32_t numbytes)
{
	// dump numbytes from the front of the buffer
	// are we dumping less than the entire buffer?
	if(numbytes < buffer->datalength)
	{
		// move index down by numbytes and decrement length by numbytes
		buffer->dataindex += numbytes;
		if(buffer->dataindex >= buffer->size)
		{
			buffer->dataindex %= buffer->size;
		}
		buffer->datalength -= numbytes;
	}
	else
	{
		// flush the whole buffer
		buffer->datalength = 0;
	}
}

/**
 * \brief			Dump data from the end of the buffer
 * 
 * \param buffer	Buffer
 * \param numbytes	Number of data to dump from the end of buffer
 * 
 * \return void
 */
void bufferDumpFromEnd(cBuffer* buffer, uint32_t numbytes)
{
	// dump numbytes from the front of the buffer
	// are we dumping less than the entire buffer?
	if(numbytes < buffer->datalength)
	{
		buffer->datalength -= numbytes;
	}
	else
	{
		// flush the whole buffer
		buffer->datalength = 0;
	}
}

/**
 * \brief			Function to get an item from the buffer
 * 
 * \param buffer	Buffer Data
 * \param index		Index of the item
 * 
 * \return uint8_t	Retrieved item value
 */
uint8_t bufferGetAtIndex(cBuffer* buffer, uint32_t index)
{
	// return character at index in buffer
	return buffer->dataptr[(buffer->dataindex+index)%(buffer->size)];
}

bool bufferAddToEnd(cBuffer* buffer, uint8_t data)
{
	// make sure the buffer has room
	if(buffer->datalength < buffer->size)
	{
		// save data byte at end of buffer
		buffer->dataptr[(buffer->dataindex + buffer->datalength) % buffer->size] = data;
		// increment the length
		buffer->datalength++;
		// return success
		return true;
	}
	else 
		return false;
}

/**
 * \brief			Full Buffer Function
 *
 * \param buffer	Buffer to check
 * 
 * \return uint32_t	Return 0 if buffer is full, !=0 if is not full
 */
uint32_t bufferIsNotFull(cBuffer* buffer)
{
	// check to see if the buffer has room
	// return true if there is not full
	return (buffer->datalength < buffer->size);
}

/**
 * \brief			Function to erase the buffer
 * 
 * \param buffer	Buffer to check
 * 
 * \return void	
 */
void bufferFlush(cBuffer* buffer)
{
	// flush contents of the buffer
	buffer->datalength = 0;
}

/**
 * \brief			Function that returns buffer size
 * 
 * \param buffer	Buffer to check
 * 
 * \return uint32_t	Size of the buffer
 */
uint32_t bufferGetDatalength(cBuffer* buffer)
{
	return buffer->datalength;
}


/**
 * \brief			Function that returns free buffer size
 *
 * \param buffer	Buffer to check
 *
 * \return uint32_t	Free Size of the buffer
 */
uint32_t bufferGetDataFree(cBuffer* buffer)
{
	if(bufferIsNotFull(buffer))
		return buffer->size-buffer->datalength;
	else
		return 0;
}
