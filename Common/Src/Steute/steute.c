/*
 * steute.c
 *
 * Created: 24/01/2021 10:30:11
 *  Author: Stefano
 */
#include "settings.h"
#ifdef	STEUTE_ON_CONTROL
#include "main.h"
#ifdef	STEUTE_CONTROL_ON_CM7
	#include "cmsis_os.h"
#endif
#include "steute.h"

//extern osThreadId SteuteTaskHandle;

//Communication striuctures

#ifdef STEUTE_CONTROL_ON_CM7
	#define STEUTE_SERIAL huart3
	extern UART_HandleTypeDef STEUTE_SERIAL;
	steute_static_buffer_t steute_rx_queue;
#endif

	static uint8_t Steute_Uart_Data[8];

#ifdef STEUTE_CONTROL_ON_CM4
	#define STEUTE_SERIAL huart3
	extern UART_HandleTypeDef STEUTE_SERIAL;
#endif

//Protocol Structures
static cBuffer steute_circRxBuffer;
static uint8_t steute_RxBuffer[MAX_BYTE_FOR_STEUTE_QUEUE];
static uint8_t Steute_Uart_Data[8];

steute_state_t steute_state;

//Make Rx queue for
void MakeSteuteQueue(){
#ifdef STEUTE_CONTROL_ON_CM7
	//Inite steute_state
	steute_state.started=STEUTE_NOT_STARTED;
	steute_state.cmd_send = STEUTE_ANY_CMD_SEND;
	steute_state.status_code = STEUTE_ANY_REPLY_RX;
	steute_state.packet_counter = 0;
	steute_state.left_pedal = STEUTE_PEDAL_UNDEF;
	steute_state.central_pedal = STEUTE_PEDAL_UNDEF;
	steute_state.right_pedal = STEUTE_PEDAL_UNDEF;
	steute_state.left_upper_pedal = STEUTE_PEDAL_UNDEF;
	steute_state.right_upper_pedal = STEUTE_PEDAL_UNDEF;
	steute_state.rssi = STEUTE_RSSI_UNDEFINED_VALUE;

    /* Create a queue capable of containing 10 uint64_t values. */
	steute_rx_queue.QueueHandle = xQueueCreateStatic( MAX_BYTE_FOR_STEUTE_QUEUE,
													sizeof(uint8_t),
													steute_rx_queue.QueueBuffer,
													&steute_rx_queue.QueueControlBlock );
	if(steute_rx_queue.QueueHandle==NULL){
		Error_Handler();
	}

#endif
	//Init RX Buffer for redundant protocol usage, and Protocol structure
	bufferInit( &steute_circRxBuffer, steute_RxBuffer, sizeof(steute_RxBuffer) );
	//Serial port register
	/* Enable the UART RX FIFO threshold interrupt */
	HAL_UART_Receive_IT(&STEUTE_SERIAL, Steute_Uart_Data, MINIMUM_CALLBACK_BYTE);
}

#ifdef STEUTE_ON_CONTROL
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
	if(huart==&STEUTE_SERIAL){
#ifdef	STEUTE_CONTROL_ON_CM7
		// We have not woken a task at the start of the ISR.
		BaseType_t xHigherPriorityTaskWoken = pdFALSE;
		BaseType_t reply_send = xQueueSendFromISR(steute_rx_queue.QueueHandle, Steute_Uart_Data, &xHigherPriorityTaskWoken);
		if( reply_send != pdPASS )
		{
			Error_Handler();
		}
		// Now the buffer is empty we can switch context if necessary.
		if( xHigherPriorityTaskWoken )
		{
			taskYIELD ();
		}
#endif
#ifdef	STEUTE_CONTROL_ON_CM4
		//Populate Steute circular Buffer
		if(!bufferAddToEnd(&steute_circRxBuffer, Steute_Uart_Data[0]))
			Steute_Uart_Data[2]++;
#endif
		HAL_UART_Receive_IT(&STEUTE_SERIAL, Steute_Uart_Data, MINIMUM_CALLBACK_BYTE);
	}
}
#endif

//xQueueReset(state_socket_pool.NetRxQueueHandle_Pool[idx_pool]);
void SteuteProtocolTask(void const * argument)
{
#ifdef STEUTE_CONTROL_ON_CM7
	uint8_t msg_data_ptr[MINIMUM_STEUTE_BYTE];
	uint32_t msg_count=0;
	BaseType_t res;
	uint8_t stxetxRxPacket[STEUTE_MAXRXPACKETSIZE+STEUTE_TRAILERLENGTH-STEUTE_CRC_LEN];

	while(1){
		res =  xQueueReceive(steute_rx_queue.QueueHandle,msg_data_ptr,portMAX_DELAY);
		if(res != pdTRUE ){
			Error_Handler();
		}else{
			msg_count = 1;
		}
		do{
			if(msg_count>0){
				copy_data_on_protocol_cBuffer(msg_data_ptr,MINIMUM_STEUTE_BYTE,&steute_circRxBuffer);
				//If active socket, process data:
				int data_check;
				data_check = process_steute_packet(&steute_circRxBuffer,stxetxRxPacket);
				if(data_check == TRUE_STEUTE_PACKET){
					Analyze_steute_packet(stxetxRxPacket);
				}
			}
			msg_count = uxQueueMessagesWaiting(steute_rx_queue.QueueHandle);
			if(msg_count>0){
				res =  xQueueReceive(steute_rx_queue.QueueHandle,msg_data_ptr,portMAX_DELAY);
			}
		}while(msg_count>0);
		osDelay(10);
	};
#endif
#ifdef STEUTE_CONTROL_ON_CM4
	uint8_t stxetxRxPacket[STEUTE_MAXRXPACKETSIZE+STEUTE_TRAILERLENGTH-STEUTE_CRC_LEN];
	int data_check = FALSE_STEUTE_PACKET;
	//Analyze all arrived messages
	do{
		data_check = process_steute_packet(&steute_circRxBuffer,stxetxRxPacket);
		if(data_check == TRUE_STEUTE_PACKET){
			Analyze_steute_packet(stxetxRxPacket);
		}
	}while(data_check==TRUE_STEUTE_PACKET);
#endif
}

#endif
