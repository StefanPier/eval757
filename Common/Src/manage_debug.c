/*
 * manage_debug.c
 *
 *  Created on: 26 gen 2022
 *      Author: stefano
 */


#include <manage_debug.h>
#include "main.h"
#include "stdio.h"
#include "stdarg.h"
#include "string.h"
#include "buffer.h"


char cmd_printf_enable[LAST_DBG] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};		//Default for customer


#ifdef	CORE_CM7
	#include "cmsis_os.h"
	#include "lwip.h"
	osThreadId DebugTaskHandle;

	//Communication striuctures
	#define	SERIAL_DEBUG	huart1
	extern UART_HandleTypeDef SERIAL_DEBUG;
	debug_static_buffer_t debug_queue;

#endif

#ifdef	CORE_CM4
	#include "IPC_m4.h"
	//Protocol Structures
	static cBuffer debug_circRxBuffer;
	static uint8_t debug_RxBuffer[MAX_BYTE_FOR_IPC_DEBUG];

	static int misuro_buffer(char * da_misurare){
		uint16_t count=0;
		while((*da_misurare++)!=0 && count<MAX_BYTE_FOR_IPC_DEBUG-1)
			count++;
		return count;
	}

	//To Know when buffer is empy or full.
	//On a circular queue of SIZE_QUEUE item, max storage SIZE_QUEUE-1 item.
	static int message_on_circular_queue_idx(int write_idx,int read_idx, int SIZE_QUEUE){
		int diff=write_idx-read_idx;

		if(diff<0){
			diff = diff + (int)SIZE_QUEUE;
		}

		return diff;
	}
#endif

/*
 * Make Rx OS queue for to debug purpose
 */
void MakeDebugQueue(){
#ifdef	CORE_CM7
    /* Create a queue capable of containing 10 uint64_t values. */
	debug_queue.QueueHandle = xQueueCreateStatic( MAX_BYTE_FOR_DEBUG_QUEUE,
													sizeof(uint8_t),
													debug_queue.QueueBuffer,
													&debug_queue.QueueControlBlock );
	if(debug_queue.QueueHandle==NULL){
		Error_Handler();
	}
#endif
#ifdef	CORE_CM4
	//Init RX Buffer for redundant protocol usage, and Protocol structure
	bufferInit( &debug_circRxBuffer, debug_RxBuffer, sizeof(debug_RxBuffer) );
#endif
}

#ifdef CORE_CM4
#undef	MAX_BYTE_FOR_DEBUG_QUEUE
#define	MAX_BYTE_FOR_DEBUG_QUEUE	MAX_BYTE_FOR_IPC_DEBUG
#endif

void debug_printf(bool debug, const char *s, ...){
	char tmp[MAX_BYTE_FOR_DEBUG_QUEUE];
	#ifdef	CORE_CM7
	/*
	if(debug<NOT_USED_DEBUG){
		tmp[0]=0;
		tmp[MAX_BYTE_FOR_DEBUG_QUEUE-1]=0;
		va_list ap;
		va_start(ap, s);
		//vsprintf(tmp,s,ap);
		vsnprintf(tmp,MAX_BYTE_FOR_DEBUG_QUEUE,s,ap);
		tmp[MAX_BYTE_FOR_DEBUG_QUEUE-1]=0;
		// metto nel buffer
		for(uint32_t i=0; i<strlen(tmp);i++)
		{
			BaseType_t reply_send = xQueueSend(debug_queue.QueueHandle, ( void * ) &tmp[i], 0);
//			if( reply_send != pdPASS )
//			{
//				Error_Handler();
//			}
		}
		va_end(ap);
	}
	*/
	if(debug){
		tmp[0]=0;
		tmp[MAX_BYTE_FOR_DEBUG_QUEUE-1]=0;
		va_list ap;
		va_start(ap, s);
		//vsprintf(tmp,s,ap);
		vsnprintf(tmp,MAX_BYTE_FOR_DEBUG_QUEUE,s,ap);
		tmp[MAX_BYTE_FOR_DEBUG_QUEUE-1]=0;
		// metto nel buffer
		for(uint32_t i=0; i<strlen(tmp);i++)
		{
			BaseType_t reply_send = xQueueSend(debug_queue.QueueHandle, ( void * ) &tmp[i], 0);
//			if( reply_send != pdPASS )
//			{
//				Error_Handler();
//			}
		}
		va_end(ap);
	}
#endif
#ifdef	CORE_CM4
//	if(debug<NOT_USED_DEBUG){
	if(debug){
		tmp[0]=0;
		va_list ap;
		va_start(ap, s);
		vsnprintf(tmp,MAX_BYTE_FOR_DEBUG_QUEUE,s,ap);
		tmp[MAX_BYTE_FOR_DEBUG_QUEUE-1]=0;
		int len=0;
		//Copyng assurin zero terminated buffer each MAX_BYTE_FOR_DEBUG_QUEUE char (MAX_BYTE_FOR_DEBUG_QUEUE char equal 0)
		len = misuro_buffer(tmp);
		for(int i=0; i<len;i++)
		{
			if(bufferAddToEnd(&debug_circRxBuffer, tmp[i])==false){
				//Re,ove Last
				bufferDumpFromEnd(&debug_circRxBuffer, &tmp[i]);
				//Add 0 terminated char
				tmp[i]=0;
				bufferAddToEnd(&debug_circRxBuffer, tmp[i]);
				break;
			}
		}
	}
#endif
}


//Flush out from serial debug
void DebugTask(void const * argument){
#ifdef	CORE_CM7
	uint8_t msd_data_ptr[MINIMUM_DEBUG_BYTE];
	BaseType_t res;

	while(1){
		//msg_count = uxQueueMessagesWaiting(steute_rx_queue.QueueHandle);
		res =  xQueueReceive(debug_queue.QueueHandle,msd_data_ptr,portMAX_DELAY);
		if(res == pdTRUE ){
			HAL_UART_Transmit(&SERIAL_DEBUG, msd_data_ptr, sizeof(msd_data_ptr), HAL_MAX_DELAY);
		}
		osDelay(1);
	};
#endif
#ifdef	CORE_CM4
	//If there are char to send as debug message, send and clear buffer
	if(bufferGetDatalength(&debug_circRxBuffer)!=0){
		IPC_SendDebug(&debug_circRxBuffer);
		bufferFlush(&debug_circRxBuffer);
//		IPC_SendUpdate();
	}
#endif
}

