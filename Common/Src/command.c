/**
\file command.c
\brief Libreria per la gestione di comandi, con associazione a funzione di esecuzione
\author Lorenzo Giardina <lorenzo@lorenzogiardina.com>
\version 1.0
\date Last revision: 1/1/2016
\note  This library is created for Qprel srl
This code is distributed under the GNU Public License which can be found at http://www.gnu.org/licenses/gpl.txt
*/

#include "command.h"
#include "stxetx.h"
#include "buffer.h"
#include "manage_debug.h"
#include <string.h>
#include "main.h"


//#define DBG_COMMAND 0  //!< Enable debug printf for this module

command_t command[N_COMMAND];
command_struct_t temp;
fifo_command_t rx_cmd[N_TRUNK];
ack_t ack;

static unsigned long packetNum=0;
unsigned char packetNum_RX=0;

//! Puntatore a funzione di esecuzione
//! \return void
//! \param ptr_data Puntatore a campo dati, vettore dove sono contenuti i parametri che servono alla funzione finale
void (*puntatore_funzione_exec) (unsigned char trunk, unsigned char* ptr_data);

//! Inizializzazione sistema di pacchettizzazione STXETX e associazione funzioni base
unsigned char command_system_init (unsigned char n_trunk,void (*out_func)(unsigned char data, void* arg), cBuffer* (*getbyte) (void * arg),void (*tx_enable_func)(int),void (*rx_enable_func)(int),void (*ms_delay) (void), void* arg)
{
	if (n_trunk < N_TRUNK)
	{
		rx_cmd[n_trunk].arg = arg;
		rx_cmd[n_trunk].dataout_func=out_func;
		rx_cmd[n_trunk].datain_func=getbyte;
		rx_cmd[n_trunk].ms_delay_func=ms_delay;

		rx_cmd[n_trunk].tx_enable=tx_enable_func;
		rx_cmd[n_trunk].rx_enable=rx_enable_func;

		rx_cmd[n_trunk].n_cmd_present=0;		//Reset del numero di comandi presenti nella fifo
		return 1;
	}
	else return 0;
}

void command_reset(){
	memset(command,0, sizeof(command_t)*N_COMMAND);
}

//! Funzione di ricerca di uno slot di memorizzazione per un comando
//! \return -1 se nessuno slot è presente oppure lo slot da 0 a N_COMMAND-1 primo disponibile
static int search_free(unsigned char comando){
	int free=-1;
	//Serch for just memorized command to overwrite or free one to add
	for (int i=0; i<N_COMMAND; i++)
	{
		//Retrieve the first free place
		if(command[i].occuped==0 && free==-1){
			free=i;
			break;
		}
	}
	return free;
}

//! Funzione di ricerca di dello slot memorizzato di un comando
//! \return -1 se nessuno slot è presente oppure lo slot del comando desiderato
static int search_command(unsigned char comando){
	int free=-1;
	//Serch for just memorized command to overwrite or free one to add
	for (int i=0; i<N_COMMAND; i++){
		//Retrieve the first free place
		if(command[i].occuped==1){
			if(command[i].command==comando){
				free=i;
				break;
			}
		}
	}
	return free;
}


//! Funzione di inizializzazione ed associazione COMANDO -> FUNZIONE DA ESEGUIRE
//! \return 0 se ERRORE (campo comando fuori range), 1 se OK
//! \param comando Numero corrispondente al comando da inizializzare (da 0 a N_COMMAND)
//! \param ptr_data Puntatore a campo dati, vettore dove sono contenuti i parametri che servono alla funzione finale
unsigned char command_init (unsigned char comando,void (*function_cmd) (unsigned char trunk, unsigned char* ptr_data))
{
	int free=search_free(comando);
//	if (comando < N_COMMAND)
	if(free!=-1)
	{
		command[free].command=comando;
		command[free].function=function_cmd;
		command[free].occuped=1;
		return 1;
	}
	else
		return 0;
}
//! Invio dati generico
//! status=1 per risposta comando
//! status=0 comando
void command_send(unsigned char n_trunk, unsigned char status, unsigned char type, unsigned char datalength, unsigned char* dataptr)
{
	DbgPrintf(DBG_COMMAND, "trunk %d, status %d, type %d, datalength %d\r\n", n_trunk, status, type, datalength);
	for(int i=0;i<datalength;i++)
		DbgPrintf(DBG_COMMAND, "%d ",dataptr[i]);
	DbgPrintf(DBG_COMMAND, "\r\n");
	
	rx_cmd[n_trunk].tx_enable((int)n_trunk);
	stxetxSend(status,type,datalength,dataptr,rx_cmd[n_trunk].dataout_func, rx_cmd[n_trunk].arg); 
	rx_cmd[n_trunk].rx_enable((int)n_trunk);
}

//! Invio dati generico con attesa ack
//! status=1 per risposta comando
//! status=0 comando
//! risponde con 0 se non ha ricevuto ack
//! risponde con 1 se ha ricevuto ack
unsigned char command_send_attend_ack(unsigned char n_trunk, unsigned char status, unsigned char type, unsigned char datalength, unsigned char* dataptr)
{
	unsigned int delay_count=0;
	char t;
	char flag;
	unsigned char tentativi=0;
	unsigned char recived_ack=0;

	t=0;

	while (tentativi<MAX_CMD_RETRY && recived_ack==0)
	{
		ack.ack_received=0;
		ack.packet_n=0;
		delay_count=0;

        DbgPrintf (DBG_COMMAND, "Sending Command with ACK verify......\r\n");
		rx_cmd[n_trunk].tx_enable(n_trunk);
		stxetxSend(status,type,datalength,dataptr,rx_cmd[n_trunk].dataout_func,rx_cmd[n_trunk].arg);
		rx_cmd[n_trunk].rx_enable((int)n_trunk);

		DbgPrintf (DBG_COMMAND, "Waiting ACK, retry n: %d   *\r\n",tentativi);
		
		flag=0;
		while (flag == 0  && delay_count <= ACK_TIMEOUT && recived_ack==0)
		{
			delay_count++;
			t=process_packet(n_trunk);

			if (t == ACK_RECEIVED /*&& ack.packet_n == packetNum*/)
			{
				flag=1;
				ack.ack_received=0;
				ack.packet_n=0;
				delay_count=0;
				DbgPrintf (DBG_COMMAND, "ACK at retry n: %d\r\n",tentativi);
				recived_ack=1;
			}
			rx_cmd[n_trunk].ms_delay_func();
		}
	tentativi++;
	}
	if (recived_ack==0)
	{
		DbgPrintf (DBG_COMMAND, "Timeout - ACK NOT received retry n: %d, packet n: %10ld\r\n",tentativi,packetNum);
	}
	return recived_ack;
}

//! Funzione di esecuzione FUNZIONE associata al comando
//! \return 0 se ERRORE (campo comando fuori range), 1 se OK
//! \param comando Numero corrispondente al comando da inizializzare (da 0 a N_COMMAND)
//! \param ptr_data Puntatore a campo dati, vettore dove sono contenuti i parametri che servono alla funzione finale
unsigned char exe_cmd_func (unsigned char comando, unsigned char trunk, unsigned char* ptr_data )
{
	int cmd_idx=search_command(comando);
//	if (comando < N_COMMAND)
	if(cmd_idx!=-1)
	{
		puntatore_funzione_exec=command[cmd_idx].function;
		if(puntatore_funzione_exec!=0)
			puntatore_funzione_exec(trunk, ptr_data);
		else{
			command_send(trunk, 1, comando, strlen("NAK")+1, "NAK");
		}
		
//		ncommand = comando; //aggiunta per gestire numero comando in modo indipendente (aggiunta da barni)
		
		DbgPrintf (DBG_COMMAND, "Executed cmd number: %d\r\n",comando);

		return 1;
	}
	
	else
	{
//		ncommand = 254; //aggiunta per gestire numero comando in modo indipendente (aggiunta da barni)
						//se il comando non è riconosciuto assegna a ncommand il valore 254
		
		DbgPrintf (DBG_COMMAND, "Not Valid Comand\r\n");
					// dump receive buffer contents to relieve deadlock

		command_send(trunk, 1, comando, strlen("NAK")+1, "NAK");

		bufferFlush(rx_cmd[trunk].datain_func(rx_cmd[trunk].arg));
		
		return 0;
	}
}

//! Funzione esecuzione funzione associata al comando con controllo presenza nel buffer comandi di un comando valido
void exe_cmd (unsigned char n_trunk)
{
	unsigned char i=0;

	while (rx_cmd[n_trunk].n_cmd_present)
	{
		DbgPrintf (DBG_COMMAND, "\r\nWaiting to process command...\r\n");
		//Safety reason
		if(strlen(rx_cmd[n_trunk].comando[i].data)>=N_MAX_DATA_LENGHT)
		{
			rx_cmd[n_trunk].comando[i].data[N_MAX_DATA_LENGHT-1]=0;
		}
       	exe_cmd_func(rx_cmd[n_trunk].comando[i].type, n_trunk, rx_cmd[n_trunk].comando[i].data);
        rx_cmd[n_trunk].n_cmd_present--;
        i++;
#ifdef CORE_CM7
        HAL_GPIO_TogglePin(LD1_GPIO_Port, LD1_Pin);
#endif
        //		ioport_toggle_pin_level(LED3_GPIO);
	}
}

//! Funzione che elabora un pacchetto dati e lo inserisce nel buffer di ricezione
//! \return Numero di pacchetto nella lista comandi inserito se errore restituisce -1 o ACK_RECEIVED se riceve un pacchetto ACK
int process_packet (unsigned char n_trunk)
{
	unsigned char* dataPtr;
	unsigned char stxetxRxPacket[STXETX_MAXRXPACKETSIZE];

	rx_cmd[n_trunk].rx_enable((int)n_trunk);
	// here we get the UART's receive buffer and give it to the STX/ETX
	// packet processing function.  If the packet processor finds a valid
	// packet in the buffer, it will return true.

	if (rx_cmd[n_trunk].n_cmd_present >= N_MAX_BUFFER_COMANDI_RX)
	{
		DbgPrintf (DBG_COMMAND, "Full Cmd Buffer!\r\n");

		return -1;
	}
	if( stxetxProcess(rx_cmd[n_trunk].datain_func(rx_cmd[n_trunk].arg),stxetxRxPacket) )
	{
		// sxtetxProcess has reported that it found a packet
		// let's get the data...

		// (NOTE: although I discard the status, type, and datalength
		// below, it would be important if I were sending more than one
		// kind of packet)


		// get the packet's type
		//temp.type=stxetxGetRxPacketType(stxetxRxPacket);
		temp.status=stxetxGetRxPacketStatus(stxetxRxPacket);

		//Analizzo se è un pacchetto di ACK
		if (temp.status == ACK_PACKET)
		{
			ack.ack_received=1;
//			ack.packet_n=stxetxGetRxPacketStatus();

			DbgPrintf (DBG_COMMAND, "ACK of packet n: %10ld\r\n",packetNum++);
			
			return ACK_RECEIVED;
		}
		else
		{
			// get the packet's status
			rx_cmd[n_trunk].comando[rx_cmd[n_trunk].n_cmd_present].status=temp.status;

			// get the packet's type
			rx_cmd[n_trunk].comando[rx_cmd[n_trunk].n_cmd_present].type=stxetxGetRxPacketType(stxetxRxPacket);

			// get the packet's datalength
			rx_cmd[n_trunk].comando[rx_cmd[n_trunk].n_cmd_present].lenght=stxetxGetRxPacketDatalength(stxetxRxPacket);

			// get a pointer to the place where the received data is stored
			dataPtr = stxetxGetRxPacketData(stxetxRxPacket);
			for (unsigned char i=0;i<rx_cmd[n_trunk].comando[rx_cmd[n_trunk].n_cmd_present].lenght;i++)
			{
				rx_cmd[n_trunk].comando[rx_cmd[n_trunk].n_cmd_present].data[i]=dataPtr[i];
			}

				DbgPrintf (DBG_COMMAND, "Process_packet number: %10ld\r\n",packetNum);
				DbgPrintf (DBG_COMMAND, "Command N: %d\r\n", rx_cmd[n_trunk].n_cmd_present+1);
				DbgPrintf (DBG_COMMAND, "Status: %d\r\n",rx_cmd[n_trunk].comando[rx_cmd[n_trunk].n_cmd_present].status);
				DbgPrintf (DBG_COMMAND, "Type: %d\r\n",rx_cmd[n_trunk].comando[rx_cmd[n_trunk].n_cmd_present].type);
				DbgPrintf (DBG_COMMAND, "Lenght: %d\r\n",rx_cmd[n_trunk].comando[rx_cmd[n_trunk].n_cmd_present].lenght);
				DbgPrintf (DBG_COMMAND, "Data: ");
				for (unsigned c=0;c<rx_cmd[n_trunk].comando[rx_cmd[n_trunk].n_cmd_present].lenght;c++)
					DbgPrintf (DBG_COMMAND, "%d - ", rx_cmd[n_trunk].comando[rx_cmd[n_trunk].n_cmd_present].data[c]);
				DbgPrintf (DBG_COMMAND, "\n\r");

			// Send ACK packet
			if(ACK_ENABLED)
			{
				rx_cmd[n_trunk].tx_enable((int)n_trunk);
					DbgPrintf (DBG_COMMAND, "Sending ACK packet\r\n");

				stxetxSendAckPacket(ACK_PACKET,rx_cmd[n_trunk].comando[rx_cmd[n_trunk].n_cmd_present].type,rx_cmd[n_trunk].dataout_func, rx_cmd[n_trunk].arg);
					DbgPrintf (DBG_COMMAND, "ACK packet sent\r\n");
			}
				

			rx_cmd[n_trunk].rx_enable((int)n_trunk);
			rx_cmd[n_trunk].n_cmd_present++;
			packetNum++;
		}
		return rx_cmd[n_trunk].n_cmd_present;
	}
	return -1;
}
